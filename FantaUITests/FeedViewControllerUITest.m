//
//  FeedViewControllerUITest.m
//  Fanta
//
//  Created by Mario Concilio on 8/10/16.
//  Copyright © 2016 Mario Concilio. All rights reserved.
//

#import "FantaUITests-Bridging-Header.h"
#import "Fanta-Swift.h"
#import <XCTest/XCTest.h>

@interface FeedViewControllerUITest : XCTestCase

@end

@implementation FeedViewControllerUITest {
    XCUIApplication *app;
}

/*
- (void)setUp {
    [super setUp];
    
    // Put setup code here. This method is called before the invocation of each test method in the class.
    
    // In UI tests it is usually best to stop immediately when a failure occurs.
    self.continueAfterFailure = NO;
    
    app = [[XCUIApplication alloc] init];
    [Snapshot setupSnapshot:app];
    [app launch];
}

- (void)tearDown {
    // Put teardown code here. This method is called after the invocation of each test method in the class.
    [super tearDown];
}

- (void)testExample {
//    XCUIElement *feedviewNavigationBar = app.navigationBars[@"FeedView"];
//    XCUIElement *fntFilterButton = feedviewNavigationBar.buttons[@"fnt filter"];
//    [fntFilterButton tap];
//    
//    XCUIElementQuery *tablesQuery = app.tables;
//    [tablesQuery.cells.staticTexts[@"Limpar Seleção"] tap];
//    [tablesQuery.cells.staticTexts[@"Selecionar Todas"] tap];
//    [fntFilterButton tap];
    
//    XCUIApplication *app = [[XCUIApplication alloc] init];
    XCUIElement *feedviewNavigationBar = app.navigationBars[@"FeedView"];
    XCUIElement *fntFilterButton = feedviewNavigationBar.buttons[@"fnt filter"];
    [fntFilterButton tap];
    [Snapshot snapshot:@"FeedGlobalFilter" waitForLoadingIndicator:YES];
    [fntFilterButton tap];
    [Snapshot snapshot:@"FeedGlobal" waitForLoadingIndicator:YES];
    XCUIElement *button = [[feedviewNavigationBar childrenMatchingType:XCUIElementTypeButton] elementBoundByIndex:1];
    [button tap];
    [Snapshot snapshot:@"Drawer" waitForLoadingIndicator:YES];
    [button tap];
    
}
 */

@end
