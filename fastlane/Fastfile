# Customise this file, documentation can be found here:
# https://github.com/fastlane/fastlane/tree/master/fastlane/docs
# All available actions: https://github.com/fastlane/fastlane/blob/master/fastlane/docs/Actions.md
# can also be listed using the `fastlane actions` command

# Change the syntax highlighting to Ruby
# All lines starting with a # are ignored when running `fastlane`

# If you want to automatically update fastlane if a new version is available:
# update_fastlane

# This is the minimum version number required.
# Update this, if you use features of a newer version
fastlane_version "1.99.0"

default_platform :ios

platform :ios do
	before_all do
    	ENV["SLACK_URL"] = "https://hooks.slack.com/services/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"
    	# cocoapods
  	end

  	desc "Runs all the tests"
  	lane :test do
    	# Execute tests
    	scan(
    		scheme: "Fanta",
    		clean: true,
    		device: "iPhone 6s"
    	)

    	# Post to Slack test results
    	slack(
      		message: "Test results",
      		channel: "#ios",
      		success: true,
      		default_payloads: [:test_result, :git_branch]
    	)
  	end

  	desc "Take screenshots, put frames on it and deliver metadata to Itunes Connect"
  	lane :screenshot do
    	# Take snapshots from app
    	snapshot

    	# Add iPhone frames
    	frameit(
      		silver: true, 
      		path: './fastlane/screenshots'
    	)
    
    	# Upload metadata to iTunes Connect
    	deliver(skip_binary_upload: true)
  	end

  	desc "Submit a new Beta Build to Apple TestFlight"
  	desc "This will also make sure the profile is up to date"
  	lane :beta do
    	# Keep certificates and provisioning profiles up to date
    	match(type: "appstore") # more information: https://codesigning.guide

    	# Make sure we don't have any uncommited changes on master and are up to date with the remote
    	ensure_git_status_clean
    	ensure_git_branch(branch: 'master')
    	git_pull
    	push_to_git_remote

    	# Ask the user of fastlane to enter a changelog
    	changelog = prompt(
    		text: "Enter the change log: ",
    		multi_line_end_keyword: "END"
    	)

    	# Increment the version number of your Xcode project
    	#version = increment_version_number(bump_type: "patch")

    	version = get_version_number
    	build = increment_build_number # automatically increment by one

    	# Build and sign your app
    	gym(scheme: "Fanta")

    	# Distribute via TestFlight
    	pilot(
    		distribute_external: false,
    		skip_waiting_for_build_processing: false
    	)

    	# Post Slack notification
    	slack(
    		message: "Successfully distributed version #{version} build #{build} :rocket:",
      		success: true,
      		channel: "#ios",
      		default_payloads: [], # reduce the notification to the minimum
      		payload: {
        		"Changes" => changelog
      		}
    	)
    
    	# sh "your_script.sh"
    	# You can also use other beta testing services here (run `fastlane actions`)
  	end

  	desc "Deploy a new version to the App Store"
  	lane :publish do
    	# match(type: "appstore")
    	# snapshot
    	gym # Build your app - more options available
    	deliver(force: true)
    	# frameit
  	end

  	# You can define as many lanes as you want

  	after_all do |lane|
    	# This block is called, only if the executed lane was successful

    	# Make sure our directory is clean, except for changes Fastlane has made
    	clean_build_artifacts

    	# slack(
    	#   message: "Successfully deployed new App Update."
    	# )
  	end

  	error do |lane, exception|
    	slack(
      		message: exception.message,
      		channel: "#ios",
      		success: false,
    	)
  	end
end


# More information about multiple platforms in fastlane: https://github.com/fastlane/fastlane/blob/master/fastlane/docs/Platforms.md
# All available actions: https://github.com/fastlane/fastlane/blob/master/fastlane/docs/Actions.md

# fastlane reports which actions are used
# No personal data is recorded. Learn more at https://github.com/fastlane/enhancer
