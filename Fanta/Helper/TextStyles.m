//
//  TextStyles.m
//  Fanta
//
//  Created by Mario Concilio on 3/14/16.
//  Copyright © 2016 Mario Concilio. All rights reserved.
//

#import "TextStyles.h"

@implementation TextStyles

#pragma mark - Welcome
+ (NSDictionary *)chooseAAMessageStyle {
    return @{NSFontAttributeName: [UIFont lightFontWithSize:16.0],
             NSForegroundColorAttributeName: [UIColor whiteColor]};
}

+ (NSDictionary *)chooseAANameDarkStyle {
    return @{NSFontAttributeName: [UIFont lightFontWithSize:16.0],
             NSForegroundColorAttributeName: [UIColor whiteColor]};
}

+ (NSDictionary *)chooseAANameLightStyle {
    return @{NSFontAttributeName: [UIFont lightFontWithSize:16.0],
             NSForegroundColorAttributeName: UIColorFromHEX(0x404040)};
}

+ (NSDictionary *)chooseAASelectAllDarkStyle {
    NSMutableParagraphStyle *paragraph = [[NSMutableParagraphStyle alloc] init];
    [paragraph setAlignment:NSTextAlignmentCenter];
    
    return @{NSFontAttributeName: [UIFont lightFontWithSize:16.0],
             NSForegroundColorAttributeName: [UIColor whiteColor],
             NSParagraphStyleAttributeName: paragraph};
}

+ (NSDictionary *)chooseAASelectAllLightStyle {
    NSMutableParagraphStyle *paragraph = [[NSMutableParagraphStyle alloc] init];
    [paragraph setAlignment:NSTextAlignmentCenter];
    
    return @{NSFontAttributeName: [UIFont lightFontWithSize:16.0],
             NSForegroundColorAttributeName: UIColorFromHEX(0x404040),
             NSParagraphStyleAttributeName: paragraph};
}

#pragma mark - Post & StoreItem
+ (NSDictionary *)contentStyle {
    return @{NSFontAttributeName: [UIFont lightFontWithSize:16.0],
             NSForegroundColorAttributeName: [UIColor blackColor]};
}

+ (NSDictionary *)contentLightStyle {
    return @{NSFontAttributeName: [UIFont lightFontWithSize:16.0],
             NSForegroundColorAttributeName: [UIColor whiteColor]};
}

+ (NSDictionary *)contentPlaceholderStyle {
    return @{NSFontAttributeName: [UIFont lightFontWithSize:16.0],
             NSForegroundColorAttributeName: [UIColor lightGrayColor]};
}

#pragma mark - Comments
+ (NSDictionary *)commentProfileNameStyle {
    return @{NSFontAttributeName: [UIFont semiboldFontWithSize:14.0],
             NSForegroundColorAttributeName: [UIColor blackColor]};
}

+ (NSDictionary *)commentContentStyle {
    return @{NSFontAttributeName: [UIFont lightFontWithSize:15.0],
             NSForegroundColorAttributeName: [UIColor blackColor]};
}

+ (NSDictionary *)commentDateStyle {
    return @{NSFontAttributeName: [UIFont boldFontWithSize:12.0],
             NSForegroundColorAttributeName: UIColorFromRGB(178, 178, 178)};
}

+ (NSDictionary *)commentButtonStyle {
    return @{NSFontAttributeName: [UIFont semiboldFontWithSize:13.0],
             NSForegroundColorAttributeName: UIColorFromRGB(178, 178, 178)};
}

+ (NSDictionary *)commentButtonSelectedStyle {
    return @{NSFontAttributeName: [UIFont semiboldFontWithSize:13.0],
             NSForegroundColorAttributeName: [UIColor customPink]};
}

#pragma mark - New Post
+ (NSDictionary *)newPostChooseAA {
    return @{NSFontAttributeName: [UIFont lightFontWithSize:14.0],
             NSForegroundColorAttributeName: [UIColor customDarkGray]};
}

#pragma mark - Cart
+ (NSDictionary *)cartTitleStyle {
    return @{NSFontAttributeName: [UIFont lightFontWithSize:18.0],
             NSForegroundColorAttributeName: [UIColor blackColor]};
}

+ (NSDictionary *)cartPriceStyle {
    return @{NSFontAttributeName: [UIFont boldFontWithSize:16.0],
             NSForegroundColorAttributeName: [UIColor blackColor]};
}

+ (NSDictionary *)cartQtyStyle {
    return @{NSFontAttributeName: [UIFont boldFontWithSize:16.0],
             NSForegroundColorAttributeName: [UIColor blackColor]};
}

#pragma mark - Store
+ (NSDictionary *)storeCellPriceStyle {
    return @{NSFontAttributeName: [UIFont semiboldFontWithSize:18.0],
             NSForegroundColorAttributeName: [UIColor blackColor]};
}

+ (NSDictionary *)storeCellTitleStyle {
    return @{NSFontAttributeName: [UIFont lightFontWithSize:16.0],
             NSForegroundColorAttributeName: [UIColor blackColor]};
}

+ (NSDictionary *)storeItemPriceStyle {
    return @{NSFontAttributeName: [UIFont boldFontWithSize:20.0],
             NSForegroundColorAttributeName: [UIColor blackColor]};
}

+ (NSDictionary *)storeItemTitleStyle {
    return @{NSFontAttributeName: [UIFont regularFontWithSize:20.0],
             NSForegroundColorAttributeName: [UIColor blackColor]};
}

#pragma mark - List Messages
+ (NSDictionary *)messageTitleStyle {
    return @{NSFontAttributeName: [UIFont semiboldFontWithSize:16.0],
             NSForegroundColorAttributeName: [UIColor blackColor]};
}

+ (NSDictionary *)messageLastUserStyle {
    return @{NSFontAttributeName: [UIFont regularFontWithSize:14.0],
             NSForegroundColorAttributeName: [UIColor blackColor]};
}

+ (NSDictionary *)messageLastStyle {
    return @{NSFontAttributeName: [UIFont lightFontWithSize:16.0],
             NSForegroundColorAttributeName: [UIColor blackColor]};
}

+ (NSDictionary *)messageDateStyle {
    return @{NSFontAttributeName: [UIFont regularFontWithSize:12.0],
             NSForegroundColorAttributeName: [UIColor lightGrayColor]};
}

#pragma mark - Profile
+ (NSDictionary *)socialNameStyle {
    return @{NSFontAttributeName: [UIFont semiboldFontWithSize:14.0],
             NSForegroundColorAttributeName: [UIColor lightGrayColor]};
}

@end
