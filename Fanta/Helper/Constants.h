//
//  Constants.h
//  Fanta
//
//  Created by Mario Concilio on 03/04/15.
//  Copyright (c) 2015 Mario Concilio. All rights reserved.
//

#import <Foundation/Foundation.h>

extern const CGFloat kButtonCornerRadius;
extern const CGFloat kViewCornerRadius;
extern const CGFloat kAAPadding;

extern const CGFloat kPostImageNodePadding;
extern const CGFloat kPostImageNodeHeightScale;
extern const CGFloat kPostImageProfileThumbSize;
extern const CGFloat kPostImageButtonSize;
extern const NSUInteger kPostMaxLinesWithImage;
extern const NSUInteger kPostMaxLinesWithoutImage;

extern const CGSize kCommentCellThumbSize;
extern const CGSize kCommentImageButtonSize;
extern const CGFloat kCommentCellThumbWidth;
extern const CGFloat kCommentCellPadding;

extern const CGFloat kCommentTableHeaderHeight;
extern const CGFloat kCommentTableFooterHeight;
extern const CGFloat kCommentTablePadding;

extern const CGFloat kBlurNavbarHeight;
extern const CGFloat kBlurToolbarHeight;
extern const CGFloat kBlurToolbarPadding;
extern const CGSize kBlurToolbarButtonSize;

extern const CGSize kVBFButtonSize;

extern const CGFloat kNavBarHeight;
extern const CGFloat kMinScale;

extern const CGPoint kLoginMinScale;
extern const CGFloat kLoginSpringSpeed;
extern const CGFloat kLoginSpringBounce;

extern const CGSize kNewPostButtonSize;
extern const CGFloat kNewPostButtonWidth;

extern const CGFloat kStoreCellPadding;
extern const CGFloat kStoreCellInsets;

extern const CGSize kCartImageSize;
extern const CGSize kCartButtonSize;
extern const CGFloat kCartPadding;
extern const NSUInteger kCartMinItems;
extern const NSUInteger kCartMaxItems;

extern const CGFloat kTabViewHeight;

extern const CGFloat kShimmerSpeedRate;

extern NSString *const kNewPostPlaceholder;

extern NSString *const kBundleID;
extern NSString *const kUserToken;
extern NSString *const kProfileInfo;

extern const CGSize kImageSize;
extern const CGSize kThumbSize;

@interface Constants : NSObject

- (instancetype)init UNAVAILABLE_ATTRIBUTE;

@end

