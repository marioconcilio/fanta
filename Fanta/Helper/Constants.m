//
//  Constants.m
//  Fanta
//
//  Created by Mario Concilio on 8/1/16.
//  Copyright © 2016 Mario Concilio. All rights reserved.
//

#import "Constants.h"

const CGFloat kButtonCornerRadius   = 2.0;
const CGFloat kViewCornerRadius     = 2.0;
const CGFloat kAAPadding            = 10.0;

const CGFloat kPostImageNodePadding         = 10.0;
const CGFloat kPostImageNodeHeightScale     = 1.0;
const CGFloat kPostImageProfileThumbSize    = 40.0;
const CGFloat kPostImageButtonSize          = 20.0;
const NSUInteger kPostMaxLinesWithImage     = 3;
const NSUInteger kPostMaxLinesWithoutImage  = 10;

const CGSize kCommentCellThumbSize      = (CGSize) {.width = 40.0, .height = 40.0};
const CGSize kCommentImageButtonSize    = (CGSize) {.width = 15.0, .height = 15.0};
const CGFloat kCommentCellThumbWidth    = 40.0;
const CGFloat kCommentCellPadding       = 10.0;

const CGFloat kCommentTableHeaderHeight     = 44.0;
const CGFloat kCommentTableFooterHeight     = 44.0;
const CGFloat kCommentTablePadding          = 0.0;

const CGFloat kBlurNavbarHeight     = 44.0;
const CGFloat kBlurToolbarHeight    = 44.0;
const CGFloat kBlurToolbarPadding   = 10.0;
const CGSize kBlurToolbarButtonSize = (CGSize) {.width = 75.0, .height = 44.0};

const CGSize kVBFButtonSize = (CGSize) {.width = 25.0, .height = 25.0};

const CGFloat kNavBarHeight = 44.0;
const CGFloat kMinScale     = 0.1;

const CGPoint kLoginMinScale        = (CGPoint) {.x = 0.6, .y = 0.6};
const CGFloat kLoginSpringSpeed     = 1.0;
const CGFloat kLoginSpringBounce    = 1.5;

const CGSize kNewPostButtonSize     = (CGSize) {.width = 44.0, .height = 44.0};
const CGFloat kNewPostButtonWidth   = 44.0;

const CGFloat kStoreCellPadding     = 10.0;
const CGFloat kStoreCellInsets      = 5.0;

const CGSize kCartImageSize     = (CGSize) {.width = 60.0, .height = 60.0};
const CGSize kCartButtonSize    = (CGSize) {.width = 20.0, .height = 20.0};
const CGFloat kCartPadding      = 0.0;
const NSUInteger kCartMinItems  = 1;
const NSUInteger kCartMaxItems  = 9;

const CGFloat kTabViewHeight = 55.0;

const CGFloat kShimmerSpeedRate = 3.0;

NSString *const kNewPostPlaceholder = @"O que você está pensando?";

NSString *const kBundleID       = @"com.marioconcilio.Fanta";
NSString *const kUserToken      = @"user_token";
NSString *const kProfileInfo    = @"profile_info";

const CGSize kImageSize     = (CGSize) {.width = 800.0, .height = 800.0};
const CGSize kThumbSize     = (CGSize) {.width = 128.0, .height = 128.0};
