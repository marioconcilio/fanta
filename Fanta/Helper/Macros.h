//
//  Macros.h
//  Fanta
//
//  Created by Mario Concilio on 19/01/15.
//  Copyright (c) 2015 Mario Concilio. All rights reserved.
//

#ifndef Fanta_Macros_h
#define Fanta_Macros_h

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

@class AppDelegate;

#define NSUserDefaults      [NSUserDefaults standardUserDefaults]

#endif

static inline AppDelegate* UIAppDelegate() {
    return ((AppDelegate *) [UIApplication sharedApplication].delegate);
}

static inline NSString* StringFromBOOL(BOOL boolean) {
    return boolean? @"YES" : @"NO";
}

#pragma mark - UIKeyWindow
/**
 * Returns the application keyWindow
 */
static inline UIWindow* UIKeyWindow() {
    return [[UIApplication sharedApplication] keyWindow];
}

/**
 * Returns the height of keyWindow bounds
 */
static inline CGFloat UIKeyWindowHeight() {
    return CGRectGetHeight([[UIApplication sharedApplication] keyWindow].bounds);
}

/**
 * Returns the width of keyWindow bounds
 */
static inline CGFloat UIKeyWindowWidth() {
    return CGRectGetWidth([[UIApplication sharedApplication] keyWindow].bounds);
}

#pragma mark - UIColor
/**
 * Creates and return UIColor* using RGB values between 0-255
 *
 * @param red Red color (0-255)
 * @param green Green color (0-255)
 * @param blue Blue color (0-255)
 *
 * @return UIColor with RGB values and alpha = 1.0
 */
static inline UIColor* UIColorFromRGB(double red, double green, double blue) {
    return [UIColor colorWithRed:(red/255.0)
                           green:(green/255.0)
                            blue:(blue/255.0)
                           alpha:(1.0)];
}

/**
 * Creates and return UIColor* using RGB values between 0-255 and alpha between 0.0-1.0
 *
 * @param red Red color (0-255)
 * @param green Green color (0-255)
 * @param blue Blue color (0-255)
 * @param alpha Alpha (0.0-1.0)
 *
 * @return UIColor with RGB values and alpha = 1.0
 */
static inline UIColor* UIColorFromRGBA(double red, double green, double blue, double alpha) {
    return [UIColor colorWithRed:(red/255.0)
                           green:(green/255.0)
                            blue:(blue/255.0)
                           alpha:(alpha)];
}

static inline UIColor* UIColorFromHEX(long hex) {
    return [UIColor colorWithRed:((double)((hex & 0xFF0000) >> 16))/255.0
                           green:((double)((hex & 0xFF00) >> 8))/255.0
                            blue:((double)(hex & 0xFF))/255.0
                           alpha:1.0];
}

static inline UIColor* UIColorFromHEXA(long hex, double a) {
    return [UIColor colorWithRed:((double)((hex & 0xFF0000) >> 16))/255.0
                           green:((double)((hex & 0xFF00) >> 8))/255.0
                            blue:((double)(hex & 0xFF))/255.0
                           alpha:a];
}

#pragma mark - GCD
typedef void(^VoidBlock)();

/**
 *  Grand Central Dispatch.
 *
 *  Execute block on main thread.
 *
 *  @param block The block to be executed.
 */
static inline void GCDOnMain(VoidBlock block) {
    dispatch_async(dispatch_get_main_queue(), block);
}

/**
 *  Grand Central Dispatch.
 *
 *  Execute block on background thread priority default.
 *
 *  @param block The block to be executed.
 */
static inline void GCDOnBackground(VoidBlock block) {
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), block);
}
