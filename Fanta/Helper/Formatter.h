//
//  Formatter.h
//  Fanta
//
//  Created by Mario Concilio on 8/15/16.
//  Copyright © 2016 Mario Concilio. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Formatter : NSObject

/**
 *  Formats a NSDecimalNumber to String with format R$1.234,56
 *
 *  @param number The value to be formatted
 *
 *  @return The String formatted e.g. R$1.234,56
 */
+ (NSString *)formatCurrency:(NSDecimalNumber *)number;

/**
 *  Formats number that will be used in likes, dislikes and comments from post to a more human readable form.
 *  e.g. if number is 1989, the output will be 1.9k. If number is 20456, the output will be 20k.
 *
 *  @param number The number to be formatted.
 *
 *  @return The number formatted as string.
 */
+ (NSString *)formatNumber:(NSUInteger)number;

/**
 *  Creates and return a NSDate from string containing date and time with format 'yyyy-MM-dd HH:mm:ss'.
 *
 *  @param str The string with format 'yyyy-MM-dd HH:mm:ss'
 *
 *  @return The NSDate object.
 */
+ (NSDate *)dateFromString:(NSString *)str;

/**
 *  Creates text explaining when the post/comment was created from NSDate with format 'Em 'dd' de 'MMMM' de 'yyyy' às 'HH:mm.
 *
 *  @param date The date when the post/commentm was created.
 *
 *  @return The string formatted like 'Em 'dd' de 'MMMM' de 'yyyy' às 'HH:mm.
 */
+ (NSString *)parseDate:(NSDate *)date;

- (instancetype)init UNAVAILABLE_ATTRIBUTE;

@end
