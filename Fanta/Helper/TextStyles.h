//
//  TextStyles.h
//  Fanta
//
//  Created by Mario Concilio on 3/14/16.
//  Copyright © 2016 Mario Concilio. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface TextStyles : NSObject

#pragma mark - AA
+ (NSDictionary *)chooseAAMessageStyle;
+ (NSDictionary *)chooseAANameDarkStyle;
+ (NSDictionary *)chooseAANameLightStyle;
+ (NSDictionary *)chooseAASelectAllDarkStyle;
+ (NSDictionary *)chooseAASelectAllLightStyle;

#pragma mark - Post & StoreItem
+ (NSDictionary *)contentStyle;
+ (NSDictionary *)contentLightStyle;
+ (NSDictionary *)contentPlaceholderStyle;

#pragma mark - New Post
+ (NSDictionary *)newPostChooseAA;

#pragma mark - Comemnts
+ (NSDictionary *)commentProfileNameStyle;
+ (NSDictionary *)commentContentStyle;
+ (NSDictionary *)commentDateStyle;
+ (NSDictionary *)commentButtonStyle;
+ (NSDictionary *)commentButtonSelectedStyle;

#pragma mark - Cart
+ (NSDictionary *)cartTitleStyle;
+ (NSDictionary *)cartPriceStyle;
+ (NSDictionary *)cartQtyStyle;

#pragma mark - Store
+ (NSDictionary *)storeCellPriceStyle;
+ (NSDictionary *)storeCellTitleStyle;
+ (NSDictionary *)storeItemPriceStyle;
+ (NSDictionary *)storeItemTitleStyle;

#pragma mark - List Messages
+ (NSDictionary *)messageTitleStyle;
+ (NSDictionary *)messageLastUserStyle;
+ (NSDictionary *)messageLastStyle;
+ (NSDictionary *)messageDateStyle;

#pragma mark - Profile
+ (NSDictionary *)socialNameStyle;

- (instancetype)init UNAVAILABLE_ATTRIBUTE;

@end
