//
//  Formatter.m
//  Fanta
//
//  Created by Mario Concilio on 8/15/16.
//  Copyright © 2016 Mario Concilio. All rights reserved.
//

#import "Formatter.h"

@implementation Formatter

+ (NSString *)formatCurrency:(NSDecimalNumber *)number {
    NSNumberFormatter *formatter = [[NSNumberFormatter alloc] init];
    formatter.numberStyle = NSNumberFormatterCurrencyStyle;
    
    NSLocale *locale = [[NSLocale alloc] initWithLocaleIdentifier:@"pt_BR"];
    formatter.locale = locale;
    
    __autoreleasing NSString *currency = [formatter stringFromNumber:number];
    return currency;
}

+ (NSString *)formatNumber:(NSUInteger)number {
    if (number < 1000) {
        return [NSString stringWithFormat:@"%ld", number];
    }
    
    NSUInteger quocient = number / 1000;
    NSUInteger rest = (number % 1000) / 100;
    
    if (quocient < 10) {
        return [NSString stringWithFormat:@"%ld.%ldk", quocient, rest];
    }
    
    return [NSString stringWithFormat:@"%ldk", quocient];
}

+ (NSDate *)dateFromString:(NSString *)str {
    NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
    [dateFormat setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
    
    __autoreleasing NSDate *date = [dateFormat dateFromString:str];
    return date;
}

+ (NSString *)parseDate:(NSDate *)date {
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    NSLocale *locale = [NSLocale localeWithLocaleIdentifier:@"pt"];
    [dateFormatter setLocale:locale];
    [dateFormatter setDateFormat:@"'Em 'dd' de 'MMMM' de 'yyyy' às 'HH:mm"];
    
    __autoreleasing NSString *str = [dateFormatter stringFromDate:date];
    return str;
}

@end
