//
//  Helper.m
//  Fanta
//
//  Created by Mario Concilio on 28/04/15.
//  Copyright (c) 2015 Mario Concilio. All rights reserved.
//

#import "Helper.h"
#import "Macros.h"
#import "Constants.h"
#import "VOUser.h"
#import "VOCart.h"
#import "VOAthletic.h"
#import "UserService.h"
#import "Generator.h"

//#import <SAMKeychain.h>
#import <JSQMessages.h>

@implementation Helper

+ (void)saveCustomObject:(id)obj forKey:(NSString *)key {
    NSData *myEncodedObject = [NSKeyedArchiver archivedDataWithRootObject:obj];
    [NSUserDefaults setObject:myEncodedObject forKey:key];
    [NSUserDefaults synchronize];
}


+ (NSArray *)loadAAList {
    //TODO: should populate from db w/ APIService
    NSMutableArray *array = [NSMutableArray array];
    
    // users AA is aways the first
    VOAthletic *aa = [[UserService loadUser] athletic];
    if (aa) [array addObject:aa];
    
    for (NSUInteger i=1; i<=30; i++) {
        @autoreleasepool {
            VOAthletic *aa1 = [[VOAthletic alloc] init];
            aa1.name = @"Each";
            aa1.athleticName = [NSString stringWithFormat:@"AAAEACH %ld", i];
            aa1.thumbnail = [Generator randomSport];
            aa1.facebookID = 188801551146783;
            aa1.athleticID = i;
            
            VOAthletic *aa2 = [[VOAthletic alloc] init];
            aa2.name = @"Each";
            aa2.athleticName = [NSString stringWithFormat:@"DASI %ld", i];
            aa2.thumbnail = [Generator randomSport];
            aa2.facebookID = 332896303412460;
            aa2.athleticID = i + 30;
            
            VOAthletic *aa3 = [[VOAthletic alloc] init];
            aa3.name = @"Each";
            aa3.athleticName = [NSString stringWithFormat:@"DAGA %ld", i];
            aa3.thumbnail = [Generator randomSport];
            aa3.facebookID = 100002591287964;
            aa3.athleticID = i + 60;
            
            [array addObject:aa1];
            [array addObject:aa2];
            [array addObject:aa3];
        }
    }
    
    return array;
}

+ (UIImage *)compressImage:(UIImage *)image size:(CGSize)size compression:(CGFloat)compression {
    double actualHeight = image.size.height;
    double actualWidth = image.size.width;
    double maxHeight = size.height;
    double maxWidth = size.width;
    double imgRatio = actualWidth/actualHeight;
    double maxRatio = maxWidth/maxHeight;
    double compressionQuality = compression; // 0.5 means 50 percent compression, min 0.1 max 1.0 - 1.0 = full quality
    
    if (actualHeight > maxHeight || actualWidth > maxWidth){
        if(imgRatio < maxRatio){
            //adjust width according to maxHeight
            imgRatio = maxHeight / actualHeight;
            actualWidth = imgRatio * actualWidth;
            actualHeight = maxHeight;
        }
        else if (imgRatio > maxRatio){
            //adjust height according to maxWidth
            imgRatio = maxWidth / actualWidth;
            actualHeight = imgRatio * actualHeight;
            actualWidth = maxWidth;
        }
        else {
            actualHeight = maxHeight;
            actualWidth = maxWidth;
        }
    }
    
    CGRect rect = CGRectMake(0.0, 0.0, actualWidth, actualHeight);
    UIGraphicsBeginImageContext(rect.size);
    [image drawInRect:rect];
    UIImage *img = UIGraphicsGetImageFromCurrentImageContext();
    NSData *imageData = UIImageJPEGRepresentation(img, compressionQuality);
    UIGraphicsEndImageContext();
    
    return [UIImage imageWithData:imageData];
}

+ (NSString *)encodeUIImageToBase64String:(UIImage *)image {
    return [UIImagePNGRepresentation(image) base64EncodedStringWithOptions:NSDataBase64Encoding64CharacterLineLength];
}

+ (NSString *)initialsFromName:(NSString *)name {
    NSMutableString * firstCharacters = [NSMutableString string];
    NSArray * words = [name componentsSeparatedByCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
    for (NSString *word in words) {
        if (word.length > 0) {
            NSString * firstLetter = [word substringWithRange:[word rangeOfComposedCharacterSequenceAtIndex:0]];
            [firstCharacters appendString:[firstLetter uppercaseString]];
        }
    }
    
    return firstCharacters;
}

+ (void)avatarFromName:(NSString *)name font:(UIFont *)font diameter:(CGFloat)diameter callback:(void (^)(UIImage *image))callback {
    GCDOnBackground(^{
        JSQMessagesAvatarImage *avatar = [JSQMessagesAvatarImageFactory avatarImageWithUserInitials:[self initialsFromName:name]
                                                                                    backgroundColor:[UIColor randomColor]
                                                                                          textColor:[UIColor whiteColor]
                                                                                               font:font
                                                                                           diameter:diameter];
        
        GCDOnMain(^{
            callback(avatar.avatarImage);
        });
    });
}

+ (void)saveMyCart:(VOCart *)cart {
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = paths[0];
    NSString *path = [documentsDirectory stringByAppendingPathComponent:@"my_cart.plist"];
    [NSKeyedArchiver archiveRootObject:cart toFile:path];
}

+ (VOCart *)loadMyCart {
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = paths[0];
    NSString *path = [documentsDirectory stringByAppendingPathComponent:@"my_cart.plist"];
    NSLog(@"full path name: %@", path);
    
    // check if file does not exist
    NSFileManager *manager = [NSFileManager defaultManager];
    if (![manager fileExistsAtPath:path]) {
        NSLog (@"file not found, creating empty cart");
        VOCart *cart = [[VOCart alloc] init];
        cart.total = [NSDecimalNumber decimalNumberWithString:@"0"];
        cart.items = [[NSMutableDictionary alloc] init];
        
        [self saveMyCart:cart];
        
//        if (![manager createFileAtPath:path contents:nil attributes:nil]){
//            NSLog(@"create file returned NO");
//        }
    }
    
    /*
     #warning DEBUG ONLY populating cart
     VOStoreItem *item1 = [[VOStoreItem alloc] init];
     item1.title = @"Item One";
     item1.content = @"content one";
     item1.price = [NSDecimalNumber decimalNumberWithString:@"15"];
     item1.images = @[[Helper randomImage],
     [Helper randomImage]];
     
     VOStoreItem *item2 = [[VOStoreItem alloc] init];
     item2.title = @"Item Two";
     item2.content = @"content two";
     item2.price = [NSDecimalNumber decimalNumberWithString:@"30.75"];
     item2.images = @[[Helper randomImage],
     [Helper randomImage]];
     
     VOCart *cart = [[VOCart alloc] init];
     cart.total = [NSDecimalNumber decimalNumberWithString:@"183.75"];
     
     NSMutableDictionary<VOStoreItem*, NSNumber*> *items = [[NSMutableDictionary alloc] initWithCapacity:2];
     [items setObject:@(2) forKey:item1];
     [items setObject:@(5) forKey:item2];
     cart.items = items;
     [Helper saveMyCart:cart];
     */
    
    return [NSKeyedUnarchiver unarchiveObjectWithFile:path];
}

@end
