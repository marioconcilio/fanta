//
//  LogFormatter.m
//  Fanta
//
//  Created by Mario Concilio on 7/28/16.
//  Copyright © 2016 Mario Concilio. All rights reserved.
//

#import "LogFormatter.h"
#import <libkern/OSAtomic.h>

@implementation LogFormatter

- (instancetype)init {
    self = [super init];
    if (self) {
        
    }
    
    return self;
}

- (NSString *)stringFromDate:(NSDate *)date {
    int32_t loggerCount = OSAtomicAdd32(0, &atomicLoggerCount);
    NSString *dateFormatString = @"dd-MM-yyyy HH:mm:ss:SSS";
    
    if (loggerCount <= 1) {
        // Single-threaded mode.
        
        if (threadUnsafeDateFormatter == nil) {
            threadUnsafeDateFormatter = [[NSDateFormatter alloc] init];
            [threadUnsafeDateFormatter setDateFormat:dateFormatString];
        }
        
        return [threadUnsafeDateFormatter stringFromDate:date];
    }
    else {
        // Multi-threaded mode.
        // NSDateFormatter is NOT thread-safe.
        
        NSString *key = @"LogFormatter_NSDateFormatter";
        
        NSMutableDictionary *threadDictionary = [[NSThread currentThread] threadDictionary];
        NSDateFormatter *dateFormatter = [threadDictionary objectForKey:key];
        
        if (dateFormatter == nil) {
            dateFormatter = [[NSDateFormatter alloc] init];
            [dateFormatter setDateFormat:dateFormatString];
            
            [threadDictionary setObject:dateFormatter forKey:key];
        }
        
        return [dateFormatter stringFromDate:date];
    }
}

- (NSString *)formatLogMessage:(DDLogMessage *)logMessage {
    NSString *logLevel;
    switch (logMessage->_flag) {
        case DDLogFlagError    : logLevel = @"ERRO"; break;
        case DDLogFlagWarning  : logLevel = @"WARN"; break;
        case DDLogFlagInfo     : logLevel = @"INFO"; break;
        case DDLogFlagDebug    : logLevel = @"DEBU"; break;
        default                : logLevel = @"VERB"; break;
    }
    
    NSString *dateAndTime = [self stringFromDate:(logMessage.timestamp)];
    NSString *logMsg = logMessage->_message;
    
    return [NSString stringWithFormat:@"[%@] %@ @ %@(%ld)# %@", logLevel, dateAndTime, logMessage.fileName, logMessage.line, logMsg];
}

- (void)didAddToLogger:(id <DDLogger>)logger {
    OSAtomicIncrement32(&atomicLoggerCount);
}

- (void)willRemoveFromLogger:(id <DDLogger>)logger {
    OSAtomicDecrement32(&atomicLoggerCount);
}

@end
