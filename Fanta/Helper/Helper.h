//
//  Helper.h
//  Fanta
//
//  Created by Mario Concilio on 28/04/15.
//  Copyright (c) 2015 Mario Concilio. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

@class VOUser, VOCart, VOAthletic;
@interface Helper : NSObject

+ (UIImage *)compressImage:(UIImage *)image size:(CGSize)size compression:(CGFloat)compression;
+ (NSString *)encodeUIImageToBase64String:(UIImage *)image;
//+ (VOUser *)loadUser;

//+ (NSString *)loadToken;
//+ (void)saveToken:(VOUser *)user;

+ (NSArray<VOAthletic *> *)loadAAList;
+ (void)saveCustomObject:(id)obj forKey:(NSString *)key;

+ (NSString *)initialsFromName:(NSString *)name;
+ (void)avatarFromName:(NSString *)name
                  font:(UIFont *)font
              diameter:(CGFloat)diameter
              callback:(void (^)(UIImage *image))callback;

+ (void)saveMyCart:(VOCart *)cart;
+ (VOCart *)loadMyCart;

- (instancetype)init UNAVAILABLE_ATTRIBUTE;

@end
