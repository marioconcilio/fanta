//
//  Generator.m
//  Fanta
//
//  Created by Mario Concilio on 8/18/16.
//  Copyright © 2016 Mario Concilio. All rights reserved.
//

#import "Generator.h"
#import "VOUser.h"

@implementation Generator

+ (NSString *)mussumIpsum {
    return @"Mussum ipsum cacilds, vidis litro abertis. Consetis adipiscings elitis. Pra lá , depois divoltis porris, paradis. Paisis, filhis, espiritis santis. Mé faiz elementum girarzis, nisi eros vermeio, in elementis mé pra quem é amistosis quis leo. Manduma pindureta quium dia nois paga. Sapien in monti palavris qui num significa nadis i pareci latim. Interessantiss quisso pudia ce receita de bolis, mais bolis eu num gostis.\nSuco de cevadiss, é um leite divinis, qui tem lupuliz, matis, aguis e fermentis. Interagi no mé, cursus quis, vehicula ac nisi. Aenean vel dui dui. Nullam leo erat, aliquet quis tempus a, posuere ut mi. Ut scelerisque neque et turpis posuere pulvinar pellentesque nibh ullamcorper. Pharetra in mattis molestie, volutpat elementum justo. Aenean ut ante turpis. Pellentesque laoreet mé vel lectus scelerisque interdum cursus velit auctor. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Etiam ac mauris lectus, non scelerisque augue. Aenean justo massa.";
}

+ (NSString *)randomImage {
    NSArray *types = @[@"abstract", @"city", @"people", @"transport", @"animals", @"food", @"nature", @"business", @"nightlife", @"sports", @"cats", @"fashion", @"technics"];
    NSString *type = types[arc4random_uniform((u_int32_t) types.count)];
    return [NSString stringWithFormat:@"http://lorempixel.com/400/400/%@/%d", type, arc4random_uniform(11)];
}

+ (NSString *)randomPerson {
    return [NSString stringWithFormat:@"http://lorempixel.com/400/400/people/%d", arc4random_uniform(11)];
}

+ (NSString *)randomSport {
    return [NSString stringWithFormat:@"http://lorempixel.com/400/400/sports/%d", arc4random_uniform(11)];
}

+ (NSMutableArray<VOUser *> *)randomUsers:(NSUInteger)count {
    NSMutableArray *tempArray = [NSMutableArray array];
    for (NSInteger i = 0; i < count; i++) {
        @autoreleasepool {
            VOUser *user = [[VOUser alloc] init];
            user.userID = (i+1) * 100;
            user.name = [Generator randomName];
            user.email = [NSString stringWithFormat:@"kyle%lu@gmail.com", user.userID];
            [tempArray addObject:user];
        }
    }
    
    return tempArray;
}

+ (NSString *)randomName {
    NSArray *names = @[@"André",@"Antônio",@"Arthur",@"Breno",@"Bruno",@"Caio",@"Carlos",@"Cauã",@"Daniel",@"Danilo",@"Davi",@"Diego",@"Douglas",@"Eduardo",@"Enzo",@"Erick",@"Felipe",@"Gabriel",@"Guilherme",@"Gustavo",@"Igor",@"José",@"João",@"Júlio",@"Kaio",@"Kauan",@"Kauã",@"Kauê",@"Leonardo",@"Luan",@"Lucas",@"Luiz",@"Luís",@"Marcos",@"Mateus",@"Matheus",@"Miguel",@"Murilo",@"Nicolas",@"Otávio",@"Paulo",@"Pedro",@"Rafael",@"Renan",@"Ryan",@"Samuel",@"Thiago",@"Victor",@"Vinícius",@"Vitór",@"Alice",@"Aline",@"Amanda",@"Ana",@"Anna",@"Beatriz",@"Bianca",@"Brenda",@"Bruna",@"Camila",@"Carolina",@"Clara",@"Eduarda",@"Emilly",@"Emily",@"Evelyn",@"Fernanda",@"Gabriela",@"Gabrielle",@"Gabrielly",@"Giovana",@"Giovanna",@"Isabela",@"Isabella",@"Isabelle",@"Júlia",@"Lara",@"Larissa",@"Laura",@"Lavinia",@"Letícia",@"Livia",@"Luana",@"Luiza",@"Manuela",@"Maria",@"Mariana",@"Marina",@"Melissa",@"Nicole",@"Rafaela",@"Raissa",@"Rebeca",@"Sarah",@"Sofia",@"Sophia",@"Thaís",@"Vitória",@"Yasmin",@"Ágatha"];
    
    NSArray *surnames = @[@"Almeida",@"Alvaréz",@"Alves",@"Araújo",@"Azevedo",@"Barbosa",@"Barboza",@"Cardoso",@"Carvalho",@"Cavalcante",@"Cavalcanti",@"Correa",@"Correia",@"Costa",@"Dias",@"Díaz",@"Fernandes",@"Fernandez",@"Ferreira",@"German",@"Gomes",@"Gomez",@"Gonzáles",@"Gonçalves",@"Gónzalez",@"Lima",@"Martins",@"Mello",@"Melo",@"Montes",@"Moraes",@"Morais",@"Oliveira",@"Pereira",@"Ribeiro",@"Rocha",@"Rodrigues",@"Santos",@"Schmidt",@"Schmitz",@"Silva",@"Sousa",@"Souza",@"Teixeira"];
    
    NSInteger j = arc4random() % names.count;
    NSInteger k = arc4random() % surnames.count;
    
    NSString *name = [NSString stringWithFormat:@"%@ %@", names[j], surnames[k]];
    
    return name;
}

@end
