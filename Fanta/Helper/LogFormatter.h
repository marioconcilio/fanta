//
//  LogFormatter.h
//  Fanta
//
//  Created by Mario Concilio on 7/28/16.
//  Copyright © 2016 Mario Concilio. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <DDLog.h>

@interface LogFormatter : NSObject <DDLogFormatter> {
    int atomicLoggerCount;
    NSDateFormatter *threadUnsafeDateFormatter;
}

@end
