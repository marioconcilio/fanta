//
//  Generator.h
//  Fanta
//
//  Created by Mario Concilio on 8/18/16.
//  Copyright © 2016 Mario Concilio. All rights reserved.
//

#import <Foundation/Foundation.h>

@class VOUser;
@interface Generator : NSObject

/**
*  Generate and returns placeholder text based on Loren Ipsum.
*
*  @return The placeholder text.
*/
+ (NSString *)mussumIpsum;

/**
 *  Generate string containing url of random image from lorenpixel.com
 *
 *  @return The URL String.
 */
+ (NSString *)randomImage;

/**
 *  Generate string containing url of random person from lorenpixel.com
 *
 *  @return The URL String.
 */
+ (NSString *)randomPerson;

/**
 *  Generate string containing url of random image sport from lorenpixel.com
 *
 *  @return The URL String.
 */
+ (NSString *)randomSport;

/**
 *  Creates and return a mutable array containing random users.
 *
 *  @param count The number of random users to create.
 *
 *  @return A mutable array with random users.
 */
+ (NSMutableArray<VOUser *> *)randomUsers:(NSUInteger)count;

/**
 *  Generates and returns a String containing random name and random surname.
 *
 *  @return The generated random name.
 */
+ (NSString *)randomName;

- (instancetype)init UNAVAILABLE_ATTRIBUTE;

@end
