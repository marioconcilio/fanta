//
//  DrawerViewController.h
//  Fanta
//
//  Created by Mario Concilio on 23/01/15.
//  Copyright (c) 2015 Mario Concilio. All rights reserved.
//

#import <UIKit/UIKit.h>

@class DrawerViewController;
@protocol DrawerDelegate <NSObject>

- (void)drawerViewDidDismiss:(DrawerViewController *) drawerView;

@end

@interface DrawerViewController : UITableViewController

@property (weak, nonatomic) IBOutlet UILabel *nameLabel;
@property (weak, nonatomic) IBOutlet UILabel *athleticLabel;
@property (weak, nonatomic) IBOutlet UIImageView *profileImageView;
@property (weak, nonatomic) IBOutlet UIImageView *storeImageView;

@property (weak, nonatomic) UIViewController<DrawerDelegate> *delegateViewController;

+ (void)presentInViewController:(UIViewController<DrawerDelegate> *)viewController;

//- (void)dismiss;

@end
