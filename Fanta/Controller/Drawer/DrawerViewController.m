//
//  DrawerViewController.m
//  Fanta
//
//  Created by Mario Concilio on 23/01/15.
//  Copyright (c) 2015 Mario Concilio. All rights reserved.
//

#import "DrawerViewController.h"
//#import "AboutViewController.h"
#import "ProfileViewController.h"
#import "FeedViewController.h"
#import "UIStoryboard+Storyboards.h"
#import "Helper.h"
#import "UserService.h"
#import "VOUser.h"
#import "VOAthletic.h"
#import "AppDelegate.h"
#import "VOCart.h"
#import "UIViewController+BaseViewController.h"

//#import <UIImageView+AFNetworking.h>
#import <UIImageView+WebCache.h>
#import <MessageUI/MessageUI.h>
#import <SWRevealViewController.h>
#import <Sheriff/GIBadgeView.h>

typedef NS_ENUM(NSUInteger, DrawerIndex) {
    profileIndex,
    feedIndex,
    messagesIndex,
    storeIndex,
    walletIndex,
    suggestIndex,
    aboutIndex
};

@interface DrawerViewController () <MFMailComposeViewControllerDelegate, UIGestureRecognizerDelegate>

@property (weak, nonatomic) GIBadgeView *badge;

@end

@implementation DrawerViewController

+ (void)presentInViewController:(UIViewController<DrawerDelegate> *)viewController {
    __autoreleasing DrawerViewController *drawer = [[DrawerViewController alloc] initWithStyle:UITableViewStylePlain];
    drawer.delegateViewController = viewController;
    
    [UIKeyWindow() addSubview:drawer.view];
    [viewController addChildViewController:drawer];
}

#pragma mark - View Life Cycle
- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.profileImageView.layer.cornerRadius = 50.0;
    self.profileImageView.clipsToBounds = YES;
    self.profileImageView.layer.borderColor = [UIColor lightGrayColor].CGColor;
    self.profileImageView.layer.borderWidth = 1.0;
    
    self.tableView.separatorColor = [UIColor customDarkBackground];
    
    GIBadgeView *badge = [[GIBadgeView alloc] init];
    badge.font = [UIFont regularFontWithSize:12.0];
    badge.backgroundColor = [UIColor customPink];
    badge.textColor = [UIColor whiteColor];
    [self.storeImageView addSubview:badge];
    _badge = badge;
    
    UIBlurEffect *blurEffect = [UIBlurEffect effectWithStyle:UIBlurEffectStyleDark];
    UIImageView *imageView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"fnt_menu_background"]];
    self.tableView.backgroundView = imageView;
    self.tableView.backgroundColor = [UIColor clearColor];
    self.tableView.separatorEffect = [UIVibrancyEffect effectForBlurEffect:blurEffect];
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    
    VOUser *user = [UserService loadUser];
    self.nameLabel.text = user.name;
    self.athleticLabel.text = [user.athletic fullName];
    
    [Helper avatarFromName:user.name font:[UIFont lightFontWithSize:32.0] diameter:120.0 callback:^(UIImage *image) {
        [self.profileImageView sd_setImageWithURL:[NSURL URLWithString:user.image]
                                 placeholderImage:image
                                          options:SDWebImageRefreshCached];
    }];
    
    VOCart *myCart = UIAppDelegate().myCart;
    self.badge.badgeValue = myCart.items.count;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (UIStatusBarStyle)preferredStatusBarStyle {
    return UIStatusBarStyleLightContent;
}

#pragma mark - Helper Methods
- (void)sendEmail {
    if (![MFMailComposeViewController canSendMail]) {
        NSLog(@"Mail services are not available");
        return;
    }
    
    NSString *subject = @"[v1.0]Sugestões";
    NSArray *toRecipents = @[@"contato@elefante.com.br"];
    MFMailComposeViewController *mc = [[MFMailComposeViewController alloc] init];
    mc.mailComposeDelegate = self;
    [mc setSubject:subject];
    [mc setToRecipients:toRecipents];
    [mc.navigationBar setTintColor: [UIColor customPink]];
    
    [self presentViewController:mc animated:YES completion:NULL];
}

#pragma mark - MailComposer Delegate
- (void)mailComposeController:(MFMailComposeViewController *)controller
          didFinishWithResult:(MFMailComposeResult)result
                        error:(NSError *)error {
    switch (result) {
        case MFMailComposeResultCancelled:
            DDLogVerbose(@"mail cancelled");
            break;
            
        case MFMailComposeResultSaved:
            DDLogVerbose(@"mail saved");
            break;
            
        case MFMailComposeResultSent:
            DDLogVerbose(@"mail sent");
            break;
            
        case MFMailComposeResultFailed:
            DDLogVerbose(@"mail failed: %@", error.debugDescription);
            break;
            
        default:
            break;
    }
    
    [self dismissViewControllerAnimated:YES completion:NULL];
}

#pragma mark - Table View Datasource
- (void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath {
    if (indexPath.section == 0) {
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
    }
    else {
        UIView *view = [[UIView alloc] init];
        view.backgroundColor = [UIColor customPink];
        cell.selectedBackgroundView = view;
    }
}

#pragma mark - Table View Delegate
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
    switch (indexPath.row) {
        case profileIndex:
            [self.revealViewController pushFrontViewController:[[UIStoryboard profileStoryboard] instantiateInitialViewController] animated:YES];
            break;
            
        case feedIndex:
            [self.revealViewController pushFrontViewController:[[UIStoryboard feedStoryboard] instantiateInitialViewController]
                                                         animated:YES];
            break;
            
        case messagesIndex:
            [self.revealViewController pushFrontViewController:[[UIStoryboard messagesStoryboard] instantiateInitialViewController]
                                                         animated:YES];
            break;
            
//        case kPubIndex:
//            [self.revealViewController pushFrontViewController:[[UIStoryboard pubStoryboard] instantiateInitialViewController] animated:YES];
//            break;
            
        case storeIndex:
            [self.revealViewController pushFrontViewController:[[UIStoryboard storeStoryboard] instantiateInitialViewController] animated:YES];
            break;
            
        case walletIndex:
            DDLogInfo(@"wallet not implemented yet");
            break;
            
//        case kFlagreiIndex:
//            NSLog(@"flagrei not implemented yet");
//            break;
            
        case suggestIndex:
            [self sendEmail];
            break;
            
        case aboutIndex:
            DDLogInfo(@"wallet not implemented yet");
//            [self.revealViewController pushFrontViewController:[AboutViewController viewControllerWithNavigation]
//                                                         animated:YES];
            break;
            
        default:
            break;
    }
    
//    [self.sideMenuViewController hideMenuViewController];
}

@end
