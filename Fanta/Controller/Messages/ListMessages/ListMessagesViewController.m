//
//  ListMessagesViewController.m
//  Fanta
//
//  Created by Mario Concilio on 10/19/15.
//  Copyright © 2015 Mario Concilio. All rights reserved.
//

#import "ListMessagesViewController.h"
#import "MessagesViewController.h"
#import "MessageCell.h"
#import "UIViewController+BaseViewController.h"
#import "VOMessage.h"
#import "Helper.h"
#import "Generator.h"

#import <AsyncDisplayKit.h>

@interface ListMessagesViewController () <ASTableDelegate, ASTableDataSource, SWRevealViewControllerDelegate> {
    ASTableView *_tableView;
}

@property (nonatomic, strong) NSArray<VOMessage *> *messages;

@end

static NSString *const kMessageSegue    = @"messageSegue";

@implementation ListMessagesViewController

- (instancetype)initWithCoder:(NSCoder *)aDecoder {
    self = [super initWithCoder:aDecoder];
    if (self) {
        _tableView = [[ASTableView alloc] initWithFrame:CGRectZero style:UITableViewStylePlain asyncDataFetching:YES];
        _tableView.asyncDataSource = self;
        _tableView.asyncDelegate = self;
    }
    
    return self;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    [self.view addSubview:_tableView];
    _tableView.tableFooterView = [[UIView alloc] initWithFrame:CGRectZero];
    
    /*
     *  base view controller
     */
    [self addDrawerButtonLight];
    [self setupReachability];
    
    self.revealViewController.delegate = self;
    
    self.messages = [self populateMessages];
    
    [self.navigationController.navigationBar setBarStyle:UIBarStyleDefault];
    [self.navigationController.navigationBar setTintColor:[UIColor customPink]];
    [self.navigationController.navigationBar setTitleTextAttributes:@{NSFontAttributeName:[UIFont titleFontWithSize:16.f],
                                                                      NSForegroundColorAttributeName:[UIColor customDarkBackground]}];
}

- (void)viewWillLayoutSubviews {
    [super viewWillLayoutSubviews];
    _tableView.frame = self.view.bounds;
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleDefault animated:YES];
//    [self.drawerButton animateToType:buttonMenuType];
}

- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
}

- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
    [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleLightContent animated:YES];
}

- (void)viewDidDisappear:(BOOL)animated {
    [super viewDidDisappear:animated];
//    [self.drawerButton animateToType:buttonDefaultType];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#warning DEBUG ONLY
- (NSMutableArray<VOMessage*> *)populateMessages {
    NSMutableArray *array = [[NSMutableArray alloc] initWithCapacity:100];
    for (long i=0; i<100; i++) {
        @autoreleasepool {
            VOMessage *message = [[VOMessage alloc] init];
            message.title = [NSString stringWithFormat:@"Mensagem %ld", i+1];
            message.thumbnail = [Generator randomPerson];
            message.lastMessage = [Generator mussumIpsum];
            message.lastUser = [Generator randomName];
            message.userID = i;
            message.groupID = 0;
            message.lastMessageDate = [[NSDate alloc] initWithTimeIntervalSinceNow:i * 600];
            
            [array addObject:message];
        }
    }
    
    return array;
}

#pragma mark - Navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    if ([segue.identifier isEqualToString:kMessageSegue]) {
        //TODO: set user and previous messages
//        MessagesViewController *vc = segue.destinationViewController;
        
    }
}

#pragma mark - Table view data source
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.messages.count;
}

- (ASCellNode *)tableView:(ASTableView *)tableView nodeForRowAtIndexPath:(NSIndexPath *)indexPath {
    MessageCell *cell = [[MessageCell alloc] initWithMessage:self.messages[indexPath.row]];
    return cell;
}

#pragma mark - Table View Delegate
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
    VOMessage *message = self.messages[indexPath.row];
    [self performSegueWithIdentifier:kMessageSegue sender:message];
}

#pragma mark - SWReveal View Controller Delegate
- (void)revealController:(SWRevealViewController *)revealController willMoveToPosition:(FrontViewPosition)position {
    if (position == FrontViewPositionRight) {
        [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleLightContent animated:YES];
        
        UIView *view = [[UIView alloc] initWithFrame:CGRectMake(0.0, -20.0, UIKeyWindowWidth(), 20.0)];
        view.backgroundColor = UIColorFromHEX(0x222222);
        view.tag = 1989;
        [self.navigationController.navigationBar addSubview:view];
    }
    else if (position == FrontViewPositionLeft) {
        [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleDefault animated:YES];
        [[self.navigationController.navigationBar viewWithTag:1989] removeFromSuperview];
    }
}

- (void)revealController:(SWRevealViewController *)revealController panGestureMovedToLocation:(CGFloat)location progress:(CGFloat)progress {
    UIView *view = [self.navigationController.navigationBar viewWithTag:1989];
    if (view) {
        view.alpha = progress;
    }
    
    if (progress > 0.5)
        [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleLightContent animated:YES];
    else
        [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleDefault animated:YES];
}

@end
