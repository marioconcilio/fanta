//
//  MessageCell.m
//  Fanta
//
//  Created by Mario Concilio on 10/19/15.
//  Copyright © 2015 Mario Concilio. All rights reserved.
//

#import "MessageCell.h"
#import "VOMessage.h"
#import "Constants.h"
#import "TextStyles.h"
#import "UIColor+CustomColor.h"

#import <POP.h>
#import <WebASDKImageManager.h>
#import <NSDate+DateTools.h>

@interface MessageCell () <ASNetworkImageNodeDelegate>

@property (nonatomic, weak) ASNetworkImageNode *thumbnailNode;
@property (nonatomic, weak) ASTextNode *titleNode;
@property (nonatomic, weak) ASTextNode *lastUserNode;
@property (nonatomic, weak) ASTextNode *lastMessageNode;
@property (nonatomic, weak) ASTextNode *timeNode;

@end

@implementation MessageCell

- (instancetype)initWithMessage:(VOMessage *)message {
    self = [super init];
    if (self) {
        /*
         * Thumbnail
         */
        ASNetworkImageNode *thumb = [[ASNetworkImageNode alloc] initWithWebImage];
        thumb.URL = [NSURL URLWithString:message.thumbnail];
        thumb.preferredFrameSize = kCommentCellThumbSize;
        thumb.cornerRadius = kCommentCellThumbWidth / 2;
        thumb.clipsToBounds = YES;
        thumb.borderWidth = 0.5;
        thumb.borderColor = UIColorFromRGB(151, 151, 151).CGColor;
        thumb.delegate = self;
        _thumbnailNode = thumb;
        [self addSubnode:_thumbnailNode];
        
        /*
         * Title
         */
        ASTextNode *title = [[ASTextNode alloc] init];
        title.attributedString = [[NSAttributedString alloc] initWithString:message.title
                                                                 attributes:[TextStyles messageTitleStyle]];
        title.layerBacked = YES;
        title.maximumNumberOfLines = 1;
        title.truncationMode = NSLineBreakByTruncatingTail;
        title.flexGrow = NO;
        title.flexShrink = NO;
        _titleNode = title;
        [self addSubnode:_titleNode];
        
        /*
         * Last User
         */
        ASTextNode *lastUser = [[ASTextNode alloc] init];
        lastUser.attributedString = [[NSAttributedString alloc] initWithString:message.lastUser
                                                                    attributes:[TextStyles messageLastUserStyle]];
        lastUser.layerBacked = YES;
        lastUser.maximumNumberOfLines = 1;
        lastUser.truncationMode = NSLineBreakByTruncatingTail;
        _lastUserNode = lastUser;
        [self addSubnode:_lastUserNode];
        
        /*
         * Last Message
         */
        ASTextNode *lastMessage = [[ASTextNode alloc] init];
        lastMessage.attributedString = [[NSAttributedString alloc] initWithString:message.lastMessage
                                                                       attributes:[TextStyles messageLastStyle]];
        lastMessage.layerBacked = YES;
        lastMessage.maximumNumberOfLines = 1;
        lastMessage.truncationMode = NSLineBreakByTruncatingTail;
        lastMessage.flexGrow = NO;
        lastMessage.flexShrink = YES;
        _lastMessageNode = lastMessage;
        [self addSubnode:_lastMessageNode];
        
        /*
         * Time
         */
        ASTextNode *time = [[ASTextNode alloc] init];
        time.attributedString = [[NSAttributedString alloc] initWithString:message.lastMessageDate.timeAgoSinceNow
                                                                attributes:[TextStyles messageDateStyle]];
        time.layerBacked = YES;
        time.flexGrow = NO;
        time.flexShrink = NO;
        _timeNode = time;
        [self addSubnode:_timeNode];
    }
    
    return self;
}

- (ASLayoutSpec *)layoutSpecThatFits:(ASSizeRange)constrainedSize {
    // flexible space between title and time
    ASLayoutSpec *spacer = [[ASLayoutSpec alloc] init];
    spacer.flexGrow = YES;
    
    // horizontal stack for title and time label
    ASStackLayoutSpec *titleStack = [ASStackLayoutSpec stackLayoutSpecWithDirection:ASStackLayoutDirectionHorizontal
                                                                            spacing:5.0
                                                                     justifyContent:ASStackLayoutJustifyContentStart
                                                                         alignItems:ASStackLayoutAlignItemsCenter
                                                                           children:@[_titleNode, spacer, _timeNode]];
    titleStack.alignSelf = ASStackLayoutAlignSelfStretch;
    
    // vertical spec of cell main content
    ASStackLayoutSpec *contentSpec = [ASStackLayoutSpec stackLayoutSpecWithDirection:ASStackLayoutDirectionVertical
                                                                             spacing:8.0
                                                                      justifyContent:ASStackLayoutJustifyContentStart
                                                                          alignItems:ASStackLayoutAlignItemsStart
                                                                            children:@[titleStack, _lastUserNode, _lastMessageNode]];
    contentSpec.alignItems = ASStackLayoutAlignSelfStretch;
//    contentSpec.flexGrow = YES;
    contentSpec.flexShrink = YES;
    
    // horizontal spec for image
    ASStackLayoutSpec *imageContentSpec = [ASStackLayoutSpec stackLayoutSpecWithDirection:ASStackLayoutDirectionHorizontal
                                                                                  spacing:10.0
                                                                           justifyContent:ASStackLayoutJustifyContentStart
                                                                               alignItems:ASStackLayoutAlignItemsStart
                                                                                 children:@[_thumbnailNode, contentSpec]];
    
    return [ASInsetLayoutSpec insetLayoutSpecWithInsets:UIEdgeInsetsMake(10, 10, 10, 10) child:imageContentSpec];
}

- (void)setHighlighted:(BOOL)highlighted {
    if (highlighted) {
        self.backgroundColor = [UIColor customPink];
    }
    else {
        self.backgroundColor = [UIColor whiteColor];
    }
}

#pragma mark - ASNetworkImage Delegate
- (void)imageNode:(ASNetworkImageNode *)imageNode didLoadImage:(UIImage *)image {
    if (image) {
        POPBasicAnimation *anim = [POPBasicAnimation animationWithPropertyNamed:kPOPViewAlpha];
        anim.fromValue = @(0.0);
        anim.toValue = @(1.0);
        [imageNode pop_removeAllAnimations];
        [imageNode pop_addAnimation:anim forKey:@"fadeIn"];
    }
}

@end
