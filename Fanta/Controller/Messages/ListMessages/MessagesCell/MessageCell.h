//
//  MessageCell.h
//  Fanta
//
//  Created by Mario Concilio on 10/19/15.
//  Copyright © 2015 Mario Concilio. All rights reserved.
//

#import <AsyncDisplayKit/AsyncDisplayKit.h>

@class VOMessage;
@interface MessageCell : ASCellNode

@property (nonatomic, weak, readonly) ASNetworkImageNode *thumbnailNode;
@property (nonatomic, weak, readonly) ASTextNode *titleNode;
@property (nonatomic, weak, readonly) ASTextNode *lastUserNode;
@property (nonatomic, weak, readonly) ASTextNode *lastMessageNode;
@property (nonatomic, weak, readonly) ASTextNode *timeNode;

- (instancetype)init __unavailable;
- (instancetype)initWithMessage:(VOMessage *)message;

@end
