//
//  PostViewController.h
//  Fanta
//
//  Created by Mario Concilio on 26/03/15.
//  Copyright (c) 2015 Mario Concilio. All rights reserved.
//

#import <UIKit/UIKit.h>

@class VOPost, ASImageNode, ASTextNode, BkgView, VBFPopFlatButton;
@interface PostViewController : UIViewController

@property (nonatomic, strong) VOPost *post;

@property (weak, nonatomic) ASImageNode *imageNode;
@property (weak, nonatomic) ASTextNode *contentNode;
@property (weak, nonatomic) BkgView *bkgView;
@property (weak, nonatomic) IBOutlet UIVisualEffectView *blurNavbar;
@property (weak, nonatomic) IBOutlet UIVisualEffectView *blurToolbar;
@property (weak, nonatomic) IBOutlet UIScrollView *scrollView;
@property (weak, nonatomic) IBOutlet UIButton *commentButton;
@property (weak, nonatomic) IBOutlet UIButton *dislikeButton;
@property (weak, nonatomic) IBOutlet UIButton *likeButton;
@property (weak, nonatomic) IBOutlet UILabel *commentLabel;
@property (weak, nonatomic) IBOutlet UILabel *dislikeLabel;
@property (weak, nonatomic) IBOutlet UILabel *likeLabel;
@property (weak, nonatomic) IBOutlet UILabel *titleLabel;
@property (weak, nonatomic) VBFPopFlatButton *closeButton;

- (void)setupPost:(VOPost *)post cachedImage:(UIImage *)image frame:(CGRect)frame;
- (void)openImage;
- (IBAction)doOpenComments:(UIButton *)sender;
- (IBAction)doDislike:(UIButton *)sender;
- (IBAction)doLike:(UIButton *)sender;

@end
