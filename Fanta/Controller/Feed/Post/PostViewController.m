//
//  PostViewController.m
//  Fanta
//
//  Created by Mario Concilio on 26/03/15.
//  Copyright (c) 2015 Mario Concilio. All rights reserved.
//

#import "APIService.h"
#import "BkgView.h"
#import "Constants.h"
#import "CommentsViewController.h"
#import "PostViewController.h"
#import "VOPost.h"
#import "VOUser.h"
#import "Helper.h"
#import "Formatter.h"
#import "PopoverTransition.h"
#import "TextStyles.h"
#import "ExpandCellTransition.h"

#import <AsyncDisplayKit.h>
#import <POP.h>
#import <VBFPopFlatButton.h>
#import <IDMPhotoBrowser.h>

@interface PostViewController () <POPAnimationDelegate, UIViewControllerTransitioningDelegate>

//@property (nonatomic, assign) CGPoint contentCenter;
//@property (nonatomic, assign) CGPoint imageCenter;
@property (nonatomic, assign) CGRect frame;
@property (nonatomic, strong) ExpandCellAnimationController *animator;

@end

static NSString *const kCommentsSegue = @"commentsSegue";

@implementation PostViewController

#pragma mark - Init
- (instancetype)initWithCoder:(NSCoder *)aDecoder {
    self = [super initWithCoder:aDecoder];
    if (self) {
        [self commonInit];
    }
    
    return self;
}

- (void)commonInit {
    self.animator = [[ExpandCellAnimationController alloc] init];
    self.modalPresentationStyle = UIModalPresentationCustom;
    self.transitioningDelegate = self;
}

#pragma mark - View Life Cycle
- (void)viewDidLoad {
    [super viewDidLoad];
    
//    self.view.frame = _frame;
//    self.scrollView.backgroundColor = [UIColor clearColor];
    self.automaticallyAdjustsScrollViewInsets = NO;
    
    self.scrollView.contentSize = CGSizeMake(UIKeyWindowWidth(),
                                             CGRectGetMaxY(_contentNode.frame) +
                                             kBlurToolbarHeight +
                                             kPostImageNodePadding);
    
    [self.scrollView addSubnode:_imageNode];
    [self.scrollView addSubnode:_contentNode];

    self.titleLabel.tintColor = [UIColor lightGrayColor];
    self.titleLabel.text = [_post.userName lowercaseString];
    
    self.blurNavbar.layer.borderWidth = 0.5;
    self.blurNavbar.layer.borderColor = [UIColor lightGrayColor].CGColor;
    self.blurNavbar.hidden = YES;
    self.blurToolbar.layer.borderWidth = 0.5;
    self.blurToolbar.layer.borderColor = [UIColor grayColor].CGColor;
    self.blurToolbar.hidden = YES;
    
//    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(handleTap:)];
//    [self.view addGestureRecognizer:tap];
//    
//    UISwipeGestureRecognizer *swipe = [[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(handleSwipe:)];
//    swipe.direction = UISwipeGestureRecognizerDirectionDown | UISwipeGestureRecognizerDirectionUp;
//    swipe.delegate = self;
//    [self.view addGestureRecognizer:swipe];
    
    VBFPopFlatButton *closeButton = [[VBFPopFlatButton alloc] initWithFrame:(CGRect) {
        .origin.x = 10,
        .origin.y = (kBlurNavbarHeight - kVBFButtonSize.height) / 2,
        .size = kVBFButtonSize,
    }
                                                                 buttonType:buttonCloseType
                                                                buttonStyle:buttonPlainStyle
                                                      animateToInitialState:NO];
    closeButton.lineThickness = 1.0;
    closeButton.tintColor = [UIColor customPink];
    [closeButton addTarget:self action:@selector(dismiss) forControlEvents:UIControlEventTouchUpInside];
    _closeButton = closeButton;
    [self.blurNavbar addSubview:closeButton];
    
    VBFPopFlatButton *actionButton = [[VBFPopFlatButton alloc] initWithFrame:(CGRect) {
        .origin.x = UIKeyWindowWidth() - kVBFButtonSize.width - 10,
        .origin.y = (kBlurNavbarHeight - kVBFButtonSize.height) / 2,
        .size = kVBFButtonSize,
    }
                                                                  buttonType:buttonDownBasicType
                                                                 buttonStyle:buttonPlainStyle
                                                       animateToInitialState:NO];
    actionButton.lineThickness = 1.0;
    actionButton.tintColor = [UIColor customPink];
    [actionButton addTarget:self action:@selector(showAction) forControlEvents:UIControlEventTouchUpInside];
    [self.blurNavbar addSubview:actionButton];
    
//    [self.commentButton addTarget:self action:@selector(showComments) forControlEvents:UIControlEventTouchUpInside];
//    [self.likeButton addTarget:self action:@selector(doLike) forControlEvents:UIControlEventTouchUpInside];
//    [self.dislikeButton addTarget:self action:@selector(doDislike) forControlEvents:UIControlEventTouchUpInside];
    
    self.likeLabel.text = [Formatter formatNumber:self.post.likes];
    self.dislikeLabel.text = [Formatter formatNumber:self.post.dislikes];
    self.commentLabel.text = [Formatter formatNumber:self.post.comments];
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    
    if ([self.post isLiked]) {
        self.likeLabel.textColor = [UIColor customPink];
        self.likeButton.tintColor = [UIColor customPink];
    }
    
    if ([self.post isDisliked]) {
        self.dislikeLabel.textColor = [UIColor customPink];
        self.dislikeButton.tintColor = [UIColor customPink];
    }
    
    [self performSelector:@selector(showBars) withObject:nil afterDelay:0.1];
}

- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
//    [self showBars];
}

- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (BOOL)prefersStatusBarHidden {
    return YES;
}

#pragma mark - Setup View
- (void)setupPost:(VOPost *)post cachedImage:(UIImage *)image frame:(CGRect)frame; {
    _post = post;
    self.expand_startFrame = frame;
    
    CGSize imageSize;
    CGFloat insets;
    
    if (post.postImage) {
        ASImageNode *imageNode = [[ASImageNode alloc] init];
        
        imageNode.image = image;
        imageNode.contentMode = UIViewContentModeScaleAspectFill;
//        imageNode.layerBacked = YES;
        [imageNode measure:CGSizeMake(frame.size.width - kPostImageNodePadding,
                                       frame.size.width * kPostImageNodeHeightScale - kPostImageNodePadding)];
        _imageNode.userInteractionEnabled = YES;
        [imageNode addTarget:self action:@selector(openImage) forControlEvents:ASControlNodeEventTouchUpInside];
        imageNode.frame = (CGRect) {
            .origin.x = 0.5 * kPostImageNodePadding,
            .origin.y = 0.5 * kPostImageNodePadding,
            .size = imageNode.calculatedSize,
        };
        
        imageSize = imageNode.frame.size;
        insets = 1.5;
        
        self.imageNode = imageNode;
    }
    else {
        imageSize = CGSizeZero;
        insets = 1.0;
    }
    
    ASTextNode *contentNode = [[ASTextNode alloc] init];
    contentNode.attributedString = [[NSAttributedString alloc] initWithString:post.content
                                                                    attributes:[TextStyles contentStyle]];
    [contentNode measure:CGSizeMake(frame.size.width, FLT_MAX)];
    contentNode.frame = (CGRect) {
        .size = contentNode.calculatedSize,
        .origin.x = kPostImageNodePadding,
        .origin.y = imageSize.height + insets * kPostImageNodePadding,
    };
    
    self.contentNode = contentNode;
    
//    _imageCenter = _imageNode.layer.center;
//    _contentCenter = _contentNode.view.center;
}

#pragma mark - Navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    if ([segue.identifier isEqualToString:kCommentsSegue]) {
        UIViewController *vc = segue.destinationViewController;
        UIView *view = (UIView *)sender;
        CGRect frame = [view.superview convertRect:view.frame toView:nil];
        vc.popover_startFrame = frame;
    }
}

#pragma mark - Actions
- (void)openImage {
    NSURL *url = [NSURL URLWithString:self.post.postImage];
    IDMPhotoBrowser *browser = [[IDMPhotoBrowser alloc] initWithPhotoURLs:@[url]];
    browser.usePopAnimation = YES;
    browser.forceHideStatusBar = YES;
    browser.displayDoneButton = NO;
    browser.trackTintColor = [UIColor whiteColor];
    [browser setInitialPageIndex:0];
    
    [[UIToolbar appearance] setTintColor:[UIColor whiteColor]];
    
    [self presentViewController:browser animated:YES completion:NULL];
}

- (IBAction)doOpenComments:(UIButton *)sender {
    [self performSegueWithIdentifier:kCommentsSegue sender:sender];
}

- (IBAction)doDislike:(UIButton *)sender {
    if ([self.post isLiked]) {
        self.post.liked = NO;
        self.likeLabel.text = [Formatter formatNumber:--self.post.likes];
        self.likeLabel.textColor = [UIColor lightGrayColor];
        self.likeButton.tintColor = [UIColor lightGrayColor];
    }
    
    if ([self.post isDisliked]) {
        self.post.disliked = NO;
        self.dislikeLabel.text = [Formatter formatNumber:--self.post.dislikes];
        self.dislikeLabel.textColor = [UIColor lightGrayColor];
        self.dislikeButton.tintColor = [UIColor lightGrayColor];
    }
    else {
        self.post.disliked = YES;
        self.dislikeLabel.text = [Formatter formatNumber:++self.post.dislikes];
        self.dislikeLabel.textColor = [UIColor customPink];
        self.dislikeButton.tintColor = [UIColor customPink];
    }
    
    /*
     [[APIService sharedInstance] likePost:self.post
     success:^{
     
     }
     failure:^(NSError *error) {
     if (error.code == 33) {
     DDLogInfo(@"post already disliked");
     #warning CORDEIRO: ao inves de jogar erro, carrega do primeiro get se ja foi curtido?
     [GoogleWearAlert showAlertMessage:@"dislike"
     image:nil
     type:Success
     duration:2.5
     viewController:self
     postion:Bottom
     canBeDismissedByUser:YES];
     }
     else {
     DDLogWarn(@"%@", error);
     [self.dislikeButton setTitle:[NSString stringWithFormat:@"%ld", _post.likes]
     forState:UIControlStateNormal];
     }
     }];
     */
}

- (IBAction)doLike:(UIButton *)sender {
    if ([self.post isDisliked]) {
        self.post.disliked = NO;
        self.dislikeLabel.text = [Formatter formatNumber:--self.post.dislikes];
        self.dislikeLabel.textColor = [UIColor lightGrayColor];
        self.dislikeButton.tintColor = [UIColor lightGrayColor];
    }
    
    if ([self.post isLiked]) {
        self.post.liked = NO;
        self.likeLabel.text = [Formatter formatNumber:--self.post.likes];
        self.likeLabel.textColor = [UIColor lightGrayColor];
        self.likeButton.tintColor = [UIColor lightGrayColor];
    }
    else {
        self.post.liked = YES;
        self.likeLabel.text = [Formatter formatNumber:++self.post.likes];
        self.likeLabel.textColor = [UIColor customPink];
        self.likeButton.tintColor = [UIColor customPink];
    }
    
    /*
     [[APIService sharedInstance] likePost:self.post
     success:^{
     //                                      _post.likes++;
     }
     failure:^(NSError *error) {
     if (error.code == 33) {
     DDLogInfo(@"post already liked");
     #warning CORDEIRO: ao inves de jogar erro, carrega do primeiro get se ja foi curtido?
     [GoogleWearAlert showAlertMessage:@"Like"
     image:nil
     type:Success
     duration:2.5
     viewController:self
     postion:Bottom
     canBeDismissedByUser:YES];
     }
     else {
     DDLogWarn(@"%@", error);
     [self.likeButton setTitle:[NSString stringWithFormat:@"%ld", _post.likes]
     forState:UIControlStateNormal];
     }
     }];
     */
}

#pragma mark - Navbar Button Actions
- (void)dismiss {
    [self dismissViewControllerAnimated:YES completion:NULL];
}

- (void)showAction {
    //TODO: show action
}

#pragma mark - Helper Methods
- (void)showBars {
    self.blurNavbar.center = CGPointMake(UIKeyWindow().center.x, -kBlurNavbarHeight/2);
    self.blurToolbar.center = CGPointMake(UIKeyWindow().center.x, UIKeyWindowHeight() + kBlurToolbarHeight/2);
    self.blurNavbar.hidden = NO;
    self.blurToolbar.hidden = NO;
    
    [UIView animateWithDuration:0.3
                     animations:^{
                         self.blurNavbar.center = CGPointMake(UIKeyWindow().center.x, kBlurNavbarHeight/2);
                         self.blurToolbar.center = CGPointMake(UIKeyWindow().center.x, UIKeyWindowHeight() - kBlurToolbarHeight/2);
                     }
                     completion:^(BOOL finished){
                         if (!self.post.postImage)
                             self.scrollView.contentInset = UIEdgeInsetsMake(kBlurNavbarHeight, 0, 0, 0);
                     }];
    
//    [UIView animateWithDuration:0.3
//                     animations:^{
//                         [self.scrollView setContentInset:UIEdgeInsetsMake(kBlurNavbarHeight, 0, kBlurToolbarHeight, 0)];
//                     }];

}

#pragma mark - Popover Protocol
- (UIPresentationController *)presentationControllerForPresentedViewController:(UIViewController *)presented presentingViewController:(UIViewController *)presenting sourceViewController:(UIViewController *)source {
    ExpandCellPresentationController *controller = [[ExpandCellPresentationController alloc] initWithPresentedViewController:presented
                                                                                              presentingViewController:presenting];
    return controller;
}

- (id <UIViewControllerAnimatedTransitioning>)animationControllerForPresentedController:(UIViewController *)presented
                                                                   presentingController:(UIViewController *)presenting
                                                                       sourceController:(UIViewController *)source {
    return self.animator;
}

- (id <UIViewControllerAnimatedTransitioning>)animationControllerForDismissedController:(UIViewController *)dismissed {
    return self.animator;
}

@end
