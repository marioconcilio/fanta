//
//  PostCommentCell.m
//  Fanta
//
//  Created by Mario Concilio on 07/04/15.
//  Copyright (c) 2015 Mario Concilio. All rights reserved.
//

#import "Constants.h"
#import "PostCommentCell.h"
#import "VOComment.h"
#import "VOUser.h"
#import "Formatter.h"
#import "TextStyles.h"

#import <AsyncDisplayKit.h>
#import <AsyncDisplayKit/ASDisplayNode+Subclasses.h>
#import <NSDate+DateTools.h>
#import <WebASDKImageManager.h>
#import <POP.h>

@interface PostCommentCell () <ASNetworkImageNodeDelegate>

@property (nonatomic, weak) ASNetworkImageNode *profileImageNode;
@property (nonatomic, weak) ASTextNode *commentTextNode;
@property (nonatomic, weak) ASTextNode *profileNameNode;
@property (nonatomic, weak) ASTextNode *dateNode;
@property (nonatomic, weak) ASImageNode *thumbsUpImageNode;
@property (nonatomic, weak) ASImageNode *thumbsDownImageNode;
@property (nonatomic, weak) ASTextNode *thumbsUpTextNode;
@property (nonatomic, weak) ASTextNode *thumbsDownTextNode;
@property (nonatomic, strong) VOComment *comment;

@end

@implementation PostCommentCell

- (instancetype)initWithComment:(VOComment *)comment {
    self = [super init];
    if (self) {
        _comment = comment;
        
        /*
         *  Profile Image
         */
        ASNetworkImageNode *profileImageNode = [[ASNetworkImageNode alloc] initWithWebImage];
        profileImageNode.URL = [NSURL URLWithString:comment.user.thumbnail];
        profileImageNode.preferredFrameSize = kCommentCellThumbSize;
        profileImageNode.cornerRadius = kCommentCellThumbWidth / 2;
        profileImageNode.clipsToBounds = YES;
        profileImageNode.borderWidth = 0.5;
        profileImageNode.borderColor = UIColorFromRGB(151, 151, 151).CGColor;
        profileImageNode.userInteractionEnabled = YES;
        profileImageNode.delegate = self;
        _profileImageNode = profileImageNode;
        [self addSubnode:_profileImageNode];
        
        /*
         *  Profile Name
         */
        ASTextNode *profileNameNode = [[ASTextNode alloc] init];
        profileNameNode.attributedString = [[NSAttributedString alloc] initWithString:comment.userName
                                                                           attributes:[TextStyles commentProfileNameStyle]];
        profileNameNode.layerBacked = YES;
        _profileNameNode = profileNameNode;
        [self addSubnode:_profileNameNode];
        
        /*
         *  Date
         */
        ASTextNode *dateNode = [[ASTextNode alloc] init];
        dateNode.attributedString = [[NSAttributedString alloc] initWithString:comment.createdAt.timeAgoSinceNow
                                                                    attributes:[TextStyles commentDateStyle]];
        dateNode.maximumNumberOfLines = 1;
        dateNode.layerBacked = YES;
        _dateNode = dateNode;
        [self addSubnode:_dateNode];
        
        /*
         *  Comment
         */
        ASTextNode *commentTextNode = [[ASTextNode alloc] init];
        commentTextNode.attributedString = [[NSAttributedString alloc] initWithString:comment.content
                                                                           attributes:[TextStyles commentContentStyle]];
        commentTextNode.layerBacked = YES;
        _commentTextNode = commentTextNode;
        [self addSubnode:_commentTextNode];
        
        /*
         *  Thumbs Up
         */
        ASImageNode *thumbsUpImageNode = [[ASImageNode alloc] init];
        thumbsUpImageNode.image = [UIImage imageNamed:@"fnt_thumbs_up"];
        thumbsUpImageNode.preferredFrameSize = kCommentImageButtonSize;
        thumbsUpImageNode.userInteractionEnabled = YES;
        thumbsUpImageNode.hitTestSlop = UIEdgeInsetsMake(-50, -50, 0, 0);
        [thumbsUpImageNode addTarget:self action:@selector(doLike) forControlEvents:ASControlNodeEventTouchUpInside];
//        thumbsUpImageNode.imageModificationBlock = ASImageNodeTintColorModificationBlock(UIColorFromRGB(178, 178, 178));
        _thumbsUpImageNode = thumbsUpImageNode;
        [self addSubnode:_thumbsUpImageNode];
        
        ASTextNode *thumbsUpTextNode = [[ASTextNode alloc] init];
        thumbsUpTextNode.layerBacked = YES;
        thumbsUpTextNode.flexGrow = YES;
        _thumbsUpTextNode = thumbsUpTextNode;
        [self addSubnode:_thumbsUpTextNode];
        
        if ([comment isLiked]) {
            thumbsUpImageNode.imageModificationBlock = ASImageNodeTintColorModificationBlock([UIColor customPink]);
            thumbsUpTextNode.attributedString = [[NSAttributedString alloc] initWithString:[Formatter formatNumber:comment.likes]
                                                                                attributes:[TextStyles commentButtonSelectedStyle]];
        }
        else {
            thumbsUpImageNode.imageModificationBlock = ASImageNodeTintColorModificationBlock(UIColorFromRGB(178, 178, 178));
            thumbsUpTextNode.attributedString = [[NSAttributedString alloc] initWithString:[Formatter formatNumber:comment.likes]
                                                                                attributes:[TextStyles commentButtonStyle]];
        }
        
        /*
         *  Thumbs Down
         */
        ASImageNode *thumbsDownImageNode = [[ASImageNode alloc] init];
        thumbsDownImageNode.image = [UIImage imageNamed:@"fnt_thumbs_down"];
        thumbsDownImageNode.preferredFrameSize = kCommentImageButtonSize;
        thumbsDownImageNode.userInteractionEnabled = YES;
        thumbsDownImageNode.hitTestSlop = UIEdgeInsetsMake(-50, 0, 0, -50);
        [thumbsDownImageNode addTarget:self action:@selector(doDislike) forControlEvents:ASControlNodeEventTouchUpInside];
//        thumbsDownImageNode.imageModificationBlock = ASImageNodeTintColorModificationBlock(UIColorFromRGB(178, 178, 178));
        _thumbsDownImageNode = thumbsDownImageNode;
        [self addSubnode:_thumbsDownImageNode];
        
        ASTextNode *thumbsDownTextNode = [[ASTextNode alloc] init];
        thumbsDownTextNode.layerBacked = YES;
        thumbsDownTextNode.flexGrow = YES;
        _thumbsDownTextNode = thumbsDownTextNode;
        [self addSubnode:_thumbsDownTextNode];
        
        if ([comment isDisliked]) {
            thumbsDownImageNode.imageModificationBlock = ASImageNodeTintColorModificationBlock([UIColor customPink]);
            thumbsDownTextNode.attributedString = [[NSAttributedString alloc] initWithString:[Formatter formatNumber:comment.dislikes]
                                                                                  attributes:[TextStyles commentButtonSelectedStyle]];
        }
        else {
            thumbsDownImageNode.imageModificationBlock = ASImageNodeTintColorModificationBlock(UIColorFromRGB(178, 178, 178));
            thumbsDownTextNode.attributedString = [[NSAttributedString alloc] initWithString:[Formatter formatNumber:comment.dislikes]
                                                                                  attributes:[TextStyles commentButtonStyle]];
        }
        
        self.selectionStyle = UITableViewCellSelectionStyleNone;
    }
    
    return self;
}

- (ASLayoutSpec *)layoutSpecThatFits:(ASSizeRange)constrainedSize {
    // vertical stack for name and date
    ASStackLayoutSpec *titleStack = [ASStackLayoutSpec stackLayoutSpecWithDirection:ASStackLayoutDirectionVertical
                                                                            spacing:8.0
                                                                     justifyContent:ASStackLayoutJustifyContentStart
                                                                         alignItems:ASStackLayoutAlignItemsStart
                                                                           children:@[_profileNameNode, _dateNode]];
    titleStack.alignSelf = ASStackLayoutAlignSelfStretch;
    titleStack.flexGrow = NO;
    
    // horizontal stack including thumbnail
    ASStackLayoutSpec *topStack = [ASStackLayoutSpec stackLayoutSpecWithDirection:ASStackLayoutDirectionHorizontal
                                                                          spacing:8.0
                                                                   justifyContent:ASStackLayoutJustifyContentStart
                                                                       alignItems:ASStackLayoutAlignItemsCenter
                                                                         children:@[_profileImageNode, titleStack]];
    topStack.alignSelf = ASStackLayoutAlignSelfStretch;
    topStack.flexGrow = YES;
    
    ASLayoutSpec *spacer = [[ASLayoutSpec alloc] init];
    spacer.flexGrow = YES;
    
    // horizontal stack for bottom buttons
    ASStackLayoutSpec *bottomStack = [ASStackLayoutSpec stackLayoutSpecWithDirection:ASStackLayoutDirectionHorizontal
                                                                             spacing:5.0
                                                                      justifyContent:ASStackLayoutJustifyContentEnd
                                                                          alignItems:ASStackLayoutAlignItemsCenter
                                                                            children:@[_thumbsUpTextNode, _thumbsUpImageNode, _thumbsDownImageNode, _thumbsDownTextNode]];
    bottomStack.alignSelf = ASStackLayoutAlignSelfEnd;
    bottomStack.flexGrow = YES;
    
    // vertical stack including content
    ASStackLayoutSpec *contentStack = [ASStackLayoutSpec stackLayoutSpecWithDirection:ASStackLayoutDirectionVertical
                                                                            spacing:8.0
                                                                     justifyContent:ASStackLayoutJustifyContentStart
                                                                         alignItems:ASStackLayoutAlignItemsCenter
                                                                           children:@[topStack, _commentTextNode, bottomStack]];
    contentStack.alignSelf = ASStackLayoutAlignSelfStretch;
    contentStack.flexGrow = YES;
    
    return [ASInsetLayoutSpec insetLayoutSpecWithInsets:UIEdgeInsetsMake(16, 16, 16, 16) child:contentStack];
}

#pragma mark - Actions
- (void)doLike {
    if ([self.comment isDisliked]) {
        self.comment.disliked = NO;
        _thumbsDownTextNode.attributedString = [[NSAttributedString alloc] initWithString:[Formatter formatNumber:--self.comment.dislikes]
                                                                               attributes:[TextStyles commentButtonStyle]];
        _thumbsDownImageNode.imageModificationBlock = ASImageNodeTintColorModificationBlock(UIColorFromRGB(178, 178, 178));
        [_thumbsDownImageNode setNeedsDisplayWithCompletion:NULL];
        [_thumbsDownTextNode setNeedsLayout];
    }
    
    if ([self.comment isLiked]) {
        self.comment.liked = NO;
        _thumbsUpTextNode.attributedString = [[NSAttributedString alloc] initWithString:[Formatter formatNumber:--self.comment.likes]
                                                                             attributes:[TextStyles commentButtonStyle]];
        _thumbsUpImageNode.imageModificationBlock = ASImageNodeTintColorModificationBlock(UIColorFromRGB(178, 178, 178));
    }
    else {
        self.comment.liked = YES;
        _thumbsUpTextNode.attributedString = [[NSAttributedString alloc] initWithString:[Formatter formatNumber:++self.comment.likes]
                                                                             attributes:[TextStyles commentButtonSelectedStyle]];
        _thumbsUpImageNode.imageModificationBlock = ASImageNodeTintColorModificationBlock([UIColor customPink]);
    }
    
    [_thumbsUpImageNode setNeedsDisplayWithCompletion:NULL];
    [_thumbsUpTextNode invalidateCalculatedLayout];
    [_thumbsUpTextNode setNeedsLayout];
}

- (void)doDislike {
    if ([self.comment isLiked]) {
        self.comment.liked = NO;
        _thumbsUpTextNode.attributedString = [[NSAttributedString alloc] initWithString:[Formatter formatNumber:--self.comment.likes]
                                                                             attributes:[TextStyles commentButtonStyle]];
        _thumbsUpImageNode.imageModificationBlock = ASImageNodeTintColorModificationBlock(UIColorFromRGB(178, 178, 178));
        [_thumbsUpImageNode setNeedsDisplayWithCompletion:NULL];
        [_thumbsUpTextNode setNeedsLayout];
    }
    
    if ([self.comment isDisliked]) {
        self.comment.disliked = NO;
        _thumbsDownTextNode.attributedString = [[NSAttributedString alloc] initWithString:[Formatter formatNumber:--self.comment.dislikes]
                                                                               attributes:[TextStyles commentButtonStyle]];
        _thumbsDownImageNode.imageModificationBlock = ASImageNodeTintColorModificationBlock(UIColorFromRGB(178, 178, 178));
    }
    else {
        self.comment.disliked = YES;
        _thumbsDownTextNode.attributedString = [[NSAttributedString alloc] initWithString:[Formatter formatNumber:++self.comment.dislikes]
                                                                               attributes:[TextStyles commentButtonSelectedStyle]];
        _thumbsDownImageNode.imageModificationBlock = ASImageNodeTintColorModificationBlock([UIColor customPink]);
    }
    
    [_thumbsDownImageNode setNeedsDisplayWithCompletion:NULL];
//    [_thumbsDownTextNode invalidateCalculatedLayout];
    [_thumbsDownTextNode setNeedsLayout];
}

#pragma mark - ASNetworkImage Delegate
- (void)imageNode:(ASNetworkImageNode *)imageNode didLoadImage:(UIImage *)image {
    if (image) {
        POPBasicAnimation *anim = [POPBasicAnimation animationWithPropertyNamed:kPOPViewAlpha];
        anim.fromValue = @(0.0);
        anim.toValue = @(1.0);
        [imageNode pop_removeAllAnimations];
        [imageNode pop_addAnimation:anim forKey:@"fadeIn"];
    }
}

@end
