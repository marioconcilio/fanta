//
//  PostCommentCell.h
//  Fanta
//
//  Created by Mario Concilio on 07/04/15.
//  Copyright (c) 2015 Mario Concilio. All rights reserved.
//

#import "ASCellNode.h"

@class VOComment, ASImageNode, ASTextNode, ASNetworkImageNode;
@interface PostCommentCell : ASCellNode

@property (nonatomic, weak, readonly) ASNetworkImageNode *profileImageNode;
@property (nonatomic, weak, readonly) ASTextNode *commentTextNode;
@property (nonatomic, weak, readonly) ASTextNode *profileNameNode;
@property (nonatomic, weak, readonly) ASTextNode *dateNode;
@property (nonatomic, weak, readonly) ASImageNode *thumbsUpImageNode;
@property (nonatomic, weak, readonly) ASImageNode *thumbsDownImageNode;
@property (nonatomic, weak, readonly) ASTextNode *thumbsUpTextNode;
@property (nonatomic, weak, readonly) ASTextNode *thumbsDownTextNode;

- (instancetype)initWithComment:(VOComment *)comment;

@end
