//
//  PostCommentsViewController.m
//  Fanta
//
//  Created by Mario Concilio on 07/04/15.
//  Copyright (c) 2015 Mario Concilio. All rights reserved.
//

#import "APIService.h"
#import "Constants.h"
#import "Helper.h"
#import "UserService.h"
#import "KeyboardController.h"
#import "PostCommentCell.h"
#import "CommentsViewController.h"
#import "VOPost.h"
#import "ProfileViewController.h"
#import "PopoverTransition.h"
#import "UIStoryboard+Storyboards.h"
#import "Generator.h"

#import <AsyncDisplayKit.h>
#import <NSDate+DateTools.h>
#import <PHFComposeBarView.h>
#import <POP.h>
#import <VBFPopFlatButton.h>
#import <TSMessage.h>

#warning DEBUG ONLY
#import "VOUser.h"
#import "VOComment.h"

@interface CommentsViewController () <ASTableViewDataSource, ASTableViewDelegate, PHFComposeBarViewDelegate, KeyboardDelegate, PopoverProtocol> {
    ASTableView *_tableView;
}

@property (nonatomic, assign, getter=isKeyboardHidden) BOOL keyboardHidden;
@property (nonatomic, strong) KeyboardController *keyboardController;
@property (nonatomic, strong) NSMutableArray *commentsDataSource;
@property (nonatomic, strong) PHFComposeBarView *composeBarView;
//@property (nonatomic, strong) PanAnimationController *animationController;
@property (nonatomic, weak) VBFPopFlatButton *doneButton;
@property (nonatomic, weak) UIRefreshControl *refresher;

@end

@implementation CommentsViewController

//CGPoint _startPoint;

//+ (void)presentInViewController:(UIViewController *)viewController withPost:(VOPost *)post fromPoint:(CGPoint)point {
//    PostCommentsViewController *vc = [[PostCommentsViewController alloc] initWithPost:post fromPoint:point];
//    _startPoint = point;
//    [UIKeyWindow addSubview:vc.view];
//    [viewController addChildViewController:vc];
//}

- (instancetype)initWithCoder:(NSCoder *)aDecoder {
    self = [super initWithCoder:aDecoder];
    if (self) {
        _tableView = [[ASTableView alloc] initWithFrame:CGRectZero style:UITableViewStylePlain asyncDataFetching:YES];
        _tableView.asyncDataSource = self;
        _tableView.asyncDelegate = self;
        self.modalPresentationStyle = UIModalPresentationCustom;
        self.transitioningDelegate = self;
    }
    
    return self;
}

//- (instancetype)initWithPost:(VOPost *)post fromPoint:(CGPoint)point {
//    self = [super init];
//    if (self) {
//        _tableView = [[ASTableView alloc] initWithFrame:CGRectZero style:UITableViewStylePlain asyncDataFetching:YES];
//        _tableView.asyncDataSource = self;
//        _tableView.asyncDelegate = self;
//        _post = post;
//        _startPoint = point;
//    }
//    
//    return self;
//}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self.view addSubview:_tableView];
    _tableView.tableFooterView = [[UIView alloc] initWithFrame:CGRectZero];
    
    // should calculate the final frame so subnodes can measure themselves
//    self.view.frame = CGRectInset(UIKeyWindow.bounds, kCommentTablePadding, kCommentTableHeaderHeight);
    self.popover_endFrame = CGRectInset(UIKeyWindow().bounds, kCommentTablePadding, kCommentTableHeaderHeight);
    self.view.layer.cornerRadius = 5.0;
    self.view.layer.masksToBounds = YES;
    
    /*
     * Keyboard Controller
     */
    self.keyboardController = [[KeyboardController alloc] initWithView:self.view andFrame:self.popover_endFrame];
    self.keyboardController.delegate = self;
    
    [self setupTableHeader];
    [self setupTableFooter];
    
    /*
     *  refresher
     */
    UIRefreshControl *refresher = [[UIRefreshControl alloc] init];
    refresher.tintColor = [UIColor customPink];
    [refresher addTarget:self action:@selector(refreshData) forControlEvents:UIControlEventValueChanged];
    _refresher = refresher;
    [_tableView addSubview:_refresher];
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    [self.keyboardController registerForKeyboardNotifications];
    
    [self.refresher beginRefreshing];
    [self refreshData];
    
    _tableView.separatorStyle = UITableViewCellSeparatorStyleSingleLine;
    
    /*
    __weak typeof(self) weakSelf = self;
    [[APIService sharedInstance] getCommentsFromPost:self.post
                                             success:^(NSMutableArray *array) {
                                                 if (array.count == 0) {
                                                     [self showEmptyLabel];
                                                 }
                                                 else {
                                                     _tableView.separatorStyle = UITableViewCellSeparatorStyleSingleLine;
                                                 }
                                                 
                                                 [weakSelf.indicator stopAnimation:NO completion:NULL];
                                                 [weakSelf.indicator removeFromSuperview];
                                                 weakSelf.indicator = nil;
                                                 
                                                 _commentsDataSource = array;
                                                 [_tableView reloadData];
                                             }
                                             failure:^(NSError *error) {
                                                 
                                             }];
     */
}

- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    self.keyboardHidden = YES;
//    [self.doneButton animateToType:buttonOkType];
    
//    _tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
//    [self.animationController show];
}

- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
    [self.keyboardController deregisterFromKeyboardNotifications];
}

- (void)viewWillLayoutSubviews {
    [super viewWillLayoutSubviews];
    _tableView.frame = self.view.bounds;
}

#pragma mark - Helper Methods
- (void)refreshData {
    __weak typeof(self) weakSelf = self;
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(2.0 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        _commentsDataSource = [[NSMutableArray alloc] initWithArray:[weakSelf populateComments]];
        [_tableView reloadData];
        [weakSelf.refresher endRefreshing];
    });
}

- (void)showEmptyLabel {
    UILabel *label = [[UILabel alloc] initWithFrame:CGRectMake(0, 0,
                                                               self.view.bounds.size.width,
                                                               self.view.bounds.size.height)];
    label.text = @"Seja o primeiro a comentar";
    label.textColor = [UIColor blackColor];
    label.numberOfLines = 0;
    label.textAlignment = NSTextAlignmentCenter;
    label.font = [UIFont lightFontWithSize:18.0];
    [label sizeToFit];
    
    _tableView.backgroundView = label;
    
    POPBasicAnimation *anim = [POPBasicAnimation animationWithPropertyNamed:kPOPViewAlpha];
    anim.fromValue = @(0.0);
    anim.toValue = @(1.0);
    [label pop_addAnimation:anim forKey:@"anim_alpha"]; 
}

- (NSArray *)populateComments {
    NSMutableArray *array = [NSMutableArray array];
    
    for (NSUInteger i = 0; i < 20; i++) {
        @autoreleasepool {
            VOUser *user = [[VOUser alloc] init];
            user.name = @"Kyle Carpenter";
            //            user.thumbnail = [NSString stringWithFormat:@"10%02ld", i];
            user.thumbnail = [Generator randomPerson];
            
            VOComment *comment = [[VOComment alloc] init];
            comment.user = user;
            comment.userName = user.name;
            //            comment.userName = @"Kyle Carpenter";
            comment.content = @"Mussum ipsum cacilds, vidis litro abertis. Consetis adipiscings elitis. Pra lá , depois divoltis porris, paradis. Paisis, filhis, espiritis santis. Mé faiz elementum girarzis, nisi eros vermeio, in elementis mé pra quem é amistosis quis leo. Manduma pindureta quium dia nois paga.";
            comment.createdAt = [NSDate dateWithYear:2015 month:8 day:i+1];
            comment.likes = i * 20;
            comment.dislikes = i * 3;
            //            comment.comment = @"asdar";
            
            [array addObject:comment];
        }
    }
    
    return array;
}

- (void)addComment:(NSString *)content {
    VOUser *user = [UserService loadUser];
    VOComment *comment = [[VOComment alloc] init];
    comment.userName = user.name;
    comment.likes = 0;
    comment.dislikes = 0;
    comment.content = content;
    comment.createdAt = [NSDate date];
    
    [self.commentsDataSource addObject:comment];
    NSIndexPath *indexPath = [NSIndexPath indexPathForRow:_commentsDataSource.count-1
                                                inSection:0];
    
    [_tableView insertRowsAtIndexPaths:@[indexPath]
                      withRowAnimation:UITableViewRowAnimationFade];
    
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.3 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        [_tableView scrollToRowAtIndexPath:indexPath
                          atScrollPosition:UITableViewScrollPositionBottom
                                  animated:YES];
    });
    
    /*
#warning CORDEIRO: servidor deve devolver comment criado? Ou crio localmente
    [[APIService sharedInstance] createComment:content
                                      fromPost:self.post
                                       success:^{
                                           VOUser *user = [Helper loadUser];
                                           VOComment *comment = [[VOComment alloc] init];
                                           comment.userName = user.name;
                                           comment.likes = 0;
                                           comment.dislikes = 0;
                                           comment.content = content;
                                           comment.createdAt = [NSDate date];
                                           
                                           [self.commentsDataSource addObject:comment];
                                           NSIndexPath *indexPath = [NSIndexPath indexPathForRow:_commentsDataSource.count-1
                                                                                       inSection:0];
                                           
                                           [_tableView insertRowsAtIndexPaths:@[indexPath]
                                                             withRowAnimation:UITableViewRowAnimationFade];
                                           
//                                           [_tableView scrollToRowAtIndexPath:indexPath
//                                                             atScrollPosition:UITableViewScrollPositionBottom
//                                                                     animated:YES];
                                       }
                                       failure:^(NSError *error) {
                                           DDLogWarn(@"%@", error);
                                       }];
     */
}

/*
- (void)dismissAnimation {
    [self.bkgView removeFromSuperview];
    
    POPBasicAnimation *positionAnimation = [POPBasicAnimation animationWithPropertyNamed:kPOPViewCenter];
    positionAnimation.toValue = [NSValue valueWithCGPoint:scaledCenter];
    [self.view pop_addAnimation:positionAnimation forKey:@"positionAnimation"];
    
    POPBasicAnimation *scaleAnimation = [POPBasicAnimation animationWithPropertyNamed:kPOPViewScaleXY];
    scaleAnimation.toValue = [NSValue valueWithCGPoint:CGPointMake(kMinScale, kMinScale)];
    scaleAnimation.completionBlock = ^(POPAnimation *anim, BOOL finished) {
        [self.delegateViewController postCommentsDidDismiss:self];
    };
    [self.view pop_addAnimation:scaleAnimation forKey:@"scaleAnimation"];
}

- (void)showAnimation {
    POPSpringAnimation *springAnimation = [POPSpringAnimation animationWithPropertyNamed:kPOPViewScaleXY];
    springAnimation.springBounciness = 6.0;
    springAnimation.springSpeed = 6.0;
    springAnimation.toValue = [NSValue valueWithCGPoint:CGPointMake(1.0, 1.0)];
    springAnimation.completionBlock = ^(POPAnimation *anim, BOOL finished) {
        [self.doneButton animateToType:buttonOkType];
    };
    [self.view pop_addAnimation:springAnimation forKey:@"springAnimation"];
    
    CGFloat width = CGRectGetWidth(UIKeyWindow.bounds);
    CGFloat height = CGRectGetHeight(UIKeyWindow.bounds);
    POPSpringAnimation *positionAnimation = [POPSpringAnimation animationWithPropertyNamed:kPOPViewCenter];
    positionAnimation.springBounciness = 6.0;
    positionAnimation.springSpeed = 6.0;
    positionAnimation.toValue = [NSValue valueWithCGPoint:CGPointMake(width/2, height/2)];
    [self.view pop_addAnimation:positionAnimation forKey:@"positionAnimation"];
}
 */

#pragma mark - Setup Methods
- (void)setupTableHeader {
    UIView *headerView = [[UIView alloc] init];
    headerView.backgroundColor = [UIColor colorWithWhite:1.0 alpha:0.9];
    headerView.frame = (CGRect) {
        .size.height = kCommentTableHeaderHeight,
        .size.width = CGRectGetWidth(self.view.bounds),
    };
    
    headerView.layer.shadowColor = [UIColor blackColor].CGColor;
    headerView.layer.shadowOffset = CGSizeMake(0.0, -1.0);
    headerView.layer.shadowOpacity = 0.6;
    headerView.layer.shadowRadius = 4.0;
    headerView.layer.shadowPath = [UIBezierPath bezierPathWithRect:headerView.frame].CGPath;
    headerView.layer.masksToBounds = NO;
    
    UILabel *label = [[UILabel alloc] init];
    label.text = @"Comentários";
    label.font = [UIFont semiboldFontWithSize:16.0];
    label.textAlignment = NSTextAlignmentCenter;
    label.frame = headerView.frame;
    [headerView addSubview:label];
    
    CGFloat x = CGRectGetWidth(self.view.bounds) - kVBFButtonSize.width - 20;
    CGFloat y = (kCommentTableHeaderHeight - kVBFButtonSize.width) / 2;
    CGRect frame = (CGRect) {
        .origin.x = x,
        .origin.y = y,
        .size = kVBFButtonSize,
    };
    
    VBFPopFlatButton *doneButton = [[VBFPopFlatButton alloc] initWithFrame:frame
                                                                buttonType:buttonOkType
                                                               buttonStyle:buttonPlainStyle
                                                     animateToInitialState:NO];
    doneButton.lineThickness = 1.0;
    doneButton.tintColor = [UIColor customPink];
    [doneButton addTarget:self action:@selector(dismiss) forControlEvents:UIControlEventTouchUpInside];
    self.doneButton = doneButton;
    [headerView addSubview:doneButton];
    [self.view addSubview:headerView];
    
    [_tableView setContentInset:UIEdgeInsetsMake(kCommentTableHeaderHeight, 0.0, PHFComposeBarViewInitialHeight, 0.0)];
    [_tableView setScrollIndicatorInsets:UIEdgeInsetsMake(kCommentTableHeaderHeight, 0.0, 0.0, 0.0)];
}

- (void)setupTableFooter {
    CGFloat height = CGRectGetHeight(self.view.bounds);
    CGFloat width = CGRectGetWidth(self.view.bounds);
    self.composeBarView = [[PHFComposeBarView alloc] initWithFrame:CGRectMake(0,
                                                                              height - PHFComposeBarViewInitialHeight,
                                                                              width,
                                                                              PHFComposeBarViewInitialHeight)];
    self.composeBarView.maxCharCount = 0;
    self.composeBarView.textView.font = [UIFont lightFontWithSize:16.f];
//    composeBarView.textView.
//    self.composeBarView.textView.delegate = self;
//    composeBarView.buttonTintColor = [UIColor customBlue];
    self.composeBarView.buttonTitle = @"Comentar";
    self.composeBarView.delegate = self;
    self.composeBarView.button.titleLabel.font = [UIFont semiboldFontWithSize:16.f];
    self.composeBarView.button.tintColor = [UIColor customPink];
    
    [self.view addSubview:self.composeBarView];
}

#pragma mark - Actions
- (void)doLike:(ASImageNode *)sender {
    
}

- (void)doDislike:(ASImageNode *)sender {
    
}

- (void)openProfile:(ASImageNode *)sender {
    VOComment *comment = self.commentsDataSource[sender.view.tag];
    UINavigationController *nav = [[UIStoryboard profileStoryboard] instantiateInitialViewController];
    ProfileViewController *vc = nav.childViewControllers[0];
    vc.user = comment.user;
    vc.me = NO;
    
    [self presentViewController:nav animated:YES completion:NULL];
}

- (void)dismiss {
    [self dismissViewControllerAnimated:YES completion:NULL];
}

#pragma mark - PHFComposeBar View Delegate
- (void)composeBarViewDidPressButton:(PHFComposeBarView *)composeBarView {
    [composeBarView resignFirstResponder];
    [self addComment:composeBarView.text];
    [composeBarView setText:nil animated:YES];
}

- (void)composeBarView:(PHFComposeBarView *)composeBarView didChangeFromFrame:(CGRect)startFrame toFrame:(CGRect)endFrame {
    _tableView.contentInset = UIEdgeInsetsMake(kCommentTableHeaderHeight , 0, CGRectGetHeight(endFrame), 0);
}

#pragma mark - Table View Data Source
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.commentsDataSource.count;
}

- (ASCellNode *)tableView:(ASTableView *)tableView nodeForRowAtIndexPath:(NSIndexPath *)indexPath {
    VOComment *comment = [self.commentsDataSource objectAtIndex:indexPath.row];
    PostCommentCell *node = [[PostCommentCell alloc] initWithComment:comment];
    [node.thumbsUpImageNode addTarget:self action:@selector(doLike:) forControlEvents:ASControlNodeEventTouchUpInside];
    [node.thumbsDownImageNode addTarget:self action:@selector(doDislike:) forControlEvents:ASControlNodeEventTouchUpInside];
    [node.profileImageNode addTarget:self action:@selector(openProfile:) forControlEvents:ASControlNodeEventTouchUpInside];
    
    node.thumbsUpImageNode.view.tag = indexPath.row;
    node.thumbsDownImageNode.view.tag = indexPath.row;
    node.profileImageNode.view.tag = indexPath.row;
    
    return node;
}

#pragma mark - Keyboard Delegate
- (void)keyboardWillShow:(CGSize)keyboardSize {
    self.keyboardHidden = NO;
}

- (void)keyboardDidHide:(CGSize)keyboardSize {
    self.keyboardHidden = YES;
}

#pragma mark - PanAnimation Delegate
- (BOOL)animationShouldResize {
    return [self isKeyboardHidden];
}

- (void)animationDidShow {
    [self.doneButton animateToType:buttonOkType];
}

- (void)animationDidDismiss {
    [self.view removeFromSuperview];
    [self removeFromParentViewController];
}

#pragma mark - Popover Protocol
- (UIPresentationController *)presentationControllerForPresentedViewController:(UIViewController *)presented presentingViewController:(UIViewController *)presenting sourceViewController:(UIViewController *)source {
    PopoverPresentationController *controller = [[PopoverPresentationController alloc] initWithPresentedViewController:presented
                                                                                              presentingViewController:presenting];
    return controller;
}

- (id <UIViewControllerAnimatedTransitioning>)animationControllerForPresentedController:(UIViewController *)presented
                                                                   presentingController:(UIViewController *)presenting
                                                                       sourceController:(UIViewController *)source {
    PopoverAnimationController *animator = [[PopoverAnimationController alloc] init];
    return animator;
}

- (id <UIViewControllerAnimatedTransitioning>)animationControllerForDismissedController:(UIViewController *)dismissed {
    PopoverAnimationController *animator = [[PopoverAnimationController alloc] init];
    return animator;
}

@end
