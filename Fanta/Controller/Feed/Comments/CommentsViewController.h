//
//  PostCommentsViewController.h
//  Fanta
//
//  Created by Mario Concilio on 07/04/15.
//  Copyright (c) 2015 Mario Concilio. All rights reserved.
//

#import <UIKit/UIKit.h>
//
//@class PostCommentsViewController, VOPost;
//@protocol PostCommentsDelegate <NSObject>
//
//- (void)postCommentsWillDismiss:(PostCommentsViewController *)viewController;
//- (void)postCommentsDidDismiss:(PostCommentsViewController *)viewController;

//@end

@interface CommentsViewController : UIViewController

//@property (nonatomic, weak) UIViewController<PostCommentsDelegate> *delegateViewController;
@property (nonatomic, strong) VOPost *post;

//+ (void)presentInViewController:(UIViewController *)viewController
//                       withPost:(VOPost *)post
//                      fromPoint:(CGPoint)point;

//- (instancetype)initWithPost:(VOPost *)post fromPoint:(CGPoint)point;

@end
