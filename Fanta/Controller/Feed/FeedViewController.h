//
//  FeedViewController.h
//  Fanta
//
//  Created by Mario Concilio on 19/03/15.
//  Copyright (c) 2015 Mario Concilio. All rights reserved.
//

#import <UIKit/UIKit.h>

@class FeedCell, RKTabView;
@interface FeedViewController : UIViewController

@property (nonatomic, weak) FeedCell *cell;
@property (nonatomic, weak) RKTabView *tabView;

@end
