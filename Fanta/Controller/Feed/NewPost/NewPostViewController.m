//
//  NewPostViewController.m
//  Fanta
//
//  Created by Mario Concilio on 16/04/15.
//  Copyright (c) 2015 Mario Concilio. All rights reserved.
//

#import "APIService.h"
#import "Constants.h"
//#import "Fanta-Swift.h"
#import "Helper.h"
#import "UserService.h"
#import "KeyboardController.h"
#import "FadeInNavigationController.h"
#import "NewPostViewController.h"
#import "UIView+Shadow.h"
#import "DashedTextView.h"
#import "ListAALightViewController.h"
#import "VOAthletic.h"
#import "VOUser.h"
#import "UIStoryboard+Storyboards.h"
#import "TextStyles.h"

#import <AssetsLibrary/AssetsLibrary.h>
#import <DBCameraContainerViewController.h>
#import <DBCameraLibraryViewController.h>
#import <DBCameraSegueViewController.h>
#import <DBCameraView.h>
#import <DBCameraViewController.h>
#import <DBPrivacyHelper/UIViewController+DBPrivacyHelper.h>
#import <POP.h>
#import <SIAlertView.h>
#import <LGSemiModalNavViewController.h>

@interface NewPostViewController () <UITextViewDelegate, DBCameraViewControllerDelegate, KeyboardDelegate, ListAADelegate>

@property (strong, nonatomic) NSArray<VOAthletic *> *selectedAAs;
@property (strong, nonatomic) KeyboardController *keyboardController;

@end

@implementation NewPostViewController

CGSize _originalContentSize;

+ (FadeInNavigationController *)viewWithNavigationController {
    NewPostViewController *vc = [[NewPostViewController alloc] init];
    FadeInNavigationController *nav = [[FadeInNavigationController alloc] initWithRootViewController:vc];
    return nav;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    self.view.backgroundColor = [UIColor whiteColor];
    
    /*
     * choose AA and open store
     */
//    [self.listAAButton setHidden:![self canChooseAA]];
    if (![self canChooseAA]) {
        NSMutableArray *toolbarItems = [[self.toolbar items] mutableCopy];
        [toolbarItems removeObject:self.listAAButton];
        [self.toolbar setItems:toolbarItems animated:NO];
    }
    
    if (![self canOpenStore]) {
        NSMutableArray *toolbarItems = [[self.toolbar items] mutableCopy];
        [toolbarItems removeObject:self.storeBarButton];
        [self.toolbar setItems:toolbarItems animated:NO];
    }
    
    /*
     * set AA name
     */
    VOAthletic *userAA = [[UserService loadUser] athletic];
    self.selectedAAs = @[userAA];
    self.listAAButton.title = [NSString stringWithFormat:@"%@ - %@", [userAA name], [userAA athleticName]];
    [self.listAAButton setTitleTextAttributes:[TextStyles newPostChooseAA]
                                     forState:UIControlStateNormal];
    
//    [self.listAAButton setTitle:[NSString stringWithFormat:@"%@ - %@", [userAA name], [userAA athleticName]]
//                       forState:UIControlStateNormal];
//    [self.listAAButton sizeToFit];
    
    /*
     * Textview
     */
    DashedTextView *textView = [[DashedTextView alloc] initWithFrame:CGRectMake(10.0,
                                                                                kBlurNavbarHeight + 10.0,
                                                                                UIKeyWindowWidth() - 20.0,
                                                                                38.0)];
    textView.font = [UIFont lightFontWithSize:16.0];
    textView.scrollEnabled = NO;
    textView.delegate = self;
//    textView.layer.borderColor = [UIColor customLightBackground].CGColor;
//    textView.layer.borderWidth = 1.0;
//    textView.layer.cornerRadius = 5.0;
    [self.scrollView addSubview:textView];
    _textView = textView;
    self.automaticallyAdjustsScrollViewInsets = NO;
    
    /*
     *  PlaceHolder
     */
    UILabel *label = [[UILabel alloc] init];
    label.backgroundColor = [UIColor clearColor];
    label.textColor = [UIColor lightGrayColor];
    label.text = kNewPostPlaceholder;
    label.font = [UIFont lightFontWithSize:16.0];
    CGSize size = [label sizeThatFits:self.textView.frame.size];
    label.frame = (CGRect) {
        .size = size,
        .origin.x = 5.0,
        .origin.y = 8.0,
    };
    
    [self.textView addSubview:label];
    _placeholderLabel = label;
    
    /*
     *  Keyboard Controller
     */
    self.view.frame = UIKeyWindow().frame;
    self.keyboardController = [[KeyboardController alloc] initWithView:self.view andFrame:self.view.frame];
    self.keyboardController.delegate = self;
    self.keyboardController.shouldResizeView = YES;
    
    /*
     *  Navbar
     */
    [self.navigationController setNavigationBarHidden:NO animated:NO];
    [self.navigationController.navigationBar setBarStyle:UIBarStyleDefault];
    [self.navigationController.navigationBar setTintColor:[UIColor customPink]];
//    [[UIApplication sharedApplication] setStatusBarHidden:YES withAnimation:UIStatusBarAnimationSlide];
    
    self.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"Cancelar"
                                                                             style:UIBarButtonItemStylePlain
                                                                            target:self
                                                                            action:@selector(doCancel)];
    [self.navigationItem.leftBarButtonItem setTitleTextAttributes:@{NSFontAttributeName: [UIFont lightFontWithSize:17.0]}
                                                         forState:UIControlStateNormal];
    
    self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"Publicar"
                                                                              style:UIBarButtonItemStylePlain
                                                                             target:self
                                                                             action:@selector(doPublish)];
    [self.navigationItem.rightBarButtonItem setTitleTextAttributes:@{NSFontAttributeName: [UIFont semiboldFontWithSize:17.0]}
                                                          forState:UIControlStateNormal];
    self.navigationItem.rightBarButtonItem.enabled = NO;
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    [self.keyboardController registerForKeyboardNotifications];
//    [self.textView becomeFirstResponder];
}

- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
//    [self.textView becomeFirstResponder];
}

- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
    [self.view endEditing:YES];
    [self.keyboardController deregisterFromKeyboardNotifications];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (BOOL)prefersStatusBarHidden {
    return YES;
}

- (UIImageView *)postImageView {
    if (!_postImageView) {
        UIImageView *imageView = [[UIImageView alloc] initWithFrame:CGRectMake(0,
                                                                               kBlurNavbarHeight,
                                                                               UIKeyWindowWidth(),
                                                                               UIKeyWindowWidth())];
        [self.scrollView addSubview:imageView];
        
        CGRect textViewFrame = self.textView.frame;
        textViewFrame.origin.y += UIKeyWindowWidth();
        self.textView.frame = textViewFrame;
        
        // store original scrollview content size
        _originalContentSize = self.scrollView.contentSize;
        
        CGSize contentSize = self.scrollView.contentSize;
        contentSize.height = UIKeyWindowWidth() + CGRectGetHeight(self.textView.frame) + 120;
        self.scrollView.contentSize = contentSize;
        
        /*
         *  Cancel Button
         */
        UIButton *button = [UIButton buttonWithType:UIButtonTypeSystem];
        button.frame = (CGRect) {
            .origin.x = UIKeyWindowWidth() - kNewPostButtonWidth - 8,
            .origin.y = UIKeyWindowWidth() - kNewPostButtonWidth - 8,
            .size = kNewPostButtonSize,
        };
        button.backgroundColor = [UIColor customOrange];
        [button addShadowWithColor:nil offset:CGSizeMake(1.0, 1.0) opacity:0.6 radius:1.0];
        button.layer.cornerRadius = kNewPostButtonSize.width/2;
        button.tintColor = [UIColor whiteColor];
        [button setImage:[UIImage imageNamed:@"fnt_cancel"] forState:UIControlStateNormal];
        [button addTarget:self action:@selector(removeImage) forControlEvents:UIControlEventTouchUpInside];
        imageView.userInteractionEnabled = YES;
        [imageView addSubview:button];
        
        _postImageView = imageView;
    }
    
    return _postImageView;
}

#pragma mark - Helper Methods
- (void)removeImage {
    CGRect textViewFrame = self.textView.frame;
    textViewFrame.origin.y -= UIKeyWindowWidth();
    
    POPBasicAnimation *positionAnimation = [POPBasicAnimation animationWithPropertyNamed:kPOPLayerPositionY];
    positionAnimation.toValue = @(0.0);
    [positionAnimation setCompletionBlock:^(POPAnimation *anim, BOOL completion) {
        [self.postImageView removeFromSuperview];
        _postImageView = nil;
    }];
    [self.postImageView pop_addAnimation:positionAnimation forKey:@"positionAnimation"];
    
    POPBasicAnimation *imageViewAnimation = [POPBasicAnimation animationWithPropertyNamed:kPOPViewScaleXY];
    imageViewAnimation.toValue = [NSValue valueWithCGSize:CGSizeMake(kMinScale, kMinScale)];
//    imageViewAnimation.springBounciness = 6.0;
//    imageViewAnimation.springSpeed = 6.0;
    [self.postImageView pop_addAnimation:imageViewAnimation forKey:@"imageViewAnimation"];

    POPSpringAnimation *textViewAnimation = [POPSpringAnimation animationWithPropertyNamed:kPOPViewFrame];
    textViewAnimation.toValue = [NSValue valueWithCGRect:textViewFrame];
    textViewAnimation.springBounciness = 6.0;
    textViewAnimation.springSpeed = 6.0;
    [self.textView pop_addAnimation:textViewAnimation forKey:@"textViewAnimation"];
    
    // restore original scrollview content size
    self.scrollView.contentSize = _originalContentSize;
}

- (void)dismiss {
    [self.navigationController dismissViewControllerAnimated:YES completion:NULL];
//    [[UIApplication sharedApplication] setStatusBarHidden:NO withAnimation:UIStatusBarAnimationSlide];
}

- (void)doCancel {
    if (_postImageView || [self.textView hasText]) {
        SIAlertView *alertView = [[SIAlertView alloc] initWithTitle:@"Atenção"
                                                         andMessage:@"Deseja descartar seu post?"];
        
        [alertView addButtonWithTitle:@"Cancelar"
                                 type:SIAlertViewButtonTypeCancel
                              handler:^(SIAlertView *alertView) {
                                  [alertView dismissAnimated:YES];
                              }];
        
        [alertView addButtonWithTitle:@"Descartar"
                                 type:SIAlertViewButtonTypeDestructive
                              handler:^(SIAlertView *alertView) {
                                  [self dismiss];
                              }];
        
        alertView.transitionStyle = SIAlertViewTransitionStyleBounce;
        [alertView show];
    }
    else {
        [self dismiss];
    }
}

- (void)doPublish {
    DDLogWarn(@"not yet implemented.");
}

#pragma mark - IB Actions
- (IBAction)doOpenGallery:(UIBarButtonItem *)sender {
//    DBCameraSegueViewController *vc = [[DBCameraSegueViewController alloc] initWithImage:self.imageView.image thumb:nil];
//    vc.delegate = self;
//    UINavigationController *nav = [[UINavigationController alloc] initWithRootViewController:vc];
//    [nav setNavigationBarHidden:YES];
//    [self presentViewController:nav animated:YES completion:nil];
    
//    ALAuthorizationStatus status = [ALAssetsLibrary authorizationStatus];
//    if (status == ALAuthorizationStatusAuthorized) {
        DBCameraLibraryViewController *vc = [[DBCameraLibraryViewController alloc] init];
        [vc setDelegate:self];
        [vc setForceQuadCrop:YES];
        [vc setUseCameraSegue:YES];
    
        FadeInNavigationController *nav = [[FadeInNavigationController alloc] initWithRootViewController:vc];
        [nav setNavigationBarHidden:YES];
        [self presentViewController:nav animated:YES completion:nil];

    //    }
//    else {
//        [self showPrivacyHelperForType:DBPrivacyTypePhoto];
//    }
}

- (IBAction)doOpenCamera:(UIBarButtonItem *)sender {
    AVAuthorizationStatus status = [AVCaptureDevice authorizationStatusForMediaType:AVMediaTypeVideo];
    if (status == AVAuthorizationStatusAuthorized) {
        DBCameraViewController *cameraController = [DBCameraViewController initWithDelegate:self];
//        [cameraController setTintColor:[UIColor customOrange]];
        [cameraController setSelectedTintColor:[UIColor customOrange]];
        [cameraController setForceQuadCrop:YES];
        [cameraController setUseCameraSegue:YES];
        
        DBCameraContainerViewController *container = [[DBCameraContainerViewController alloc] initWithDelegate:self];
//        [container setTintColor:[UIColor customOrange]];
//        [container setSelectedTintColor:[UIColor customPink]];
        [container setCameraViewController:cameraController];
        [container setFullScreenMode];
        
        FadeInNavigationController *nav = [[FadeInNavigationController alloc] initWithRootViewController:container];
        [nav setNavigationBarHidden:YES];
        [self presentViewController:nav animated:YES completion:nil];
    }
    else {
        [self showPrivacyHelperForType:DBPrivacyTypeCamera];
    }
}

- (IBAction)doOpenStore:(UIBarButtonItem *)sender {
    NSLog(@"opening store");
//    UINavigationController *nav = [[UIStoryboard storeStoryboard] instantiateInitialViewController];
//    [self presentViewController:nav animated:YES completion:NULL];
}

- (IBAction)doOpenListAA:(UIButton *)sender {
    CGFloat s = CGRectGetHeight([[UIApplication sharedApplication] statusBarFrame]);
    CGFloat n = CGRectGetHeight(self.navigationController.navigationBar.frame);
    
    ListAALightViewController *vc = [[ListAALightViewController alloc] init];
    vc.delegate = self;
    vc.filtering = NO;
    vc.modalPresentation = YES;
//    vc.listAA = self.selectedAAs;
    
    LGSemiModalNavViewController *nav = [[LGSemiModalNavViewController alloc] initWithRootViewController:vc];
    nav.view.frame = CGRectMake(0.0, 0.0, UIKeyWindowWidth(), UIKeyWindowHeight() - s - n - 10.0);
    nav.backgroundShadeColor = [UIColor blackColor];
    nav.tapDismissEnabled = YES;
    nav.scaleTransform = CGAffineTransformMakeScale(0.94, 0.94);
    
    [self presentViewController:nav animated:YES completion:nil];
}

#pragma mark - TextView Delegate
- (void)textViewDidChange:(UITextView *)textView {
    CGSize size = textView.frame.size;
    CGSize newSize = [textView sizeThatFits:CGSizeMake(size.width, CGFLOAT_MAX)];
    
    // change textview height
    if (size.height != newSize.height) {
        CGRect newFrame = textView.frame;
        newFrame.size.height = newSize.height;
        textView.frame = newFrame;
        
//        [self.textView drawDashedBorderAroundView];
        
        // if not exists imageview
        if (!_postImageView)
            self.scrollView.contentSize = CGSizeMake(UIKeyWindowWidth(), newSize.height + 120);
        else
            self.scrollView.contentSize = CGSizeMake(UIKeyWindowWidth(), newSize.height + UIKeyWindowWidth() + 120);
        
        newFrame.size.height += 64;
        [self.scrollView scrollRectToVisible:newFrame animated:YES];
    }
    
    // sets the placeholder
    if ([textView hasText]) {
        self.placeholderLabel.hidden = YES;
        self.navigationItem.rightBarButtonItem.enabled = YES;
    }
    else {
        self.placeholderLabel.hidden = NO;
        self.navigationItem.rightBarButtonItem.enabled = NO;
    }
}

#pragma mark - Keyboard Delegate
- (void)keyboardDidShow:(CGSize)keyboardSize {
    CGFloat textViewH = CGRectGetHeight(_textView.frame);
    CGFloat viewH = CGRectGetHeight(self.view.frame);
    
    // scroll only if textview frame is bigger enough or has imageview
    if (_postImageView || textViewH > viewH - keyboardSize.height) {
        CGPoint scrollPoint = CGPointMake(0,
                                          self.textView.frame.origin.y - keyboardSize.height + textViewH);
        
        dispatch_async(dispatch_get_main_queue(), ^{
            [self.scrollView setContentOffset:scrollPoint animated:YES];
        });
    }
    
}

#pragma mark - DBCameraViewControllerDelegate
- (void)camera:(id)cameraViewController didFinishWithImage:(UIImage *)image withMetadata:(NSDictionary *)metadata {
    self.postImageView.image = image;
    [self.presentedViewController dismissViewControllerAnimated:YES completion:nil];
    
    NSString *source = [metadata objectForKey:@"DBCameraSource"];
    if( [source hasSuffix:@"Camera"] ){
        UIImageWriteToSavedPhotosAlbum(image, nil, nil, nil);
    }
}

-(void)dismissCamera:(id)cameraViewController {
    [self dismissViewControllerAnimated:YES completion:nil];
    [cameraViewController restoreFullScreenMode];
}

#pragma mark - List AA Delegate
- (void)listAA:(ListAAViewController *)vc didSelectAA:(VOAthletic *)athletic {
    DDLogVerbose(@"%@", athletic);
    self.selectedAAs = @[athletic];
    self.listAAButton.title = [NSString stringWithFormat:@"%@ - %@", [athletic name], [athletic athleticName]];
//    [self.listAAButton setTitle:[NSString stringWithFormat:@"%@ - %@", athletic.name, athletic.athleticName]
//                       forState:UIControlStateNormal];
    
//    [vc dismissViewControllerAnimated:YES completion:NULL];
}

- (void)listAA:(ListAAViewController *)vc didSelectListAA:(NSArray<VOAthletic *> *)list {
    self.selectedAAs = list;
    
    if (list.count > 1) {
        self.listAAButton.title = [NSString stringWithFormat:@"%ld atléticas", list.count];
//        [self.listAAButton setTitle:[NSString stringWithFormat:@"%ld atléticas", list.count]
//                           forState:UIControlStateNormal];
    }
    else {
        self.listAAButton.title = [NSString stringWithFormat:@"%@ - %@", list[0].name, list[0].athleticName];
//        [self.listAAButton setTitle:list[0].name
//                           forState:UIControlStateNormal];
    }
    
//    [self.listAAButton sizeToFit];
    
    DDLogVerbose(@"%ld AAs selected", self.selectedAAs.count);
}

@end
