//
//  NewPostViewController.h
//  Fanta
//
//  Created by Mario Concilio on 16/04/15.
//  Copyright (c) 2015 Mario Concilio. All rights reserved.
//

#import <UIKit/UIKit.h>

@class FadeInNavigationController, DashedTextView;
@interface NewPostViewController : UIViewController

@property (nonatomic, assign, getter=canChooseAA) BOOL chooseAA;
@property (nonatomic, assign, getter=canOpenStore) BOOL openStore;

@property (weak, nonatomic) UIImageView *postImageView;
@property (weak, nonatomic) UILabel *placeholderLabel;
@property (weak, nonatomic) DashedTextView *textView;
@property (weak, nonatomic) IBOutlet UIScrollView *scrollView;
@property (weak, nonatomic) IBOutlet UIBarButtonItem *galleryBarButton;
@property (weak, nonatomic) IBOutlet UIBarButtonItem *cameraBarButton;
@property (strong, nonatomic) IBOutlet UIBarButtonItem *listAAButton; // strong reference for removing from toolbar
@property (strong, nonatomic) IBOutlet UIBarButtonItem *storeBarButton; // strong reference for removing from toolbar
@property (weak, nonatomic) IBOutlet UIToolbar *toolbar;

+ (FadeInNavigationController *)viewWithNavigationController;

- (IBAction)doOpenGallery:(UIBarButtonItem *)sender;
- (IBAction)doOpenCamera:(UIBarButtonItem *)sender;
- (IBAction)doOpenStore:(UIBarButtonItem *)sender;
- (IBAction)doOpenListAA:(UIBarButtonItem *)sender;

@end
