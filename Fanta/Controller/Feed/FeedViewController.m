//
//  FeedViewController.m
//  Fanta
//
//  Created by Mario Concilio on 19/03/15.
//  Copyright (c) 2015 Mario Concilio. All rights reserved.
//

#import "APIService.h"
#import "AppDelegate.h"
#import "Constants.h"
#import "FeedCell.h"
#import "NewPostCell.h"
#import "FeedViewController.h"
#import "NewPostViewController.h"
#import "FadeInNavigationController.h"
#import "Helper.h"
#import "PostViewController.h"
#import "CommentsViewController.h"
#import "ProfileViewController.h"
#import "PopoverTransition.h"
#import "FriendsViewController.h"
//#import "ExpandCellTransition.h"
#import "FeedPlaceholder.h"
#import "ListAAViewController.h"
#import "Generator.h"

#import "UIStoryboard+Storyboards.h"
#import "UIViewController+BaseViewController.h"

//#import <Fanta-Swift.h>
#import <AsyncDisplayKit.h>
#import <POP.h>
#import <VBFPopFlatButton.h>
#import <TSMessage.h>
#import <RKTabView.h>
//#import <TLYShyNavBarManager.h>
//#import <SQTShyNavigationBar.h>

#import "VOUser.h"
#import "VOPost.h"
#import "VOAthletic.h"

//#define kURL   @"http://54.94.130.154"

typedef enum {
    usersIndex,
    globalIndex,
    athleticsIndex,
} TabIndex;

@interface FeedViewController() <ASTableViewDataSource, ASTableViewDelegate, UIScrollViewDelegate, ListAADelegate, SWRevealViewControllerDelegate, RKTabViewDelegate> {
    ASTableView *_tableView;
}

@property (nonatomic, weak) FeedPlaceholder *placeholderNode;
@property (nonatomic, weak) UIRefreshControl *refresher;
@property (nonatomic, strong) NSMutableArray *postsDataSource;
@property (nonatomic, strong) NSArray<VOAthletic *> *selectedAAs;
@property (nonatomic, strong) UIBarButtonItem *listAABarButton;

@end

static const NSUInteger kMaxNumberOfPosts = 100;

static NSString *const kPostSegue      = @"postSegue";
static NSString *const kNewPostSegue   = @"newPostSegue";
static NSString *const kCommentsSegue  = @"commentsSegue";

@implementation FeedViewController

#pragma mark - Init
- (instancetype)init {
    self = [super init];
    if (self) {
        _tableView = [[ASTableView alloc] initWithFrame:CGRectZero style:UITableViewStylePlain asyncDataFetching:YES];
        _tableView.asyncDataSource = self;
        _tableView.asyncDelegate = self;
    }
    
    return self;
}

- (instancetype)initWithCoder:(NSCoder *)aDecoder {
    self = [super initWithCoder:aDecoder];
    if (self) {
        _tableView = [[ASTableView alloc] initWithFrame:CGRectZero style:UITableViewStylePlain asyncDataFetching:YES];
        _tableView.asyncDataSource = self;
        _tableView.asyncDelegate = self;
    }
    
    return self;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    self.postsDataSource = [NSMutableArray array];
    self.automaticallyAdjustsScrollViewInsets = YES;
    
    /*
     *  base view controller
     */
    [self addDrawerButtonDark];
    [self setupReachability];
    
    /*
     * AA menu and array
     */
    self.selectedAAs = [Helper loadAAList];
    
    ListAAViewController *vc = [[ListAAViewController alloc] init];
    vc.delegate = self;
    vc.filtering = YES;
//    vc.listAA = self.selectedAAs;
    
    UINavigationController *nav = [[UINavigationController alloc] initWithRootViewController:vc];
    [self.revealViewController setRightViewController:nav];
    
    /*
     *  friends barbutton
     *
    UINavigationController *nav = (UINavigationController *) self.revealViewController.rightViewController;
    FriendsViewController *friendsVC = (FriendsViewController *) nav.childViewControllers[0];
    friendsVC.delegate = self;
     */
    
    self.view.backgroundColor = [UIColor customDarkBackground];
    
    /*
     *  placeholder
     */
    FeedPlaceholder *placeholder = [[FeedPlaceholder alloc] initWithNumberOfRows:2];
    placeholder.frame = self.view.bounds;
//    placeholder.view.contentInset = UIEdgeInsetsMake(0.0, 0.0, kTabViewHeight, 0.0);
    _placeholderNode = placeholder;
    [self.view addSubnode:_placeholderNode];
    
    /*
     *  tableview
     */
    _tableView.backgroundColor = [UIColor customDarkBackground];
    _tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    _tableView.alpha = 0.0;
    _tableView.contentInset = UIEdgeInsetsMake(64.0, 0.0, kTabViewHeight, 0.0);
    [self.view addSubview:_tableView];
    
//    [self.navigationController.toolbar setBarStyle:UIBarStyleBlack];
    [self.navigationController setToolbarHidden:YES animated:NO];
    
    self.listAABarButton = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"fnt_navbar_filter"]
                                                            style:UIBarButtonItemStylePlain
                                                           target:self
                                                           action:@selector(toggleFilterAAList)];
    
//    self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"fnt_navbar_filter"]
//                                                                              style:UIBarButtonItemStylePlain
//                                                                             target:self
//                                                                             action:@selector(toggleFilterAAList)];
    
    /*
     *  tabbar
     */
    RKTabItem *userItem = [RKTabItem createUsualItemWithImageEnabled:[UIImage imageNamed:@"fnt_users_selected"]
                                                       imageDisabled:[UIImage imageNamed:@"fnt_users"]];
    RKTabItem *globalItem = [RKTabItem createUsualItemWithImageEnabled:[UIImage imageNamed:@"fnt_feed_all_selected"]
                                                       imageDisabled:[UIImage imageNamed:@"fnt_feed_all"]];
    RKTabItem *athleticItem = [RKTabItem createUsualItemWithImageEnabled:[UIImage imageNamed:@"fnt_feed_athletic_selected"]
                                                           imageDisabled:[UIImage imageNamed:@"fnt_feed_athletic"]];
    
    userItem.titleString = @"Estudantes";
    globalItem.titleString = @"Global";
    athleticItem.titleString = @"Atléticas";
    
    RKTabView *tabView = [[RKTabView alloc] initWithFrame:CGRectMake(0.0, UIKeyWindowHeight() - kTabViewHeight, UIKeyWindowWidth(), kTabViewHeight)];
    tabView.tabItems = @[userItem, globalItem, athleticItem];
    tabView.titlesFont = [UIFont lightFontWithSize:12.0];
    tabView.titlesFontColor = [UIColor whiteColor];
    tabView.titlesFontColorEnabled = [UIColor customOrange];
    tabView.horizontalInsets = HorizontalEdgeInsetsMake(70.0, 70.0);
    tabView.darkensBackgroundForEnabledTabs = YES;
    tabView.upperSeparatorLineColor = [UIColor lightGrayColor];
    tabView.backgroundColor = [UIColor clearColor];
    tabView.delegate = self;
    [tabView switchTabIndex:globalIndex];
    _tabView = tabView;
    [self.view addSubview:tabView];
    
    UIBlurEffect *blurEffect = [UIBlurEffect effectWithStyle:UIBlurEffectStyleDark];
    UIVisualEffectView *blurEffectView = [[UIVisualEffectView alloc] initWithEffect:blurEffect];
    blurEffectView.frame = CGRectMake(0.0, 0.0, UIKeyWindowWidth(), kTabViewHeight);
    tabView.backgroundColor = [UIColor clearColor];
    [tabView addSubview:blurEffectView];
    
    /*
     *  refresher
     */
    UIRefreshControl *refresher = [[UIRefreshControl alloc] init];
    refresher.tintColor = [UIColor lightGrayColor];
    [refresher addTarget:self action:@selector(refreshData) forControlEvents:UIControlEventValueChanged];
    _refresher = refresher;
    [_tableView addSubview:_refresher];
    
//    [self.refresher beginRefreshing];
    [self refreshData];
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
//    [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleLightContent animated:YES];
    
//    [self.drawerButton animateToType:buttonMenuType];
}

- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
}

- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
}

- (void)viewDidDisappear:(BOOL)animated {
    [super viewDidDisappear:animated];
//    [self.drawerButton animateToType:buttonDefaultType];
}

- (void)viewWillLayoutSubviews {
    [super viewWillLayoutSubviews];
    _tableView.frame = self.view.bounds;
}

- (BOOL)prefersStatusBarHidden {
    return NO;
}

- (UIStatusBarStyle)preferredStatusBarStyle {
    return UIStatusBarStyleLightContent;
}

#pragma mark - Navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    if ([segue.identifier isEqualToString:kCommentsSegue]) {        // comments
        ASImageNode *node = (ASImageNode *)sender;
        CGRect frame = [node.view.superview convertRect:node.view.frame toView:nil];
        VOPost *post = [self.postsDataSource objectAtIndex:node.view.tag];
        CommentsViewController *vc = segue.destinationViewController;
        vc.post = post;
        vc.popover_startFrame = frame;
    }
    else if ([segue.identifier isEqualToString:kPostSegue]) {       // open post
        VOPost *post = (VOPost *)sender;
        PostViewController *vc = segue.destinationViewController;
        [vc setupPost:post
          cachedImage:self.cell.imageNode.image
                frame:[self.cell.view convertRect:self.cell.frameNode.frame toView:nil]];
    }
    else if ([segue.identifier isEqualToString:kNewPostSegue]) {    // new post
        UINavigationController *nav = segue.destinationViewController;
        NewPostViewController *vc = (NewPostViewController *) nav.topViewController;
        
        //TODO: verificar se usuario eh atletica
        if ([self.tabView.selectedTabItem.titleString isEqualToString:@"Estudantes"]) {
            vc.chooseAA = YES;
            vc.openStore = YES; // NO se for estudante
        }
        else if ([self.tabView.selectedTabItem.titleString isEqualToString:@"Atléticas"]) {
            vc.chooseAA = YES;  // NO se for atletica
            vc.openStore = YES; // NO se for estudante
        }
        else { //global
            vc.chooseAA = NO;
            vc.openStore = YES; // NO se for estudante
        }
    }
}

#pragma mark - Helper Methods
- (void)beginRefreshing {
    
//    if (_tableView.contentOffset.y == 0) {
        [UIView animateWithDuration:0.25 delay:0.0 options:UIViewAnimationOptionBeginFromCurrentState animations:^{
            _tableView.contentOffset = CGPointMake(0.0, -self.refresher.frame.size.height-64.0);
        } completion:NULL];
//    }
    
    [self.refresher beginRefreshing];
}

- (void)refreshData {
    __weak typeof(self) weakSelf = self;
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(2.0 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        _postsDataSource = [[NSMutableArray alloc] initWithArray:[weakSelf populatePosts]];
        [_tableView reloadData];
        [weakSelf.refresher endRefreshing];
        
        POPBasicAnimation *fadeIn = [POPBasicAnimation animationWithPropertyNamed:kPOPViewAlpha];
        fadeIn.toValue = @(1.0);
        fadeIn.completionBlock = ^(POPAnimation *anim, BOOL finished) {
            if (finished) {
//                    [self.placeholderNode removeFromSupernode];
//                    self.placeholderNode = nil;
                self.placeholderNode.hidden = YES;
            }
        };
        
        [_tableView pop_addAnimation:fadeIn forKey:@"fadeIn"];
        
    });
    
    
    /*
    __weak typeof(self) weakSelf = self;
    [[APIService sharedInstance] getPosts:APIPostTypeRegular
                           fromUniversity:1
                                  success:^(NSArray *array) {
                                      [_postsDataSource addObjectsFromArray:array];
                                      [_postsDataSource addObjectsFromArray:[self populatePosts]];
//                                      _postsDataSource = array;
                                      
                                      [_tableView reloadData];
                                      [weakSelf.refresher endRefreshing];
                                      DDLogInfo(@"posts retrieved");
                                  }
                                  failure:^(NSError *error) {
                                      DDLogWarn(@"%@", error);
                                      [weakSelf.refresher endRefreshing];
                                      [TSMessage showNotificationWithTitle:@"Ops..."
                                                                  subtitle:@"A conexão com a Internet falhou. Por favor verifique"
                                                                      type:TSMessageNotificationTypeError];
                                  }];
     */
    
}

- (NSArray *)populatePosts {
    NSMutableArray *array = [NSMutableArray array];
    for (NSUInteger i = 0; i <= 15; i++) {
        @autoreleasepool {
            VOUser *user = [[VOUser alloc] init];
            user.name = [Generator randomName];
            user.thumbnail = [Generator randomPerson];
            user.userID = (i+1) * 100;
            
            user.facebook = 0;
            user.twitter = @"JeremyClarkson";
            user.instagram = @"topgear";
            
            VOAthletic *aa = [[VOAthletic alloc] init];
            aa.name = @"EACH - DASI";
            aa.thumbnail = [Generator randomSport];
            aa.facebookID = 188801551146783;
            user.athletic = aa;
            
            NSString *emailName = [[user.name stringByReplacingOccurrencesOfString:@" " withString:@""] lowercaseString];
            user.email = [NSString stringWithFormat:@"%@%lu@gmail.com", emailName, user.userID];
            
            VOPost *post = [[VOPost alloc] init];
            post.user = user;
            post.userName = user.name;
            
            if (i%2 == 0) {
                post.content = [NSString stringWithFormat:@"%@ %@", [Generator mussumIpsum], [Generator mussumIpsum]];
            }
            else {
                post.content = [Generator mussumIpsum];
            }
            
            if (i%3 == 0) {
                post.postImage = [Generator randomImage];
            }

            post.createdAt = [[NSDate alloc] initWithTimeIntervalSinceNow:i * 60000];
            post.likes = (i+1) * 1000;
            post.dislikes = (i+1) * 200;
            post.comments = (i+1) * 300;
            
            [array addObject:post];
        }
    }

    return array;
}

- (void)toggleFilterAAList {
    [self.revealViewController rightRevealToggleAnimated:YES];
}

#pragma mark - Action Methods
- (IBAction)toolbarButtonTouchDown:(UIButton *)sender {
//    [sender.superview pop_removeAllAnimations];
    
    POPSpringAnimation *anim = [POPSpringAnimation animationWithPropertyNamed:kPOPViewScaleXY];
//    anim.springBounciness = 20.0;
//    anim.springSpeed = 2.0;
    anim.toValue = [NSValue valueWithCGSize:CGSizeMake(0.8, 0.8)];
    [sender.superview pop_addAnimation:anim forKey:@"decrease"];
}

- (IBAction)doToolbarButton:(UIButton *)sender {
//    [sender.superview pop_removeAllAnimations];
    
    POPSpringAnimation *anim = [POPSpringAnimation animationWithPropertyNamed:kPOPViewScaleXY];
    anim.springBounciness = 20.0;
    anim.springSpeed = 2.0;
    anim.toValue = [NSValue valueWithCGSize:CGSizeMake(1.0, 1.0)];
    
    [sender.superview pop_addAnimation:anim forKey:@"increase"];
}

- (void)doThumbsUp:(ASImageNode *)sender {
    
//    sender.imageModificationBlock = ASImageNodeTintColorModificationBlock([UIColor blueColor]);
//    [sender setNeedsDisplayWithCompletion:NULL];
}

- (void)doThumbsDown:(ASImageNode *)sender {
    
}

- (void)doComments:(ASImageNode *)sender {
    [self performSegueWithIdentifier:kCommentsSegue sender:sender];
//    VOPost *post = [self.postsDataSource objectAtIndex:sender.view.tag];
//    
//    CGPoint point = [sender.view.superview convertPoint:sender.view.center toView:nil];
//    [PostCommentsViewController presentInViewController:self
//                                               withPost:post
//                                              fromPoint:point];
    
//    PostCommentsViewController *vc = [[PostCommentsViewController alloc] initWithPost:post fromPoint:point];
//    vc.modalPresentationStyle = UIModalPresentationCurrentContext;
//    [self presentViewController:vc animated:YES completion:NULL];
}

- (void)openProfile:(ASImageNode *)sender {
    VOPost *post = self.postsDataSource[sender.view.tag];
    UINavigationController *nav = [[UIStoryboard profileStoryboard] instantiateInitialViewController];
    ProfileViewController *vc = nav.childViewControllers[0];
    vc.user = post.user;
    vc.me = NO;
    
    [self presentViewController:nav animated:YES completion:NULL];
}

- (void)openNewPost {
    [self performSegueWithIdentifier:kNewPostSegue sender:self];
}

#pragma mark - Table View Data Source
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 2;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    if (section == 0) {
        return 1;
    }
    
    return self.postsDataSource.count;
}

- (ASCellNode *)tableView:(ASTableView *)tableView nodeForRowAtIndexPath:(NSIndexPath *)indexPath {
    if (indexPath.section == 0) {
        NewPostCell *cell = [[NewPostCell alloc] init];
        return cell;
    }
    
    VOPost *post = [self.postsDataSource objectAtIndex:indexPath.row];
    FeedCell *node = [[FeedCell alloc] initWithPost:post];
    [node.thumbsUpImageNode addTarget:self action:@selector(doThumbsUp:) forControlEvents:ASControlNodeEventTouchUpInside];
    [node.thumbsDownImageNode addTarget:self action:@selector(doThumbsDown:) forControlEvents:ASControlNodeEventTouchUpInside];
    [node.commentImageNode addTarget:self action:@selector(doComments:) forControlEvents:ASControlNodeEventTouchUpInside];
    [node.profileImageNode addTarget:self action:@selector(openProfile:) forControlEvents:ASControlNodeEventTouchUpInside];
    
    node.thumbsUpImageNode.view.tag = indexPath.row;
    node.thumbsDownImageNode.view.tag = indexPath.row;
    node.commentImageNode.view.tag = indexPath.row;
    node.profileImageNode.view.tag = indexPath.row;
    
    return node;
}

#pragma mark - Table View Delegate
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    if (indexPath.section == 0) {
        [self performSegueWithIdentifier:kNewPostSegue sender:self];
    }
    else {
        FeedCell *cell = (FeedCell *) [_tableView nodeForRowAtIndexPath:indexPath];
        if (![cell.contentNode isTruncated] && !cell.imageNode) {
            return;
        }
        
        self.cell = cell;
        VOPost *post = [self.postsDataSource objectAtIndex:indexPath.row];
        [self performSegueWithIdentifier:kPostSegue sender:post];
    }
}

- (BOOL)shouldBatchFetchForTableView:(ASTableView *)tableView {
//    return self.postsDataSource.count < kMaxNumberOfPosts;
    return NO;
}

- (void)tableView:(ASTableView *)tableView willBeginBatchFetchWithContext:(ASBatchContext *)context {
    NSArray *newPosts = [self populatePosts];
    
    NSMutableArray *indexPaths = [NSMutableArray array];
    NSInteger existingPosts = self.postsDataSource.count;
    for (NSUInteger i = 0; i < newPosts.count; i++) {
        [indexPaths addObject:[NSIndexPath indexPathForRow:existingPosts + i inSection:0]];
    }
    
//    self.postsDataSource = [self.postsDataSource arrayByAddingObjectsFromArray:newPosts];
    [self.postsDataSource addObjectsFromArray:newPosts];
    [tableView insertRowsAtIndexPaths:indexPaths withRowAnimation:UITableViewRowAnimationFade];
    
    [context completeBatchFetching:YES];
    NSLog(@"posts added");
}

#pragma mark - List AA Delegate
- (void)listAA:(ListAAViewController *)vc didSelectAA:(VOAthletic *)athletic {
    self.selectedAAs = @[athletic];  // new array with one athletic
    [self.revealViewController setFrontViewPosition:FrontViewPositionLeft animated:YES];
    DDLogVerbose(@"%@", self.selectedAAs[0]);
}

- (void)listAA:(ListAAViewController *)vc didSelectListAA:(NSArray<VOAthletic *> *)list {
    self.selectedAAs = list;
    DDLogVerbose(@"%ld AAs selected", self.selectedAAs.count);
}

#pragma mark - RKTabView Delegate
- (void)tabView:(RKTabView *)tabView tabBecameEnabledAtIndex:(NSUInteger)index tab:(RKTabItem *)tabItem {
    _placeholderNode.hidden = NO;
    _tableView.alpha = 0.0;
    
    self.postsDataSource = nil;
    [_tableView reloadData];
//    [self beginRefreshing];
    [self refreshData];
    
    UINavigationController *nav = (UINavigationController *) self.revealViewController.rightViewController;
    ListAAViewController *vc = (ListAAViewController *) nav.topViewController;
    if (index == globalIndex) {
        vc.filtering = YES;
        self.navigationItem.rightBarButtonItem = nil;
//        vc.listAA = self.selectedAAs;
    }
    else {
        vc.filtering = NO;
        self.navigationItem.rightBarButtonItem = self.listAABarButton;
//        vc.listAA = @[self.selectedAAs[0]];
    }
}

- (void)tabView:(RKTabView *)tabView tabBecameDisabledAtIndex:(NSUInteger)index tab:(RKTabItem *)tabItem {
    
}

@end
