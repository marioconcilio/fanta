//
//  NewPostCell.m
//  Fanta
//
//  Created by Mario Concilio on 7/16/16.
//  Copyright © 2016 Mario Concilio. All rights reserved.
//

#import "NewPostCell.h"
#import "TextStyles.h"
#import "Constants.h"

#import <AsyncDisplayKit/ASDisplayNode+Subclasses.h>
#import <WebASDKImageManager.h>
#import <POP.h>

@interface NewPostCell () <ASNetworkImageNodeDelegate>

@property(nonatomic, weak) ASDisplayNode *frameNode;
@property(nonatomic, weak) ASTextNode *textNode;
//@property(nonatomic, weak) ASNetworkImageNode *profileImageNode;

@end

@implementation NewPostCell

- (instancetype)init {
    self = [super init];
    if (self) {
        /*
         *  frame
         */
        ASDisplayNode *frameNode = [[ASDisplayNode alloc] init];
        frameNode.backgroundColor = [UIColor whiteColor];
//        frameNode.layerBacked = YES;
        frameNode.userInteractionEnabled = YES;
        _frameNode = frameNode;
        [self addSubnode:_frameNode];
        
        /*
         *  text
         */
        ASTextNode *text = [[ASTextNode alloc] init];
//        text.layerBacked = YES;
        text.attributedString = [[NSAttributedString alloc] initWithString:@"O que você está pensando?"
                                                                attributes:[TextStyles contentPlaceholderStyle]];
        _textNode = text;
        [_frameNode addSubnode:_textNode];
        
        /*
         *  profile image
         *
        ASNetworkImageNode *profileImageNode = [[ASNetworkImageNode alloc] initWithWebImage];
        
        //!!!: CORDEIRO verificar atributo imagem do usuario
//        profileImageNode.URL = [NSURL URLWithString:post.user.thumbnail];
        profileImageNode.cornerRadius = kPostImageProfileThumbSize / 2;
        profileImageNode.clipsToBounds = YES;
        profileImageNode.borderWidth = 0.5;
        profileImageNode.borderColor = UIColorFromRGB(151, 151, 151).CGColor;
        profileImageNode.delegate = self;
        _profileImageNode = profileImageNode;
        [_frameNode addSubnode:_profileImageNode];
         */
        
        self.backgroundColor = [UIColor clearColor];
        self.selectionStyle = UITableViewCellSelectionStyleNone;
    }
    
    return self;
}

- (CGSize)calculateSizeThatFits:(CGSize)constrainedSize {
    CGSize textSize = [_textNode measure:CGSizeMake(constrainedSize.width - 4*kPostImageNodePadding, FLT_MAX)];
    
    return CGSizeMake(constrainedSize.width, textSize.height + 2*kPostImageNodePadding + 4*kPostImageNodePadding);
}

- (void)layout {
    [super layout];
    
    /*
     *  frame
     */
    _frameNode.frame = (CGRect) {
        .origin.x = kPostImageNodePadding,
        .origin.y = kPostImageNodePadding,
        .size.width = self.calculatedSize.width - 2 * kPostImageNodePadding,
        .size.height = _textNode.calculatedSize.height + 4*kPostImageNodePadding,
    };
    
    /*
     *  text
     */
    _textNode.frame = (CGRect) {
        .origin.x = kPostImageNodePadding,
        .origin.y = 2*kPostImageNodePadding,
        .size = _textNode.calculatedSize,
    };
}

#pragma mark - ASNetworkImage Delegate
- (void)imageNode:(ASNetworkImageNode *)imageNode didLoadImage:(UIImage *)image {
    if (image) {
        POPBasicAnimation *anim = [POPBasicAnimation animationWithPropertyNamed:kPOPViewAlpha];
        anim.fromValue = @(0.0);
        anim.toValue = @(1.0);
        [imageNode pop_removeAllAnimations];
        [imageNode pop_addAnimation:anim forKey:@"fadeIn"];
    }
}

@end
