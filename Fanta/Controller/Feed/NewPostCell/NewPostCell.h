//
//  NewPostCell.h
//  Fanta
//
//  Created by Mario Concilio on 7/16/16.
//  Copyright © 2016 Mario Concilio. All rights reserved.
//

#import <AsyncDisplayKit/AsyncDisplayKit.h>

@interface NewPostCell : ASCellNode

@property(nonatomic, weak, readonly) ASDisplayNode *frameNode;
@property(nonatomic, weak, readonly) ASTextNode *textNode;
//@property(nonatomic, weak, readonly) ASNetworkImageNode *profileImageNode;

@end
