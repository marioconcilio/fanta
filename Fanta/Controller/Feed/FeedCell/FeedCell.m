//
//  FeedCell.m
//  Fanta
//
//  Created by Mario Concilio on 19/03/15.
//  Copyright (c) 2015 Mario Concilio. All rights reserved.
//

#import "AppDelegate.h"
#import "Constants.h"
#import "FeedCell.h"
#import "Formatter.h"
#import "VOPost.h"
#import "VOUser.h"

#import <AsyncDisplayKit/ASDisplayNode+Subclasses.h>
#import <POP.h>
#import <WebASDKImageManager.h>
#import <FBShimmeringLayer.h>

@interface FeedCell() <ASNetworkImageNodeDelegate>

@property (nonatomic, weak) ASDisplayNode *frameNode;
@property (nonatomic, weak) ASNetworkImageNode *profileImageNode;
@property (nonatomic, weak) ASTextNode *contentNode;
@property (nonatomic, weak) ASTextNode *dateNode;
@property (nonatomic, weak) ASTextNode *profileNameNode;
@property (nonatomic, weak) ASNetworkImageNode *imageNode;
@property (nonatomic, weak) ASDisplayNode *imagePlaceholderNode;
@property (nonatomic, weak) ASImageNode *thumbsUpImageNode;
@property (nonatomic, weak) ASImageNode *thumbsDownImageNode;
@property (nonatomic, weak) ASImageNode *commentImageNode;
@property (nonatomic, weak) ASTextNode *thumbsUpTextNode;
@property (nonatomic, weak) ASTextNode *thumbsDownTextNode;
@property (nonatomic, weak) ASTextNode *commentTextNode;

@property (nonatomic, weak) FBShimmeringLayer *imageShimmer;

@property (nonatomic, strong) VOPost *post;

@end

@implementation FeedCell

- (instancetype)initWithPost:(VOPost *)post {
    self = [super init];
    if (self) {
        _post = post;
        
        /*
         *  Profile Image
         */
        ASNetworkImageNode *profileImageNode = [[ASNetworkImageNode alloc] initWithWebImage];
        
        //!!!: CORDEIRO verificar atributo imagem do usuario
        profileImageNode.URL = [NSURL URLWithString:post.user.thumbnail];
        profileImageNode.cornerRadius = kPostImageProfileThumbSize / 2;
        profileImageNode.clipsToBounds = YES;
        profileImageNode.borderWidth = 0.5;
        profileImageNode.borderColor = UIColorFromRGB(151, 151, 151).CGColor;
        profileImageNode.userInteractionEnabled = YES;
        profileImageNode.delegate = self;
        _profileImageNode = profileImageNode;
        [self addSubnode:_profileImageNode];
        
        /*
         *  Profile Name
         */
        ASTextNode *profileNameNode = [[ASTextNode alloc] init];
        profileNameNode.attributedString = [[NSAttributedString alloc] initWithString:post.userName
                                                                           attributes:[self profileNameStyle]];
        profileNameNode.maximumNumberOfLines = 1;
        profileNameNode.layerBacked = YES;
        _profileNameNode = profileNameNode;
        [self addSubnode:_profileNameNode];
        
        /*
         *  Date
         */
        ASTextNode *dateNode = [[ASTextNode alloc] init];
        dateNode.attributedString = [[NSAttributedString alloc] initWithString:[Formatter parseDate:post.createdAt]
                                                                    attributes:[self postDateStyle]];
        dateNode.maximumNumberOfLines = 1;
        dateNode.layerBacked = YES;
        _dateNode = dateNode;
        [self addSubnode:_dateNode];
        
        /*
         *  Frame
         */
        ASDisplayNode *frameNode = [[ASDisplayNode alloc] init];
        frameNode.backgroundColor = [UIColor whiteColor];
//        frameNode.layerBacked = YES;
        frameNode.userInteractionEnabled = YES;
        _frameNode = frameNode;
        [self addSubnode:_frameNode];
        
        /*
         *  Post Image
         */
//!!!: CORDEIRO verificar com cordeiro qual nome do atributo post image
        if (post.postImage) {
            ASNetworkImageNode *imageNode = [[ASNetworkImageNode alloc] initWithWebImage];
            imageNode.URL = [NSURL URLWithString:post.postImage];
            imageNode.contentMode = UIViewContentModeScaleAspectFill;
            imageNode.layerBacked = YES;
            imageNode.userInteractionEnabled = YES;
            imageNode.delegate = self;
            _imageNode = imageNode;
            
             [_frameNode addSubnode:_imageNode];
            
            /*
             *  image placeholder
             */
            ASDisplayNode *image = [[ASDisplayNode alloc] init];
            image.layerBacked = YES;
            image.backgroundColor = UIColorFromHEX(0xe5e5e5);
            _imagePlaceholderNode = image;
            
            FBShimmeringLayer *imageShimmer = [[FBShimmeringLayer alloc] init];
            imageShimmer.shimmering = YES;
            _imageShimmer = imageShimmer;
            
//            dispatch_async(dispatch_get_main_queue(), ^{
                imageShimmer.contentLayer = image.layer;
//                [_frameNode.layer addSublayer:imageShimmer];
//            });
        }
        
        /*
         *  Content
         */
        ASTextNode *contentNode = [[ASTextNode alloc] init];
        contentNode.attributedString = [[NSAttributedString alloc] initWithString:post.content
                                                                       attributes:[self contentStyle]];
        contentNode.additionalTruncationMessage = [[NSAttributedString alloc] initWithString:@" ver mais"
                                                                                  attributes:[self seeMoreStyle]];
        contentNode.layerBacked = YES;
        contentNode.userInteractionEnabled = YES;
        if (_imageNode) {
            contentNode.maximumNumberOfLines = kPostMaxLinesWithImage;
        }
        else {
            contentNode.maximumNumberOfLines = kPostMaxLinesWithoutImage;
        }
        _contentNode = contentNode;
        [_frameNode addSubnode:_contentNode];
        
        /*
         *  Thumbs Up
         */
        ASImageNode *thumbsUpImageNode = [[ASImageNode alloc] init];
        thumbsUpImageNode.image = [UIImage imageNamed:@"fnt_thumbs_up"];
        thumbsUpImageNode.contentMode = UIViewContentModeScaleAspectFit;
        thumbsUpImageNode.userInteractionEnabled = YES;
        thumbsUpImageNode.hitTestSlop = UIEdgeInsetsMake(0, -50, -50, 0);
        [thumbsUpImageNode addTarget:self action:@selector(doLike) forControlEvents:ASControlNodeEventTouchUpInside];
        _thumbsUpImageNode = thumbsUpImageNode;
        [self addSubnode:_thumbsUpImageNode];
        
        ASTextNode *thumbsUpTextNode = [[ASTextNode alloc] init];
//        thumbsUpTextNode.attributedString = [[NSAttributedString alloc] initWithString:[Helper formatNumber:post.likes]
//                                                                             attributes:[self buttonStyle]];
        thumbsUpTextNode.layerBacked = YES;
        _thumbsUpTextNode = thumbsUpTextNode;
        [self addSubnode:_thumbsUpTextNode];
        
        if ([self.post isLiked]) {
            // thumbs up orange
            thumbsUpImageNode.imageModificationBlock = ASImageNodeTintColorModificationBlock([UIColor customOrange]);
            thumbsUpTextNode.attributedString = [[NSAttributedString alloc] initWithString:[Formatter formatNumber:post.likes]
                                                                                attributes:[self buttonSelectedStyle]];
        }
        else {
            // thumbs up gray
            thumbsUpImageNode.imageModificationBlock = ASImageNodeTintColorModificationBlock(UIColorFromRGB(178, 178, 178));
            thumbsUpTextNode.attributedString = [[NSAttributedString alloc] initWithString:[Formatter formatNumber:post.likes]
                                                                                attributes:[self buttonStyle]];
        }
        
        /*
         *  Thumbs Down
         */
        ASImageNode *thumbsDownImageNode = [[ASImageNode alloc] init];
        thumbsDownImageNode.image = [UIImage imageNamed:@"fnt_thumbs_down"];
        thumbsDownImageNode.contentMode = UIViewContentModeScaleAspectFit;
        thumbsDownImageNode.userInteractionEnabled = YES;
        thumbsDownImageNode.hitTestSlop = UIEdgeInsetsMake(0, 0, -50, -50);
        [thumbsDownImageNode addTarget:self action:@selector(doDislike) forControlEvents:ASControlNodeEventTouchUpInside];
//        thumbsDownImageNode.imageModificationBlock = ASImageNodeTintColorModificationBlock(UIColorFromRGB(178, 178, 178));
        _thumbsDownImageNode = thumbsDownImageNode;
        [self addSubnode:_thumbsDownImageNode];
        
        ASTextNode *thumbsDownTextNode = [[ASTextNode alloc] init];
//        thumbsDownTextNode.attributedString = [[NSAttributedString alloc] initWithString:[Helper formatNumber:post.dislikes]
//                                                                               attributes:[self buttonStyle]];
        thumbsDownTextNode.layerBacked = YES;
        _thumbsDownTextNode = thumbsDownTextNode;
        [self addSubnode:_thumbsDownTextNode];
        
        if ([self.post isDisliked]) {
            // thumbs down orange
            thumbsDownImageNode.imageModificationBlock = ASImageNodeTintColorModificationBlock([UIColor customOrange]);
            thumbsDownTextNode.attributedString = [[NSAttributedString alloc] initWithString:[Formatter formatNumber:post.dislikes]
                                                                                  attributes:[self buttonSelectedStyle]];
        }
        else {
            // thumbs down gray
            thumbsDownImageNode.imageModificationBlock = ASImageNodeTintColorModificationBlock(UIColorFromRGB(178, 178, 178));
            thumbsDownTextNode.attributedString = [[NSAttributedString alloc] initWithString:[Formatter formatNumber:post.dislikes]
                                                                                  attributes:[self buttonStyle]];
        }
        
        /*
         *  Comments
         */
        ASImageNode *commentImageNode = [[ASImageNode alloc] init];
        commentImageNode.image = [UIImage imageNamed:@"fnt_comment"];
        commentImageNode.contentMode = UIViewContentModeScaleAspectFit;
        commentImageNode.imageModificationBlock = ASImageNodeTintColorModificationBlock(UIColorFromRGB(178, 178, 178));
        commentImageNode.userInteractionEnabled = YES;
        commentImageNode.hitTestSlop = UIEdgeInsetsMake(0, -50, -50, 0);
        _commentImageNode = commentImageNode;
        [self addSubnode:_commentImageNode];
        
        ASTextNode *commentTextNode = [[ASTextNode alloc] init];
        commentTextNode.attributedString = [[NSAttributedString alloc] initWithString:[Formatter formatNumber:post.comments]
                                                                           attributes:[self buttonStyle]];
        commentTextNode.layerBacked = YES;
        _commentTextNode = commentTextNode;
        [self addSubnode:_commentTextNode];
        
        self.backgroundColor = [UIColor clearColor];
        self.selectionStyle = UITableViewCellSelectionStyleNone;
    }
    
    return self;
}

- (CGSize)calculateSizeThatFits:(CGSize)constrainedSize {
    CGFloat numberOfInsets;
    CGSize imageSize;
    if (_imageNode) {
        CGFloat imageWidth = constrainedSize.width - 3 * kPostImageNodePadding;
        CGFloat imageHeight = imageWidth * kPostImageNodeHeightScale;
        imageSize = CGSizeMake(imageWidth, imageHeight);
        numberOfInsets = 7.5;
    }
    else {
        imageSize = CGSizeZero;
        numberOfInsets = 7.0;
    }
    
    CGSize contentSize = [_contentNode measure:CGSizeMake(constrainedSize.width - (4 * kPostImageNodePadding),
                                                          constrainedSize.height)];
    
    [_profileNameNode measure:CGSizeMake(constrainedSize.width - kPostImageProfileThumbSize - 3*kPostImageNodePadding,
                                         constrainedSize.height)];
    [_dateNode measure:CGSizeMake(constrainedSize.width - kPostImageProfileThumbSize - 3*kPostImageNodePadding,
                                  constrainedSize.height)];
    [_thumbsUpTextNode measure:constrainedSize];
    [_thumbsDownTextNode measure:constrainedSize];
    [_commentTextNode measure:constrainedSize];
    
    return CGSizeMake(constrainedSize.width,
                      kPostImageProfileThumbSize +
                      imageSize.height +
                      contentSize.height +
                      kPostImageButtonSize +
                      numberOfInsets*kPostImageNodePadding);
}

- (void)layout {
    [super layout];
    
    /*
     *  Profile Image
     */
    _profileImageNode.frame = (CGRect) {
        .origin.x = kPostImageNodePadding,
        .origin.y = kPostImageNodePadding,
        .size.width = kPostImageProfileThumbSize,
        .size.height = kPostImageProfileThumbSize,
    };
    _profileImageNode.hitTestSlop = UIEdgeInsetsMake(0, 0, 0, -_profileNameNode.calculatedSize.width);
    
    /*
     *  Profile Name
     */
    _profileNameNode.frame = (CGRect) {
        .origin.x = kPostImageProfileThumbSize + (2 * kPostImageNodePadding),
        .origin.y = kPostImageNodePadding,
        .size = _profileNameNode.calculatedSize,
    };
    
    /*
     *  Date
     */
    _dateNode.frame = (CGRect) {
        .origin.x = kPostImageProfileThumbSize + (2 * kPostImageNodePadding),
        .origin.y = kPostImageProfileThumbSize + kPostImageNodePadding - _dateNode.calculatedSize.height,
        .size = _dateNode.calculatedSize,
    };
    
    // check if has image
    CGSize imageSize;
    if (_imageNode) {
        CGFloat imageWidth = self.calculatedSize.width - 3 * kPostImageNodePadding;
        CGFloat imageHeight = imageWidth * kPostImageNodeHeightScale;
        imageSize = CGSizeMake(imageWidth, imageHeight);
    }
    else {
        imageSize = CGSizeZero;
    }
    
    /*
     *  Frame
     */
    CGFloat frameHeight;
    if (_imageNode) {
        frameHeight = imageSize.height + _contentNode.calculatedSize.height + 2.5 * kPostImageNodePadding;
    }
    else {
        frameHeight = _contentNode.calculatedSize.height + 2.0 * kPostImageNodePadding;
    }

    _frameNode.frame = (CGRect) {
        .origin.x = kPostImageNodePadding,
        .origin.y = kPostImageProfileThumbSize + 2 * kPostImageNodePadding,
        .size.width = self.calculatedSize.width - 2 * kPostImageNodePadding,
        .size.height = frameHeight,
    };
    
    /*
     *  Post Image
     */
    CGFloat contentPadding;
    if (_imageNode) {
        _imageNode.frame = (CGRect) {
            .origin.x = 0.5 * kPostImageNodePadding,
            .origin.y = 0.5 * kPostImageNodePadding,
            .size = imageSize,
        };
        contentPadding = 1.5;
        
        /*
         *  image placeholder
         */
//        _imagePlaceholderNode.frame = (CGRect) {
//            .size = imageSize,
//        };
        
//            _imageShimmer.frame = (CGRect) {
//                .origin.x = 0.5 * kPostImageNodePadding,
//                .origin.y = 0.5 * kPostImageNodePadding,
//                .size = imageSize,
//            };
        
    }
    else {
        contentPadding = 1.0;
    }
    
    /*
     *  Content
     */
    _contentNode.frame = (CGRect) {
        .origin.x = kPostImageNodePadding,
        .origin.y = imageSize.height + contentPadding * kPostImageNodePadding,
        .size = _contentNode.calculatedSize,
    };
    
    /*
     *  Thumbs Up
     */
    _thumbsUpImageNode.frame = (CGRect) {
        .origin.x = kPostImageNodePadding + 40,
        .origin.y = kPostImageProfileThumbSize + frameHeight + 3 * kPostImageNodePadding,
        .size.width = kPostImageButtonSize,
        .size.height = kPostImageButtonSize,
    };
    
    _thumbsUpTextNode.frame = (CGRect) {
        .origin.x = CGRectGetMinX(_thumbsUpImageNode.frame) - kPostImageNodePadding - _thumbsUpTextNode.calculatedSize.width,
        .origin.y = kPostImageProfileThumbSize + frameHeight + 3 * kPostImageNodePadding + kPostImageButtonSize/2 - _thumbsUpTextNode.calculatedSize.height/2,
        .size = _thumbsUpTextNode.calculatedSize,
    };
    
    /*
     *  Thumbs Down
     */
    _thumbsDownImageNode.frame = (CGRect) {
        .origin.x = CGRectGetMaxX(_thumbsUpImageNode.frame) + kPostImageNodePadding,
        .origin.y = kPostImageProfileThumbSize + frameHeight + 3 * kPostImageNodePadding,
        .size.width = kPostImageButtonSize,
        .size.height = kPostImageButtonSize,
    };
    
    _thumbsDownTextNode.frame = (CGRect) {
        .origin.x = CGRectGetMaxX(_thumbsDownImageNode.frame) + kPostImageNodePadding,
        .origin.y = kPostImageProfileThumbSize + frameHeight + 3 * kPostImageNodePadding + kPostImageButtonSize/2 - _thumbsDownTextNode.calculatedSize.height/2,
        .size = _thumbsDownTextNode.calculatedSize,
    };
    
    /*
     *  Comment
     */
    _commentImageNode.frame = (CGRect) {
        .origin.x = _frameNode.frame.size.width - kPostImageButtonSize,
        .origin.y = kPostImageProfileThumbSize + frameHeight + 3 * kPostImageNodePadding,
        .size.width = kPostImageButtonSize,
        .size.height = kPostImageButtonSize,
    };
    
    _commentTextNode.frame = (CGRect) {
        .origin.x = CGRectGetMinX(_commentImageNode.frame) - kPostImageNodePadding - _commentTextNode.calculatedSize.width,
        .origin.y = kPostImageProfileThumbSize + frameHeight + 3 * kPostImageNodePadding + kPostImageButtonSize/2 - _commentTextNode.calculatedSize.height/2,
        .size = _commentTextNode.calculatedSize,
    };
}

- (void)reloadThumbs {
    // if liked
    if ([self.post isLiked]) {
        // thumbs up blue
        _thumbsUpTextNode.attributedString = [[NSAttributedString alloc] initWithString:[Formatter formatNumber:self.post.likes]
                                                                             attributes:[self buttonSelectedStyle]];
        _thumbsUpImageNode.imageModificationBlock = ASImageNodeTintColorModificationBlock([UIColor customOrange]);
        
        // thumbs down gray
        _thumbsDownTextNode.attributedString = [[NSAttributedString alloc] initWithString:[Formatter formatNumber:self.post.dislikes]
                                                                               attributes:[self buttonStyle]];
        _thumbsDownImageNode.imageModificationBlock = ASImageNodeTintColorModificationBlock(UIColorFromRGB(178, 178, 178));
    }
    // if is not liked
    else {
        // thumbs up gray
        _thumbsUpTextNode.attributedString = [[NSAttributedString alloc] initWithString:[Formatter formatNumber:self.post.likes]
                                                                             attributes:[self buttonStyle]];
        _thumbsUpImageNode.imageModificationBlock = ASImageNodeTintColorModificationBlock(UIColorFromRGB(178, 178, 178));
        
        // if is disliked
        if ([self.post isDisliked]) {
            // thumbs down blue
            _thumbsDownTextNode.attributedString = [[NSAttributedString alloc] initWithString:[Formatter formatNumber:self.post.dislikes]
                                                                                   attributes:[self buttonSelectedStyle]];
            _thumbsDownImageNode.imageModificationBlock = ASImageNodeTintColorModificationBlock([UIColor customOrange]);
        }
        // if is not disliked
        else {
            // thumbs down gray
            _thumbsDownTextNode.attributedString = [[NSAttributedString alloc] initWithString:[Formatter formatNumber:self.post.dislikes]
                                                                                   attributes:[self buttonStyle]];
            _thumbsDownImageNode.imageModificationBlock = ASImageNodeTintColorModificationBlock(UIColorFromRGB(178, 178, 178));
        }
    }
 
    [_thumbsUpImageNode setNeedsDisplayWithCompletion:NULL];
    [_thumbsDownImageNode setNeedsDisplayWithCompletion:NULL];
}

#pragma mark - Actions
- (void)doLike {
    if ([self.post isDisliked]) {
        self.post.disliked = NO;
        self.post.dislikes--;
    }
    
    if ([self.post isLiked]) {
        self.post.liked = NO;
        self.post.likes--;
    }
    else {
        self.post.liked = YES;
        self.post.likes++;
    }
    
    [self reloadThumbs];
}

- (void)doDislike {
    if ([self.post isLiked]) {
        self.post.liked = NO;
        self.post.likes--;
    }
    
    if ([self.post isDisliked]) {
        self.post.disliked = NO;
        self.post.dislikes--;
    }
    else {
        self.post.disliked = YES;
        self.post.dislikes++;
    }
    
    [self reloadThumbs];
}

#pragma mark - ASNetworkImage Delegate
- (void)imageNode:(ASNetworkImageNode *)imageNode didLoadImage:(UIImage *)image {
    if (image) {
        POPBasicAnimation *anim = [POPBasicAnimation animationWithPropertyNamed:kPOPViewAlpha];
        anim.fromValue = @(0.0);
        anim.toValue = @(1.0);
        [imageNode pop_removeAllAnimations];
        [imageNode pop_addAnimation:anim forKey:@"fadeIn"];
    }
}

#pragma mark - Text Styles
- (NSDictionary *)profileNameStyle {
    return @{NSFontAttributeName: [UIFont semiboldFontWithSize:14.0],
             NSForegroundColorAttributeName: [UIColor whiteColor]};
}

- (NSDictionary *)postDateStyle {
    return @{NSFontAttributeName: [UIFont boldFontWithSize:12.0],
             NSForegroundColorAttributeName: UIColorFromRGB(178, 178, 178)};
}

- (NSDictionary *)seeMoreStyle {
    return @{NSFontAttributeName: [UIFont semiboldFontWithSize:16.0],
             NSForegroundColorAttributeName: [UIColor blackColor]};
}

- (NSDictionary *)contentStyle {
    return @{NSFontAttributeName: [UIFont lightFontWithSize:16.0],
             NSForegroundColorAttributeName: [UIColor blackColor]};
}

- (NSDictionary *)buttonStyle {
    return @{NSFontAttributeName: [UIFont lightFontWithSize:15.0],
             NSForegroundColorAttributeName: UIColorFromRGB(178, 178, 178)};
}

- (NSDictionary *)buttonSelectedStyle {
    return @{NSFontAttributeName: [UIFont lightFontWithSize:15.0],
             NSForegroundColorAttributeName: [UIColor customOrange]};
}

@end
