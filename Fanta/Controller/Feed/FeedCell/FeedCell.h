//
//  FeedCell.h
//  Fanta
//
//  Created by Mario Concilio on 19/03/15.
//  Copyright (c) 2015 Mario Concilio. All rights reserved.
//

#import "ASCellNode.h"
#import <AsyncDisplayKit.h>

@class VOPost;
@interface FeedCell : ASCellNode

@property(nonatomic, weak, readonly) ASDisplayNode *frameNode;
@property(nonatomic, weak, readonly) ASNetworkImageNode *profileImageNode;
@property(nonatomic, weak, readonly) ASTextNode *contentNode;
@property(nonatomic, weak, readonly) ASTextNode *dateNode;
@property(nonatomic, weak, readonly) ASTextNode *profileNameNode;
@property(nonatomic, weak, readonly) ASNetworkImageNode *imageNode;
@property(nonatomic, weak, readonly) ASImageNode *thumbsUpImageNode;
@property(nonatomic, weak, readonly) ASImageNode *thumbsDownImageNode;
@property(nonatomic, weak, readonly) ASImageNode *commentImageNode;
@property(nonatomic, weak, readonly) ASTextNode *thumbsUpTextNode;
@property(nonatomic, weak, readonly) ASTextNode *thumbsDownTextNode;
@property(nonatomic, weak, readonly) ASTextNode *commentTextNode;

- (instancetype)initWithPost:(VOPost *)post;
- (void)reloadThumbs;

@end
