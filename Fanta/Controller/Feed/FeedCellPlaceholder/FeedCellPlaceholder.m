//
//  FeedCellPlaceholder.m
//  Fanta
//
//  Created by Mario Concilio on 4/8/16.
//  Copyright © 2016 Mario Concilio. All rights reserved.
//

#import "FeedCellPlaceholder.h"
#import "Constants.h"

#import <FBShimmeringView.h>
#import <FBShimmeringLayer.h>

@interface FeedCellPlaceholder ()

@property (nonatomic, weak) ASDisplayNode *thumbNode;
@property (nonatomic, weak) ASDisplayNode *nameNode;
@property (nonatomic, weak) ASDisplayNode *dateNode;
@property (nonatomic, weak) ASDisplayNode *frameNode;
@property (nonatomic, weak) ASDisplayNode *imageNode;
@property (nonatomic, weak) ASDisplayNode *line1Node;
@property (nonatomic, weak) ASDisplayNode *line2Node;
@property (nonatomic, weak) ASDisplayNode *line3Node;
@property (nonatomic, weak) ASImageNode *thumbsUpImageNode;
@property (nonatomic, weak) ASImageNode *thumbsDownImageNode;
@property (nonatomic, weak) ASImageNode *commentImageNode;

@property (nonatomic, weak) FBShimmeringLayer *thumbShimmer;
@property (nonatomic, weak) FBShimmeringLayer *nameShimmer;
@property (nonatomic, weak) FBShimmeringLayer *dateShimmer;
@property (nonatomic, weak) FBShimmeringLayer *imageShimmer;
@property (nonatomic, weak) FBShimmeringLayer *line1Shimmer;
@property (nonatomic, weak) FBShimmeringLayer *line2Shimmer;
@property (nonatomic, weak) FBShimmeringLayer *line3Shimmer;

@end

@implementation FeedCellPlaceholder

static CGFloat lineHeight = 15.15;

- (instancetype)init {
    self = [super init];
    if (self) {
        /*
         * thumb
         */
        ASDisplayNode *thumb = [[ASDisplayNode alloc] init];
        thumb.layerBacked = YES;
        thumb.cornerRadius = kPostImageProfileThumbSize / 2;
        thumb.clipsToBounds = YES;
        thumb.backgroundColor = UIColorFromHEX(0x3f3f3f);
        _thumbNode = thumb;
        
        FBShimmeringLayer *thumbShimmer = [[FBShimmeringLayer alloc] init];
        thumbShimmer.shimmering = YES;
        _thumbShimmer = thumbShimmer;
        
        /*
         *  name
         */
        ASDisplayNode *name = [[ASDisplayNode alloc] init];
        name.layerBacked = YES;
        name.backgroundColor = UIColorFromHEX(0x3f3f3f);
        _nameNode = name;
        
        FBShimmeringLayer *nameShimmer = [[FBShimmeringLayer alloc] init];
        nameShimmer.shimmering = YES;
        _nameShimmer = nameShimmer;
        
        /*
         *  date
         */
        ASDisplayNode *date = [[ASDisplayNode alloc] init];
        date.layerBacked = YES;
        date.backgroundColor = UIColorFromHEX(0x3f3f3f);
        _dateNode = date;
        
        FBShimmeringLayer *dateShimmer = [[FBShimmeringLayer alloc] init];
        dateShimmer.shimmering = YES;
        _dateShimmer = dateShimmer;
        
        /*
         *  frame
         */
        ASDisplayNode *frame = [[ASDisplayNode alloc] init];
        frame.layerBacked = YES;
        frame.backgroundColor = [UIColor whiteColor];
        _frameNode = frame;
        [self addSubnode:_frameNode];
        
        /*
         *  image
         */
        ASDisplayNode *image = [[ASDisplayNode alloc] init];
        image.layerBacked = YES;
        image.backgroundColor = UIColorFromHEX(0xe5e5e5);
        _imageNode = image;
        
        FBShimmeringLayer *imageShimmer = [[FBShimmeringLayer alloc] init];
        imageShimmer.shimmering = YES;
        _imageShimmer = imageShimmer;
        
        /*
         *  line1
         */
        ASDisplayNode *line1 = [[ASDisplayNode alloc] init];
        line1.layerBacked = YES;
        line1.backgroundColor = UIColorFromHEX(0xe5e5e5);
        _line1Node = line1;
        
        FBShimmeringLayer *line1Shimmer = [[FBShimmeringLayer alloc] init];
        line1Shimmer.shimmering = YES;
        _line1Shimmer = line1Shimmer;
        
        /*
         *  line2
         */
        ASDisplayNode *line2 = [[ASDisplayNode alloc] init];
        line2.layerBacked = YES;
        line2.backgroundColor = UIColorFromHEX(0xe5e5e5);
        _line2Node = line2;
        
        FBShimmeringLayer *line2Shimmer = [[FBShimmeringLayer alloc] init];
        line2Shimmer.shimmering = YES;
        _line2Shimmer = line2Shimmer;
        
        /*
         *  line3
         */
        ASDisplayNode *line3 = [[ASDisplayNode alloc] init];
        line3.layerBacked = YES;
        line3.backgroundColor = UIColorFromHEX(0xe5e5e5);
        _line3Node = line3;
        
        FBShimmeringLayer *line3Shimmer = [[FBShimmeringLayer alloc] init];
        line3Shimmer.shimmering = YES;
        _line3Shimmer = line3Shimmer;
        
        /*
         *  Thumbs Up
         */
        ASImageNode *thumbsUpImageNode = [[ASImageNode alloc] init];
        thumbsUpImageNode.layerBacked = YES;
        thumbsUpImageNode.image = [UIImage imageNamed:@"fnt_thumbs_up"];
        thumbsUpImageNode.contentMode = UIViewContentModeScaleAspectFit;
        thumbsUpImageNode.imageModificationBlock = ASImageNodeTintColorModificationBlock(UIColorFromRGB(178, 178, 178));
        _thumbsUpImageNode = thumbsUpImageNode;
        [self addSubnode:_thumbsUpImageNode];
        
        /*
         *  Thumbs Down
         */
        ASImageNode *thumbsDownImageNode = [[ASImageNode alloc] init];
        thumbsDownImageNode.layerBacked = YES;
        thumbsDownImageNode.image = [UIImage imageNamed:@"fnt_thumbs_down"];
        thumbsDownImageNode.contentMode = UIViewContentModeScaleAspectFit;
        thumbsDownImageNode.imageModificationBlock = ASImageNodeTintColorModificationBlock(UIColorFromRGB(178, 178, 178));
        _thumbsDownImageNode = thumbsDownImageNode;
        [self addSubnode:_thumbsDownImageNode];
        
        /*
         *  Comments
         */
        ASImageNode *commentImageNode = [[ASImageNode alloc] init];
        commentImageNode.layerBacked = YES;
        commentImageNode.image = [UIImage imageNamed:@"fnt_comment"];
        commentImageNode.contentMode = UIViewContentModeScaleAspectFit;
        commentImageNode.imageModificationBlock = ASImageNodeTintColorModificationBlock(UIColorFromRGB(178, 178, 178));
        _commentImageNode = commentImageNode;
        [self addSubnode:_commentImageNode];
        
        dispatch_async(dispatch_get_main_queue(), ^{
            thumbShimmer.contentLayer = thumb.layer;
            [self.layer addSublayer:thumbShimmer];
            
            nameShimmer.contentLayer = name.layer;
            [self.layer addSublayer:nameShimmer];
            
            dateShimmer.contentLayer = date.layer;
            [self.layer addSublayer:dateShimmer];

            imageShimmer.contentLayer = image.layer;
            [_frameNode.layer addSublayer:imageShimmer];

            line1Shimmer.contentLayer = line1.layer;
            [_frameNode.layer addSublayer:line1Shimmer];

            line2Shimmer.contentLayer = line2.layer;
            [_frameNode.layer addSublayer:line2Shimmer];

            line3Shimmer.contentLayer = line3.layer;
            [_frameNode.layer addSublayer:line3Shimmer];
        });
        
    }
    
    return self;
}

- (CGSize)calculateSizeThatFits:(CGSize)constrainedSize {
    CGFloat frameHeight = 4*lineHeight + 3.5*kPostImageNodePadding;
    CGFloat footerHeight = 20.0 + 1*kPostImageNodePadding;
    CGFloat imageWidth = constrainedSize.width - 3 * kPostImageNodePadding;
    CGFloat imageHeight = imageWidth * kPostImageNodeHeightScale;
    
    return CGSizeMake(constrainedSize.width,
                      frameHeight + footerHeight + imageHeight + kPostImageProfileThumbSize + 3*kPostImageNodePadding);
}

- (void)layout {
    [super layout];
    
    /*
     *  thumb
     */
    _thumbNode.frame = (CGRect) {
        .size.width = kPostImageProfileThumbSize,
        .size.height = kPostImageProfileThumbSize,
    };
    
    _thumbShimmer.frame = (CGRect) {
        .origin.x = kPostImageNodePadding,
        .origin.y = kPostImageNodePadding,
        .size.width = kPostImageProfileThumbSize,
        .size.height = kPostImageProfileThumbSize,
    };
    
    _thumbShimmer.shimmeringSpeed = CGRectGetWidth(_thumbShimmer.frame)*kShimmerSpeedRate;
    
    /*
     *  name
     */
    _nameNode.frame = (CGRect) {
        .size.width = 120.5,
        .size.height = 17.0,
    };
    
    _nameShimmer.frame = (CGRect) {
        .origin.x = kPostImageProfileThumbSize + (2 * kPostImageNodePadding),
        .origin.y = kPostImageNodePadding,
        .size.width = 120.5,
        .size.height = 17.0,
    };
    
    _nameShimmer.shimmeringSpeed = CGRectGetWidth(_nameShimmer.frame)*kShimmerSpeedRate;
    
    /*
     *  date
     */
    _dateNode.frame = (CGRect) {
        .size.width = 164.5,
        .size.height = 15.5,
    };
    
    _dateShimmer.frame = (CGRect) {
        .origin.x = kPostImageProfileThumbSize + (2 * kPostImageNodePadding),
        .origin.y = kPostImageProfileThumbSize + kPostImageNodePadding - 15.5,
        .size.width = 164.5,
        .size.height = 15.5,
    };
    
    _dateShimmer.shimmeringSpeed = CGRectGetWidth(_dateShimmer.frame)*kShimmerSpeedRate;
    
    /*
     *  frame
     */
    CGFloat imageWidth = self.calculatedSize.width - 3 * kPostImageNodePadding;
    CGFloat imageHeight = imageWidth * kPostImageNodeHeightScale;
    _frameNode.frame = (CGRect) {
        .origin.x = kPostImageNodePadding,
        .origin.y = kPostImageProfileThumbSize + 2 * kPostImageNodePadding,
        .size.width = self.calculatedSize.width - 2 * kPostImageNodePadding,
        .size.height = imageHeight + 4*lineHeight + 2.5*kPostImageNodePadding
    };
    
    /*
     *  image
     */
    _imageNode.frame = (CGRect) {
        .size.width = imageWidth,
        .size.height = imageHeight,
    };
    
    _imageShimmer.frame = (CGRect) {
        .origin.x = 0.5 * kPostImageNodePadding,
        .origin.y = 0.5 * kPostImageNodePadding,
        .size.width = imageWidth,
        .size.height = imageHeight,
    };
    
    _imageShimmer.shimmeringSpeed = CGRectGetWidth(_imageShimmer.frame)*kShimmerSpeedRate;
    
    /*
     *  line1
     */
    _line1Node.frame = (CGRect) {
        .size.width = (CGRectGetWidth(_frameNode.frame) - 2*kPostImageNodePadding) * 1.0,
        .size.height = lineHeight,
    };
    
    _line1Shimmer.frame = (CGRect) {
        .origin.x = kPostImageNodePadding,
//        .origin.y = kPostImageNodePadding,
        .origin.y = imageHeight + 1.5*kPostImageNodePadding,
        .size.width = (CGRectGetWidth(_frameNode.frame) - 2*kPostImageNodePadding) * 1.0,
        .size.height = lineHeight,
    };
    
    _line1Shimmer.shimmering = CGRectGetWidth(_line1Shimmer.frame)*kShimmerSpeedRate;
    
    /*
     *  line2
     */
    _line2Node.frame = (CGRect) {
        .size.width = (CGRectGetWidth(_frameNode.frame) - 2*kPostImageNodePadding) * 0.85,
        .size.height = lineHeight,
    };
    
    _line2Shimmer.frame = (CGRect) {
        .origin.x = kPostImageNodePadding,
        .origin.y = kPostImageNodePadding / 2 + CGRectGetMaxY(_line1Shimmer.frame),
        .size.width = (CGRectGetWidth(_frameNode.frame) - 2*kPostImageNodePadding) * 0.85,
        .size.height = lineHeight,
    };
    
    _line2Shimmer.shimmering = CGRectGetWidth(_line2Shimmer.frame)*kShimmerSpeedRate;
    
    /*
     *  line3
     */
    _line3Node.frame = (CGRect) {
        .size.width = (CGRectGetWidth(_frameNode.frame) - 2*kPostImageNodePadding) * 0.80,
        .size.height = lineHeight,
    };
    
    _line3Shimmer.frame = (CGRect) {
        .origin.x = kPostImageNodePadding,
        .origin.y = kPostImageNodePadding / 2 + CGRectGetMaxY(_line2Shimmer.frame),
        .size.width = (CGRectGetWidth(_frameNode.frame) - 2*kPostImageNodePadding) * 0.80,
        .size.height = lineHeight,
    };
    
    _line3Shimmer.shimmering = CGRectGetWidth(_line3Shimmer.frame)*kShimmerSpeedRate;
    
    /*
     *  Thumbs Up
     */
    _thumbsUpImageNode.frame = (CGRect) {
        .origin.x = kPostImageNodePadding + 40,
        .origin.y = kPostImageProfileThumbSize + CGRectGetHeight(_frameNode.frame) + 3 * kPostImageNodePadding,
        .size.width = kPostImageButtonSize,
        .size.height = kPostImageButtonSize,
    };
    
    /*
     *  Thumbs Down
     */
    _thumbsDownImageNode.frame = (CGRect) {
        .origin.x = CGRectGetMaxX(_thumbsUpImageNode.frame) + kPostImageNodePadding,
        .origin.y = kPostImageProfileThumbSize + CGRectGetHeight(_frameNode.frame) + 3 * kPostImageNodePadding,
        .size.width = kPostImageButtonSize,
        .size.height = kPostImageButtonSize,
    };
    
    /*
     *  Comment
     */
    _commentImageNode.frame = (CGRect) {
        .origin.x = _frameNode.frame.size.width - kPostImageButtonSize,
        .origin.y = kPostImageProfileThumbSize + CGRectGetHeight(_frameNode.frame) + 3 * kPostImageNodePadding,
        .size.width = kPostImageButtonSize,
        .size.height = kPostImageButtonSize,
    };
}

@end
