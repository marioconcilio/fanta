//
//  FeedCellPlaceholder.h
//  Fanta
//
//  Created by Mario Concilio on 4/8/16.
//  Copyright © 2016 Mario Concilio. All rights reserved.
//

#import <AsyncDisplayKit/AsyncDisplayKit.h>

@interface FeedCellPlaceholder : ASDisplayNode

@end
