//
//  FeedPlaceholder.m
//  Fanta
//
//  Created by Mario Concilio on 4/11/16.
//  Copyright © 2016 Mario Concilio. All rights reserved.
//

#import "FeedPlaceholder.h"
#import "FeedCellPlaceholder.h"

@implementation FeedPlaceholder

- (instancetype)initWithNumberOfRows:(NSUInteger)number {
    self = [super init];
    if (self) {
        self.view.bounces = YES;
        self.view.alwaysBounceVertical = YES;
        
        [self setupPlaceholdersCell:number];
    }
    
    return self;
}

- (void)setupPlaceholdersCell:(NSUInteger)number {
    CGFloat lastMaxY = 0.0;
    
    for (long i=0; i<number; i++) {
        @autoreleasepool {
            FeedCellPlaceholder *placeholder = [[FeedCellPlaceholder alloc] init];
            [placeholder measure:CGSizeMake(UIKeyWindowWidth(), FLT_MAX)];
            placeholder.frame = (CGRect) {
                .size = placeholder.calculatedSize,
                .origin.y = lastMaxY,
            };
            
            lastMaxY = CGRectGetMaxY(placeholder.frame);
            [self addSubnode:placeholder];
        }
    }
    
//    self.view.contentSize = CGSizeMake(UIKeyWindowWidth, lastMaxY);
}

@end
