//
//  KeyboardController.h
//  Fanta
//
//  Created by Mario Concilio on 30/04/15.
//  Copyright (c) 2015 Mario Concilio. All rights reserved.
//

#import <Foundation/Foundation.h>

@class KeyboardController;
@protocol KeyboardDelegate <NSObject>

@optional
- (void)keyboardWillChangeFrame:(CGSize)keyboardSize;
- (void)keyboardDidChangeFrame:(CGSize)keyboardSize;
- (void)keyboardWillShow:(CGSize)keyboardSize;
- (void)keyboardDidShow:(CGSize)keyboardSize;
- (void)keyboardWillHide:(CGSize)keyboardSize;
- (void)keyboardDidHide:(CGSize)keyboardSize;

@end

/**
 *  Controller for managing view`s frame responding to keyboard.
 *  Should registerForKeyboardNotifications on viewWillAppear and deregisterFromKeyboardNotifications on viewWillDisappear
 */
@interface KeyboardController : NSObject

/// Default set to YES
@property (nonatomic, assign) BOOL dismissOnTap;

/// If NO view will slide up. Default set to YES
@property (nonatomic, assign) BOOL shouldResizeView;

@property (nonatomic, assign) CGFloat margin;

@property (nonatomic, weak) id<KeyboardDelegate> delegate;

- (instancetype)initWithView:(UIView *)view andFrame:(CGRect)frame;
- (void)registerForKeyboardNotifications;
- (void)deregisterFromKeyboardNotifications;

@end
