//
//  MessagesViewController.h
//  Fanta
//
//  Created by Mario Concilio on 9/22/15.
//  Copyright (c) 2015 Mario Concilio. All rights reserved.
//

#import <JSQMessagesViewController.h>

@interface MessagesViewController : JSQMessagesViewController

//@property (nonatomic, assign) IBInspectable BOOL hideNavigationBar;

//@property (nonatomic, strong) NSMutableArray *users;
- (void)setUsers:(NSArray *)users;

@end
