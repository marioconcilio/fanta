//
//  MessagesViewController.m
//  Fanta
//
//  Created by Mario Concilio on 9/22/15.
//  Copyright (c) 2015 Mario Concilio. All rights reserved.
//

#import "APIService.h"
#import "Helper.h"
#import "UserService.h"
#import "VOUser.h"
#import "MessagesViewController.h"
#import "MessagesDataController.h"
#import "Constants.h"


#import <JSQMessages.h>
#import <UIImageView+AFNetworking.h>
#import <VBFPopFlatButton.h>

@interface MessagesViewController () <JSQMessagesComposerTextViewPasteDelegate>

//@property (strong, nonatomic) NSMutableArray *messages;
//@property (strong, nonatomic) NSMutableArray *avatars;
@property (strong, nonatomic) JSQMessagesBubbleImage *outgoingBubbleImageData;
@property (strong, nonatomic) JSQMessagesBubbleImage *incomingBubbleImageData;
//@property (strong, nonatomic) JSQMessagesAvatarImage *meAvatar;
//@property (strong, nonatomic) JSQMessagesAvatarImage *userAvatar;
@property (strong, nonatomic) MessagesDataController *dataController;
//@property (strong, nonatomic) UILabel *titleLabel;
@property (strong, nonatomic) UIImageView *thumbnail;

@end

@implementation MessagesViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
//    self.title = @"Mensagens";
//    self.messages = [NSMutableArray array];
//    self.collectionView.backgroundColor = [UIColor customLightBackground];
    self.collectionView.collectionViewLayout.messageBubbleFont = [UIFont lightFontWithSize:16.0];
    
//    self.titleLabel.tintColor = [UIColor customDarkBackground];
    
    /**
     *  You MUST set your senderId and display name
     */
    VOUser *me = [UserService loadUser];
    self.senderId = [NSString stringWithFormat:@"%lu", (unsigned long)me.userID];
    self.senderDisplayName = me.name;
    
    self.inputToolbar.contentView.textView.pasteDelegate = self;
    self.inputToolbar.contentView.textView.placeHolder = @"Nova Mensagem";
//    [self.inputToolbar.contentView.rightBarButtonItem setTitle:@"Enviar" forState:UIControlStateNormal];
    self.inputToolbar.contentView.rightBarButtonItem.titleLabel.font = [UIFont semiboldFontWithSize:16.0];
    self.inputToolbar.contentView.rightBarButtonItem.tintColor = [UIColor customPink];
    self.inputToolbar.contentView.textView.font = [UIFont lightFontWithSize:16.0];
    
    /**
     *  Create message bubble images objects.
     */
    JSQMessagesBubbleImageFactory *bubbleFactory = [[JSQMessagesBubbleImageFactory alloc] init];
    self.outgoingBubbleImageData = [bubbleFactory outgoingMessagesBubbleImageWithColor:[UIColor customLightBackground]];
    self.incomingBubbleImageData = [bubbleFactory incomingMessagesBubbleImageWithColor:[UIColor customOrange]];
    
//    self.dataController = [[MessagesDataController alloc] initWithUsers:self.users];
    
    /**
     *  Setting up avatars
     *
    if (me.thumbnail) {
        [APIService imageFromURL:[NSURL URLWithString:me.thumbnail] callback:^(UIImage *image, NSError *error) {
            if (!error) {
                self.meAvatar = [JSQMessagesAvatarImageFactory avatarImageWithImage:image
                                                                           diameter:kJSQMessagesCollectionViewAvatarSizeDefault];
            }
            else {
                self.meAvatar = [JSQMessagesAvatarImageFactory avatarImageWithUserInitials:[Helper initialsFromName:me.name]
                                                                           backgroundColor:[UIColor randomColor]
                                                                                 textColor:[UIColor whiteColor]
                                                                                      font:[UIFont lightFontWithSize:13.0]
                                                                                  diameter:kJSQMessagesCollectionViewAvatarSizeDefault];
            }
        }];
    }
    else {
        self.meAvatar = [JSQMessagesAvatarImageFactory avatarImageWithUserInitials:[Helper initialsFromName:me.name]
                                                                   backgroundColor:[UIColor randomColor]
                                                                         textColor:[UIColor whiteColor]
                                                                              font:[UIFont lightFontWithSize:13.0]
                                                                          diameter:kJSQMessagesCollectionViewAvatarSizeDefault];
    }
    
    
    if (self.user.thumbnail) {
#warning DEBUG ONLY fake network image
        NSURL *url = [[NSBundle mainBundle] URLForResource:self.user.thumbnail withExtension:@"jpg"];
        [APIService imageFromURL:url callback:^(UIImage *image, NSError *error) {
            if (!error) {
                self.userAvatar = [JSQMessagesAvatarImageFactory avatarImageWithImage:image
                                                                             diameter:kJSQMessagesCollectionViewAvatarSizeDefault];
            }
            else {
                self.userAvatar = [JSQMessagesAvatarImageFactory avatarImageWithUserInitials:[Helper initialsFromName:self.user.name]
                                                                             backgroundColor:[UIColor randomColor]
                                                                                   textColor:[UIColor whiteColor]
                                                                                        font:[UIFont lightFontWithSize:13.0]
                                                                                    diameter:kJSQMessagesCollectionViewAvatarSizeDefault];
            }
        }];
    }
    else {
        
        self.userAvatar = [JSQMessagesAvatarImageFactory avatarImageWithUserInitials:[Helper initialsFromName:self.user.name]
                                                                     backgroundColor:[UIColor randomColor]
                                                                           textColor:[UIColor whiteColor]
                                                                                font:[UIFont lightFontWithSize:13.0]
                                                                            diameter:kJSQMessagesCollectionViewAvatarSizeDefault];
    }
    */
    
    /**
     *  Load up our fake data for the demo
     */
//    self.demoData = [[DemoModelData alloc] init];
    
    /**
     *  You can set custom avatar sizes
     *
    if (![NSUserDefaults incomingAvatarSetting]) {
        self.collectionView.collectionViewLayout.incomingAvatarViewSize = CGSizeZero;
    }
    
    if (![NSUserDefaults outgoingAvatarSetting]) {
        self.collectionView.collectionViewLayout.outgoingAvatarViewSize = CGSizeZero;
    }
     */
    
    self.showLoadEarlierMessagesHeader = NO;
//    self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithImage:[UIImage jsq_defaultTypingIndicatorImage]
//                                                                              style:UIBarButtonItemStylePlain
//                                                                             target:self
//                                                                             action:@selector(receiveMessagePressed:)];
    
    UIButton *testButton = [UIButton buttonWithType:UIButtonTypeCustom];
    testButton.frame = CGRectMake(8.0, 72.0, 44.0, 44.0);
    [testButton setImage:[UIImage jsq_defaultTypingIndicatorImage] forState:UIControlStateNormal];
    [testButton addTarget:self action:@selector(receiveMessagePressed:) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:testButton];
    
    /**
     *  Register custom menu actions for cells.
     *
    [JSQMessagesCollectionViewCell registerMenuAction:@selector(customAction:)];
    [UIMenuController sharedMenuController].menuItems = @[ [[UIMenuItem alloc] initWithTitle:@"Custom Action"
                                                                                      action:@selector(customAction:)] ];
     */
    
    /**
     *  OPT-IN: allow cells to be deleted
     */
    [JSQMessagesCollectionViewCell registerMenuAction:@selector(delete:)];
    
    /**
     *  Customize your toolbar buttons
     *
     *  self.inputToolbar.contentView.leftBarButtonItem = custom button or nil to remove
     *  self.inputToolbar.contentView.rightBarButtonItem = custom button or nil to remove
     */
    self.inputToolbar.contentView.leftBarButtonItem = nil;
    
    /**
     *  Set a maximum height for the input toolbar
     *
     *  self.inputToolbar.maximumHeight = 150;
     */
    
    /**
     * NAVBAR
     */
    [self.navigationController.navigationBar setBarStyle:UIBarStyleDefault];
    [self.navigationController.navigationBar setTintColor:[UIColor customPink]];
    [self.navigationController.navigationBar setTitleTextAttributes:@{NSFontAttributeName:[UIFont titleFontWithSize:16.f],
                                                                      NSForegroundColorAttributeName:[UIColor customDarkBackground]}];
    
    VBFPopFlatButton *backButton = [[VBFPopFlatButton alloc] initWithFrame:(CGRect) {
        .origin.x = 10,
        .origin.y = (kBlurNavbarHeight - kVBFButtonSize.height) / 2,
        .size = kVBFButtonSize,
    }
                                                                  buttonType:buttonBackType
                                                                 buttonStyle:buttonPlainStyle
                                                       animateToInitialState:NO];
    backButton.lineThickness = 1.0;
    backButton.tintColor = [UIColor customPink];
    [backButton addTarget:self action:@selector(dismiss) forControlEvents:UIControlEventTouchUpInside];
    self.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc] initWithCustomView:backButton];
    self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithCustomView:self.thumbnail];
    
    /*
    if (self.hideNavigationBar) {
        CGFloat h = CGRectGetHeight([UIApplication sharedApplication].statusBarFrame);
        UIBlurEffect *blurEffect = [UIBlurEffect effectWithStyle:UIBlurEffectStyleLight];
        UIVisualEffectView *navbar = [[UIVisualEffectView alloc] initWithEffect:blurEffect];
        navbar.frame = CGRectMake(0.0, 0.0, UIKeyWindowWidth, kBlurNavbarHeight + h);
        navbar.layer.borderWidth = 0.5;
        navbar.layer.borderColor = [UIColor lightGrayColor].CGColor;
        [self.view addSubview:navbar];
        self.topContentAdditionalInset = kBlurNavbarHeight + h;
        
        [navbar addSubview:self.titleLabel];
        [navbar addSubview:self.thumbnail];
        
        VBFPopFlatButton *closeButton = [[VBFPopFlatButton alloc] initWithFrame:(CGRect) {
            .origin.x = 10,
            .origin.y = h + ((kBlurNavbarHeight - kVBFButtonSize.height) / 2),
            .size = kVBFButtonSize,
        }
                                                                     buttonType:buttonBackType
                                                                    buttonStyle:buttonPlainStyle
                                                          animateToInitialState:NO];
        closeButton.lineThickness = 1.0;
        closeButton.tintColor = [UIColor customPink];
        [closeButton addTarget:self action:@selector(dismiss) forControlEvents:UIControlEventTouchUpInside];
        [navbar addSubview:closeButton];
    }
    */
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleDefault animated:YES];
    
//    if (self.hideNavigationBar) {
//        [self.navigationController setNavigationBarHidden:YES animated:YES];
//    }
}

- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
}

- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
    [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleLightContent animated:YES];
    
//    if (self.hideNavigationBar) {
//        [self.navigationController setNavigationBarHidden:NO animated:YES];
//    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Public Methods
- (void)setUsers:(NSArray *)users {
    self.dataController = [[MessagesDataController alloc] initWithUsers:users];
    if (users.count == 1) {
        VOUser *user = users[0];
        self.navigationItem.title = [user.name lowercaseString];
        
        [Helper avatarFromName:user.name font:[UIFont lightFontWithSize:13.0] diameter:kVBFButtonSize.width callback:^(UIImage *image) {
            if (user.thumbnail) {
                NSURL *url = [NSURL URLWithString:user.thumbnail];
                [self.thumbnail setImageWithURL:url placeholderImage:image];
            }
            else {
                self.thumbnail.image = image;
            }
        }];
    }
}

#pragma mark - Getters 
/*
- (UILabel *)titleLabel {
    if (!_titleLabel) {
        UILabel *label = [[UILabel alloc] initWithFrame:CGRectMake(18.0 + kVBFButtonSize.width,
                                                                   31.0,
                                                                   UIKeyWindowWidth - 2*(18.0 + kVBFButtonSize.width),
                                                                   21.0)];
        label.font = [UIFont titleFontWithSize:16.0];
        label.textAlignment = NSTextAlignmentCenter;
        _titleLabel = label;
    }
    
    return _titleLabel;
}
 */

- (UIImageView *)thumbnail {
    if (!_thumbnail) {
        CGSize size = CGSizeMake(kBlurNavbarHeight - 8.0, kBlurNavbarHeight - 8.0);
        CGFloat h = CGRectGetHeight([UIApplication sharedApplication].statusBarFrame);
        UIImageView *thumbnail = [[UIImageView alloc] initWithFrame:CGRectMake(UIKeyWindowWidth() - size.width - 5.0,
                                                                               h + 5.0,
                                                                               size.width,
                                                                               size.height)];
        thumbnail.clipsToBounds = YES;
        thumbnail.layer.cornerRadius = size.width/2;
        thumbnail.layer.borderColor = [UIColor lightGrayColor].CGColor;
        thumbnail.layer.borderWidth = 0.5;
        
        UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(doOpenProfileImage)];
        [thumbnail addGestureRecognizer:tap];
        thumbnail.userInteractionEnabled = YES;
        
        _thumbnail = thumbnail;
    }
    
    return _thumbnail;
}

#pragma mark - Actions
- (void)dismiss {
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)doOpenProfileImage {
    /*
    [ImageViewController presentInViewController:self withImage:self.thumbnail.image];
    
    NSURL *url = [NSURL URLWithString:self.post.postImage];
    IDMPhotoBrowser *browser = [[IDMPhotoBrowser alloc] initWithPhotoURLs:@[url]];
    browser.usePopAnimation = YES;
    browser.forceHideStatusBar = YES;
    browser.displayDoneButton = NO;
    browser.trackTintColor = [UIColor whiteColor];
    [browser setInitialPageIndex:0];
    
    [[UIToolbar appearance] setTintColor:[UIColor whiteColor]];
    
    [self presentViewController:browser animated:YES completion:NULL];
     */
}

#warning DEBUG ONLY
- (void)receiveMessagePressed:(UIBarButtonItem *)sender {
    /**
     *  DEMO ONLY
     *
     *  The following is simply to simulate received messages for the demo.
     *  Do not actually do this.
     */
    
    
    /**
     *  Show the typing indicator to be shown
     */
    self.showTypingIndicator = !self.showTypingIndicator;
    
    /**
     *  Scroll to actually view the indicator
     */
    [self scrollToBottomAnimated:YES];
    
    /**
     *  Copy last sent message, this will be the new "received" message
     */
    JSQMessage *copyMessage = [[self.dataController.messages lastObject] copy];
    
    if (!copyMessage) {
        copyMessage = [JSQMessage messageWithSenderId:self.senderId
                                          displayName:@"name"
                                                 text:@"First received!"];
    }
    
    /**
     *  Allow typing indicator to show
     */
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(1.0 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        
        NSArray *userIds = [self.dataController.avatars allKeys];
//        [userIds removeObject:self.senderId];
//        NSString *randomUserId = userIds[arc4random_uniform((int)[userIds count])];
        NSUInteger randomUserId = [userIds[arc4random_uniform((int)[userIds count])] intValue];
        
        VOUser *user = [self.dataController userFromID:randomUserId];

        JSQMessage *newMessage = [JSQMessage messageWithSenderId:[NSString stringWithFormat:@"%lu", (unsigned long)randomUserId]
                                                     displayName:user.name
                                                            text:copyMessage.text];
        
        /**
         *  Upon receiving a message, you should:
         *
         *  1. Play sound (optional)
         *  2. Add new id<JSQMessageData> object to your data source
         *  3. Call `finishReceivingMessage`
         */
        [JSQSystemSoundPlayer jsq_playMessageReceivedSound];
        [self.dataController.messages addObject:newMessage];
        [self finishReceivingMessageAnimated:YES];
        
    });
}

#pragma mark - JSQMessagesViewController method overrides
- (void)didPressSendButton:(UIButton *)button
           withMessageText:(NSString *)text
                  senderId:(NSString *)senderId
         senderDisplayName:(NSString *)senderDisplayName
                      date:(NSDate *)date {
    /**
     *  Sending a message. Your implementation of this method should do *at least* the following:
     *
     *  1. Play sound (optional)
     *  2. Add new id<JSQMessageData> object to your data source
     *  3. Call `finishSendingMessage`
     */
    [JSQSystemSoundPlayer jsq_playMessageSentSound];
    
    JSQMessage *message = [[JSQMessage alloc] initWithSenderId:senderId
                                             senderDisplayName:senderDisplayName
                                                          date:date
                                                          text:text];
    
    [self.dataController.messages addObject:message];
    [self finishSendingMessageAnimated:YES];
}

/*
- (void)didPressAccessoryButton:(UIButton *)sender
{
    UIActionSheet *sheet = [[UIActionSheet alloc] initWithTitle:@"Media messages"
                                                       delegate:self
                                              cancelButtonTitle:@"Cancel"
                                         destructiveButtonTitle:nil
                                              otherButtonTitles:@"Send photo", @"Send location", @"Send video", nil];
    
    [sheet showFromToolbar:self.inputToolbar];
}
 */
/*
- (void)actionSheet:(UIActionSheet *)actionSheet didDismissWithButtonIndex:(NSInteger)buttonIndex
{
    if (buttonIndex == actionSheet.cancelButtonIndex) {
        return;
    }
    
    switch (buttonIndex) {
        case 0:
            [self.demoData addPhotoMediaMessage];
            break;
            
        case 1:
        {
            __weak UICollectionView *weakView = self.collectionView;
            
            [self.demoData addLocationMediaMessageCompletion:^{
                [weakView reloadData];
            }];
        }
            break;
            
        case 2:
            [self.demoData addVideoMediaMessage];
            break;
    }
    
    [JSQSystemSoundPlayer jsq_playMessageSentSound];
    
    [self finishSendingMessageAnimated:YES];
}
*/


#pragma mark - JSQMessages CollectionView DataSource
- (id<JSQMessageData>)collectionView:(JSQMessagesCollectionView *)collectionView messageDataForItemAtIndexPath:(NSIndexPath *)indexPath {
    return [self.dataController.messages objectAtIndex:indexPath.item];
}

- (void)collectionView:(JSQMessagesCollectionView *)collectionView didDeleteMessageAtIndexPath:(NSIndexPath *)indexPath {
    [self.dataController.messages removeObjectAtIndex:indexPath.item];
}

- (id<JSQMessageBubbleImageDataSource>)collectionView:(JSQMessagesCollectionView *)collectionView messageBubbleImageDataForItemAtIndexPath:(NSIndexPath *)indexPath {
    /**
     *  You may return nil here if you do not want bubbles.
     *  In this case, you should set the background color of your collection view cell's textView.
     *
     *  Otherwise, return your previously created bubble image data objects.
     */
    
    JSQMessage *message = [self.dataController.messages objectAtIndex:indexPath.item];
    
    if ([message.senderId isEqualToString:self.senderId]) {
        return self.outgoingBubbleImageData;
    }
    
    return self.incomingBubbleImageData;
}

- (id<JSQMessageAvatarImageDataSource>)collectionView:(JSQMessagesCollectionView *)collectionView avatarImageDataForItemAtIndexPath:(NSIndexPath *)indexPath {
    /**
     *  Return `nil` here if you do not want avatars.
     *  If you do return `nil`, be sure to do the following in `viewDidLoad`:
     *
     *  self.collectionView.collectionViewLayout.incomingAvatarViewSize = CGSizeZero;
     *  self.collectionView.collectionViewLayout.outgoingAvatarViewSize = CGSizeZero;
     *
     *  It is possible to have only outgoing avatars or only incoming avatars, too.
     */
    
    /**
     *  Return your previously created avatar image data objects.
     *
     *  Note: these the avatars will be sized according to these values:
     *
     *  self.collectionView.collectionViewLayout.incomingAvatarViewSize
     *  self.collectionView.collectionViewLayout.outgoingAvatarViewSize
     *
     *  Override the defaults in `viewDidLoad`
     */
    JSQMessage *message = [self.dataController.messages objectAtIndex:indexPath.item];
    
    if ([message.senderId isEqualToString:self.senderId]) {
        return self.dataController.meAvatar;
    }
    
    return [self.dataController.avatars objectForKey:message.senderId];
}

- (NSAttributedString *)collectionView:(JSQMessagesCollectionView *)collectionView attributedTextForCellTopLabelAtIndexPath:(NSIndexPath *)indexPath {
    /**
     *  This logic should be consistent with what you return from `heightForCellTopLabelAtIndexPath:`
     *  The other label text delegate methods should follow a similar pattern.
     *
     *  Show a timestamp for every 3rd message
     */
    if (indexPath.item % 3 == 0) {
        JSQMessage *message = [self.dataController.messages objectAtIndex:indexPath.item];
        return [[JSQMessagesTimestampFormatter sharedFormatter] attributedTimestampForDate:message.date];
    }
    
    return nil;
}

- (NSAttributedString *)collectionView:(JSQMessagesCollectionView *)collectionView attributedTextForMessageBubbleTopLabelAtIndexPath:(NSIndexPath *)indexPath {
    JSQMessage *message = [self.dataController.messages objectAtIndex:indexPath.item];
    
    /**
     *  iOS7-style sender name labels
     */
    if ([message.senderId isEqualToString:self.senderId]) {
        return nil;
    }
    
    if (indexPath.item - 1 > 0) {
        JSQMessage *previousMessage = [self.dataController.messages objectAtIndex:indexPath.item - 1];
        if ([[previousMessage senderId] isEqualToString:message.senderId]) {
            return nil;
        }
    }
    
    /**
     *  Don't specify attributes to use the defaults.
     */
    return [[NSAttributedString alloc] initWithString:message.senderDisplayName];
}

- (NSAttributedString *)collectionView:(JSQMessagesCollectionView *)collectionView attributedTextForCellBottomLabelAtIndexPath:(NSIndexPath *)indexPath {
    return nil;
}

#pragma mark - UICollectionView DataSource
- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    return [self.dataController.messages count];
}

- (UICollectionViewCell *)collectionView:(JSQMessagesCollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    /**
     *  Override point for customizing cells
     */
    JSQMessagesCollectionViewCell *cell = (JSQMessagesCollectionViewCell *)[super collectionView:collectionView cellForItemAtIndexPath:indexPath];
    
    /**
     *  Configure almost *anything* on the cell
     *
     *  Text colors, label text, label colors, etc.
     *
     *
     *  DO NOT set `cell.textView.font` !
     *  Instead, you need to set `self.collectionView.collectionViewLayout.messageBubbleFont` to the font you want in `viewDidLoad`
     *
     *
     *  DO NOT manipulate cell layout information!
     *  Instead, override the properties you want on `self.collectionView.collectionViewLayout` from `viewDidLoad`
     */
    
    JSQMessage *msg = [self.dataController.messages objectAtIndex:indexPath.item];
    
    if (!msg.isMediaMessage) {
        
        if ([msg.senderId isEqualToString:self.senderId]) {
            cell.textView.textColor = [UIColor blackColor];
        }
        else {
            cell.textView.textColor = [UIColor whiteColor];
        }
        
        cell.textView.linkTextAttributes = @{ NSForegroundColorAttributeName : cell.textView.textColor,
                                              NSUnderlineStyleAttributeName : @(NSUnderlineStyleSingle | NSUnderlinePatternSolid) };
    }
    
    return cell;
}



#pragma mark - UICollectionView Delegate
#pragma mark - Custom menu items
- (BOOL)collectionView:(UICollectionView *)collectionView canPerformAction:(SEL)action forItemAtIndexPath:(NSIndexPath *)indexPath withSender:(id)sender {
    if (action == @selector(customAction:)) {
        return YES;
    }
    
    return [super collectionView:collectionView canPerformAction:action forItemAtIndexPath:indexPath withSender:sender];
}

- (void)collectionView:(UICollectionView *)collectionView performAction:(SEL)action forItemAtIndexPath:(NSIndexPath *)indexPath withSender:(id)sender {
    if (action == @selector(customAction:)) {
        [self customAction:sender];
        return;
    }
    
    [super collectionView:collectionView performAction:action forItemAtIndexPath:indexPath withSender:sender];
}

- (void)customAction:(id)sender {
    NSLog(@"Custom action received! Sender: %@", sender);
    
    [[[UIAlertView alloc] initWithTitle:@"Custom Action"
                                message:nil
                               delegate:nil
                      cancelButtonTitle:@"OK"
                      otherButtonTitles:nil]
     show];
}



#pragma mark - JSQMessages collection view flow layout delegate
#pragma mark - Adjusting cell label heights
- (CGFloat)collectionView:(JSQMessagesCollectionView *)collectionView
                   layout:(JSQMessagesCollectionViewFlowLayout *)collectionViewLayout heightForCellTopLabelAtIndexPath:(NSIndexPath *)indexPath {
    /**
     *  Each label in a cell has a `height` delegate method that corresponds to its text dataSource method
     */
    
    /**
     *  This logic should be consistent with what you return from `attributedTextForCellTopLabelAtIndexPath:`
     *  The other label height delegate methods should follow similarly
     *
     *  Show a timestamp for every 3rd message
     */
    if (indexPath.item % 3 == 0) {
        return kJSQMessagesCollectionViewCellLabelHeightDefault;
    }
    
    return 0.0f;
}

- (CGFloat)collectionView:(JSQMessagesCollectionView *)collectionView
                   layout:(JSQMessagesCollectionViewFlowLayout *)collectionViewLayout heightForMessageBubbleTopLabelAtIndexPath:(NSIndexPath *)indexPath {
    /**
     *  iOS7-style sender name labels
     */
    JSQMessage *currentMessage = [self.dataController.messages objectAtIndex:indexPath.item];
    if ([[currentMessage senderId] isEqualToString:self.senderId]) {
        return 0.0f;
    }
    
    if (indexPath.item - 1 > 0) {
        JSQMessage *previousMessage = [self.dataController.messages objectAtIndex:indexPath.item - 1];
        if ([[previousMessage senderId] isEqualToString:[currentMessage senderId]]) {
            return 0.0f;
        }
    }
    
    return kJSQMessagesCollectionViewCellLabelHeightDefault;
}

- (CGFloat)collectionView:(JSQMessagesCollectionView *)collectionView
                   layout:(JSQMessagesCollectionViewFlowLayout *)collectionViewLayout heightForCellBottomLabelAtIndexPath:(NSIndexPath *)indexPath {
    return 0.0f;
}

#pragma mark - Responding to collection view tap events
- (void)collectionView:(JSQMessagesCollectionView *)collectionView
                header:(JSQMessagesLoadEarlierHeaderView *)headerView didTapLoadEarlierMessagesButton:(UIButton *)sender {
    NSLog(@"Load earlier messages!");
}

- (void)collectionView:(JSQMessagesCollectionView *)collectionView didTapAvatarImageView:(UIImageView *)avatarImageView atIndexPath:(NSIndexPath *)indexPath {
    NSLog(@"Tapped avatar!");
}

- (void)collectionView:(JSQMessagesCollectionView *)collectionView didTapMessageBubbleAtIndexPath:(NSIndexPath *)indexPath {
    NSLog(@"Tapped message bubble!");
}

- (void)collectionView:(JSQMessagesCollectionView *)collectionView didTapCellAtIndexPath:(NSIndexPath *)indexPath touchLocation:(CGPoint)touchLocation {
    NSLog(@"Tapped cell at %@!", NSStringFromCGPoint(touchLocation));
}

#pragma mark - JSQMessagesComposerTextViewPasteDelegate methods
- (BOOL)composerTextView:(JSQMessagesComposerTextView *)textView shouldPasteWithSender:(id)sender {
    if ([UIPasteboard generalPasteboard].image) {
        // If there's an image in the pasteboard, construct a media item with that image and `send` it.
        JSQPhotoMediaItem *item = [[JSQPhotoMediaItem alloc] initWithImage:[UIPasteboard generalPasteboard].image];
        JSQMessage *message = [[JSQMessage alloc] initWithSenderId:self.senderId
                                                 senderDisplayName:self.senderDisplayName
                                                              date:[NSDate date]
                                                             media:item];
        [self.dataController.messages addObject:message];
        [self finishSendingMessage];
        
        return NO;
    }
    
    return YES;
}


@end
