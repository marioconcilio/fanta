//
//  ProfileViewController.m
//  Fanta
//
//  Created by Mario Concilio on 8/25/15.
//  Copyright (c) 2015 Mario Concilio. All rights reserved.
//

#import "ProfileViewController.h"
#import "VOUser.h"
#import "VOAthletic.h"
#import "Helper.h"
#import "UserService.h"
#import "UIViewController+BaseViewController.h"
#import "FadeInNavigationController.h"
#import "Constants.h"
#import "AppDelegate.h"
#import "APIService.h"
#import "UIStoryboard+Storyboards.h"
#import "PopoverTransition.h"
#import "MyProfilePopoverViewController.h"
#import "SettingsProfileViewController.h"
#import "ProfilePopoverViewController.h"
#import "MessagesViewController.h"
#import "SocialNode.h"
#import "BLMultiColorLoader+DefaultLoader.h"
#import "VBFPopFlatButton+DefaultButtons.h"
//#import "CameraViewController.h"
#import "UserService.h"
#import "UIImage+Resize.h"

//#import <Fanta-Swift.h>
//#import <UIImageView+AFNetworking.h>
#import <UIImageView+WebCache.h>
#import <DBCameraViewController.h>
#import <DBCameraContainerViewController.h>
#import <DBPrivacyHelper/UIViewController+DBPrivacyHelper.h>
//#import <ZFModalTransitionAnimator.h>
//#import <MessageUI/MessageUI.h>
#import <IDMPhotoBrowser.h>
#import <SIAlertView.h>
#import <LGSemiModalNavViewController.h>
#import <KVNProgress.h>
//#import <JDAvatarProgress.h>

static NSString *const kMyPopoverSegue      = @"myPopoverSegue";
static NSString *const kPopoverSegue        = @"popoverSegue";
static NSString *const kSettingsSegue       = @"settingsSegue";
static NSString *const kPasswordSegue       = @"passwordSegue";
static NSString *const kMessagesSegue       = @"messagesSegue";

@interface ProfileViewController () <DBCameraViewControllerDelegate, MyProfilePopoverDelegate, ProfilePopoverDelegate, SocialNodeDelegate,MFMailComposeViewControllerDelegate>

@end

@implementation ProfileViewController

+ (UINavigationController *)viewControllerWithNavigationUser:(VOUser *)user {
    ProfileViewController *vc = [[ProfileViewController alloc] initWithUser:user];
    return [[UINavigationController alloc] initWithRootViewController:vc];
}

#pragma mark - Init
- (instancetype)init {
    self = [self initWithUser:[UserService loadUser]];
    if (self) {
        _me = YES;
    }
    
    return self;
}

- (id)initWithCoder:(NSCoder *)aDecoder {
    self = [super initWithCoder:aDecoder];
    if (self) {
        _me = YES;
        _user = [UserService loadUser];
    }
    
    return self;
}

- (instancetype)initWithUser:(VOUser *)user {
    self = [super initWithNibName:@"ProfileViewController" bundle:nil];
    if (self) {
        _user = user;
        _me = NO;
    }
    
    return self;
}

#pragma mark - View Life Cycle
- (void)viewDidLoad {
    [super viewDidLoad];
    
    /*
     *  base view controller
     */
    [self setupReachability];
    
    self.view.backgroundColor = [UIColor customDarkBackground];
//    self.animator = [[CBStoreHouseTransitionAnimator alloc] init];
//    self.animator.duration = 0.4;
    
    self.navigationItem.backBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@""
                                                                             style:UIBarButtonItemStylePlain
                                                                            target:nil
                                                                            action:nil];
    
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(doOpenProfileImage)];
    [self.profileImageView addGestureRecognizer:tap];
    self.profileImageView.userInteractionEnabled = YES;
    
    VBFPopFlatButton *actionButton = [VBFPopFlatButton actionButtonDark];
    [actionButton addTarget:self action:@selector(doOpenPopup:) forControlEvents:UIControlEventTouchUpInside];
    self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithCustomView:actionButton];
    self.actionButton = actionButton;
    
    BLMultiColorLoader *indicator = [BLMultiColorLoader defaultLoader];
    [self.backgroundImageView addSubview:indicator];
    self.friendsIndicator = indicator;
    
    self.friendsLabel.hidden = YES;
    
    if ([self isMe]) {
         [self addDrawerButtonDark];
//        [self.drawerButton animateToType:buttonMenuType];
        
        self.addFriendButton.hidden = YES;
        self.changePictureButton.layer.cornerRadius = 2.0;
        self.logoutButton.layer.cornerRadius = 2.0;
        
//        [self.changePictureButton addTarget:self action:@selector(doChangePicture:) forControlEvents:UIControlEventTouchUpInside];
        [self.logoutButton addTarget:self action:@selector(doLogout:) forControlEvents:UIControlEventTouchUpInside];
    }
    else {
        [self addCloseButton];
        
        self.changePictureButton.hidden = YES;
        self.logoutButton.hidden = YES;
        self.addFriendButton.layer.cornerRadius = 2.0;
        
        [self.addFriendButton addTarget:self action:@selector(doAddFriend:) forControlEvents:UIControlEventTouchUpInside];
    }
    
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
        SocialNode *socialNode = [[SocialNode alloc] initWithUser:self.user];
        socialNode.delegate = self;
        [socialNode measure:CGSizeMake(UIKeyWindowWidth(), FLT_MAX)];
        socialNode.frame = (CGRect) {
            .origin.x = UIKeyWindowWidth()/2 - socialNode.calculatedSize.width/2,
            .origin.y = UIKeyWindowHeight() - socialNode.calculatedSize.height - 40.0,
            .size = socialNode.calculatedSize,
        };
        
        dispatch_async(dispatch_get_main_queue(), ^{
            [self.scrollView addSubnode:socialNode];
        });
    });
    
    /*
     *  load profile image
     */
    [Helper avatarFromName:self.user.name font:[UIFont lightFontWithSize:38.0] diameter:120.0 callback:^(UIImage *image) {
        if (self.user.image) {
            [self.profileImageView sd_setImageWithURL:[NSURL URLWithString:self.user.image]
                                     placeholderImage:image
                                              options:SDWebImageRefreshCached];
        }
        else {
            self.profileImageView.image = image;
        }
    }];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    [self.navigationController.navigationBar setBarStyle:UIBarStyleBlack];
    [self.navigationController.navigationBar setTintColor:[UIColor whiteColor]];
    [self.navigationController.navigationBar setTitleTextAttributes:@{NSFontAttributeName:[UIFont titleFontWithSize:16.f],
                                                                      NSForegroundColorAttributeName:[UIColor whiteColor]}];
    
    self.nameLabel.text = self.user.name;
    self.athleticLabel.text = [self.user.athletic fullName];
}

- (void)viewDidLayoutSubviews {
    [super viewDidLayoutSubviews];
    
    self.friendsIndicator.center = self.friendsLabel.center;
    
    self.circleView.layer.cornerRadius = CGRectGetHeight(self.circleView.frame)/2;
    self.circleView.clipsToBounds = YES;
    
    self.profileImageView.layer.cornerRadius = CGRectGetHeight(self.profileImageView.frame)/2;
    self.profileImageView.layer.borderColor = [UIColor lightGrayColor].CGColor;
    self.profileImageView.layer.borderWidth = 1.0;
    self.profileImageView.clipsToBounds = YES;
}

- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
}

- (UIStatusBarStyle)preferredStatusBarStyle {
    return UIStatusBarStyleLightContent;
}

#pragma mark - Navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    if ([segue.identifier isEqualToString:kPopoverSegue]) {
        ProfilePopoverViewController *vc = segue.destinationViewController;
        UIView *view = (UIView *)sender;
        CGRect frame = [view.superview convertRect:view.frame toView:nil];
        vc.popover_startFrame = frame;
        vc.user = self.user;
        vc.delegate = self;
    }
    else if ([segue.identifier isEqualToString:kMyPopoverSegue]) {
        CGFloat s = CGRectGetHeight([[UIApplication sharedApplication] statusBarFrame]);
        CGFloat n = CGRectGetHeight(self.navigationController.navigationBar.frame);
        
        LGSemiModalNavViewController *nav = segue.destinationViewController;
        nav.view.frame = CGRectMake(0.0, 0.0, UIKeyWindowWidth(), UIKeyWindowHeight() -s -n -10.0);
        nav.backgroundShadeColor = [UIColor blackColor];
        nav.tapDismissEnabled = NO;
        nav.scaleTransform = CGAffineTransformMakeScale(0.94, 0.94);
        
        MyProfilePopoverViewController *vc = (MyProfilePopoverViewController *) nav.topViewController;
        vc.delegate = self;
//        vc.user = self.user;
    }
    else if ([segue.identifier isEqualToString:kSettingsSegue]) {
        UINavigationController *nav = segue.destinationViewController;
        SettingsProfileViewController *vc = (SettingsProfileViewController *) nav.topViewController;
        vc.settings = [sender integerValue];
//        [self.interactiveTransition attachToViewController:vc];
    }
    else if ([segue.identifier isEqualToString:kMessagesSegue]) {
        MessagesViewController *vc = segue.destinationViewController;
//        vc.user = self.user;
        [vc setUsers:@[self.user]];
//        [vc setUsers:[Helper randomUsers]];
        
    }

}

#pragma mark - Setter
- (void)setUser:(VOUser *)user {
    _user = user;
    _me = NO;
}

#pragma mark - Helper Methods
- (void)addCloseButton {
    VBFPopFlatButton *closeButton = [VBFPopFlatButton closeButtonDark];
    [closeButton addTarget:self action:@selector(dismiss) forControlEvents:UIControlEventTouchUpInside];
    self.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc] initWithCustomView:closeButton];
}

- (void)dismiss {
    [self dismissViewControllerAnimated:YES completion:NULL];
}

- (NSString *)formatFriendsNumber:(NSInteger)number {
    if (number == 1) {
        return [NSString stringWithFormat:@"%ld Amigo", number];
    }
    
    return [NSString stringWithFormat:@"%ld Amigos", number];
}

- (NSString *)formatPostNumber:(NSInteger)number {
    if (number == 1) {
        return [NSString stringWithFormat:@"%ld Post", number];
    }
    
    return [NSString stringWithFormat:@"%ld Posts", number];
}

#pragma mark - Actions
- (void)doOpenProfileImage {
    if (self.user.image) {
        NSURL *url = [NSURL URLWithString:self.user.image];
        IDMPhotoBrowser *browser = [[IDMPhotoBrowser alloc] initWithPhotoURLs:@[url]];
        browser.usePopAnimation = YES;
        browser.forceHideStatusBar = YES;
//        browser.displayDoneButton = NO;
        browser.trackTintColor = [UIColor whiteColor];
        browser.doneButtonImage = [UIImage imageNamed:@"fnt_check"];
        [browser setInitialPageIndex:0];
        
        [[UIToolbar appearance] setTintColor:[UIColor whiteColor]];
        [self presentViewController:browser animated:YES completion:NULL];
    }
}

- (void)doSendEmail {
    if (![MFMailComposeViewController canSendMail]) {
        DDLogWarn(@"Mail services are not available");
        return;
    }
    
    NSArray *toRecipents = @[self.user.email];
    
    MFMailComposeViewController *mc = [[MFMailComposeViewController alloc] init];
    mc.mailComposeDelegate = self;
    [mc setToRecipients:toRecipents];
    [mc.navigationBar setTintColor: [UIColor customPink]];
    
    [self presentViewController:mc animated:YES completion:NULL];
}

- (void)doAddFriend:(UIButton *)sender {
    //TODO: add friend
}

- (void)doOpenPopup:(UIButton *)sender {
    if ([self isMe]) {
        [self performSegueWithIdentifier:kMyPopoverSegue sender:sender];
    }
    else {
        [self performSegueWithIdentifier:kPopoverSegue sender:sender];
    }
}

- (void)doLogout:(UIButton *)sender {
    SIAlertView *alertView = [[SIAlertView alloc] initWithTitle:@"Hey..."
                                                     andMessage:@"Você deseja sair da sua conta?"];
    
    [alertView addButtonWithTitle:@"Cancelar"
                             type:SIAlertViewButtonTypeCancel
                          handler:^(SIAlertView *alertView) {
                              [alertView dismissAnimated:YES];
                          }];
    
    [alertView addButtonWithTitle:@"Sair"
                             type:SIAlertViewButtonTypeDestructive
                          handler:^(SIAlertView *alertView) {
                              [[APIService sharedInstance] logout];
                              [UIAppDelegate() defineRootViewControllerAnimated:YES];
                          }];
    
    alertView.transitionStyle = SIAlertViewTransitionStyleBounce;
    [alertView show];
}

- (void)doChangePicture:(UIButton *)sender {
    AVAuthorizationStatus status = [AVCaptureDevice authorizationStatusForMediaType:AVMediaTypeVideo];
    if (status == AVAuthorizationStatusAuthorized) {
//        CameraViewController *camera = [[CameraViewController alloc] init];
        
//        UINavigationController *camera = [CameraViewController cameraWithDelegate:self];
//        [self presentViewController:camera animated:YES completion:NULL];
        
        
        DBCameraViewController *cameraController = [DBCameraViewController initWithDelegate:self];
        [cameraController setForceQuadCrop:YES];
        [cameraController setUseCameraSegue:YES];
        
        DBCameraContainerViewController *container = [[DBCameraContainerViewController alloc] initWithDelegate:self];
        [container setCameraViewController:cameraController];
        [container setFullScreenMode];
        
        FadeInNavigationController *nav = [[FadeInNavigationController alloc] initWithRootViewController:container];
        [nav setNavigationBarHidden:YES];
        
//        [[UIApplication sharedApplication] setStatusBarHidden:YES withAnimation:UIStatusBarAnimationSlide];
        [self presentViewController:nav animated:YES completion:nil];
        
    }
    else {
//        [self showPrivacyHelperForType:DBPrivacyTypeCamera];
    }

}

#pragma mark - DBCameraViewControllerDelegate
- (void)camera:(id)cameraViewController didFinishWithImage:(UIImage *)image withMetadata:(NSDictionary *)metadata {
    [self.presentedViewController dismissViewControllerAnimated:YES completion:nil];
    [KVNProgress show];
    
    dispatch_semaphore_t semaphore = dispatch_semaphore_create(0);
    
    /*
     *  save to album
     */
    NSString *source = [metadata objectForKey:@"DBCameraSource"];
    if([source hasSuffix:@"Camera"]) {
        UIImageWriteToSavedPhotosAlbum(image, nil, nil, nil);
    }
    
    /*
     *  clear cache
     */
    SDImageCache *cache = [SDImageCache sharedImageCache];
    [cache removeImageForKey:[self.user thumbnail] fromDisk:YES];
    [cache removeImageForKey:[self.user image] fromDisk:YES withCompletion:^{
        dispatch_semaphore_signal(semaphore);
    }];
    
    /*
     *  resize images with correct sizes
     */
    UIImage *newImage = [image resizedImage:kImageSize interpolationQuality:kCGInterpolationHigh];
    UIImage *thumb = [image resizedImage:kThumbSize interpolationQuality:kCGInterpolationHigh];
    
    /*
     *  upload to server
     */
    UserService *service = [[UserService alloc] init];
    [service editUserName:nil
                 password:nil
                    image:newImage
                thumbnail:thumb
               completion:^(NSError *error) {
                   dispatch_semaphore_wait(semaphore, DISPATCH_TIME_FOREVER);
                   
                   if (error) {
                       //TODO: show no connection
                       [KVNProgress showError];
                   }
                   else {
                       GCDOnMain(^{
                           [self.profileImageView setImage:newImage];
                           [KVNProgress showSuccess];
                       });
                   }
               }];
}

-(void)dismissCamera:(id)cameraViewController {
//    [[UIApplication sharedApplication] setStatusBarHidden:NO withAnimation:UIStatusBarAnimationSlide];
    [self dismissViewControllerAnimated:YES completion:nil];
    [cameraViewController restoreFullScreenMode];
}

#pragma mark - MyProfilePopover Delegate
- (void)popoverDidDismissWithUser:(VOUser *)user {
    self.user = user;
    self.me = YES;
    self.nameLabel.text = [self.user name];
    self.athleticLabel.text = [self.user.athletic fullName];
}

#pragma mark - Profile Popover Delegate
- (void)profilePopover:(ProfilePopoverViewController *)popover didSelectItem:(ProfilePopoverItem)item {
    if (item == ProfilePopoverMessage) {
        [self performSegueWithIdentifier:kMessagesSegue sender:self];
    }
    else if (item == ProfilePopoverEmail) {
        [self doSendEmail];
    }
}

#pragma mark - Social Node Delegate
- (void)socialNodeDidFacebook:(SocialNode *)socialNode {
    //TODO: save facebook id and open with it
}

- (void)socialNodeDidInstragram:(SocialNode *)socialNode {
    NSString *string = [NSString stringWithFormat:@"instagram://user?username=%@", self.user.instagram];
    NSURL *url = [NSURL URLWithString:string];
    
    // try to open Instagram APP
    if ([[UIApplication sharedApplication] canOpenURL:url]) {
        [[UIApplication sharedApplication] openURL:url];
    }
    // open Instagram Profile in browser
    else {
        string = [NSString stringWithFormat:@"https://instagram.com/%@", self.user.instagram];
        url = [NSURL URLWithString:string];
        [[UIApplication sharedApplication] openURL:url];
    }
}

- (void)socialNodeDidTwitter:(SocialNode *)socialNode {
    NSString *string = [NSString stringWithFormat:@"twitter://user?screen_name=%@", self.user.twitter];
    NSURL *url = [NSURL URLWithString:string];
    
    // try to open Twitter APP
    if ([[UIApplication sharedApplication] canOpenURL:url]) {
        [[UIApplication sharedApplication] openURL:url];
    }
    // open Twitter Profile in browser
    else {
        string = [NSString stringWithFormat:@"https://twitter.com/%@", self.user.twitter];
        url = [NSURL URLWithString:string];
        [[UIApplication sharedApplication] openURL:url];
    }
}

#pragma mark - MailComposer Delegate
- (void)mailComposeController:(MFMailComposeViewController *)controller
          didFinishWithResult:(MFMailComposeResult)result
                        error:(NSError *)error {
    switch (result) {
        case MFMailComposeResultCancelled:
            NSLog(@"mail cancelled");
            break;
            
        case MFMailComposeResultSaved:
            NSLog(@"mail saved");
            break;
            
        case MFMailComposeResultSent:
            NSLog(@"mail sent");
            break;
            
        case MFMailComposeResultFailed:
            NSLog(@"mail failed: %@", error.debugDescription);
            break;
            
        default:
            break;
    }
    
    [self dismissViewControllerAnimated:YES completion:NULL];
}

@end
