//
//  ProfilePopoverViewController.h
//  Fanta
//
//  Created by Mario Concilio on 9/2/15.
//  Copyright (c) 2015 Mario Concilio. All rights reserved.
//

#import <UIKit/UIKit.h>

typedef NS_ENUM(NSInteger, ProfilePopoverItem) {
    ProfilePopoverFacebook,
    ProfilePopoverMessage,
    ProfilePopoverEmail
};

@class ProfilePopoverViewController;
@protocol ProfilePopoverDelegate <NSObject>

- (void)profilePopover:(ProfilePopoverViewController *)popover didSelectItem:(ProfilePopoverItem)item;

@end

@class VOUser;
@interface ProfilePopoverViewController : UITableViewController

@property (nonatomic, weak) id<ProfilePopoverDelegate> delegate;
@property (nonatomic, strong) VOUser *user;

@end
