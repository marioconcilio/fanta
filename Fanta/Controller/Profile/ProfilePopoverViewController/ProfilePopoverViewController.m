//
//  ProfilePopoverViewController.m
//  Fanta
//
//  Created by Mario Concilio on 9/2/15.
//  Copyright (c) 2015 Mario Concilio. All rights reserved.
//

#import "ProfilePopoverViewController.h"
#import "PopoverTransition.h"
#import "VOUser.h"
#import "Constants.h"

#import <SIAlertView.h>

typedef NS_ENUM(NSUInteger, PopoverRow) {
    messageRow,
    blockRow,
    reportRow,
};

@interface ProfilePopoverViewController () <UIViewControllerTransitioningDelegate>

@end

@implementation ProfilePopoverViewController

#pragma mark - Init
- (instancetype)init {
    self = [super init];
    if (self) {
        [self commonInit];
    }
    
    return self;
}

- (instancetype)initWithCoder:(NSCoder *)aDecoder {
    self = [super initWithCoder:aDecoder];
    if (self) {
        [self commonInit];
    }
    
    return self;
}

- (instancetype)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil {
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        [self commonInit];
    }
    
    return self;
}

- (void)commonInit {
    self.modalPresentationStyle = UIModalPresentationCustom;
    self.transitioningDelegate = self;
}

#pragma mark - View Life Cycle
- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.view.layer.cornerRadius = 5.0;
    self.view.clipsToBounds = YES;
    
//    self.popover_endFrame = CGRectMake(50, 64, 260, 300);
    
    CGFloat statusHeight = CGRectGetHeight([[UIApplication sharedApplication] statusBarFrame]);
    CGFloat width = 260.0;
    self.popover_endFrame = (CGRect) {
        .origin.x = UIKeyWindowWidth() - width - 10.0,
        .origin.y = statusHeight + kBlurNavbarHeight,
        .size.width = width,
        .size.height = 180.0
    };
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Actions
- (void)doSendMessage {
    [self dismissViewControllerAnimated:NO completion:NULL];
    [self.delegate profilePopover:self didSelectItem:ProfilePopoverMessage];
}

- (void)doBlockUser {
    NSString *message = [NSString stringWithFormat:@"Deseja bloquear o perfil de %@?", self.user.name];
    SIAlertView *alertView = [[SIAlertView alloc] initWithTitle:@"Atenção"
                                                     andMessage:message];
    
    [alertView addButtonWithTitle:@"Cancelar"
                             type:SIAlertViewButtonTypeCancel
                          handler:^(SIAlertView *alertView) {
                              [alertView dismissAnimated:YES];
                          }];
    
    [alertView addButtonWithTitle:@"Bloquear"
                             type:SIAlertViewButtonTypeDestructive
                          handler:^(SIAlertView *alertView) {
                              //TODO: block user
                          }];
    
    alertView.transitionStyle = SIAlertViewTransitionStyleBounce;
    [alertView show];
}

- (void)doReportUser {
    NSString *message = [NSString stringWithFormat:@"Deseja denunciar o perfil de %@?", self.user.name];
    SIAlertView *alertView = [[SIAlertView alloc] initWithTitle:@"Atenção"
                                                     andMessage:message];
    
    [alertView addButtonWithTitle:@"Cancelar"
                             type:SIAlertViewButtonTypeCancel
                          handler:^(SIAlertView *alertView) {
                              [alertView dismissAnimated:YES];
                          }];
    
    [alertView addButtonWithTitle:@"Denunciar"
                             type:SIAlertViewButtonTypeDestructive
                          handler:^(SIAlertView *alertView) {
                              //TODO: report user
                          }];
    
    alertView.transitionStyle = SIAlertViewTransitionStyleBounce;
    [alertView show];
}

#pragma mark - View Controller Transitioning Delegate
- (UIPresentationController *)presentationControllerForPresentedViewController:(UIViewController *)presented presentingViewController:(UIViewController *)presenting sourceViewController:(UIViewController *)source {
    PopoverPresentationController *controller = [[PopoverPresentationController alloc] initWithPresentedViewController:presented
                                                                                              presentingViewController:presenting];
    return controller;
}

- (id <UIViewControllerAnimatedTransitioning>)animationControllerForPresentedController:(UIViewController *)presented
                                                                   presentingController:(UIViewController *)presenting
                                                                       sourceController:(UIViewController *)source {
    PopoverAnimationController *animator = [[PopoverAnimationController alloc] init];
    return animator;
}

- (id <UIViewControllerAnimatedTransitioning>)animationControllerForDismissedController:(UIViewController *)dismissed {
    PopoverAnimationController *animator = [[PopoverAnimationController alloc] init];
    return animator;
}

#pragma mark - Table View Datasource
- (void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath {
    UIView *view = [[UIView alloc] init];
    view.backgroundColor = [UIColor customPink];
    cell.selectedBackgroundView = view;
}

#pragma mark - TableView Delegate
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
    switch (indexPath.row) {
        case messageRow:
            [self doSendMessage];
            break;
            
        case blockRow:
            [self doBlockUser];
            break;

        case reportRow:
            [self doReportUser];
            break;
            
        default:
            break;
    }
}

@end
