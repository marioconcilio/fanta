//
//  MyProfilePopoverViewController.h
//  Fanta
//
//  Created by Mario Concilio on 9/4/15.
//  Copyright (c) 2015 Mario Concilio. All rights reserved.
//

#import <UIKit/UIKit.h>

@class VOUser;
@protocol MyProfilePopoverDelegate <NSObject>

- (void)popoverDidDismissWithUser:(VOUser *)user;

@end

@interface MyProfilePopoverViewController : UITableViewController

@property (nonatomic, weak) id<MyProfilePopoverDelegate> delegate;

@property (weak, nonatomic) IBOutlet UILabel *nameLabel;
@property (weak, nonatomic) IBOutlet UILabel *athleticLabel;
@property (nonatomic, strong) VOUser *user;

@end
