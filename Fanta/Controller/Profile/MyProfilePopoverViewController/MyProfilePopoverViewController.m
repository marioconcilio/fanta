//
//  MyProfilePopoverViewController.m
//  Fanta
//
//  Created by Mario Concilio on 9/4/15.
//  Copyright (c) 2015 Mario Concilio. All rights reserved.
//

#import "MyProfilePopoverViewController.h"
#import "Constants.h"
#import "UserService.h"
#import "VOUser.h"
#import "VOAthletic.h"
#import "SettingsProfileViewController.h"
#import "ListAALightViewController.h"

static NSString *const kSettingsSegue = @"settings_segue";

@interface MyProfilePopoverViewController () <ListAADelegate>

@end

@implementation MyProfilePopoverViewController

#pragma mark - Init
- (instancetype)init {
    self = [super init];
    if (self) {
//        [self commonInit];
    }
    
    return self;
}

- (instancetype)initWithCoder:(NSCoder *)aDecoder {
    self = [super initWithCoder:aDecoder];
    if (self) {
//        [self commonInit];
    }
    
    return self;
}

- (instancetype)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil {
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
//        [self commonInit];
    }
    
    return self;
}

- (void)commonInit {
//    self.modalPresentationStyle = UIModalPresentationCustom;
//    self.transitioningDelegate = self;
}

#pragma mark - View Life Cycle
- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self.navigationController setNavigationBarHidden:NO];
    [self.navigationController.navigationBar setBarStyle:UIBarStyleDefault];
    [self.navigationController.navigationBar setTintColor:[UIColor customPink]];
    
    self.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"fnt_cancel"]
                                                                             style:UIBarButtonItemStylePlain
                                                                            target:self
                                                                            action:@selector(dismiss)];
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    
    self.user = [UserService loadUser];
    self.nameLabel.text = self.user.name;
    self.athleticLabel.text = [self.user.athletic fullName];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)dismiss {
    [self.delegate popoverDidDismissWithUser:self.user];
    [self dismissViewControllerAnimated:YES completion:NULL];
}

#pragma mark - Navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    if ([segue.identifier isEqualToString:kSettingsSegue]) {
        NSIndexPath *path = sender;
        SettingsProfileViewController *vc = segue.destinationViewController;
        vc.settings = path.row;
        vc.user = self.user;
    }
}

#pragma mark - Table View Datasource
- (void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath {
//    if (indexPath.section == 0) {
//        cell.selectionStyle = UITableViewCellSelectionStyleNone;
//    }
//    else {
        UIView *view = [[UIView alloc] init];
        view.backgroundColor = [UIColor customOrange];
        cell.selectedBackgroundView = view;
//    }
}

#pragma mark - TableView Delegate
- (NSIndexPath *)tableView:(UITableView *)tableView willSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    if (indexPath.row == SettingsChangeProfile) {
        return nil;
    }
    
    return indexPath;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
    switch (indexPath.row) {
        case SettingsName:
        case SettingsPassword:
            [self performSegueWithIdentifier:kSettingsSegue sender:indexPath];
            break;
            
        case SettingsAthletic: {
            ListAALightViewController *vc = [[ListAALightViewController alloc] init];
            vc.filtering = NO;
            vc.delegate = self;
            vc.pushPresentation = YES;
            
            [self.navigationController pushViewController:vc animated:YES];
            break;
        }
        default:
            return;
    }
}

#pragma mark - List AA Delegate
- (void)listAA:(ListAAViewController *)vc didSelectAA:(VOAthletic *)athletic {
    DDLogVerbose(@"%@", athletic);
    self.user.athletic = athletic;
    [UserService saveUser:self.user];
    [vc pop];
}

- (void)listAA:(ListAAViewController *)vc didSelectListAA:(NSArray<VOAthletic *> *)list {
    
}

@end
