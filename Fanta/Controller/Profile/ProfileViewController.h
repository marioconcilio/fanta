//
//  ProfileViewController.h
//  Fanta
//
//  Created by Mario Concilio on 8/25/15.
//  Copyright (c) 2015 Mario Concilio. All rights reserved.
//

#import <UIKit/UIKit.h>

@class VOUser, BLMultiColorLoader, VBFPopFlatButton;
@interface ProfileViewController : UIViewController

@property (weak, nonatomic) IBOutlet UIScrollView *scrollView;
@property (weak, nonatomic) IBOutlet UIImageView *profileImageView;
@property (weak, nonatomic) IBOutlet UILabel *nameLabel;
@property (weak, nonatomic) IBOutlet UILabel *athleticLabel;
@property (weak, nonatomic) IBOutlet UIButton *addFriendButton;
@property (weak, nonatomic) IBOutlet UILabel *friendsLabel;
@property (weak, nonatomic) IBOutlet UILabel *postsLabel;
@property (weak, nonatomic) IBOutlet UIView *circleView;
@property (weak, nonatomic) IBOutlet UIButton *changePictureButton;
@property (weak, nonatomic) IBOutlet UIButton *logoutButton;
@property (weak, nonatomic) IBOutlet UIImageView *backgroundImageView;
@property (weak, nonatomic) VBFPopFlatButton *actionButton;
@property (weak, nonatomic) BLMultiColorLoader *friendsIndicator;

@property (nonatomic, strong) VOUser *user;
@property (assign, nonatomic, getter=isMe) BOOL me;

+ (UINavigationController *)viewControllerWithNavigationUser:(VOUser *)user;
- (instancetype)initWithUser:(VOUser *)user;

- (void)doOpenProfileImage;
- (void)doAddFriend:(UIButton *)sender;
- (void)doOpenPopup:(UIButton *)sender;
- (void)doLogout:(UIButton *)sender;
- (IBAction)doChangePicture:(UIButton *)sender;

@end
