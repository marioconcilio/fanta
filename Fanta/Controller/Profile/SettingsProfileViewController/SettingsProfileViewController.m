//
//  SettingsProfileViewController.m
//  Fanta
//
//  Created by Mario Concilio on 8/31/15.
//  Copyright (c) 2015 Mario Concilio. All rights reserved.
//

#import "SettingsProfileViewController.h"
#import "FloatLabeledTextField.h"
#import "VOUser.h"
#import "Helper.h"
#import "UserService.h"
//#import "LoadingView.h"
#import <KVNProgress.h>

@interface SettingsProfileViewController ()

@end

@implementation SettingsProfileViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    VOUser *user = [UserService loadUser];
    
    NSString *placeholder;
    switch (self.settings) {
        case SettingsName: {
            placeholder = @"Nome";
            self.textField.text = user.name;
            break;
        }
        case SettingsPassword: {
            placeholder = @"Nova Senha";
            self.confirmTextField.attributedPlaceholder = [[NSAttributedString alloc] initWithString:@"Confirmar Senha"
                                                                                          attributes:@{NSForegroundColorAttributeName: [UIColor lightGrayColor]}];
            self.textField.secureTextEntry = YES;
            self.confirmTextField.secureTextEntry = YES;
            [self.confirmTextField addTarget:self
                               action:@selector(textFieldDidChange:)
                     forControlEvents:UIControlEventEditingChanged];
            break;
        }
        default:
            break;
    }

    [self.textField addTarget:self
                       action:@selector(textFieldDidChange:)
             forControlEvents:UIControlEventEditingChanged];
    self.textField.attributedPlaceholder = [[NSAttributedString alloc] initWithString:placeholder
                                                                           attributes:@{NSForegroundColorAttributeName: [UIColor lightGrayColor]}];
    
    /*
     *  Navbar
     */
    UIBarButtonItem *backButton = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"fnt_navbar_back"]
                                                                   style:UIBarButtonItemStylePlain
                                                                  target:self
                                                                  action:@selector(dismiss)];
    self.navigationItem.leftBarButtonItem = backButton;
    
    self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"Salvar"
                                                                              style:UIBarButtonItemStylePlain
                                                                             target:self
                                                                             action:@selector(doSave)];
    [self.navigationItem.rightBarButtonItem setTitleTextAttributes:@{NSFontAttributeName: [UIFont semiboldFontWithSize:16.0]}
                                                          forState:UIControlStateNormal];
    self.navigationItem.rightBarButtonItem.enabled = NO;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (BOOL)prefersStatusBarHidden {
    return NO;
}

#pragma mark - Helper Methods
- (void)dismiss {
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)textFieldDidChange:(UITextField *)textField {
    // if textfield is empty, save button is disabled
    if (textField.text.length == 0) {
        self.navigationItem.rightBarButtonItem.enabled = NO;
    }
    else {
        // if is updating pass, check if both pass match
        if (self.settings == SettingsPassword) {
            if ([self.textField.text isEqualToString:self.confirmTextField.text]) {
                self.navigationItem.rightBarButtonItem.enabled = YES;
            }
            else {
                self.navigationItem.rightBarButtonItem.enabled = NO;
            }
        }
        // is not updating pass and text is not empty, enable save button
        else {
            self.navigationItem.rightBarButtonItem.enabled = YES;
        }
    }
    
}

#pragma mark - Actions
- (void)doSave {
    [KVNProgress show];
    UserService *service = [[UserService alloc] init];
    
    switch (self.settings) {
        case SettingsName: {
            [service editUserName:[self.textField text]
                         password:nil
                            image:nil
                        thumbnail:nil
                       completion:^(NSError *error) {
                           if (error) {
                               [KVNProgress showError];
                           }
                           else {
                               [KVNProgress showSuccess];
                               self.user.name = [self.textField text];
                               [UserService saveUser:self.user];
                               [self dismiss];
                           }
                       }];
            break;
        }
        case SettingsPassword: {
            [service editUserName:nil
                         password:[self.textField text]
                            image:nil
                        thumbnail:nil
                       completion:^(NSError *error) {
                           if (error) {
                               [KVNProgress showError];
                           }
                           else {
                               [KVNProgress showSuccess];
                           }
                       }];
            break;
        }
        default:
            break;
    }
}

#pragma mark - Table view data source
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    if (self.settings == SettingsPassword)
        return 2;
    
    return 1;
}

@end
