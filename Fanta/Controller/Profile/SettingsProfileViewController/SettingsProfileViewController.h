//
//  SetttingsProfileViewController.h
//  Fanta
//
//  Created by Mario Concilio on 8/31/15.
//  Copyright (c) 2015 Mario Concilio. All rights reserved.
//

#import <UIKit/UIKit.h>

typedef NS_ENUM(NSInteger, Settings) {
    SettingsName,
    SettingsPassword,
    SettingsAthletic,
    SettingsChangeProfile
};

@class VOUser, FloatLabeledTextField;
@interface SettingsProfileViewController : UITableViewController

@property (nonatomic, strong) VOUser *user;
@property (nonatomic, assign) Settings settings;

@property (weak, nonatomic) IBOutlet FloatLabeledTextField *textField;
@property (weak, nonatomic) IBOutlet FloatLabeledTextField *confirmTextField;

@end
