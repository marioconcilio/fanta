//
//  SocialNode.m
//  Fanta
//
//  Created by Mario Concilio on 3/28/16.
//  Copyright © 2016 Mario Concilio. All rights reserved.
//

#import "SocialNode.h"
#import "VOUser.h"
#import "TextStyles.h"

@interface SocialNode ()

@property (nonatomic, weak) ASImageNode *facebookNode;
@property (nonatomic, weak) ASImageNode *instagramNode;
@property (nonatomic, weak) ASImageNode *twitterNode;

@end

@implementation SocialNode

- (instancetype)initWithUser:(VOUser *)user {
    self = [super init];
    if (self) {
//        _user = user;
        
        /*
         *  Facebook
         */
        if (user.facebook) {
            ASImageNode *imageNode = [[ASImageNode alloc] init];
            imageNode.image = [UIImage imageNamed:@"fnt_social_face"];
            imageNode.layerBacked = YES;
            imageNode.preferredFrameSize = CGSizeMake(33.0, 33.0);
            imageNode.userInteractionEnabled = YES;
            imageNode.hitTestSlop = UIEdgeInsetsMake(-10.0, -10.0, -10.0, -10.0);
            [imageNode addTarget:self action:@selector(doFacebook) forControlEvents:ASControlNodeEventTouchUpInside];
            _facebookNode = imageNode;
            [self addSubnode:_facebookNode];
        }
        
        /*
         *  Instagram
         */
        if (user.instagram) {
            ASImageNode *imageNode = [[ASImageNode alloc] init];
            imageNode.image = [UIImage imageNamed:@"fnt_social_insta"];
            imageNode.preferredFrameSize = CGSizeMake(33.0, 33.0);
            imageNode.userInteractionEnabled = YES;
            imageNode.hitTestSlop = UIEdgeInsetsMake(-10.0, -10.0, -10.0, -10.0);
            [imageNode addTarget:self action:@selector(doInstagram) forControlEvents:ASControlNodeEventTouchUpInside];
            _instagramNode = imageNode;
            [self addSubnode:_instagramNode];
        }
        
        /*
         *  Twitter
         */
        if (user.twitter) {
            ASImageNode *imageNode = [[ASImageNode alloc] init];
            imageNode.image = [UIImage imageNamed:@"fnt_social_tweet"];
            imageNode.preferredFrameSize = CGSizeMake(33.0, 33.0);
            imageNode.userInteractionEnabled = YES;
            imageNode.hitTestSlop = UIEdgeInsetsMake(-10.0, -10.0, -10.0, -10.0);
            [imageNode addTarget:self action:@selector(doTwitter) forControlEvents:ASControlNodeEventTouchUpInside];
            _twitterNode = imageNode;
            [self addSubnode:_twitterNode];
        }
        
    }
    
    return self;
}

- (ASLayoutSpec *)layoutSpecThatFits:(ASSizeRange)constrainedSize {
    NSMutableArray *children = [NSMutableArray array];
    if (_facebookNode) {
        [children addObject:_facebookNode];
    }
    
    if (_instagramNode) {
        [children addObject:_instagramNode];
    }
    
    if (_twitterNode) {
        [children addObject:_twitterNode];
    }
    
    ASStackLayoutSpec *stack = [ASStackLayoutSpec stackLayoutSpecWithDirection:ASStackLayoutDirectionHorizontal
                                                                       spacing:30.0
                                                                justifyContent:ASStackLayoutJustifyContentStart
                                                                    alignItems:ASStackLayoutAlignItemsCenter
                                                                      children:children];
    stack.alignSelf = ASStackLayoutAlignSelfStretch;
    
    return stack;
}

#pragma mark - Action Methods
- (void)doFacebook {
    [self.delegate socialNodeDidFacebook:self];
}

- (void)doInstagram {
    [self.delegate socialNodeDidInstragram:self];
}

- (void)doTwitter {
    [self.delegate socialNodeDidTwitter:self];
}

@end
