//
//  SocialNode.h
//  Fanta
//
//  Created by Mario Concilio on 3/28/16.
//  Copyright © 2016 Mario Concilio. All rights reserved.
//

#import <AsyncDisplayKit/AsyncDisplayKit.h>

@class SocialNode;
@protocol SocialNodeDelegate <NSObject>

- (void)socialNodeDidFacebook:(SocialNode *)socialNode;
- (void)socialNodeDidInstragram:(SocialNode *)socialNode;
- (void)socialNodeDidTwitter:(SocialNode *)socialNode;

@end

@class VOUser;
@interface SocialNode : ASDisplayNode

@property (nonatomic, weak) id<SocialNodeDelegate> delegate;

- (instancetype)initWithUser:(VOUser *)user;

+ (instancetype)new UNAVAILABLE_ATTRIBUTE;
- (instancetype)init UNAVAILABLE_ATTRIBUTE;

@end
