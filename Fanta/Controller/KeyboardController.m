//
//  KeyboardController.m
//  Fanta
//
//  Created by Mario Concilio on 30/04/15.
//  Copyright (c) 2015 Mario Concilio. All rights reserved.
//

#import "KeyboardController.h"
//#import <POP.h>

#define D_KB_WILL_CHANGE_FRAME(s)   if (delegateRespondsTo.willChangeFrame) [delegate keyboardWillChangeFrame:s]
#define D_KB_DID_CHANGE_FRAME(s)    if (delegateRespondsTo.didChangeFrame)  [delegate keyboardDidChangeFrame:s]
#define D_KB_WILL_SHOW(s)           if (delegateRespondsTo.willShow)        [delegate keyboardWillShow:s]
#define D_KB_DID_SHOW(s)            if (delegateRespondsTo.didShow)         [delegate keyboardDidShow:s]
#define D_KB_WILL_HIDE(s)           if (delegateRespondsTo.willHide)        [delegate keyboardWillHide:s]
#define D_KB_DID_HIDE(s)            if (delegateRespondsTo.didHide)         [delegate keyboardDidHide:s]

@interface KeyboardController () <UIGestureRecognizerDelegate>

@property (nonatomic, strong) UITapGestureRecognizer *tap;
@property (nonatomic, strong) UIView *view;

@end

@implementation KeyboardController {
    struct {
        unsigned int willChangeFrame:1;
        unsigned int didChangeFrame:1;
        unsigned int willShow:1;
        unsigned int didShow:1;
        unsigned int willHide:1;
        unsigned int didHide:1;
    } delegateRespondsTo;
    
    CGRect _viewFrame;
}

@synthesize delegate;
- (void)setDelegate:(id<KeyboardDelegate>)aDelegate {
    if (delegate != aDelegate) {
        delegate = aDelegate;
        
        delegateRespondsTo.willChangeFrame = [delegate respondsToSelector:@selector(keyboardWillChangeFrame:)];
        delegateRespondsTo.didChangeFrame = [delegate respondsToSelector:@selector(keyboardDidChangeFrame:)];
        delegateRespondsTo.willShow = [delegate respondsToSelector:@selector(keyboardWillShow:)];
        delegateRespondsTo.didShow = [delegate respondsToSelector:@selector(keyboardDidShow:)];
        delegateRespondsTo.willHide = [delegate respondsToSelector:@selector(keyboardWillHide:)];
        delegateRespondsTo.didHide = [delegate respondsToSelector:@selector(keyboardDidHide:)];
    }
}

- (instancetype)initWithView:(UIView *)view andFrame:(CGRect)frame {
    self = [super init];
    if (self) {
        _view = view;
//        _viewFrame = self.view.frame;
//        _viewFrame = [UIKeyWindow convertRect:self.view.frame toView:nil];
        _viewFrame = frame;
        _dismissOnTap = YES;
        _shouldResizeView = YES;
        _margin = 0.0;
        _tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(_dismissKeyboard:)];
    }
    
    return self;
}

#pragma mark - Public Methods
- (void)registerForKeyboardNotifications {
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(_keyboardWillChangeFrame:)
                                                 name:UIKeyboardWillChangeFrameNotification
                                               object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(_keyboardDidChangeFrame:)
                                                 name:UIKeyboardDidChangeFrameNotification
                                               object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(_keyboardWillShow:)
                                                 name:UIKeyboardWillShowNotification
                                               object:nil];
    
//    [[NSNotificationCenter defaultCenter] addObserver:self
//                                             selector:@selector(_keyboardDidShow:)
//                                                 name:UIKeyboardDidShowNotification
//                                               object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(_keyboardWillHide:)
                                                 name:UIKeyboardWillHideNotification
                                               object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(_keyboardDidHide:)
                                                 name:UIKeyboardDidHideNotification
                                               object:nil];
}

- (void)deregisterFromKeyboardNotifications {
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

#pragma mark - Private Methods
- (void)_keyboardWillChangeFrame:(NSNotification *)notification {
    NSDictionary* info = [notification userInfo];
    CGRect keyboardFrame = [[info objectForKey:UIKeyboardFrameEndUserInfoKey] CGRectValue];
    D_KB_WILL_CHANGE_FRAME(keyboardFrame.size);
    
    if (_shouldResizeView) {
        CGFloat diff = CGRectGetMaxY(UIKeyWindow().bounds) - CGRectGetMaxY(_viewFrame);
        CGRect newFrame = (CGRect) {
            .origin = _viewFrame.origin,
            .size.width = _viewFrame.size.width,
            .size.height = _viewFrame.size.height - keyboardFrame.size.height + diff - _margin,
        };
        
//        self.view.frame = newFrame;
//        [self.view layoutSubviews];
        
        [UIView animateKeyframesWithDuration:0.3
                                       delay:0.0
                                     options:UIViewKeyframeAnimationOptionAllowUserInteraction
                                  animations:^{
                                      self.view.frame = newFrame;
                                  }
                                  completion:^(BOOL finished) {
                                      if (finished) {
                                          D_KB_DID_SHOW(keyboardFrame.size);
                                      }
                                  }];
        
//        POPBasicAnimation *anim = [POPBasicAnimation animationWithPropertyNamed:kPOPViewFrame];
//        anim.toValue = [NSValue valueWithCGRect:newFrame];
//        [anim setCompletionBlock:^(POPAnimation *anim, BOOL completed) {
//            if (completed) {
//                D_KB_DID_SHOW(keyboardFrame.size);
//            }
//        }];
//        [self.view pop_addAnimation:anim forKey:@"anim"];
    }
    else {
        CGRect frame = self.view.frame;
        frame.origin.y = keyboardFrame.origin.y - frame.size.height;
        self.view.frame = frame;
        D_KB_DID_SHOW(keyboardFrame.size);
    }
}

- (void)_keyboardDidChangeFrame:(NSNotification *)notification {
    NSDictionary* info = [notification userInfo];
    CGRect keyboardFrame = [[info objectForKey:UIKeyboardFrameEndUserInfoKey] CGRectValue];
    D_KB_DID_CHANGE_FRAME(keyboardFrame.size);
}

- (void)_keyboardWillShow:(NSNotification *)notification {
    NSDictionary* info = [notification userInfo];
    CGRect keyboardFrame = [[info objectForKey:UIKeyboardFrameEndUserInfoKey] CGRectValue];
    D_KB_WILL_SHOW(keyboardFrame.size);
    
    if (_dismissOnTap) {
        [UIKeyWindow() addGestureRecognizer:self.tap];
    }
}

//- (void)_keyboardDidShow:(NSNotification *)notification {
//    D_KB_DID_SHOW;
//}

- (void)_keyboardWillHide:(NSNotification *)notification {
    NSDictionary* info = [notification userInfo];
    CGRect keyboardFrame = [[info objectForKey:UIKeyboardFrameEndUserInfoKey] CGRectValue];
    D_KB_WILL_HIDE(keyboardFrame.size);
    
    if (_shouldResizeView) {
        [UIView animateKeyframesWithDuration:0.3
                                       delay:0.0
                                     options:UIViewKeyframeAnimationOptionAllowUserInteraction
                                  animations:^{
                                      self.view.frame = _viewFrame;
                                  }
                                  completion:^(BOOL finished) {
                                      if (finished) {
                                          D_KB_DID_SHOW(keyboardFrame.size);
                                      }
                                  }];
        
//        POPBasicAnimation *anim = [POPBasicAnimation animationWithPropertyNamed:kPOPViewFrame];
//        anim.toValue = [NSValue valueWithCGRect:_viewFrame];
//        [self.view pop_addAnimation:anim forKey:@"anim"];
//        self.view.frame = _viewFrame;
    }
    
    if (_dismissOnTap) {
        [UIKeyWindow() removeGestureRecognizer:self.tap];
    }
}

- (void)_keyboardDidHide:(NSNotification *)notification {
    NSDictionary* info = [notification userInfo];
    CGRect keyboardFrame = [[info objectForKey:UIKeyboardFrameEndUserInfoKey] CGRectValue];
    D_KB_DID_HIDE(keyboardFrame.size);
}

- (void)_dismissKeyboard:(UIGestureRecognizer *)gesture {
    [self.view endEditing:YES];
}

#pragma mark - Gesture Recognizer Delegate
- (BOOL)gestureRecognizer:(UIGestureRecognizer *)gestureRecognizer shouldRecognizeSimultaneouslyWithGestureRecognizer:(UIGestureRecognizer *)otherGestureRecognizer {
    return YES;
}

@end
