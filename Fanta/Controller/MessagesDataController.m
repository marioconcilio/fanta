//
//  MessagesDataController.m
//  Fanta
//
//  Created by Mario Concilio on 9/25/15.
//  Copyright (c) 2015 Mario Concilio. All rights reserved.
//

#import "APIService.h"
#import "Helper.h"
#import "MessagesDataController.h"
#import "VOUser.h"
#import "UserService.h"

#import <JSQMessages.h>
#import <UIImageView+AFNetworking.h>

@implementation MessagesDataController

- (instancetype)initWithUsers:(NSArray *)users {
    self = [super init];
    if (self) {
        _users = [users mutableCopy];
        [self loadAvatars];
        [self loadMessages];
    }
    
    return self;
}

#pragma mark - Setter
//- (void)setUsers:(NSArray *)users {
//    _users = [users mutableCopy];
//    [self loadAvatars];
//    [self loadMessages];
//}

#pragma mark - Helper Methods
- (void)loadAvatars {
    VOUser *me = [UserService loadUser];
    
    if (me.thumbnail) {
        [APIService imageFromURL:[NSURL URLWithString:me.thumbnail] callback:^(UIImage *image, NSError *error) {
            if (!error) {
                self.meAvatar = [JSQMessagesAvatarImageFactory avatarImageWithImage:image
                                                                           diameter:kJSQMessagesCollectionViewAvatarSizeDefault];
            }
            else {
                self.meAvatar = [JSQMessagesAvatarImageFactory avatarImageWithUserInitials:[Helper initialsFromName:me.name]
                                                                           backgroundColor:[UIColor randomColor]
                                                                                 textColor:[UIColor whiteColor]
                                                                                      font:[UIFont lightFontWithSize:13.0]
                                                                                  diameter:kJSQMessagesCollectionViewAvatarSizeDefault];
            }
        }];
    }
    else {
        self.meAvatar = [JSQMessagesAvatarImageFactory avatarImageWithUserInitials:[Helper initialsFromName:me.name]
                                                                   backgroundColor:[UIColor randomColor]
                                                                         textColor:[UIColor whiteColor]
                                                                              font:[UIFont lightFontWithSize:13.0]
                                                                          diameter:kJSQMessagesCollectionViewAvatarSizeDefault];
    }
    
    self.avatars = [[NSMutableDictionary alloc] init];
    for (VOUser *user in self.users) {
        @autoreleasepool {
            if (user.thumbnail) {
                NSURL *url = [NSURL URLWithString:user.thumbnail];
                
                [APIService imageFromURL:url callback:^(UIImage *image, NSError *error) {
                    JSQMessagesAvatarImage *userAvatar;
                    
                    if (!error) {
                        userAvatar = [JSQMessagesAvatarImageFactory avatarImageWithImage:image
                                                                                diameter:kJSQMessagesCollectionViewAvatarSizeDefault];
                    }
                    else {
                        userAvatar = [JSQMessagesAvatarImageFactory avatarImageWithUserInitials:[Helper initialsFromName:user.name]
                                                                                backgroundColor:[UIColor randomColor]
                                                                                      textColor:[UIColor whiteColor]
                                                                                           font:[UIFont lightFontWithSize:13.0]
                                                                                       diameter:kJSQMessagesCollectionViewAvatarSizeDefault];
                    }
                    
                    NSString *userKey = [NSString stringWithFormat:@"%lu", (unsigned long)user.userID];
                    [self.avatars setObject:userAvatar forKey:userKey];
                }];
            }
            else {
                JSQMessagesAvatarImage *userAvatar = [JSQMessagesAvatarImageFactory avatarImageWithUserInitials:[Helper initialsFromName:user.name]
                                                                                                backgroundColor:[UIColor randomColor]
                                                                                                      textColor:[UIColor whiteColor]
                                                                                                           font:[UIFont lightFontWithSize:13.0]
                                                                                                       diameter:kJSQMessagesCollectionViewAvatarSizeDefault];
                
                NSString *userKey = [NSString stringWithFormat:@"%lu", (unsigned long)user.userID];
                [self.avatars setObject:userAvatar forKey:userKey];
            }
            
        }
    }
    
}

- (void)loadMessages {
    self.messages = [NSMutableArray array];
    if (self.users.count > 1) {
        //TODO: group messages
    }
    else {
        //TODO: get messages
    }
}

#pragma mark - Public Methods
- (void)addUser:(VOUser *)user {
    [self.users addObject:user];
    
    __block JSQMessagesAvatarImage *userAvatar;
    if (user.thumbnail) {
        NSURL *url = [NSURL URLWithString:user.thumbnail];
        [APIService imageFromURL:url callback:^(UIImage *image, NSError *error) {
            if (!error) {
                userAvatar = [JSQMessagesAvatarImageFactory avatarImageWithImage:image
                                                                        diameter:kJSQMessagesCollectionViewAvatarSizeDefault];
            }
            else {
                userAvatar = [JSQMessagesAvatarImageFactory avatarImageWithUserInitials:[Helper initialsFromName:user.name]
                                                                        backgroundColor:[UIColor randomColor]
                                                                              textColor:[UIColor whiteColor]
                                                                                   font:[UIFont lightFontWithSize:13.0]
                                                                               diameter:kJSQMessagesCollectionViewAvatarSizeDefault];
            }
        }];
    }
    else {
        userAvatar = [JSQMessagesAvatarImageFactory avatarImageWithUserInitials:[Helper initialsFromName:user.name]
                                                                backgroundColor:[UIColor randomColor]
                                                                      textColor:[UIColor whiteColor]
                                                                           font:[UIFont lightFontWithSize:13.0]
                                                                       diameter:kJSQMessagesCollectionViewAvatarSizeDefault];
    }
    
    NSString *userKey = [NSString stringWithFormat:@"%lu", (unsigned long)user.userID];
    [self.avatars setObject:userAvatar forKey:userKey];
}

- (void)removeUser:(VOUser *)user {
    [self.users removeObject:user];
    
    NSString *userKey = [NSString stringWithFormat:@"%lu", (unsigned long)user.userID];
    [self.avatars removeObjectForKey:userKey];
}

- (VOUser *)userFromID:(NSUInteger)userID {
    for (VOUser *user in self.users) {
//        if ([user.userID isEqualToString:userID]) {
//            return user;
//        }
        
        if (user.userID == userID)
            return user;
    }
    
    return nil;
}

@end
