//
//  StoreViewController.m
//  Fanta
//
//  Created by Mario Concilio on 3/7/16.
//  Copyright © 2016 Mario Concilio. All rights reserved.
//

#import "StoreViewController.h"
#import "StoreCell.h"
#import "VOStoreItem.h"
#import "UIViewController+BaseViewController.h"
#import "Constants.h"
#import "ItemViewController.h"
#import "Helper.h"
#import "Formatter.h"
#import "VOCart.h"
#import "VOStoreItem.h"
#import "CartViewController.h"
#import "PopoverTransition.h"
#import "AppDelegate.h"
#import "StorePlaceholder.h"
#import "Generator.h"

#import <AsyncDisplayKit.h>
#import <Sheriff/GIBadgeView.h>
#import <CardIO.h>
#import <RKTabView.h>
#import <POP.h>

@interface StoreViewController () <ASCollectionDataSource, ASCollectionDelegate, CartViewDelegate, CardIOPaymentViewControllerDelegate, RKTabViewDelegate> {
    ASCollectionView *_collectionView;
}

@property (nonatomic, strong) NSMutableArray<VOStoreItem *> *dataSource;
@property (nonatomic, weak) GIBadgeView *badge;
@property (nonatomic, weak) UIRefreshControl *refresher;
@property (nonatomic, weak) StorePlaceholder *placeholderNode;

@end

static NSString *const kItemSegue       = @"itemSegue";
static NSString *const kCartSegue       = @"cartSegue";
static NSString *const kCheckoutSegue   = @"checkoutSegue";

@implementation StoreViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    /*
     *  base view controller
     */
    [self addDrawerButtonDark];
    [self setupReachability];
    
    self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"fnt_shopping_cart"]
                                                                              style:UIBarButtonItemStylePlain
                                                                             target:self
                                                                             action:@selector(doOpenShoppingCart:)];
    
    GIBadgeView *badge = [[GIBadgeView alloc] init];
    badge.font = [UIFont regularFontWithSize:12.0];
    badge.backgroundColor = [UIColor customPink];
    badge.textColor = [UIColor whiteColor];
    badge.rightOffset = 10.0;
    badge.topOffset = 5.0;
    
    UIView *barButtonView = [self.navigationItem.rightBarButtonItem valueForKey:@"view"];
    [barButtonView addSubview:badge];
    _badge = badge;
    
    /*
     *  placeholder
     */
    StorePlaceholder *placeholder = [[StorePlaceholder alloc] initWithNumberOfRows:3];
    placeholder.frame = self.view.bounds;
//    placeholder.view.contentInset = UIEdgeInsetsMake(0.0, 0.0, kTabViewHeight, 0.0);
    _placeholderNode = placeholder;
    [self.view addSubnode:_placeholderNode];
    
    /*
     *  collection view
     */
    UICollectionViewFlowLayout *layout = [[UICollectionViewFlowLayout alloc] init];
    layout.minimumInteritemSpacing = kStoreCellPadding;
    layout.minimumLineSpacing = kStoreCellPadding;
    
    _collectionView = [[ASCollectionView alloc] initWithFrame:self.view.bounds collectionViewLayout:layout];
    _collectionView.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight;
    _collectionView.asyncDataSource = self;
    _collectionView.asyncDelegate = self;
    _collectionView.alpha = 0.0;
    _collectionView.backgroundColor = [UIColor customDarkBackground];
    _collectionView.contentInset = UIEdgeInsetsMake(64.0, 0.0, kTabViewHeight, 0.0);
    
    [self.view addSubview:_collectionView];
    
    /*
     *  tabbar
     */
    RKTabItem *partyItem = [RKTabItem createUsualItemWithImageEnabled:[UIImage imageNamed:@"fnt_store_party_selected"]
                                                       imageDisabled:[UIImage imageNamed:@"fnt_store_party"]];
    RKTabItem *athleticItem = [RKTabItem createUsualItemWithImageEnabled:[UIImage imageNamed:@"fnt_store_clothes_selected"]
                                                           imageDisabled:[UIImage imageNamed:@"fnt_store_clothes"]];
    
    partyItem.titleString = @"Festas";
    athleticItem.titleString = @"Atlética";
    
    RKTabView *tabView = [[RKTabView alloc] initWithFrame:CGRectMake(0.0, UIKeyWindowHeight() - kTabViewHeight, UIKeyWindowWidth(), kTabViewHeight)];
    tabView.tabItems = @[partyItem, athleticItem];
    tabView.titlesFont = [UIFont lightFontWithSize:12.0];
    tabView.titlesFontColor = [UIColor whiteColor];
    tabView.titlesFontColorEnabled = [UIColor customOrange];
    tabView.horizontalInsets = HorizontalEdgeInsetsMake(70.0, 70.0);
    tabView.darkensBackgroundForEnabledTabs = YES;
    tabView.upperSeparatorLineColor = [UIColor lightGrayColor];
    tabView.delegate = self;
    [tabView switchTabIndex:0];
    [self.view addSubview:tabView];
    
    UIBlurEffect *blurEffect = [UIBlurEffect effectWithStyle:UIBlurEffectStyleDark];
    UIVisualEffectView *blurEffectView = [[UIVisualEffectView alloc] initWithEffect:blurEffect];
    blurEffectView.frame = CGRectMake(0.0, 0.0, UIKeyWindowWidth(), kTabViewHeight);
    tabView.backgroundColor = [UIColor clearColor];
    [tabView addSubview:blurEffectView];
    
    /*
     *  refresher
     */
    UIRefreshControl *refresher = [[UIRefreshControl alloc] init];
    refresher.tintColor = [UIColor lightGrayColor];
    [refresher addTarget:self action:@selector(refreshData) forControlEvents:UIControlEventValueChanged];
    _refresher = refresher;
    [_collectionView addSubview:_refresher];
    
//    [self.refresher beginRefreshing];
    [self refreshData];
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    [CardIOUtilities preload];
    [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleLightContent animated:YES];
//    [self refreshData];
//    [self.addButton animateToType:buttonAddType];
//    [self.drawerButton animateToType:buttonMenuType];
    
    VOCart *myCart = [UIAppDelegate() myCart];
    self.badge.badgeValue = myCart.items.count;
}

- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Helper Methods
- (void)refreshData {
    __weak typeof(self) weakSelf = self;
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(2.0 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        _dataSource = [[NSMutableArray alloc] initWithArray:[weakSelf populateItems]];
        [_collectionView reloadData];
        [weakSelf.refresher endRefreshing];
        
        POPBasicAnimation *fadeIn = [POPBasicAnimation animationWithPropertyNamed:kPOPViewAlpha];
        fadeIn.toValue = @(1.0);
        fadeIn.completionBlock = ^(POPAnimation *anim, BOOL finished) {
            if (finished) {
//                    [self.placeholderNode removeFromSupernode];
//                    self.placeholderNode = nil;
                self.placeholderNode.hidden = YES;
            }
        };
        
        [_collectionView pop_addAnimation:fadeIn forKey:@"fadeIn"];
    });
}

#warning DEBUG ONLY
- (NSMutableArray *)populateItems {
    NSMutableArray *array = [[NSMutableArray alloc] initWithCapacity:105];
    for (long i=0; i<100; i++) {
        @autoreleasepool {
            long x = i % 15;
            
            VOStoreItem *item = [[VOStoreItem alloc] init];
            item.title = [NSString stringWithFormat:@"Item %ld", x];
            item.content = [Generator mussumIpsum];
            item.price = [NSDecimalNumber decimalNumberWithString:[NSString stringWithFormat:@"%ld", i*5 + 10]];
            item.images = @[[Generator randomImage],
                            [Generator randomImage],
                            [Generator randomImage],
                            [Generator randomImage],
                            [Generator randomImage]];
            
            [array addObject:item];
        }
    }
    
    return array;
}

#pragma mark - Navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    if ([segue.identifier isEqualToString:kItemSegue]) {
        VOStoreItem *item = (VOStoreItem *)sender;
        ItemViewController *vc = segue.destinationViewController;
        [vc setupItem:item
          cachedImage:self.cell.imageNode.image
            withFrame:[self.cell.view convertRect:[self.cell.view frame] toView:nil]];
    }
    else if ([segue.identifier isEqualToString:kCartSegue]) {
        CartViewController *vc = segue.destinationViewController;
        vc.delegate = self;
        
        UIView *view = (UIView *) sender;
        CGRect frame = [view.superview convertRect:view.frame toView:nil];
        vc.popover_startFrame = frame;
    }
}

- (void)doOpenShoppingCart:(UIBarButtonItem *)sender {
    UIView *view = [sender valueForKey:@"view"];
    [self performSegueWithIdentifier:kCartSegue sender:view];
}

#pragma mark - CollectionView Data Source
- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    return self.dataSource.count;
}

- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView {
    return 1;
}

- (ASCellNode *)collectionView:(ASCollectionView *)collectionView nodeForItemAtIndexPath:(NSIndexPath *)indexPath {
    VOStoreItem *item = [self.dataSource objectAtIndex:indexPath.row];
    StoreCell *node = [[StoreCell alloc] initWithStoreItem:item];
    
    return node;
}

- (UIEdgeInsets)collectionView:(ASCollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout insetForSectionAtIndex:(NSInteger)section {
    return UIEdgeInsetsMake(kStoreCellPadding, kStoreCellPadding, kStoreCellPadding, kStoreCellPadding);
}

#pragma mark - CollectionView Delegate
- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath {
    /* 
     since [collectionView cellForItemAtIndexPath:] returns _ASCollectionViewCell*, a private class,
     this forces the method [_ASCollectionViewCell node] to be called, returning ASCellNode*
     */
    id viewCell = [collectionView cellForItemAtIndexPath:indexPath];
    self.cell = [viewCell performSelector:@selector(node)];
    
    // the apple way
//    self.storeCell = [[collectionView cellForItemAtIndexPath:indexPath] performselector:@selector(node)];

    VOStoreItem *item = self.dataSource[indexPath.row];
    [self performSegueWithIdentifier:kItemSegue sender:item];
}

#pragma mark - Cart View Delegate
- (void)cartDidCheckout:(CartViewController *)cart {
    VOCart *myCart = [UIAppDelegate() myCart];
    if (myCart.items.count < 1)
        return;
    
    CardIOPaymentViewController *scanViewController = [[CardIOPaymentViewController alloc] initWithPaymentDelegate:self];
    scanViewController.languageOrLocale = @"pt_BR";
    scanViewController.navigationBar.tintColor = [UIColor customPink];
//    scanViewController.navigationItem.title = nil;
    scanViewController.modalPresentationStyle = UIModalPresentationFormSheet;
    scanViewController.navigationBarStyle = UIBarStyleBlack;
    scanViewController.keepStatusBarStyle = YES;
    scanViewController.hideCardIOLogo = YES;
    
//    [self presentViewController:scanViewController animated:YES completion:NULL];
    
    UIViewController *vc = scanViewController.topViewController;
//    vc.navigationItem.title = nil;
//    vc.navigationItem.title = @" ";
//    vc.view.backgroundColor = [UIColor customDarkBackground];
    
    UILabel *totalLabel = [[UILabel alloc] initWithFrame:CGRectMake(10.0, 0.0, UIKeyWindowWidth() - 20.0, 20.0)];
    totalLabel.text = [NSString stringWithFormat:@"Total: %@", [Formatter formatCurrency:myCart.total]];
    totalLabel.font = [UIFont boldFontWithSize:16.0];
    totalLabel.textColor = [UIColor blackColor];
    totalLabel.textAlignment = NSTextAlignmentRight;
    
    UIView *footerView = [[UIView alloc] init];
    [footerView addSubview:totalLabel];
    
    UITableView *tableView = [[[vc.view.subviews firstObject] subviews] firstObject];
    tableView.scrollEnabled = YES;
    tableView.tableFooterView = footerView;
    
    [self presentViewController:scanViewController animated:YES completion:NULL];
}

#pragma mark - CardIO Payment View Controller Delegate
- (void)userDidProvideCreditCardInfo:(CardIOCreditCardInfo *)info inPaymentViewController:(CardIOPaymentViewController *)paymentViewController {
    NSLog(@"Scan succeeded with info: %@", info);
    
    // Do whatever needs to be done to deliver the purchased items.
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (void)userDidCancelPaymentViewController:(CardIOPaymentViewController *)paymentViewController {
    NSLog(@"User cancelled scan");
    [self dismissViewControllerAnimated:YES completion:nil];
}

#pragma mark - RKTabView Delegate
- (void)tabView:(RKTabView *)tabView tabBecameEnabledAtIndex:(NSUInteger)index tab:(RKTabItem *)tabItem {
    _placeholderNode.hidden = NO;
    _collectionView.alpha = 0.0;
    
    self.dataSource = nil;
    [_collectionView reloadData];
    [self refreshData];
}

@end
