//
//  CartViewController.m
//  Fanta
//
//  Created by Mario Concilio on 3/14/16.
//  Copyright © 2016 Mario Concilio. All rights reserved.
//

#import "CartViewController.h"
#import "PopoverTransition.h"
#import "CartCell.h"
#import "VOCart.h"
#import "VOStoreItem.h"
#import "Constants.h"
#import "Helper.h"
#import "Formatter.h"
#import "AppDelegate.h"

#import <AsyncDisplayKit.h>
#import <VBFPopFlatButton.h>
#import <CardIO.h>
#import <MoipSDK.h>

@interface CartViewController () <ASTableDelegate, ASTableDataSource, PopoverProtocol, CardIOPaymentViewControllerDelegate> {
    ASTableView *_tableView;
}

@property (weak, nonatomic) IBOutlet UIVisualEffectView *headerView;
@property (weak, nonatomic) IBOutlet UIVisualEffectView *footerView;
@property (weak, nonatomic) IBOutlet UILabel *totalLabel;
@property (weak, nonatomic) IBOutlet UIButton *checkoutButton;

@end

@implementation CartViewController

- (instancetype)initWithCoder:(NSCoder *)aDecoder {
    self = [super initWithCoder:aDecoder];
    if (self) {
        _tableView = [[ASTableView alloc] initWithFrame:CGRectZero style:UITableViewStylePlain asyncDataFetching:YES];
        _tableView.asyncDataSource = self;
        _tableView.asyncDelegate = self;
        
        self.modalPresentationStyle = UIModalPresentationCustom;
        self.transitioningDelegate = self;
    }
    
    return self;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    [self.view addSubview:_tableView];
    _tableView.tableFooterView = [[UIView alloc] initWithFrame:CGRectZero];
    _tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    
    if (self.transitioningDelegate) {
        self.view.layer.cornerRadius = 5.0;
        self.view.clipsToBounds = YES;
    }
    
    VOCart *myCart = [UIAppDelegate() myCart];
    [self updateTotalLabel:myCart.total];
    [myCart addObserver:self forKeyPath:@"total" options:NSKeyValueObservingOptionNew context:nil];
    
    self.checkoutButton.layer.cornerRadius = 5.0;
    self.checkoutButton.clipsToBounds = YES;

    self.headerView.layer.borderWidth = 0.5;
    self.headerView.layer.borderColor = [UIColor lightGrayColor].CGColor;
    self.footerView.layer.borderWidth = 0.5;
    self.footerView.layer.borderColor = [UIColor lightGrayColor].CGColor;
    
    CGFloat statusHeight = CGRectGetHeight([[UIApplication sharedApplication] statusBarFrame]);
    self.popover_endFrame = (CGRect) {
        .origin.x = kCartPadding,
        .origin.y = statusHeight + kBlurNavbarHeight,
        .size.width = UIKeyWindowWidth() - 2*kCartPadding,
        .size.height = UIKeyWindowHeight() - kBlurNavbarHeight - kBlurToolbarHeight,
    };
    
    CGFloat y = (kCommentTableHeaderHeight - kVBFButtonSize.width) / 2;
    CGRect frame = (CGRect) {
        .origin.x = 20.0,
        .origin.y = y,
        .size = kVBFButtonSize,
    };
    
    VBFPopFlatButton *closeButton = [[VBFPopFlatButton alloc] initWithFrame:frame
                                                                 buttonType:buttonCloseType
                                                                buttonStyle:buttonPlainStyle
                                                      animateToInitialState:NO];
    closeButton.lineThickness = 1.0;
    closeButton.tintColor = [UIColor customPink];
    [closeButton addTarget:self action:@selector(dismiss) forControlEvents:UIControlEventTouchUpInside];
    [self.headerView addSubview:closeButton];
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
//    [CardIOUtilities preload];
}

- (void)viewWillLayoutSubviews {
    [super viewWillLayoutSubviews];
    _tableView.frame = self.view.bounds;
}

- (void)viewDidLayoutSubviews {
    [super viewDidLayoutSubviews];
    _tableView.contentInset = UIEdgeInsetsMake(CGRectGetHeight(self.headerView.frame),
                                               0,
                                               CGRectGetHeight(self.footerView.frame),
                                               0);
    
//    self.headerView.layer.shadowColor = [UIColor blackColor].CGColor;
//    self.headerView.layer.shadowOffset = CGSizeMake(0.0, -1.0);
//    self.headerView.layer.shadowOpacity = 0.6;
//    self.headerView.layer.shadowRadius = 4.0;
//    self.headerView.layer.shadowPath = [UIBezierPath bezierPathWithRect:self.headerView.frame].CGPath;
//    self.headerView.layer.masksToBounds = NO;
}

- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
//    [Helper saveMyCart:self.cart];
}

- (void)dealloc {
    VOCart *myCart = [UIAppDelegate() myCart];
    [myCart removeObserver:self forKeyPath:@"total"];
}

#pragma mark - Helper Methods
- (void)updateTotalLabel:(NSDecimalNumber *)newTotal {
    self.totalLabel.text = [NSString stringWithFormat:@"Total: %@", [Formatter formatCurrency:newTotal]];
}

- (void)observeValueForKeyPath:(NSString *)keyPath ofObject:(id)object change:(NSDictionary<NSString *,id> *)change context:(void *)context {
    if ([keyPath isEqualToString:@"total"]) {
        [self updateTotalLabel:change[@"new"]];
    }
}

#pragma mark - Action Methods
- (void)addQtyItem:(ASImageNode *)sender {
    NSIndexPath *indexPath = [_tableView indexPathForNode:(ASCellNode *) sender.supernode];
    VOCart *myCart = [UIAppDelegate() myCart];
    VOStoreItem *item = myCart.items.allKeys[indexPath.row];
    [myCart updateStoreItem:item plusQuantity:1];
}

- (void)minusQtyItem:(ASImageNode *)sender {
    NSIndexPath *indexPath = [_tableView indexPathForNode:(ASCellNode *) sender.supernode];
    VOCart *myCart = [UIAppDelegate() myCart];
    VOStoreItem *item = myCart.items.allKeys[indexPath.row];
    [myCart updateStoreItem:item minusQuantity:1];
}

- (void)deleteItem:(ASImageNode *)sender {
    NSIndexPath *indexPath = [_tableView indexPathForNode:(ASCellNode *) sender.supernode];
    VOCart *myCart = [UIAppDelegate() myCart];
    VOStoreItem *item = myCart.items.allKeys[indexPath.row];
    [myCart removeStoreItem:item];

    [_tableView beginUpdates];
    [_tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationRight];
    [_tableView endUpdates];
}

- (IBAction)doCheckout:(UIButton *)sender {
    if (self.transitioningDelegate) {
        [self dismissViewControllerAnimated:YES completion:NULL];
        [self.delegate cartDidCheckout:self];
    }
    else {
        VOCart *myCart = [UIAppDelegate() myCart];
        if (myCart.items.count < 1)
            return;
        
        CardIOPaymentViewController *scanViewController = [[CardIOPaymentViewController alloc] initWithPaymentDelegate:self];
        scanViewController.languageOrLocale = @"pt_BR";
        scanViewController.navigationBar.tintColor = [UIColor customPink];
        //    scanViewController.navigationItem.title = nil;
        scanViewController.modalPresentationStyle = UIModalPresentationFormSheet;
        scanViewController.navigationBarStyle = UIBarStyleBlack;
        scanViewController.keepStatusBarStyle = YES;
        scanViewController.hideCardIOLogo = YES;
        
        //    [self presentViewController:scanViewController animated:YES completion:NULL];
        
        UIViewController *vc = scanViewController.topViewController;
        //    vc.navigationItem.title = nil;
        //    vc.navigationItem.title = @" ";
        //    vc.view.backgroundColor = [UIColor customDarkBackground];
        
        UILabel *totalLabel = [[UILabel alloc] initWithFrame:CGRectMake(10.0, 0.0, UIKeyWindowWidth() - 20.0, 20.0)];
        totalLabel.text = [NSString stringWithFormat:@"Total: %@", [Formatter formatCurrency:myCart.total]];
        totalLabel.font = [UIFont boldFontWithSize:16.0];
        totalLabel.textColor = [UIColor blackColor];
        totalLabel.textAlignment = NSTextAlignmentRight;
        
        UIView *footerView = [[UIView alloc] init];
        [footerView addSubview:totalLabel];
        
        UITableView *tableView = [[[vc.view.subviews firstObject] subviews] firstObject];
        tableView.scrollEnabled = YES;
        tableView.tableFooterView = footerView;
        
        [self presentViewController:scanViewController animated:YES completion:NULL];
    }
    
//    CardIOPaymentViewController *scanViewController = [[CardIOPaymentViewController alloc] initWithPaymentDelegate:self];
//    scanViewController.modalPresentationStyle = UIModalPresentationFormSheet;
//    [self presentViewController:scanViewController animated:YES completion:nil];
    
//    CardIOView *view = [[CardIOView alloc] initWithFrame:CGRectMake(0, 44, UIKeyWindowWidth(), UIKeyWindowWidth())];
//    view.delegate = self;
//    [self.view addSubview:view];
}

- (void)dismiss {
    [self dismissViewControllerAnimated:YES completion:NULL];
}

#pragma mark - Table View Data Source
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    VOCart *myCart = [UIAppDelegate() myCart];
    return myCart.items.allKeys.count;
}

- (ASCellNode *)tableView:(ASTableView *)tableView nodeForRowAtIndexPath:(NSIndexPath *)indexPath {
    VOCart *myCart = [UIAppDelegate() myCart];
    VOStoreItem *item = myCart.items.allKeys[indexPath.row];
    NSUInteger qty = [myCart.items[item] unsignedIntegerValue];
    CartCell *cell = [[CartCell alloc] initWithStoreItem:item quantity:qty];
    
    [cell.addNode addTarget:self action:@selector(addQtyItem:) forControlEvents:ASControlNodeEventTouchUpInside];
    [cell.minusNode addTarget:self action:@selector(minusQtyItem:) forControlEvents:ASControlNodeEventTouchUpInside];
    [cell.deleteNode addTarget:self action:@selector(deleteItem:) forControlEvents:ASControlNodeEventTouchUpInside];
    
    return cell;
}

#pragma mark - Table View Delegate
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    //TODO: open store item
}

- (BOOL)shouldBatchFetchForTableView:(ASTableView *)tableView {
//    return self.postsDataSource.count < kMaxNumberOfPosts;
    return NO;
}

#pragma mark - View Controller Transitioning Delegate
- (UIPresentationController *)presentationControllerForPresentedViewController:(UIViewController *)presented presentingViewController:(UIViewController *)presenting sourceViewController:(UIViewController *)source {
    PopoverPresentationController *controller = [[PopoverPresentationController alloc] initWithPresentedViewController:presented
                                                                                              presentingViewController:presenting];
    return controller;
}

- (id <UIViewControllerAnimatedTransitioning>)animationControllerForPresentedController:(UIViewController *)presented
                                                                   presentingController:(UIViewController *)presenting
                                                                       sourceController:(UIViewController *)source {
    PopoverAnimationController *animator = [[PopoverAnimationController alloc] init];
    return animator;
}

- (id <UIViewControllerAnimatedTransitioning>)animationControllerForDismissedController:(UIViewController *)dismissed {
    PopoverAnimationController *animator = [[PopoverAnimationController alloc] init];
    return animator;
}

#pragma mark - CardIO Payment View Controller Delegate
- (void)userDidProvideCreditCardInfo:(CardIOCreditCardInfo *)info inPaymentViewController:(CardIOPaymentViewController *)paymentViewController {
    NSLog(@"Scan succeeded with info: %@", info);
    
    [MoipSDK importPublicKey:@"OQ3EY2OXB9RJGULIG8QSJBUUWSSAPYXH7HHIXXPP"];
    MPKCreditCard *card = [[MPKCreditCard alloc] init];
    card.number = info.cardNumber;
    card.cvc = info.cvv;
    card.expirationMonth = [NSString stringWithFormat:@"%lu", info.expiryMonth];
    card.expirationYear = [NSString stringWithFormat:@"%lu", info.expiryYear];
    
    NSString *cryptCard = [MoipSDK encryptCreditCard:card];
    NSLog(@"%@", cryptCard);
    
    // Do whatever needs to be done to deliver the purchased items.
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (void)userDidCancelPaymentViewController:(CardIOPaymentViewController *)paymentViewController {
    NSLog(@"User cancelled scan");
    [self dismissViewControllerAnimated:YES completion:nil];
}

@end
