//
//  CartCell.h
//  Fanta
//
//  Created by Mario Concilio on 3/16/16.
//  Copyright © 2016 Mario Concilio. All rights reserved.
//

#import <AsyncDisplayKit/AsyncDisplayKit.h>

@class VOStoreItem;
@interface CartCell : ASCellNode

@property (nonatomic, weak, readonly) ASNetworkImageNode *imageNode;
@property (nonatomic, weak, readonly) ASTextNode *titleNode;
@property (nonatomic, weak, readonly) ASTextNode *qtyNode;
@property (nonatomic, weak, readonly) ASTextNode *priceNode;
@property (nonatomic, weak, readonly) ASImageNode *deleteNode;
@property (nonatomic, weak, readonly) ASImageNode *addNode;
@property (nonatomic, weak, readonly) ASImageNode *minusNode;

- (instancetype)initWithStoreItem:(VOStoreItem *)item quantity:(NSUInteger)qty;

+ (instancetype)new UNAVAILABLE_ATTRIBUTE;
- (instancetype)init UNAVAILABLE_ATTRIBUTE;

@end
