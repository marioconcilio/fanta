//
//  CartCell.m
//  Fanta
//
//  Created by Mario Concilio on 3/16/16.
//  Copyright © 2016 Mario Concilio. All rights reserved.
//

#import "CartCell.h"
#import "VOStoreItem.h"
#import "TextStyles.h"
#import "Constants.h"
#import "Formatter.h"

#import <POP.h>
#import <WebASDKImageManager.h>

@interface CartCell () <ASNetworkImageNodeDelegate>

@property (nonatomic, weak) ASNetworkImageNode *imageNode;
@property (nonatomic, weak) ASTextNode *titleNode;
@property (nonatomic, weak) ASTextNode *qtyNode;
@property (nonatomic, weak) ASTextNode *priceNode;
@property (nonatomic, weak) ASImageNode *deleteNode;
@property (nonatomic, weak) ASImageNode *addNode;
@property (nonatomic, weak) ASImageNode *minusNode;

@property (nonatomic, strong) VOStoreItem *item;
@property (nonatomic, assign) NSUInteger qty;

@end

@implementation CartCell

- (instancetype)initWithStoreItem:(VOStoreItem *)item quantity:(NSUInteger)qty {
    self = [super init];
    if (self) {
        _item = item;
        _qty = qty;
        
        /*
         * Image Node
         */
        ASNetworkImageNode *imageNode = [[ASNetworkImageNode alloc] initWithWebImage];
        imageNode.URL = [NSURL URLWithString:item.images[0]];
        imageNode.preferredFrameSize = kCartImageSize;
        imageNode.delegate = self;
        _imageNode = imageNode;
        [self addSubnode:_imageNode];
        
        /*
         * Title Node
         */
        ASTextNode *titleNode = [[ASTextNode alloc] init];
        titleNode.attributedString = [[NSAttributedString alloc] initWithString:item.title
                                                                     attributes:[TextStyles cartTitleStyle]];
        titleNode.flexGrow = NO;
        titleNode.flexShrink = NO;
        titleNode.truncationMode = NSLineBreakByTruncatingTail;
        titleNode.maximumNumberOfLines = 1;
        _titleNode = titleNode;
        [self addSubnode:_titleNode];
        
        /*
         * Qty Node
         */
        ASTextNode *qtyNode = [[ASTextNode alloc] init];
        qtyNode.attributedString = [[NSAttributedString alloc] initWithString:[NSString stringWithFormat:@"%lu", qty]
                                                                   attributes:[TextStyles cartQtyStyle]];
        qtyNode.flexGrow = NO;
        qtyNode.flexShrink = NO;
        qtyNode.maximumNumberOfLines = 1;
        _qtyNode = qtyNode;
        [self addSubnode:_qtyNode];
        
        /*
         * Price Node
         */
        ASTextNode *priceNode = [[ASTextNode alloc] init];
        NSDecimal qtyNumber = [@(qty) decimalValue];
        NSDecimalNumber *subtotal = [item.price decimalNumberByMultiplyingBy:[NSDecimalNumber decimalNumberWithDecimal:qtyNumber]];
        priceNode.attributedString = [[NSAttributedString alloc] initWithString:[Formatter formatCurrency:subtotal]
                                                                     attributes:[TextStyles cartPriceStyle]];
        priceNode.flexShrink = NO;
//        priceNode.flexGrow = YES;
        priceNode.maximumNumberOfLines = 1;
        _priceNode = priceNode;
        [self addSubnode:_priceNode];
        
        /*
         * Delete Node
         */
        ASImageNode *deleteNode = [[ASImageNode alloc] init];
        deleteNode.image = [UIImage imageNamed:@"fnt_cart_thrash"];
        deleteNode.preferredFrameSize = kCartButtonSize;
        deleteNode.userInteractionEnabled = YES;
        deleteNode.hitTestSlop = UIEdgeInsetsMake(-20.0, -20.0, -20.0, -20.0);
        _deleteNode = deleteNode;
        [self addSubnode:_deleteNode];
        
        /*
         * Add Node
         */
        ASImageNode *addNode = [[ASImageNode alloc] init];
        addNode.image = [UIImage imageNamed:@"fnt_add_filled"];
        addNode.preferredFrameSize = kCartButtonSize;
        addNode.userInteractionEnabled = YES;
        addNode.hitTestSlop = UIEdgeInsetsMake(-20.0, -20.0, -20.0, -20.0);
        [addNode addTarget:self action:@selector(doIncrementQty) forControlEvents:ASControlNodeEventTouchUpInside];
        _addNode = addNode;
        [self addSubnode:_addNode];
        
        /*
         * Minus Node
         */
        ASImageNode *minusNode = [[ASImageNode alloc] init];
        minusNode.image = [UIImage imageNamed:@"fnt_minus_filled"];
        minusNode.preferredFrameSize = kCartButtonSize;
        minusNode.userInteractionEnabled = YES;
        minusNode.hitTestSlop = UIEdgeInsetsMake(-20.0, -20.0, -20.0, -20.0);
        [minusNode addTarget:self action:@selector(doDecrementQty) forControlEvents:ASControlNodeEventTouchUpInside];
        _minusNode = minusNode;
        [self addSubnode:_minusNode];
        
        self.selectionStyle = UITableViewCellSelectionStyleNone;
        
        [self addObserver:self forKeyPath:@"qty" options:NSKeyValueObservingOptionNew context:nil];
    }
    
    return self;
}

- (ASLayoutSpec *)layoutSpecThatFits:(ASSizeRange)constrainedSize {
    // flexible spacer between title and delete button
    ASLayoutSpec *spacer = [[ASLayoutSpec alloc] init];
    spacer.flexGrow = YES;
    
    // horizontal stack for title and delete button
    ASStackLayoutSpec *titleStack = [ASStackLayoutSpec stackLayoutSpecWithDirection:ASStackLayoutDirectionHorizontal
                                                                            spacing:5.0
                                                                     justifyContent:ASStackLayoutJustifyContentStart
                                                                         alignItems:ASStackLayoutAlignItemsCenter
                                                                           children:@[_titleNode, spacer, _deleteNode]];
    titleStack.alignSelf = ASStackLayoutAlignSelfStretch;
    
    ASStackLayoutSpec *bottomStack = [ASStackLayoutSpec stackLayoutSpecWithDirection:ASStackLayoutDirectionHorizontal
                                                                             spacing:10.0
                                                                      justifyContent:ASStackLayoutJustifyContentStart
                                                                          alignItems:ASStackLayoutAlignItemsCenter
                                                                            children:@[_minusNode, _qtyNode, _addNode, spacer, _priceNode]];
    bottomStack.alignSelf = ASStackLayoutAlignSelfStretch;

    // vertical spec of cell main content
    ASStackLayoutSpec *contentSpec = [ASStackLayoutSpec stackLayoutSpecWithDirection:ASStackLayoutDirectionVertical
                                                                             spacing:10.0
                                                                      justifyContent:ASStackLayoutJustifyContentStart
                                                                          alignItems:ASStackLayoutAlignItemsStart
                                                                            children:@[titleStack, spacer, bottomStack]];
    contentSpec.alignItems = ASStackLayoutAlignSelfStretch;
    contentSpec.flexGrow = YES;
    
    // horizontal spec for image
    ASStackLayoutSpec *imageContentSpec = [ASStackLayoutSpec stackLayoutSpecWithDirection:ASStackLayoutDirectionHorizontal
                                                                                  spacing:10.0
                                                                           justifyContent:ASStackLayoutJustifyContentStart
                                                                               alignItems:ASStackLayoutAlignItemsStart
                                                                                 children:@[_imageNode, contentSpec]];
    
    return [ASInsetLayoutSpec insetLayoutSpecWithInsets:UIEdgeInsetsMake(10, 10, 10, 10) child:imageContentSpec];
}

- (void)observeValueForKeyPath:(NSString *)keyPath ofObject:(id)object change:(NSDictionary<NSString *,id> *)change context:(void *)context {
    if ([keyPath isEqualToString:@"qty"]) {
        NSUInteger newQty = [change[@"new"] unsignedIntegerValue];
        _qtyNode.attributedString = [[NSAttributedString alloc] initWithString:[NSString stringWithFormat:@"%lu", newQty]
                                                                    attributes:[TextStyles cartQtyStyle]];
        
        NSDecimal decimal = [@(newQty) decimalValue];
        NSDecimalNumber *newSubtotal = [self.item.price decimalNumberByMultiplyingBy:[NSDecimalNumber decimalNumberWithDecimal:decimal]];
        _priceNode.attributedString = [[NSAttributedString alloc] initWithString:[Formatter formatCurrency:newSubtotal]
                                                                      attributes:[TextStyles cartPriceStyle]];
        [self setNeedsLayout];
    }
}

- (void)dealloc {
    [self removeObserver:self forKeyPath:@"qty" context:nil];
}

#pragma mark - Actions
- (void)doIncrementQty {
    if (_qty == 9) return;
    [self setValue:@(++_qty) forKey:@"qty"];
}

- (void)doDecrementQty {
    if (_qty == 1) return;
    [self setValue:@(--_qty) forKey:@"qty"];
}

#pragma mark - ASNetworkImage Delegate
- (void)imageNode:(ASNetworkImageNode *)imageNode didLoadImage:(UIImage *)image {
    if (image) {
        POPBasicAnimation *anim = [POPBasicAnimation animationWithPropertyNamed:kPOPViewAlpha];
        anim.fromValue = @(0.0);
        anim.toValue = @(1.0);
        [imageNode pop_removeAllAnimations];
        [imageNode pop_addAnimation:anim forKey:@"fadeIn"];
    }
}

@end
