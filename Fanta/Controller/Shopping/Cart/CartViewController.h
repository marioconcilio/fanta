//
//  CartViewController.h
//  Fanta
//
//  Created by Mario Concilio on 3/14/16.
//  Copyright © 2016 Mario Concilio. All rights reserved.
//

#import <UIKit/UIKit.h>

@class CartViewController;
@protocol CartViewDelegate <NSObject>

- (void)cartDidCheckout:(CartViewController *)cart;

@end

@interface CartViewController : UIViewController

@property (nonatomic, weak) id<CartViewDelegate> delegate;

@end
