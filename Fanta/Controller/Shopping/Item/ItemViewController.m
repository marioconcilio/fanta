//
//  ItemViewController.m
//  Fanta
//
//  Created by Mario Concilio on 3/8/16.
//  Copyright © 2016 Mario Concilio. All rights reserved.
//

#import "ItemViewController.h"
#import "Constants.h"
#import "VOStoreItem.h"
#import "ExpandStoreCellTransition.h"
#import "Constants.h"
#import "TextStyles.h"
#import "Formatter.h"
#import "CartViewController.h"
#import "PopoverTransition.h"
#import "VOCart.h"
#import "AppDelegate.h"

#import <AsyncDisplayKit.h>
#import <VBFPopFlatButton.h>
#import <IDMPhotoBrowser.h>
#import <WebASDKImageManager.h>
#import <POP.h>

#pragma mark -
#pragma mark - PageItemViewController

@interface PageItemViewController : UIViewController <ASNetworkImageNodeDelegate>

@property (nonatomic, weak) ASNetworkImageNode *imageNode;
@property (nonatomic, assign) NSUInteger itemIndex;
@property (nonatomic, strong) NSString *imageName;

@end

@implementation PageItemViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
        ASNetworkImageNode *imageNode = [[ASNetworkImageNode alloc] initWithWebImage];
        imageNode.URL = [NSURL URLWithString:self.imageName];
        imageNode.contentMode = UIViewContentModeScaleAspectFill;
        imageNode.frame = (CGRect) {
            .size.width = UIKeyWindowWidth(),
            .size.height = UIKeyWindowWidth() * kPostImageNodeHeightScale,
        };
        imageNode.delegate = self;
        _imageNode = imageNode;
        
        dispatch_async(dispatch_get_main_queue(), ^{
            [self.view addSubnode:imageNode];
        });
    });
}

- (void)setImageName:(NSString *)imageName {
    _imageName = imageName;
    _imageNode.URL = [NSURL URLWithString:imageName];
}

#pragma mark - ASNetworkImage Delegate
- (void)imageNode:(ASNetworkImageNode *)imageNode didLoadImage:(UIImage *)image {
    if (image) {
        POPBasicAnimation *anim = [POPBasicAnimation animationWithPropertyNamed:kPOPViewAlpha];
        anim.fromValue = @(0.0);
        anim.toValue = @(1.0);
        [imageNode pop_removeAllAnimations];
        [imageNode pop_addAnimation:anim forKey:@"fadeIn"];
    }
}


@end

#pragma mark -
#pragma mark - ItemViewController

@interface ItemViewController () <UIPageViewControllerDataSource, UIPageViewControllerDelegate, UIViewControllerTransitioningDelegate>

@property (nonatomic, weak) UIScrollView *scrollView;
@property (nonatomic, strong) ExpandStoreCellAnimationController *animator;

@end

static NSString *const kAddToCartSegue = @"addToCartSegue";

@implementation ItemViewController

- (instancetype)initWithCoder:(NSCoder *)aDecoder {
    self = [super initWithCoder:aDecoder];
    if (self) {
        self.animator = [[ExpandStoreCellAnimationController alloc] init];
        self.modalPresentationStyle = UIModalPresentationCustom;
        self.transitioningDelegate = self;
    }
    
    return self;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    self.automaticallyAdjustsScrollViewInsets = NO;
    
    self.blurNavbar.layer.borderWidth = 0.5;
    self.blurNavbar.layer.borderColor = [UIColor lightGrayColor].CGColor;
    self.blurNavbar.hidden = YES;
    
    VBFPopFlatButton *closeButton = [[VBFPopFlatButton alloc] initWithFrame:(CGRect) {
        .origin.x = 10,
        .origin.y = (kBlurNavbarHeight - kVBFButtonSize.height) / 2,
        .size = kVBFButtonSize,
    }
                                                                 buttonType:buttonCloseType
                                                                buttonStyle:buttonPlainStyle
                                                      animateToInitialState:NO];
    closeButton.lineThickness = 1.0;
    closeButton.tintColor = [UIColor customPink];
    [closeButton addTarget:self action:@selector(dismiss) forControlEvents:UIControlEventTouchUpInside];
    [self.blurNavbar addSubview:closeButton];

    
    UIPageViewController *page = [[UIPageViewController alloc] initWithTransitionStyle:UIPageViewControllerTransitionStyleScroll
                                                                 navigationOrientation:UIPageViewControllerNavigationOrientationHorizontal
                                                                               options:nil];
    page.dataSource = self;
    page.delegate = self;
    page.view.frame = (CGRect) {
        .size.width = UIKeyWindowWidth(),
        .size.height = UIKeyWindowWidth() * kPostImageNodeHeightScale,
    };
    
    [page setViewControllers:@[[self itemControllerForIndex:0]]
                   direction:UIPageViewControllerNavigationDirectionForward
                    animated:NO
                  completion:NULL];
    
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(doOpenGallery)];
    [page.view addGestureRecognizer:tap];
    
    self.page = page;
    
    [self addChildViewController:page];
//    [self.view addSubview:page.view];
    [page didMoveToParentViewController:self];
    
    UIPageControl *pageControl = [[UIPageControl alloc] initWithFrame:CGRectMake(0, page.view.frame.size.height - 50, UIKeyWindowWidth(), 50)];
    pageControl.numberOfPages = self.item.images.count;
    pageControl.currentPage = 0;
    self.pageControl = pageControl;
    [page.view addSubview:pageControl];
    
    [[UIPageControl appearance] setPageIndicatorTintColor:[UIColor customLightBackground]];
    [[UIPageControl appearance] setCurrentPageIndicatorTintColor:[UIColor customOrange]];
    [[UIPageControl appearance] setBackgroundColor:[UIColor colorWithWhite:0.0 alpha:0.2]];
    
    UIScrollView *scrollView = [[UIScrollView alloc] initWithFrame:UIKeyWindow().frame];
    scrollView.autoresizingMask = UIViewAutoresizingFlexibleHeight | UIViewAutoresizingFlexibleWidth;
    scrollView.bounces = YES;
    scrollView.alwaysBounceVertical = YES;
    self.scrollView = scrollView;
    
    [scrollView addSubview:page.view];
    [self.view addSubview:scrollView];
    
    /*
     * Nodes
     */
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
        ASTextNode *titleNode = [[ASTextNode alloc] init];
        titleNode.attributedString = [[NSAttributedString alloc] initWithString:self.item.title
                                                                     attributes:[TextStyles storeItemTitleStyle]];
        [titleNode measure:CGSizeMake(UIKeyWindowWidth() - 2*kPostImageNodePadding, FLT_MAX)];
        titleNode.frame = (CGRect) {
            .origin.x = kPostImageNodePadding,
            .origin.y = CGRectGetHeight(self.page.view.frame) + kPostImageNodePadding,
            .size = titleNode.calculatedSize
        };
        
        ASTextNode *priceNode = [[ASTextNode alloc] init];
        priceNode.attributedString = [[NSAttributedString alloc] initWithString:[Formatter formatCurrency:self.item.price]
                                                                     attributes:[TextStyles storeItemPriceStyle]];
        [priceNode measure:CGSizeMake(UIKeyWindowWidth(), FLT_MAX)];
        priceNode.frame = (CGRect) {
            .origin.x = kPostImageNodePadding,
            .origin.y = CGRectGetMaxY(titleNode.frame) + kPostImageNodePadding,
            .size = priceNode.calculatedSize,
        };
        
        ASTextNode *contentNode = [[ASTextNode alloc] init];
        contentNode.attributedString = [[NSAttributedString alloc] initWithString:self.item.content
                                                                       attributes:[TextStyles contentStyle]];
        [contentNode measure:CGSizeMake(UIKeyWindowWidth() - 2*kPostImageNodePadding, FLT_MAX)];
        contentNode.frame = (CGRect) {
            .origin.x = kPostImageNodePadding,
            .origin.y = CGRectGetMaxY(priceNode.frame) + kPostImageNodePadding,
            .size = contentNode.calculatedSize,
        };
        
        dispatch_async(dispatch_get_main_queue(), ^{
            [self.scrollView addSubnode:titleNode];
            [self.scrollView addSubnode:priceNode];
            [self.scrollView addSubnode:contentNode];
//            [self.scrollView addSubnode:buyButtonNode];
//            [self.scrollView addSubview:buyButton];
//            [self.view addSubview:buyButton];
            
            self.scrollView.contentSize = CGSizeMake(UIKeyWindowWidth(),
                                                     CGRectGetMaxY(contentNode.frame) +
                                                     CGRectGetHeight(self.buyButton.frame) +
                                                     kPostImageNodePadding);
        });
    });
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    [self performSelector:@selector(showBars) withObject:nil afterDelay:0.2];
}

- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
}

- (void)viewDidLayoutSubviews {
    [super viewDidLayoutSubviews];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    if ([segue.identifier isEqualToString:kAddToCartSegue]) {
        VOCart *myCart = [UIAppDelegate() myCart];
        [myCart addStoreItem:self.item quantity:1];

        CartViewController *vc = segue.destinationViewController;
        vc.modalPresentationStyle = UIModalPresentationFullScreen;
        vc.transitioningDelegate = nil;
    }
}

#pragma mark - Action Methods
- (void)doOpenGallery {
    NSMutableArray<NSURL *> *photosURL = [[NSMutableArray alloc] initWithCapacity:self.item.images.count];
    for (NSString *str in self.item.images) {
        [photosURL addObject:[NSURL URLWithString:str]];
    }
    
    IDMPhotoBrowser *browser = [[IDMPhotoBrowser alloc] initWithPhotoURLs:photosURL];
    browser.usePopAnimation = YES;
    browser.forceHideStatusBar = YES;
    browser.displayDoneButton = NO;
    browser.leftArrowImage = [UIImage imageNamed:@"fnt_pb_left"];
    browser.rightArrowImage = [UIImage imageNamed:@"fnt_pb_right"];
    browser.trackTintColor = [UIColor whiteColor];
    [browser setInitialPageIndex:0];
    
    [[UIToolbar appearance] setTintColor:[UIColor whiteColor]];
    [self presentViewController:browser animated:YES completion:NULL];
}

#pragma mark - Public Methods
- (void)setupItem:(VOStoreItem *)item cachedImage:(UIImage *)image withFrame:(CGRect)frame {
    _item = item;
    self.expand_startFrame = frame;
    
    ASImageNode *imageNode = [[ASImageNode alloc] init];
    imageNode.image = image;
    imageNode.contentMode = UIViewContentModeScaleAspectFill;
    [imageNode measure:CGSizeMake(frame.size.width - 2*kStoreCellInsets, frame.size.width - 2*kStoreCellInsets)];
    imageNode.frame = (CGRect) {
        .origin.x = kStoreCellInsets,
        .origin.y = kStoreCellInsets,
        .size = imageNode.calculatedSize
    };
    self.imageNode = imageNode;
    
    [self.view addSubnode:imageNode];
}

- (PageItemViewController *)itemControllerForIndex:(NSUInteger)index {
    PageItemViewController *vc = [[PageItemViewController alloc] init];
    vc.itemIndex = index;
    vc.imageName = self.item.images[index];
    
    return vc;
}

#pragma mark - Helper Methods
- (void)showBars {
    self.blurNavbar.center = CGPointMake(UIKeyWindow().center.x, -kBlurNavbarHeight/2);
//    self.blurToolbar.center = CGPointMake(UIKeyWindow.center.x, UIKeyWindowHeight + kBlurToolbarHeight/2);
    self.blurNavbar.hidden = NO;
//    self.blurToolbar.hidden = NO;
    
    [UIView animateWithDuration:0.3
                     animations:^{
//                         self.scrollView.contentInset = UIEdgeInsetsMake(kBlurNavbarHeight, 0, kBlurToolbarHeight, 0);
                         self.blurNavbar.center = CGPointMake(UIKeyWindow().center.x, kBlurNavbarHeight/2);
//                         self.blurToolbar.center = CGPointMake(UIKeyWindow.center.x, UIKeyWindowHeight - kBlurToolbarHeight/2);
                     }
                     completion:NULL];
    
}

- (void)dismiss {
    [self dismissViewControllerAnimated:YES completion:NULL];
}

#pragma mark - PageViewController Data Source
- (UIViewController *)pageViewController:(UIPageViewController *)pageViewController viewControllerBeforeViewController:(UIViewController *)viewController {
    PageItemViewController *itemController = (PageItemViewController *) viewController;
    
    if (itemController.itemIndex > 0) {
        return [self itemControllerForIndex: itemController.itemIndex - 1];
    }
    
    return nil;
}

- (UIViewController *)pageViewController:(UIPageViewController *)pageViewController viewControllerAfterViewController:(UIViewController *)viewController {
    PageItemViewController *itemController = (PageItemViewController *) viewController;
    
    if (itemController.itemIndex + 1 < self.item.images.count) {
        return [self itemControllerForIndex:itemController.itemIndex + 1];
    }
    
    return nil;
}

- (void)pageViewController:(UIPageViewController *)pageViewController willTransitionToViewControllers:(NSArray<UIViewController *> *)pendingViewControllers {
    PageItemViewController *vc = (PageItemViewController *) pendingViewControllers[0];
    self.pageControl.currentPage = vc.itemIndex;
}

- (void)pageViewController:(UIPageViewController *)pageViewController didFinishAnimating:(BOOL)finished previousViewControllers:(NSArray<UIViewController *> *)previousViewControllers transitionCompleted:(BOOL)completed {
    if (completed) {
        PageItemViewController *vc = (PageItemViewController *) [self.page.viewControllers lastObject];
        self.currentImage = vc.imageNode.image;
    }
}

#pragma mark - Expand Protocol
- (UIPresentationController *)presentationControllerForPresentedViewController:(UIViewController *)presented presentingViewController:(UIViewController *)presenting sourceViewController:(UIViewController *)source {
    ExpandCellPresentationController *controller = [[ExpandCellPresentationController alloc] initWithPresentedViewController:presented
                                                                                                    presentingViewController:presenting];
    return controller;
}

- (id <UIViewControllerAnimatedTransitioning>)animationControllerForPresentedController:(UIViewController *)presented
                                                                   presentingController:(UIViewController *)presenting
                                                                       sourceController:(UIViewController *)source {
    return self.animator;
}

- (id <UIViewControllerAnimatedTransitioning>)animationControllerForDismissedController:(UIViewController *)dismissed {
    return self.animator;
}

@end

