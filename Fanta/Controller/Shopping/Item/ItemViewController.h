//
//  ItemViewController.h
//  Fanta
//
//  Created by Mario Concilio on 3/8/16.
//  Copyright © 2016 Mario Concilio. All rights reserved.
//

#import <UIKit/UIKit.h>

@class ASImageNode, VOStoreItem;
@interface ItemViewController : UIViewController

@property (nonatomic, strong) VOStoreItem *item;
@property (nonatomic, strong) UIImage *currentImage;
@property (nonatomic, weak) ASImageNode *imageNode;
@property (nonatomic, weak) UIPageViewController *page;
@property (nonatomic, weak) UIPageControl *pageControl;

@property (weak, nonatomic) IBOutlet UIVisualEffectView *blurNavbar;
@property (weak, nonatomic) IBOutlet UIButton *buyButton;

- (void)setupItem:(VOStoreItem *)item cachedImage:(UIImage *)image withFrame:(CGRect)frame;

@end
