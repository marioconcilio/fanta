//
//  StoreCell.h
//  Fanta
//
//  Created by Mario Concilio on 3/7/16.
//  Copyright © 2016 Mario Concilio. All rights reserved.
//

#import <AsyncDisplayKit/AsyncDisplayKit.h>

@class VOStoreItem;
@interface StoreCell : ASCellNode

@property (nonatomic, weak, readonly) ASNetworkImageNode *imageNode;
@property (nonatomic, weak, readonly) ASTextNode *priceNode;
@property (nonatomic, weak, readonly) ASTextNode *titleNode;

- (instancetype)initWithStoreItem:(VOStoreItem *)item;

+ (instancetype)new UNAVAILABLE_ATTRIBUTE;
- (instancetype)init UNAVAILABLE_ATTRIBUTE;

@end
