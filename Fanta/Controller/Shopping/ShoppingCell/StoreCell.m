//
//  StoreCell.m
//  Fanta
//
//  Created by Mario Concilio on 3/7/16.
//  Copyright © 2016 Mario Concilio. All rights reserved.
//

#import "StoreCell.h"
#import "VOStoreItem.h"
#import "Helper.h"
#import "Formatter.h"
#import "Constants.h"
#import "TextStyles.h"

#import <AsyncDisplayKit/ASDisplayNode+Subclasses.h>
#import <WebASDKImageManager.h>
#import <POP.h>
#import <FBShimmeringView.h>

@interface StoreCell() <ASNetworkImageNodeDelegate> {
    CGFloat _cellWidth;
}

@property (nonatomic, weak) ASNetworkImageNode *imageNode;
@property (nonatomic, weak) ASTextNode *priceNode;
@property (nonatomic, weak) ASTextNode *titleNode;
@property (nonatomic, weak) FBShimmeringView *shimmering;
//@property (nonatomic, strong) VOStoreItem *item;

@end

@implementation StoreCell

- (instancetype)initWithStoreItem:(VOStoreItem *)item {
    self = [super init];
    if (self) {
//        _item = item;
        
        self.backgroundColor = [UIColor whiteColor];
        self.selectionStyle = UITableViewCellSelectionStyleNone;
        _cellWidth = (UIKeyWindowWidth() - 3*kStoreCellPadding) / 2.0;
        
        /*
         * Image
         */
        ASNetworkImageNode *imageNode = [[ASNetworkImageNode alloc] initWithWebImage];
        imageNode.URL = [NSURL URLWithString:item.images[0]];
        imageNode.delegate = self;
        _imageNode = imageNode;
        [self addSubnode:_imageNode];
        
        /*
         *  Shimmering Layer
         *
        dispatch_async(dispatch_get_main_queue(), ^{
            FBShimmeringView *shimmering = [[FBShimmeringView alloc] initWithFrame:CGRectMake(kStoreCellInsets,
                                                                                              kStoreCellInsets,
                                                                                              _cellWidth - 2*kStoreCellInsets,
                                                                                              _cellWidth - 2*kStoreCellInsets)];
            _shimmering = shimmering;
//            _shimmering.frame = (CGRect) {
//                .origin.x = kStoreCellInsets,
//                .origin.y = kStoreCellInsets,
//                .size.width = _cellWidth - 2*kStoreCellInsets,
//                .size.height = _cellWidth - 2*kStoreCellInsets,
//            };
            
//            [self.layer addSublayer:_shimmering];
//            [self.view addSubview:_shimmering];
            
//            UILabel *label = [[UILabel alloc] initWithFrame:shimmering.bounds];
//            label.textAlignment = NSTextAlignmentCenter;
//            label.text = @"Carregando";
//            label.font = [UIFont lightFontWithSize:20.0];
//            _shimmering.contentView = label;
            
            UIImageView *imageView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"fnt_camera"]];
            _shimmering.contentView = imageView;
            
            _shimmering.shimmering = YES;
        });
        */
        
        
        /*
         * Price
         */
        ASTextNode *priceNode = [[ASTextNode alloc] init];
        priceNode.attributedString = [[NSAttributedString alloc] initWithString:[Formatter formatCurrency:item.price]
                                                                     attributes:[TextStyles storeCellPriceStyle]];
        priceNode.maximumNumberOfLines = 1;
        priceNode.layerBacked = YES;
        _priceNode = priceNode;
        [self addSubnode:_priceNode];
        
        /*
         * Title
         */
        ASTextNode *titleNode = [[ASTextNode alloc] init];
        titleNode.attributedString = [[NSAttributedString alloc] initWithString:item.title
                                                                     attributes:[TextStyles storeCellTitleStyle]];
        titleNode.maximumNumberOfLines = 1;
        titleNode.layerBacked = YES;
        _titleNode = titleNode;
        [self addSubnode:_titleNode];
    }
    
    return self;
}

- (CGSize)calculateSizeThatFits:(CGSize)constrainedSize {
//    CGSize imageSize = [_imageNode measure:CGSizeMake(constrainedSize.width - 2*kShoppingCellInsets, constrainedSize.width - 2*kShoppingCellInsets)];
    CGSize titleSize = [_titleNode measure:CGSizeMake(constrainedSize.width - 2*kStoreCellInsets, constrainedSize.width - 2*kStoreCellInsets)];
    CGSize priceSize = [_priceNode measure:CGSizeMake(constrainedSize.width - 2*kStoreCellInsets, constrainedSize.width - 2*kStoreCellInsets)];
    
    return CGSizeMake(_cellWidth,
                      _cellWidth + priceSize.height + titleSize.height + 3.5*kStoreCellInsets);
}

- (void)layout {
    [super layout];
    
    /*
     * Image
     */
    _imageNode.frame = (CGRect) {
        .origin.x = kStoreCellInsets,
        .origin.y = kStoreCellInsets,
        .size.width = _cellWidth - 2*kStoreCellInsets,
        .size.height = _cellWidth - 2*kStoreCellInsets,
    };
    
    /*
     * Price
     */
    _priceNode.frame = (CGRect) {
        .origin.x = kStoreCellInsets,
        .origin.y = _imageNode.frame.size.height + 2*kStoreCellInsets,
        .size = _priceNode.calculatedSize,
    };
    
    /*
     * Title
     */
    _titleNode.frame = (CGRect) {
        .origin.x = kStoreCellInsets,
        .origin.y = _imageNode.frame.size.height + _priceNode.calculatedSize.height + 3*kStoreCellInsets,
        .size = _titleNode.calculatedSize,
    };
}

#pragma mark - ASNetworkImage Delegate
- (void)imageNode:(ASNetworkImageNode *)imageNode didLoadImage:(UIImage *)image {
    if (image) {
        POPBasicAnimation *anim = [POPBasicAnimation animationWithPropertyNamed:kPOPViewAlpha];
        anim.fromValue = @(0.0);
        anim.toValue = @(1.0);
        [imageNode pop_removeAllAnimations];
        [imageNode pop_addAnimation:anim forKey:@"fadeIn"];
        
//        _shimmering.alpha = 0.0;
        
//        POPBasicAnimation *fadeOut = [POPBasicAnimation animationWithPropertyNamed:kPOPViewAlpha];
//        anim.toValue = @(0.0);
//        [_shimmering pop_removeAllAnimations];
//        [_shimmering pop_addAnimation:fadeOut forKey:@"fadeOut"];
        
    }
}

@end
