//
//  StorePlaceholder.h
//  Fanta
//
//  Created by Mario Concilio on 4/11/16.
//  Copyright © 2016 Mario Concilio. All rights reserved.
//

#import <AsyncDisplayKit/AsyncDisplayKit.h>

@interface StorePlaceholder : ASScrollNode

- (instancetype)initWithNumberOfRows:(NSUInteger)number;

@end
