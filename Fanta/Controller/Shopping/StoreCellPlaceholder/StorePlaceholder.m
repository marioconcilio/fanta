//
//  StorePlaceholder.m
//  Fanta
//
//  Created by Mario Concilio on 4/11/16.
//  Copyright © 2016 Mario Concilio. All rights reserved.
//

#import "StorePlaceholder.h"
#import "StoreCellPlaceholder.h"
#import "Constants.h"

@implementation StorePlaceholder

- (instancetype)initWithNumberOfRows:(NSUInteger)number {
    self = [super init];
    if (self) {
        self.view.bounces = YES;
        self.view.alwaysBounceVertical = YES;
        
        [self setupPlaceholdersCell:number];
    }
    
    return self;
}

- (void)setupPlaceholdersCell:(NSUInteger)number {
    CGFloat lastMaxY = 0.0;
    for (long i=0; i<number; i++) {
        @autoreleasepool {
            StoreCellPlaceholder *placeholder1 = [[StoreCellPlaceholder alloc] init];
            [placeholder1 measure:CGSizeMake(UIKeyWindowWidth(), FLT_MAX)];
            placeholder1.frame = (CGRect) {
                .origin.y = lastMaxY,
                .size = placeholder1.calculatedSize,
            };
            
            StoreCellPlaceholder *placeholder2 = [[StoreCellPlaceholder alloc] init];
            [placeholder2 measure:CGSizeMake(UIKeyWindowWidth(), FLT_MAX)];
            placeholder2.frame = (CGRect) {
                .origin.x = CGRectGetWidth(placeholder1.frame),
                .origin.y = lastMaxY,
                .size = placeholder2.calculatedSize,
            };
            
            lastMaxY = CGRectGetMaxY(placeholder1.frame);
            [self addSubnode:placeholder1];
            [self addSubnode:placeholder2];
        }
    }
}

@end
