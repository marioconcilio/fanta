//
//  StoreCellPlaceholder.m
//  Fanta
//
//  Created by Mario Concilio on 4/11/16.
//  Copyright © 2016 Mario Concilio. All rights reserved.
//

#import "StoreCellPlaceholder.h"
#import "Constants.h"

#import <AsyncDisplayKit.h>
#import <FBShimmeringLayer.h>

@interface StoreCellPlaceholder ()

@property (nonatomic, weak) ASDisplayNode *frameNode;
@property (nonatomic, weak) ASDisplayNode *imageNode;
@property (nonatomic, weak) ASDisplayNode *priceNode;
@property (nonatomic, weak) ASDisplayNode *titleNode;

@property (nonatomic, weak) FBShimmeringLayer *imageShimmer;
@property (nonatomic, weak) FBShimmeringLayer *priceShimmer;
@property (nonatomic, weak) FBShimmeringLayer *titleShimmer;

@end

static CGFloat _cellWidth;
static CGFloat _priceHeight;
static CGFloat _titleHeight;

@implementation StoreCellPlaceholder

- (instancetype)init {
    self = [super init];
    if (self) {
        _cellWidth = (UIKeyWindowWidth() - 3*kStoreCellPadding) / 2.0;
        _priceHeight = 23.0;
        _titleHeight = 20.5;
        
        /*
         *  frame
         */
        ASDisplayNode *frame = [[ASDisplayNode alloc] init];
//        frame.layerBacked = YES;
        frame.backgroundColor = [UIColor whiteColor];
        _frameNode = frame;
        [self addSubnode:_frameNode];
        
        /*
         *  image
         */
        ASDisplayNode *image = [[ASDisplayNode alloc] init];
//        image.layerBacked = YES;
        image.backgroundColor = UIColorFromHEX(0xe5e5e5);
        _imageNode = image;
        
        FBShimmeringLayer *imageShimmer = [[FBShimmeringLayer alloc] init];
        imageShimmer.shimmering = YES;
        _imageShimmer = imageShimmer;
        
        /*
         *  price
         */
        ASDisplayNode *price = [[ASDisplayNode alloc] init];
        price.layerBacked = YES;
        price.backgroundColor = UIColorFromHEX(0xe5e5e5);
        _priceNode = price;
        
        FBShimmeringLayer *priceShimmer = [[FBShimmeringLayer alloc] init];
        priceShimmer.shimmering = YES;
        _priceShimmer = priceShimmer;
        
        /*
         *  title
         */
        ASDisplayNode *title = [[ASDisplayNode alloc] init];
        title.layerBacked = YES;
        title.backgroundColor = UIColorFromHEX(0xe5e5e5);
        _titleNode = title;
        
        FBShimmeringLayer *titleShimmer = [[FBShimmeringLayer alloc] init];
        titleShimmer.shimmering = YES;
        _titleShimmer = titleShimmer;
        
        dispatch_async(dispatch_get_main_queue(), ^{
            imageShimmer.contentLayer = image.layer;
            [_frameNode.layer addSublayer:imageShimmer];
            
            priceShimmer.contentLayer = price.layer;
            [_frameNode.layer addSublayer:priceShimmer];
            
            titleShimmer.contentLayer = title.layer;
            [_frameNode.layer addSublayer:titleShimmer];
        });
    }
    
    return self;
}

- (CGSize)calculateSizeThatFits:(CGSize)constrainedSize {
    return CGSizeMake(constrainedSize.width/2 - 0.5*kStoreCellPadding,
                      constrainedSize.width/2 + _priceHeight + _titleHeight + 3.5*kStoreCellInsets - 0.5*kStoreCellPadding);
}

- (void)layout {
    [super layout];
    
    /*
     *  frame
     */
    _frameNode.frame = (CGRect) {
        .origin.x = kStoreCellPadding,
        .origin.y = kStoreCellPadding,
        .size.width = self.calculatedSize.width - kStoreCellPadding,
        .size.height = self.calculatedSize.height - kStoreCellPadding,
    };
    
    /*
     * Image
     */
    _imageNode.frame = (CGRect) {
        .size.width = _cellWidth - 2.0*kStoreCellInsets,
        .size.height = _cellWidth - 2*kStoreCellInsets,
    };
    
    _imageShimmer.frame = (CGRect) {
        .origin.x = kStoreCellInsets,
        .origin.y = kStoreCellInsets,
        .size.width = _cellWidth - 2.0*kStoreCellInsets,
        .size.height = _cellWidth - 2*kStoreCellInsets,
    };
    
    _imageShimmer.shimmeringSpeed = CGRectGetWidth(_imageNode.frame)*kShimmerSpeedRate;
    
    /*
     * Price
     */
    _priceNode.frame = (CGRect) {
        .size.width = (CGRectGetWidth(_frameNode.frame) - 2*kStoreCellInsets) * 0.50,
        .size.height = _priceHeight,
    };
    
    _priceShimmer.frame = (CGRect) {
        .origin.x = kStoreCellInsets,
        .origin.y = CGRectGetMaxY(_imageShimmer.frame) + kStoreCellInsets,
        .size.width = (CGRectGetWidth(_frameNode.frame) - 2*kStoreCellInsets) * 0.50,
        .size.height = _priceHeight,
    };
    
    _priceShimmer.shimmeringSpeed = CGRectGetWidth(_priceShimmer.frame)*kShimmerSpeedRate;
    
    /*
     * Title
     */
    _titleNode.frame = (CGRect) {
        .size.width = (CGRectGetWidth(_frameNode.frame) - 2*kStoreCellInsets) * 0.70,
        .size.height = _titleHeight,
    };
    
    _titleShimmer.frame = (CGRect) {
        .origin.x = kStoreCellInsets,
        .origin.y = CGRectGetMaxY(_priceShimmer.frame) + kStoreCellInsets,
        .size.width = (CGRectGetWidth(_frameNode.frame) - 2*kStoreCellInsets) * 0.70,
        .size.height = _titleHeight,
    };
    
    _titleShimmer.shimmeringSpeed = CGRectGetWidth(_titleShimmer.frame)*kShimmerSpeedRate;
}

@end
