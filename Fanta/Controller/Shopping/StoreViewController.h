//
//  StoreViewController.h
//  Fanta
//
//  Created by Mario Concilio on 3/7/16.
//  Copyright © 2016 Mario Concilio. All rights reserved.
//

#import <UIKit/UIKit.h>

@class StoreCell;
@interface StoreViewController : UIViewController

@property (nonatomic, weak) StoreCell *cell;

@end
