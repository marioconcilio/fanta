//
//  RegisterUserController.h
//  Fanta
//
//  Created by Mario Concilio on 8/26/16.
//  Copyright © 2016 Mario Concilio. All rights reserved.
//

#import <Foundation/Foundation.h>

@protocol RegisterUserDelegate <NSObject>

- (void)registerUserSuccessful;
- (void)registerUserFailedWithError:(NSError *)error;
- (void)registerUserRequestWillStart;

@end

@class VOUser;
@interface RegisterUserController : NSObject

@property (nonatomic, weak) id<RegisterUserDelegate> delegate;

- (instancetype)init UNAVAILABLE_ATTRIBUTE;
- (instancetype)initWithDelegate:(id<RegisterUserDelegate>)delegate;
- (void)downloadUserImage:(NSString *)path;
- (void)registerUser:(VOUser *)user;

@end
