//
//  FBLoginController.m
//  Fanta
//
//  Created by Mario Concilio on 8/25/16.
//  Copyright © 2016 Mario Concilio. All rights reserved.
//

#import "FBLoginController.h"
#import "VOAthletic.h"
#import "VOUser.h"
#import "AthleticService.h"
#import "UserService.h"

#import <FBSDKLoginKit/FBSDKLoginKit.h>
#import <FBSDKCoreKit/FBSDKCoreKit.h>
#import <Crashlytics/Answers.h>

static NSString *const kEmailPerm   = @"email";
static NSString *const kPagesPerm   = @"pages_show_list";
static NSString *const kProfilePerm = @"public_profile";

@interface FBLoginController () <FBSDKGraphRequestConnectionDelegate>

@property (nonatomic, strong) VOUser *user;
@property (nonatomic, strong) NSMutableSet<VOAthletic *> *listAthletics;
@property (nonatomic, assign, getter=isAthleticMember) BOOL athleticMember;
@property (nonatomic, assign) NSUInteger attempts;

@end

@implementation FBLoginController {
    dispatch_semaphore_t _listSem, _filterSem;
}

- (instancetype)initWithDelegate:(id<FBLoginDelegate>)delegate {
    self = [super init];
    if (self) {
        _delegate = delegate;
        _listSem = dispatch_semaphore_create(0);
        _filterSem = dispatch_semaphore_create(0);
        _attempts = 1;
//        [self fetchAthleticList];
    }
    
    return self;
}

- (void)fetchAthleticList {
    DDLogVerbose(@"will fetch list athletics");
    
    AthleticService *service = [[AthleticService alloc] init];
    [service listAllAthleticsWithCompletion:^(NSArray<VOAthletic *> *list, NSError *error) {
        dispatch_semaphore_signal(_listSem);
        
        if (error) {
            DDLogVerbose(@"list athletics HAS NOT BEEN populated");
        }
        else {
            self.listAthletics = [NSMutableSet setWithArray:list];
            DDLogVerbose(@"list athletics HAS BEEN populated");
        }
    }];
}

- (void)doLogin {
    if (!self.listAthletics) {
        [self fetchAthleticList];
    }
    else {
        dispatch_semaphore_signal(_listSem);
    }
    
    FBSDKLoginManager *login = [[FBSDKLoginManager alloc] init];
    [login logOut];
    [login logInWithReadPermissions:@[kEmailPerm, kProfilePerm, kPagesPerm]
                 fromViewController:nil
                            handler:^(FBSDKLoginManagerLoginResult *result, NSError *error) {
                                [self loginWithResult:result error:error];
                            }];
}

- (void)loginWithResult:(FBSDKLoginManagerLoginResult *)result error:(NSError *)error {
    if (error) {
        DDLogWarn(@"%@", error.description);
    }
    else if (result.isCancelled) {
        DDLogWarn(@"cancelled by user");
    }
    else {
//        [LoadingView show];
        [self.delegate fbLoginRequestWillStart];
        
        NSDictionary *paramsMe = @{@"fields": @"id, first_name, last_name, email, picture.height(800).width(800)"};
        FBSDKGraphRequest *requestMe = [[FBSDKGraphRequest alloc] initWithGraphPath:@"me"
                                                                         parameters:paramsMe
                                                                         HTTPMethod:@"GET"];
        
        FBSDKGraphRequestConnection *connection = [[FBSDKGraphRequestConnection alloc] init];
        [connection addRequest:requestMe
             completionHandler:^(FBSDKGraphRequestConnection *connection, id result, NSError *error) {
                 if (error) {
                     DDLogWarn(@"%@", error.description);
                 }
                 else {
                     self.user = [[VOUser alloc] initWithFacebookDictionary:result];
                 }
             }];
        
        // @{@"fields": @"is_business, is_place, is_promotable"}
        NSDictionary *paramsAccount = @{@"fields": @"id, name, perms, picture.height(800).width(800)"};
        FBSDKGraphRequest *requestAccount = [[FBSDKGraphRequest alloc] initWithGraphPath:@"me/accounts"
                                                                              parameters:paramsAccount
                                                                              HTTPMethod:@"GET"];
        [connection addRequest:requestAccount
             completionHandler:^(FBSDKGraphRequestConnection *connection, id result, NSError *error) {
                 if (error) {
                     DDLogWarn(@"%@", error.description);
                 }
                 else {
                     [self filterUserPages:result];
                 }
             }];
        
        connection.delegate = self;
        [connection start];
    }
}

- (void)filterUserPages:(NSDictionary *)dict {
    DDLogVerbose(@"will filter user pages");
    dispatch_semaphore_wait(_listSem, DISPATCH_TIME_FOREVER);
    
    if (self.listAthletics) {
        NSArray *data = dict[@"data"];
        
        // user has pages = YES
        if (data.count > 0) {
            
            // filter pages using listAthletics
            // first create NSSet with all user pages
            NSMutableSet<VOAthletic *> *pages = [NSMutableSet set];
            for (NSDictionary *dict in data) {
                @autoreleasepool {
                    VOAthletic *aa = [[VOAthletic alloc] initWithFacebookDictionary:dict];
                    [pages addObject:aa];
                }
            }
            
            // interset listAthletics with user pages
            // compare using facebook_id
            
            // if user has pages
            if ([self.listAthletics intersectsSet:pages]) {
                self.athleticMember = YES;
                [self.listAthletics intersectSet:pages];
            }
            // user has no pages
            else {
                self.athleticMember = NO;
            }
            
        }
        // user has pages = NO
        else {
            self.athleticMember = NO;
        }
    }
    
    DDLogVerbose(@"finished filtering user pages");
    dispatch_semaphore_signal(_filterSem);
}

#pragma mark - FBSDK Graph Request Connection
- (void)requestConnectionDidFinishLoading:(FBSDKGraphRequestConnection *)connection {
    DDLogVerbose(@"will finish fbsdk request");
    dispatch_semaphore_wait(_filterSem, DISPATCH_TIME_FOREVER);
//    [LoadingView hide];
    [self.delegate fbLoginRequestDidFinish];
    
    NSSet *deniedPerms = [[FBSDKAccessToken currentAccessToken] declinedPermissions];
    if ([deniedPerms containsObject:kEmailPerm]) {
        [self.delegate fbLoginUserDeniedEmailAccess];
    }
    else if ([deniedPerms containsObject:kPagesPerm] && _attempts == 1) {
        _attempts++;
        [self.delegate fbLoginUserDeniedPagesAccess];
    }
    else {
        if (self.listAthletics) {
            DDLogVerbose(@"login successful");
            NSDictionary *att = @{@"Pages Permission": [deniedPerms containsObject:kPagesPerm]? @"No" : @"Yes"};
            [Answers logLoginWithMethod:@"Facebook"
                                success:@YES
                       customAttributes:att];
            
            [self.delegate fbLoginSuccessfulWithUser:self.user
                                    isAthleticMember:self.athleticMember
                                        athleticList:[self.listAthletics allObjects]];
        }
        else {
            DDLogVerbose(@"login error, list athletics is nil");
            [self.delegate fbLoginError];
        }
    }
    
    DDLogVerbose(@"did finish fbsdk request");
}

@end
