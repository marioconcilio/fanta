//
//  RegisterUserController.m
//  Fanta
//
//  Created by Mario Concilio on 8/26/16.
//  Copyright © 2016 Mario Concilio. All rights reserved.
//

#import "RegisterUserController.h"
#import "APIClient.h"
#import "UIImage+Resize.h"
#import "UserService.h"
#import "VOUser.h"
#import "VOAthletic.h"
#import "Constants.h"

@interface RegisterUserController ()

@property (nonatomic, strong) UIImage *image;
@property (nonatomic, strong) UIImage *thumb;
@property (nonatomic, assign) BOOL hasImage;

@end

@implementation RegisterUserController {
    dispatch_semaphore_t _imageSem;
}

- (instancetype)initWithDelegate:(id<RegisterUserDelegate>)delegate {
    self = [super init];
    if (self) {
        _delegate = delegate;
        _imageSem = dispatch_semaphore_create(0);
    }
    
    return self;
}

- (void)downloadUserImage:(NSString *)path {
    self.hasImage = (path != nil);
    
    // if user has profile image
    if ([self hasImage]) {
        DDLogVerbose(@"user HAS profile image");
        NSURL *url = [NSURL URLWithString:path];
        [[APIClient sharedInstance] imageFromURL:url
                                      completion:^(UIImage *image, NSError *error) {
                                          dispatch_semaphore_signal(_imageSem);
                                          
                                          if (!error) {
                                              self.image = image;
                                              self.thumb = [image resizedImage:CGSizeMake(128.0, 128.0) interpolationQuality:kCGInterpolationHigh];
                                              DDLogVerbose(@"profile image HAS BEEN downloaded");
                                          }
                                          else {
                                              DDLogVerbose(@"profile image HAS NOT BEEN donwloaded");
                                          }
                                      }];
        
    }
    // if user has no profile image
    else {
        DDLogVerbose(@"user HAS NO profile image");
//        dispatch_semaphore_signal(_imageSem);
    }
}

- (void)registerUser:(VOUser *)user {
    [self.delegate registerUserRequestWillStart];
    
    if ([user isValid]) {
        [self registerFacebookUser:user];
    }
    else {
        [self registerEmailUser:user];
    }
}

- (void)registerFacebookUser:(VOUser *)user {
    DDLogVerbose(@"registering facebook user");
    
    if ([self hasImage]) {
        DDLogVerbose(@"waiting for image");
        dispatch_semaphore_wait(_imageSem, DISPATCH_TIME_FOREVER);
        DDLogVerbose(@"finished waiting for image");
        
        // if image has not been downloaded
        if (!self.image) {
            NSError *error = [NSError errorWithDomain:kBundleID code:-404 userInfo:@{NSLocalizedDescriptionKey: @"Image failed to download"}];
            [self.delegate registerUserFailedWithError:error];
//            [LoadingView hide];
//            [self showAlertView];
            return;
        }
    }
    
    UserService *service = [[UserService alloc] init];
    [service registerUserFromFacebook:user
                                image:self.image
                            thumbnail:self.thumb
                           completion:^(NSError *error) {
                               if (!error) {
                                   [self.delegate registerUserSuccessful];
                               }
                               else {
                                   if ([self hasImage]) {
                                       dispatch_semaphore_signal(_imageSem);
                                   }
                                   
                                   [self.delegate registerUserFailedWithError:error];
                               }
                           }];
}

- (void)registerEmailUser:(VOUser *)user {
    DDLogVerbose(@"registering email user");
    //TODO: edit user setting his athletic
//    VOAthletic *aa = [user athletic];
    
    //TODO: on success, save user info on NSDefaults
//    [UserService saveUser:user];
//    [self.delegate registerUserSuccessful];
}

@end
