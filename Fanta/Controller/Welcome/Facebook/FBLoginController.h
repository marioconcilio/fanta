//
//  FBLoginController.h
//  Fanta
//
//  Created by Mario Concilio on 8/25/16.
//  Copyright © 2016 Mario Concilio. All rights reserved.
//

#import <Foundation/Foundation.h>

@class VOAthletic, VOUser;
@protocol FBLoginDelegate <NSObject>

- (void)fbLoginSuccessfulWithUser:(VOUser *)user isAthleticMember:(BOOL)member athleticList:(NSArray<VOAthletic *> *)list;
- (void)fbLoginError;
- (void)fbLoginUserDeniedEmailAccess;
- (void)fbLoginUserDeniedPagesAccess;
- (void)fbLoginRequestWillStart;
- (void)fbLoginRequestDidFinish;

@end

@class VOAthletic, VOUser;
@interface FBLoginController : NSObject

@property (nonatomic, weak) id<FBLoginDelegate> delegate;

- (instancetype)init UNAVAILABLE_ATTRIBUTE;
- (instancetype)initWithDelegate:(id<FBLoginDelegate>)delegate;
- (void)doLogin;

@end
