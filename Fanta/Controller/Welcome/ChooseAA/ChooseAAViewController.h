//
//  ChooseAAViewController.h
//  Fanta
//
//  Created by Mario Concilio on 5/13/16.
//  Copyright © 2016 Mario Concilio. All rights reserved.
//

#import <UIKit/UIKit.h>

@class VOAthletic, VOUser;
@interface ChooseAAViewController : UIViewController

@property (nonatomic, strong) NSArray<VOAthletic *> *listAthletics;
@property (nonatomic, strong) VOUser *user;
@property (nonatomic, assign, getter=isAthleticMember) BOOL athleticMember;

@end
