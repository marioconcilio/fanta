//
//  ChooseAAViewController.m
//  Fanta
//
//  Created by Mario Concilio on 5/13/16.
//  Copyright © 2016 Mario Concilio. All rights reserved.
//

#import "ChooseAAViewController.h"
#import "TextStyles.h"
#import "VOAthletic.h"
#import "Helper.h"
#import "AACell.h"
#import "Constants.h"
#import "VOUser.h"
#import "Macros.h"
#import "AppDelegate.h"
#import "UserService.h"
#import "AthleticService.h"
#import "APIClient.h"
#import "UIImage+Resize.h"
#import "RegisterUserController.h"

#import <AsyncDisplayKit.h>
#import <SIAlertView.h>
#import <KVNProgress.h>

@interface ChooseAAViewController () <ASTableDataSource, ASTableDelegate, UISearchResultsUpdating, RegisterUserDelegate> {
    ASTableView *_tableView;
}

@property (nonatomic, strong) UISearchController *searchController;
@property (nonatomic, strong) NSArray *searchResults;
//@property (nonatomic, strong) NSCache *thumbCache;
@property (nonatomic, strong) UIImage *profileImage;
@property (nonatomic, strong) UIImage *profileThumb;
@property (nonatomic, strong) RegisterUserController *registerController;

@end

@implementation ChooseAAViewController

- (instancetype)initWithCoder:(NSCoder *)aDecoder {
    self = [super initWithCoder:aDecoder];
    if (self) {
        _tableView = [[ASTableView alloc] initWithFrame:CGRectZero style:UITableViewStylePlain asyncDataFetching:YES];
        _tableView.asyncDataSource = self;
        _tableView.asyncDelegate = self;
    }
    
    return self;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    [self.view addSubview:_tableView];
    
    self.registerController = [[RegisterUserController alloc] initWithDelegate:self];
    [self.registerController downloadUserImage:[self.user image]];
    
    /*
     * search bar
     */
    self.searchController = [[UISearchController alloc] initWithSearchResultsController:nil];
    self.searchController.searchResultsUpdater = self;
    self.searchController.dimsBackgroundDuringPresentation = NO;
    self.searchController.searchBar.searchBarStyle = UISearchBarStyleMinimal;
    self.searchController.searchBar.barStyle = UIBarStyleBlack;
    
    self.searchController.hidesNavigationBarDuringPresentation = NO;
    self.definesPresentationContext = YES;
    [self.searchController.searchBar sizeToFit];
    self.navigationItem.titleView = self.searchController.searchBar;
//    self.thumbCache = [[NSCache alloc] init];
    
    /*
     * message view
     */
    UIBlurEffect *blurEffect = [UIBlurEffect effectWithStyle:UIBlurEffectStyleDark];
    UIImageView *imageView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"fnt_menu_background"]];
    _tableView.backgroundView = imageView;
    _tableView.backgroundColor = [UIColor clearColor];
    _tableView.separatorEffect = [UIVibrancyEffect effectForBlurEffect:blurEffect];
    _tableView.tableFooterView = [[UIView alloc] initWithFrame:CGRectZero];
    
    UIBlurEffect *headerEffect = [UIBlurEffect effectWithStyle:UIBlurEffectStyleLight];
    UIVisualEffectView *headerView = [[UIVisualEffectView alloc] initWithEffect:headerEffect];
    
//    UIView *headerView = [[UIView alloc] init];
//    headerView.backgroundColor = [UIColor clearColor];
    
    ASTextNode *messageNode = [[ASTextNode alloc] init];
    NSString *message;
    
    if ([self isAthleticMember]) {
        message = @"Seja bem vindo! Encontramos essas atléticas associadas ao seu perfil no Facebook. Você participa de alguma delas?";
    }
    else {
        message = @"Seja bem vindo! Para melhor experiência com nosso App, nos diga de qual atlética seu curso faz parte.";
    }
    
    messageNode.attributedString = [[NSAttributedString alloc] initWithString:message
                                                                   attributes:[TextStyles chooseAAMessageStyle]];
    [messageNode measure:CGSizeMake(UIKeyWindowWidth() - 2*kAAPadding, FLT_MAX)];
    messageNode.frame = (CGRect) {
        .origin.x = kAAPadding,
        .origin.y = kAAPadding,
        .size = messageNode.calculatedSize,
    };
    
    headerView.frame = (CGRect) {
        .origin.y = CGRectGetHeight(self.navigationController.navigationBar.frame),
        .size.width = UIKeyWindowWidth(),
        .size.height = messageNode.calculatedSize.height + 2*kAAPadding,
    };
    
    [headerView addSubnode:messageNode];
    [self.view addSubview:headerView];
    
    _tableView.contentInset = UIEdgeInsetsMake(headerView.frame.size.height, 0.0, 0.0, 0.0);
    
    /*
     *  navbar dismiss button
     */
    UIBarButtonItem *dismissButton = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"fnt_cancel"]
                                                                      style:UIBarButtonItemStylePlain
                                                                     target:self
                                                                     action:@selector(dismiss)];
    self.navigationItem.leftBarButtonItem = dismissButton;
}

- (void)viewWillLayoutSubviews {
    [super viewWillLayoutSubviews];
    _tableView.frame = self.view.bounds;
}

- (void)viewDidLayoutSubviews {
    [super viewDidLayoutSubviews];
}

- (void)dealloc {
    [self.searchController loadViewIfNeeded];
}

- (BOOL)prefersStatusBarHidden {
    return YES;
}

#pragma mark - Action
- (void)dismiss {
    [self dismissViewControllerAnimated:YES completion:NULL];
}

#pragma mark - Table View Data Source
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    if ([self.searchController isActive]) {
        return self.searchResults.count;
    }
    
    return self.listAthletics.count;
}

- (ASCellNode *)tableView:(ASTableView *)tableView nodeForRowAtIndexPath:(NSIndexPath *)indexPath {
    VOAthletic *aa;
    if ([self.searchController isActive]) {
        aa = self.searchResults[indexPath.row];
    }
    else {
        aa = self.listAthletics[indexPath.row];
    }
    
    AACell *cell = [[AACell alloc] initDarkStyleWithAthletic:aa];
    return cell;
}

#pragma mark - Table View Delegate
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    VOAthletic *aa;
    if ([self.searchController isActive])
        aa = self.searchResults[indexPath.row];
    else
        aa = self.listAthletics[indexPath.row];
    /*
    NSString *message;
    if ([self isAthleticMember])
        message = [NSString stringWithFormat:@"Você é membro de %@ - %@?", aa.name, aa.athleticName];
    else
        message = [NSString stringWithFormat:@"A atlética do seu curso é %@ - %@?", aa.name, aa.athleticName];
    
    SIAlertView *alertView = [[SIAlertView alloc] initWithTitle:@"Então..."
                                                     andMessage:message];
    [alertView addButtonWithTitle:@"Não"
                             type:SIAlertViewButtonTypeCancel
                          handler:^(SIAlertView *alertView) {
                              [alertView dismissAnimated:YES];
                          }];
    
    [alertView addButtonWithTitle:@"Sim"
                             type:SIAlertViewButtonTypeDestructive
                          handler:^(SIAlertView *alertView) {
                              [alertView dismissAnimated:YES];
                              [self.user setAthletic:aa];
                              [self.registerController registerUser:self.user];
                          }];
    */
    [self.user setAthletic:aa];
    [self.registerController registerUser:self.user];
    
//    alertView.transitionStyle = SIAlertViewTransitionStyleBounce;
//    [alertView show];
}

#pragma mark - Search Results Updating
- (void)updateSearchResultsForSearchController:(UISearchController *)searchController {
    self.searchResults = nil;
    
//    NSPredicate *resultPredicate = [NSPredicate predicateWithFormat:@"name contains[c] %@", searchController.searchBar.text];
    NSPredicate *resultPredicate = [NSPredicate predicateWithFormat:@"(name contains[c] %@) OR (athleticName contains[c] %@)",
                                    [searchController.searchBar text],
                                    [searchController.searchBar text]];
    self.searchResults = [self.listAthletics filteredArrayUsingPredicate:resultPredicate];
    
    [_tableView reloadData];
}

#pragma mark - Register User Delegate
- (void)registerUserRequestWillStart {
    [KVNProgress show];
}

- (void)registerUserSuccessful {
    [KVNProgress showSuccess];
    
    if ([self.user isValid]) {
        [UIAppDelegate() defineRootViewControllerAnimated:YES];
    }
    else {
        SIAlertView *alertView = [[SIAlertView alloc] initWithTitle:@"Precisamos confirmar seu email"
                                                         andMessage:@"Texto informando sobre a necessidade da confirnação do email ao efetuar a primeira compra."];
        
        [alertView addButtonWithTitle:@"Ok"
                                 type:SIAlertViewButtonTypeDestructive
                              handler:^(SIAlertView *alertView) {
                                  [UIAppDelegate() defineRootViewControllerAnimated:YES];
                              }];
        
        alertView.transitionStyle = SIAlertViewTransitionStyleBounce;
        [alertView show];
    }
    
}

- (void)registerUserFailedWithError:(NSError *)error {
    [KVNProgress showErrorWithCompletion:^{
        SIAlertView *alertView = [[SIAlertView alloc] initWithTitle:@"Ops..."
                                                         andMessage:@"Não conseguimos realizar o login. Verifique sua conexão com a Internet."];
        
        [alertView addButtonWithTitle:@"Ok"
                                 type:SIAlertViewButtonTypeDestructive
                              handler:^(SIAlertView *alertView) {
                                  if (error.code == -404) {
                                      [self.registerController downloadUserImage:[self.user image]];
                                  }
                                  
                                  [alertView dismissAnimated:YES];
                              }];
        
        alertView.transitionStyle = SIAlertViewTransitionStyleBounce;
        [alertView show];
    }];
}

@end
