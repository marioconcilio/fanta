//
//  WelcomeViewController.m
//  Fanta
//
//  Created by Mario Concilio on 5/29/15.
//  Copyright (c) 2015 Mario Concilio. All rights reserved.
//

#import "WelcomeViewController.h"
#import "LoginViewController.h"
#import "SignupViewController.h"
#import "Constants.h"
#import "FadeInNavigationController.h"
#import "VOUser.h"
#import "VOAthletic.h"
#import "AppDelegate.h"

#import "ChooseAAViewController.h"

#import "FBLoginController.h"

#import <POP.h>
#import <SIAlertView.h>
#import <KVNProgress.h>

static NSString * const kAASegue = @"aa_segue";

@interface WelcomeViewController () <FBLoginDelegate>

@property (nonatomic, strong) FBLoginController *fbLoginController;

@end

@implementation WelcomeViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.signupButton.layer.cornerRadius = kButtonCornerRadius;
    self.facebookButton.layer.cornerRadius = kButtonCornerRadius;
    self.loginButton.layer.cornerRadius = kButtonCornerRadius;
    
    /*
     * transparent navbar
     */
    [self.navigationController.navigationBar setBackgroundImage:[[UIImage alloc] init]
                                                  forBarMetrics:UIBarMetricsDefault];
    self.navigationController.navigationBar.shadowImage = [[UIImage alloc] init];
    self.navigationController.navigationBar.translucent = YES;
    self.navigationController.view.backgroundColor = [UIColor clearColor];
    self.navigationController.navigationBar.backgroundColor = [UIColor clearColor];
    self.navigationItem.backBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"" style:UIBarButtonItemStylePlain target:nil action:nil];
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    
    POPSpringAnimation *scaleAnimation = [POPSpringAnimation animationWithPropertyNamed:kPOPViewScaleXY];
    scaleAnimation.fromValue = [NSValue valueWithCGPoint:kLoginMinScale];
    scaleAnimation.toValue = [NSValue valueWithCGPoint:CGPointMake(1.0, 1.0)];
    scaleAnimation.springBounciness = kLoginSpringBounce;
    scaleAnimation.springSpeed = kLoginSpringSpeed;
    [self.containerView pop_addAnimation:scaleAnimation forKey:@"scaleAnimation"];
}

- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
    
    POPSpringAnimation *scaleAnimation = [POPSpringAnimation animationWithPropertyNamed:kPOPViewScaleXY];
    scaleAnimation.toValue = [NSValue valueWithCGPoint:kLoginMinScale];
    scaleAnimation.springBounciness = kLoginSpringBounce;
    scaleAnimation.springSpeed = kLoginSpringSpeed;
    [self.containerView pop_addAnimation:scaleAnimation forKey:@"scaleAnimation"];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (BOOL)prefersStatusBarHidden {
    return YES;
}

- (FBLoginController *)fbLoginController {
    if (!_fbLoginController) {
        _fbLoginController = [[FBLoginController alloc] initWithDelegate:self];
    }
    
    return _fbLoginController;
}

#pragma mark - Actions
- (IBAction)doFacebookLogin:(UIButton *)sender {
    [self.fbLoginController doLogin];
}

#pragma mark - Navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    if ([segue.identifier isEqualToString:kAASegue]) {
        NSDictionary *params = sender;
        
        UINavigationController *nav = segue.destinationViewController;
        ChooseAAViewController *vc = (ChooseAAViewController *) nav.topViewController;
        vc.user = params[@"user"];
        vc.athleticMember = [params[@"athletic_member"] boolValue];
        vc.listAthletics = params[@"list"];
    }
}

#pragma mark - FBLogin Delegate
- (void)fbLoginRequestWillStart {
//    [LoadingView show];
    [KVNProgress show];
}

- (void)fbLoginRequestDidFinish {
//    [LoadingView hide];
    [KVNProgress dismiss];
}

- (void)fbLoginSuccessfulWithUser:(VOUser *)user isAthleticMember:(BOOL)member athleticList:(NSArray<VOAthletic *> *)list {
    NSDictionary *params = @{@"user":               user,
                             @"athletic_member":    @(member),
                             @"list":               list};
    [self performSegueWithIdentifier:kAASegue sender:params];
}

- (void)fbLoginError {
    SIAlertView *alertView = [[SIAlertView alloc] initWithTitle:@"Falha Login"
                                                     andMessage:@"Não conseguimos realizar o login. Verifique sua conexão com a Internet."];
    
    [alertView addButtonWithTitle:@"Ok"
                             type:SIAlertViewButtonTypeDestructive
                          handler:^(SIAlertView *alertView) {
                              [alertView dismissAnimated:YES];
                          }];
    
    alertView.transitionStyle = SIAlertViewTransitionStyleBounce;
    [alertView show];
}

- (void)fbLoginUserDeniedEmailAccess {
    SIAlertView *alertView = [[SIAlertView alloc] initWithTitle:@"Faltou Email"
                                                     andMessage:@"Texto explicando da necessidade do email ao fazer login."];
    
    [alertView addButtonWithTitle:@"Ok"
                             type:SIAlertViewButtonTypeDestructive
                          handler:^(SIAlertView *alertView) {
                              [alertView dismissAnimated:YES];
                          }];
    
    alertView.transitionStyle = SIAlertViewTransitionStyleBounce;
    [alertView show];
}

- (void)fbLoginUserDeniedPagesAccess {
    SIAlertView *alertView = [[SIAlertView alloc] initWithTitle:@"Faltou Páginas"
                                                     andMessage:@"Se você for membro de atlética, deixe o E-lefante acessar suas páginas. Se não for, tudo bem."];
    
    [alertView addButtonWithTitle:@"Ok"
                             type:SIAlertViewButtonTypeDestructive
                          handler:^(SIAlertView *alertView) {
                              [alertView dismissAnimated:YES];
                              [self.fbLoginController doLogin];
                          }];
    /*
    [alertView addButtonWithTitle:@"Cancelar"
                             type:SIAlertViewButtonTypeCancel
                          handler:^(SIAlertView *alertView) {
                              [alertView dismissAnimated:YES];
                              [self.fbLoginController doLogin];
                          }];
     */
    
    alertView.transitionStyle = SIAlertViewTransitionStyleBounce;
    [alertView show];
}

@end
