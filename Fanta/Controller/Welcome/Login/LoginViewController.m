//
//  LoginViewController.m
//  Fanta
//
//  Created by Mario Concilio on 28/04/15.
//  Copyright (c) 2015 Mario Concilio. All rights reserved.
//

#import "UserService.h"
#import "AppDelegate.h"
#import "Constants.h"
#import "LoginViewController.h"
#import "FloatLabeledTextField.h"

#import <AFViewShaker.h>
#import <POP.h>
#import <SIAlertView.h>
#import <Crashlytics/Answers.h>
#import <KVNProgress.h>

typedef NS_ENUM(NSUInteger, LoginTag) {
    emailTag = 89,
    passTag,
};

@interface LoginViewController () 

@end

@implementation LoginViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.emailTextField.tag = emailTag;
    self.passwordTextField.tag = passTag;
    
    self.emailTextField.delegate = self;
    self.passwordTextField.delegate = self;
    
    self.loginButton.layer.cornerRadius = kButtonCornerRadius;
    self.errorLabel.alpha = 0.0;
    
    NSDictionary *placeholderStyle = @{NSForegroundColorAttributeName: [UIColor lightGrayColor]};
    self.emailTextField.attributedPlaceholder = [[NSAttributedString alloc] initWithString:@"Email"
                                                                                attributes:placeholderStyle];
    self.passwordTextField.attributedPlaceholder = [[NSAttributedString alloc] initWithString:@"Senha"
                                                                                   attributes:placeholderStyle];
    
    UIBarButtonItem *backButton = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"fnt_navbar_back"]
                                                                   style:UIBarButtonItemStylePlain
                                                                  target:self
                                                                  action:@selector(dismiss)];
    self.navigationItem.leftBarButtonItem = backButton;
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    
    POPSpringAnimation *scaleAnimation = [POPSpringAnimation animationWithPropertyNamed:kPOPViewScaleXY];
    scaleAnimation.fromValue = [NSValue valueWithCGPoint:kLoginMinScale];
    scaleAnimation.toValue = [NSValue valueWithCGPoint:CGPointMake(1.0, 1.0)];
    scaleAnimation.springBounciness = kLoginSpringBounce;
    scaleAnimation.springSpeed = kLoginSpringSpeed;
    [self.containerView pop_addAnimation:scaleAnimation forKey:@"scaleAnimation"];
    
    POPSpringAnimation *positionAnimation = [POPSpringAnimation animationWithPropertyNamed:kPOPLayerPositionY];
    positionAnimation.fromValue = @(200);
    positionAnimation.springBounciness = kLoginSpringBounce;
    positionAnimation.springSpeed = kLoginSpringSpeed;
    [self.containerView pop_addAnimation:positionAnimation forKey:@"positionAnimation"];
}

- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
    
    POPSpringAnimation *scaleAnimation = [POPSpringAnimation animationWithPropertyNamed:kPOPViewScaleXY];
    scaleAnimation.toValue = [NSValue valueWithCGPoint:kLoginMinScale];
    scaleAnimation.springBounciness = kLoginSpringBounce;
    scaleAnimation.springSpeed = kLoginSpringSpeed;
    [self.containerView pop_addAnimation:scaleAnimation forKey:@"scaleAnimation"];
    
    POPSpringAnimation *positionAnimation = [POPSpringAnimation animationWithPropertyNamed:kPOPLayerPositionY];
    positionAnimation.toValue = @(200);
    positionAnimation.springBounciness = kLoginSpringBounce;
    positionAnimation.springSpeed = kLoginSpringSpeed;
    [self.containerView pop_addAnimation:positionAnimation forKey:@"positionAnimation"];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Actions Methods
- (IBAction)doLogin:(UIButton *)sender {
    [self.view endEditing:YES];
    
    if (self.emailTextField.text.length == 0 || self.passwordTextField.text.length == 0) {
        [self showError:@"Preencha todos os campos."];
        return;
    }
    
    [KVNProgress show];
    UserService *service = [[UserService alloc] init];
    [service login:[self.emailTextField text]
          password:[self.passwordTextField text]
        completion:^(NSError *error) {
            if (!error) {
                [KVNProgress showSuccess];
                
                DDLogVerbose(@"login success");
                [UIView animateWithDuration:0.3 animations:^{
                    self.errorLabel.alpha = 0.0;
                }];
                
                [Answers logLoginWithMethod:@"Email"
                                    success:@YES
                           customAttributes:@{}];
                
                [UIAppDelegate() defineRootViewControllerAnimated:YES];
            }
            else {
                [KVNProgress showError];
                
                switch (error.code) {
                    case 8: // invalid password
                    case 9: // email not registered
                        [self showError:@"Email e/ou senha incorretos."];
                        break;
                        
                    default:
                        [self showAlertView];
                        break;
                }
            }
        }];
}

#pragma mark - Helper Methods
- (void)showError:(NSString *)message {
    self.errorLabel.text = message;
    
    AFViewShaker *shaker = [[AFViewShaker alloc] initWithView:self.containerView];
    [shaker shake];
    
    [UIView animateWithDuration:0.3 animations:^{
        self.errorLabel.alpha = 1.0;
    }];
}

- (void)showAlertView {
    SIAlertView *alertView = [[SIAlertView alloc] initWithTitle:@"Ops..."
                                                     andMessage:@"Não conseguimos realizar o login. Verifique sua conexão com a Internet."];
    
    [alertView addButtonWithTitle:@"Ok"
                             type:SIAlertViewButtonTypeDestructive
                          handler:^(SIAlertView *alertView) {
                              [alertView dismissAnimated:YES];
                          }];
    
    alertView.transitionStyle = SIAlertViewTransitionStyleBounce;
    [alertView show];
}

- (void)dismiss {
    [self.navigationController popViewControllerAnimated:YES];
}

#pragma mark - TextField Delegate
- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    if (textField.tag == emailTag) {
        [self.passwordTextField becomeFirstResponder];
    }
    else {
        [textField resignFirstResponder];
    }
    
    return YES;
}

@end
