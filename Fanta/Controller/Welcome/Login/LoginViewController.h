//
//  LoginViewController.h
//  Fanta
//
//  Created by Mario Concilio on 28/04/15.
//  Copyright (c) 2015 Mario Concilio. All rights reserved.
//

#import <UIKit/UIKit.h>

@class FloatLabeledTextField;
@interface LoginViewController : UIViewController <UITextFieldDelegate>

@property (weak, nonatomic) IBOutlet FloatLabeledTextField *emailTextField;
@property (weak, nonatomic) IBOutlet FloatLabeledTextField *passwordTextField;
@property (weak, nonatomic) IBOutlet UIButton *loginButton;
@property (weak, nonatomic) IBOutlet UIView *containerView;
@property (weak, nonatomic) IBOutlet UILabel *errorLabel;

- (IBAction)doLogin:(UIButton *)sender;
- (void)showError:(NSString *)message;

@end
