//
//  SignupViewController.m
//  Fanta
//
//  Created by Mario Concilio on 28/04/15.
//  Copyright (c) 2015 Mario Concilio. All rights reserved.
//

#import "SignupViewController.h"
#import "Constants.h"
#import "UserService.h"
#import "FloatLabeledTextField.h"
#import "LoadingView.h"
#import "VOUser.h"
#import "ChooseAAViewController.h"
#import "AthleticService.h"

#import <AFViewShaker.h>
#import <POP.h>
#import <SIAlertView.h>
#import <Crashlytics/Answers.h>

typedef enum {
    nameTag = 89,
    surnameTag,
    emailTag,
    passTag,
} TextFieldTag;

NSString *const kAASegue = @"aa_segue";

@interface SignupViewController () <UITextFieldDelegate>

@end

@implementation SignupViewController {
    dispatch_semaphore_t _listSem;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    _listSem = dispatch_semaphore_create(0);
    
    /*
     * fetch db for list of all athletics
     */
    [self fetchAthleticList];
    
    self.nameTextField.tag = nameTag;
    self.surnameTextField.tag = surnameTag;
    self.emailTextField.tag = emailTag;
    self.passwordTextField.tag = passTag;
    
    self.nameTextField.delegate = self;
    self.surnameTextField.delegate = self;
    self.emailTextField.delegate = self;
    self.passwordTextField.delegate = self;
    
    self.signupButton.layer.cornerRadius = kButtonCornerRadius;
    self.errorLabel.alpha = 0.0;
    
    NSDictionary *placeholderStyle = @{NSForegroundColorAttributeName: [UIColor lightGrayColor]};
    self.nameTextField.attributedPlaceholder = [[NSAttributedString alloc] initWithString:@"Nome"
                                                                               attributes:placeholderStyle];
    self.surnameTextField.attributedPlaceholder = [[NSAttributedString alloc] initWithString:@"Sobrenome"
                                                                                  attributes:placeholderStyle];
    self.emailTextField.attributedPlaceholder = [[NSAttributedString alloc] initWithString:@"Email"
                                                                                attributes:placeholderStyle];
    self.passwordTextField.attributedPlaceholder = [[NSAttributedString alloc] initWithString:@"Senha"
                                                                                   attributes:placeholderStyle];
    
    UIBarButtonItem *backButton = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"fnt_navbar_back"]
                                                                   style:UIBarButtonItemStylePlain
                                                                  target:self
                                                                  action:@selector(dismiss)];
    self.navigationItem.leftBarButtonItem = backButton;
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    
    POPSpringAnimation *scaleAnimation = [POPSpringAnimation animationWithPropertyNamed:kPOPViewScaleXY];
    scaleAnimation.fromValue = [NSValue valueWithCGPoint:kLoginMinScale];
    scaleAnimation.toValue = [NSValue valueWithCGPoint:CGPointMake(1.0, 1.0)];
    scaleAnimation.springBounciness = kLoginSpringBounce;
    scaleAnimation.springSpeed = kLoginSpringSpeed;
    [self.containerView pop_addAnimation:scaleAnimation forKey:@"scaleAnimation"];
    
    POPSpringAnimation *positionAnimation = [POPSpringAnimation animationWithPropertyNamed:kPOPLayerPositionY];
    positionAnimation.fromValue = @(250);
    positionAnimation.springBounciness = kLoginSpringBounce;
    positionAnimation.springSpeed = kLoginSpringSpeed;
    [self.containerView pop_addAnimation:positionAnimation forKey:@"positionAnimation"];
}

- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
    
    POPSpringAnimation *scaleAnimation = [POPSpringAnimation animationWithPropertyNamed:kPOPViewScaleXY];
    scaleAnimation.toValue = [NSValue valueWithCGPoint:kLoginMinScale];
    scaleAnimation.springBounciness = kLoginSpringBounce;
    scaleAnimation.springSpeed = kLoginSpringSpeed;
    [self.containerView pop_addAnimation:scaleAnimation forKey:@"scaleAnimation"];
    
    POPSpringAnimation *positionAnimation = [POPSpringAnimation animationWithPropertyNamed:kPOPLayerPositionY];
    positionAnimation.toValue = @(250);
    positionAnimation.springBounciness = kLoginSpringBounce;
    positionAnimation.springSpeed = kLoginSpringSpeed;
    [self.containerView pop_addAnimation:positionAnimation forKey:@"positionAnimation"];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    if ([segue.identifier isEqualToString:kAASegue]) {
        VOUser *user = sender;
        UINavigationController *nav = [segue destinationViewController];
        ChooseAAViewController *vc = (ChooseAAViewController *) [nav topViewController];
        vc.user = user;
        vc.athleticMember = NO;
        vc.listAthletics = self.listAthletics;
    }
}

#pragma mark - Actions
- (IBAction)doSignup:(UIButton *)sender {
    [self.view endEditing:YES];
    if (self.nameTextField.text.length == 0 ||
        self.surnameTextField.text.length == 0 ||
        self.emailTextField.text.length == 0 ||
        self.passwordTextField.text.length == 0) {
        [self showError:@"Preencha todos os campos."];
        return;
    }
    
    [LoadingView show];
    
    GCDOnBackground(^{
        DDLogVerbose(@"waiting for list athletics");
        dispatch_semaphore_wait(_listSem, DISPATCH_TIME_FOREVER);
        DDLogVerbose(@"finished waiting for list athletics");
        
        GCDOnMain(^{
            if (self.listAthletics) {
                NSString *name = [NSString stringWithFormat:@"%@ %@", self.nameTextField.text, self.surnameTextField.text];
                UserService *servive = [[UserService alloc] init];
                [servive registerUser:name
                                email:[self.emailTextField text]
                             password:[self.passwordTextField text]
                           completion:^(VOUser *user, NSError *error) {
                               if (!error) {
                                   [Answers logSignUpWithMethod:@"Email"
                                                        success:@YES
                                               customAttributes:@{}];
                                   
                                   [LoadingView hide];
                                   DDLogVerbose(@"register success");
                                   [self performSegueWithIdentifier:kAASegue sender:user];
                                   
                                   [UIView animateWithDuration:0.3 animations:^{
                                       self.errorLabel.alpha = 0.0;
                                   }];
                               }
                               else {
                                   [Answers logSignUpWithMethod:@"Email"
                                                        success:@NO
                                               customAttributes:@{@"error": [error localizedDescription]}];
                                   
                                   dispatch_semaphore_signal(_listSem);
                                   [LoadingView hide];
                                   
                                   switch (error.code) {
                                       case 0:
                                           break;
                                           
                                       case 2:
                                           [self showError:@"Email inválido."];
                                           break;
                                           
                                       case 5:
                                           [self showError:@"Email já cadastrado."];
                                           break;
                                           
                                       default:
                                           [self showAlertView];
                                           break;
                                   }
                               }
                           }];
                
            }
            else {
                [LoadingView hide];
                [self showAlertView];
            }
        });
    });

}

#pragma mark - Helper Methods
- (void)showError:(NSString *)message {
    self.errorLabel.text = message;
    
    AFViewShaker *shaker = [[AFViewShaker alloc] initWithView:self.containerView];
    [shaker shake];
    
    [UIView animateWithDuration:0.3 animations:^{
        self.errorLabel.alpha = 1.0;
    }];
}

- (void)showAlertView {
    SIAlertView *alertView = [[SIAlertView alloc] initWithTitle:@"Ops..."
                                                     andMessage:@"Não conseguimos realizar o login. Verifique sua conexão com a Internet."];
    
    [alertView addButtonWithTitle:@"Ok"
                             type:SIAlertViewButtonTypeDestructive
                          handler:^(SIAlertView *alertView) {
                              if (!self.listAthletics) {
                                  DDLogVerbose(@"will fetch athletic list again");
                                  [self fetchAthleticList];
                              }
                              
                              [alertView dismissAnimated:YES];
                          }];
    
    alertView.transitionStyle = SIAlertViewTransitionStyleBounce;
    [alertView show];
}

- (void)fetchAthleticList {
    AthleticService *service = [[AthleticService alloc] init];
    [service listAllAthleticsWithCompletion:^(NSArray<VOAthletic *> *list, NSError *error) {
        dispatch_semaphore_signal(_listSem);
        
        if (error) {
            DDLogVerbose(@"list athletics HAS NOT BEEN populated");
        }
        else {
            DDLogVerbose(@"list athletics HAS BEEN populated");
            self.listAthletics = list;
        }
    }];
}

- (void)dismiss {
    [self.navigationController popViewControllerAnimated:YES];
}

#pragma mark - TextField Delegate
- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    if (textField.tag == passTag) {
        [textField resignFirstResponder];
    }
    else {
        UITextField *txt = (UITextField *) [self.view viewWithTag:textField.tag + 1];
        [txt becomeFirstResponder];
    }
    
    return YES;
}

@end
