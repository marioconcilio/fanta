//
//  SignupViewController.h
//  Fanta
//
//  Created by Mario Concilio on 28/04/15.
//  Copyright (c) 2015 Mario Concilio. All rights reserved.
//

#import <UIKit/UIKit.h>

@class FloatLabeledTextField, VOAthletic;
@interface SignupViewController : UIViewController

@property (nonatomic, strong) NSArray<VOAthletic *> *listAthletics;
@property (weak, nonatomic) IBOutlet FloatLabeledTextField *nameTextField;
@property (weak, nonatomic) IBOutlet FloatLabeledTextField *surnameTextField;
@property (weak, nonatomic) IBOutlet FloatLabeledTextField *emailTextField;
@property (weak, nonatomic) IBOutlet FloatLabeledTextField *passwordTextField;
@property (weak, nonatomic) IBOutlet UIButton *signupButton;
@property (weak, nonatomic) IBOutlet UIView *containerView;
@property (weak, nonatomic) IBOutlet UILabel *errorLabel;

- (IBAction)doSignup:(UIButton *)sender;
- (void)showError:(NSString *)message;

@end
