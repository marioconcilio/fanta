//
//  FriendsViewController.h
//  Fanta
//
//  Created by Mario Concilio on 9/21/15.
//  Copyright (c) 2015 Mario Concilio. All rights reserved.
//

#import <UIKit/UIKit.h>

@class FriendsViewController, VOUser;
@protocol FriendsDelegate <NSObject>

- (void)friendsViewController:(FriendsViewController *)vc didSelectUser:(VOUser *)user;

@end

@interface FriendsViewController : UITableViewController

@property (nonatomic, weak) id<FriendsDelegate> delegate;

@end
