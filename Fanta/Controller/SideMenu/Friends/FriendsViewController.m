//
//  FriendsViewController.m
//  Fanta
//
//  Created by Mario Concilio on 9/21/15.
//  Copyright (c) 2015 Mario Concilio. All rights reserved.
//

#import "VOUser.h"
#import "Helper.h"
#import "FriendsCell.h"
#import "FriendsViewController.h"
#import "Generator.h"
//#import "MCSideMenuViewController.h"

#import <SWRevealViewController.h>

@interface FriendsViewController () <UISearchResultsUpdating>

@property (nonatomic, strong) NSArray *friendsArray;
@property (nonatomic, strong) NSArray *searchResults;
@property (nonatomic, strong) NSCache *thumbCache;
@property (nonatomic, strong) UISearchController *searchController;

@end

static NSString *const CellID = @"Cell";

@implementation FriendsViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.tableView.separatorColor = [UIColor customDarkBackground];
    
    self.searchController = [[UISearchController alloc] initWithSearchResultsController:nil];
    self.searchController.searchResultsUpdater = self;
    self.searchController.dimsBackgroundDuringPresentation = NO;
    self.searchController.searchBar.searchBarStyle = UISearchBarStyleMinimal;
    self.searchController.searchBar.barStyle = UIBarStyleBlack;
    
    self.searchController.hidesNavigationBarDuringPresentation = NO;
    self.definesPresentationContext = YES;
    [self.searchController.searchBar sizeToFit];
//    self.tableView.tableHeaderView = self.searchController.searchBar;
    self.navigationItem.titleView = self.searchController.searchBar;
    
//    UIBlurEffect *blurEffect = [UIBlurEffect effectWithStyle:UIBlurEffectStyleDark];
//    UIVisualEffectView *blurEffectView = [[UIVisualEffectView alloc] initWithEffect:blurEffect];
//    self.tableView.backgroundView = blurEffectView;
//    self.tableView.backgroundColor = [UIColor clearColor];
//    self.tableView.separatorEffect = [UIVibrancyEffect effectForBlurEffect:blurEffect];
//    self.tableView.tableFooterView = [[UIView alloc] init];
    
    [self.navigationController.navigationBar setTintColor:[UIColor customOrange]];
    [self.navigationController.navigationBar setTitleTextAttributes:@{NSFontAttributeName:[UIFont lightFontWithSize:16.f],
                                                                      NSForegroundColorAttributeName:[UIColor customOrange]}];
    
    self.friendsArray = [Generator randomUsers:50];
    self.thumbCache = [[NSCache alloc] init];
//    self.tableView.contentInset = UIEdgeInsetsMake(20, 0, 0, 0);
    
//    [self.navigationController.navigationBar setBackgroundImage:[[UIImage alloc] init]
//                                                  forBarMetrics:UIBarMetricsDefault];
//    self.navigationController.navigationBar.shadowImage = [[UIImage alloc] init];
//    self.navigationController.navigationBar.translucent = YES;
//    self.navigationController.view.backgroundColor = [UIColor clearColor];
//    self.navigationController.navigationBar.backgroundColor = [UIColor clearColor];
//    self.navigationController.view.backgroundColor = [UIColor clearColor];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

#pragma mark - Table View Data Source
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    if ([self.searchController isActive]) {
        return self.searchResults.count;
    }
    
    return self.friendsArray.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    FriendsCell *cell = [tableView dequeueReusableCellWithIdentifier:CellID forIndexPath:indexPath];
    VOUser *user;
    if ([self.searchController isActive]) {
        user = self.searchResults[indexPath.row];
    }
    else {
        user = self.friendsArray[indexPath.row];
    }
    
    UIImage *thumb = [self.thumbCache objectForKey:user];
    if (thumb) {
        cell.profileImageView.image = thumb;
    }
    else {
        [Helper avatarFromName:user.name font:[UIFont lightFontWithSize:18.0] diameter:40.0 callback:^(UIImage *image) {
            cell.profileImageView.image = image;
            [self.thumbCache setObject:image forKey:user];
        }];
    }
    
    cell.nameLabel.text = user.name;
    
    return cell;
}

#pragma mark - Table View Delegate
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
    if ([self.searchController isActive]) {
        [self.delegate friendsViewController:self didSelectUser:self.searchResults[indexPath.row]];
    }
    else {
        [self.delegate friendsViewController:self didSelectUser:self.friendsArray[indexPath.row]];
    }
    
//    [self.revealViewController setFrontViewPosition:FrontViewPositionLeftSide animated:YES];
//    [self.sideMenuViewController hideMenuViewController];
}

#pragma mark - Search Results Updating
- (void)updateSearchResultsForSearchController:(UISearchController *)searchController {
    self.searchResults = nil;
    
    NSPredicate *resultPredicate = [NSPredicate predicateWithFormat:@"name contains[c] %@", searchController.searchBar.text];
    self.searchResults = [self.friendsArray filteredArrayUsingPredicate:resultPredicate];
    
    [self.tableView reloadData];
}

@end
