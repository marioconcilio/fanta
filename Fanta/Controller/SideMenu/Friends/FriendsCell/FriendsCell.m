//
//  FriendsCell.m
//  Fanta
//
//  Created by Mario Concilio on 9/21/15.
//  Copyright (c) 2015 Mario Concilio. All rights reserved.
//

#import "FriendsCell.h"

@implementation FriendsCell

- (void)awakeFromNib {
    self.profileImageView.layer.cornerRadius = 20.0;
    self.profileImageView.layer.borderColor = [UIColor lightGrayColor].CGColor;
    self.profileImageView.layer.borderWidth = 0.5;
    self.profileImageView.clipsToBounds = YES;
    
    self.backgroundColor = [UIColor clearColor];
    
    UIView *view = [[UIView alloc] init];
    view.backgroundColor = [UIColor customPink];
    self.selectedBackgroundView = view;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
