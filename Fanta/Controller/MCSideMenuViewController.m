/*
The MIT License (MIT)

Copyright (c) 2015 Mario Concilio

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*/

#import "MCSideMenuViewController.h"
#import "UIImage+GPUImage.h"
#import <POP.h>
#import <AsyncDisplayKit.h>

#define MCKeyWindow     [[UIApplication sharedApplication] keyWindow]

#define SIDEMENU_WILL_SHOW(vc)      if (delegateRespondsTo.willShow)    [delegate sideMenu:self willShowMenu:vc fromSide:self.sideMenu]
#define SIDEMENU_DID_SHOW(vc)       if (delegateRespondsTo.didShow)     [delegate sideMenu:self didShowMenu:vc fromSide:self.sideMenu]
#define SIDEMENU_WILL_DISMISS(vc)   if (delegateRespondsTo.willDismiss) [delegate sideMenu:self willDismissMenu:vc fromSide:self.sideMenu]
#define SIDEMENU_DID_DISMISS(vc)    if (delegateRespondsTo.didDismiss)  [delegate sideMenu:self didDismissMenu:vc fromSide:self.sideMenu]

#pragma mark - Category
@implementation UIViewController (MCSideMenuViewController)

static const void *LeftMenuKey = &LeftMenuKey;
static const void *RightMenuKey = &RightMenuKey;
static const void *ContentKey = &ContentKey;

#pragma mark - Getters & Setters
#pragma mark Side Menu View Controller
- (MCSideMenuViewController *)sideMenuViewController {
    UIViewController *vc = self.parentViewController;
    while (vc) {
        if ([vc isKindOfClass:[MCSideMenuViewController class]]) {
            return (MCSideMenuViewController *)vc;
        }
        else if (vc.parentViewController && vc.parentViewController != vc) {
            vc = vc.parentViewController;
        }
        else {
            vc = nil;
        }
    }
    
    return nil;
}

#pragma mark - Helper Methods
- (void)addDefaultShadow {
    [self addShadowWithColor:[UIColor blackColor] offset:CGSizeZero opacity:0.8 radius:5.0];
}

- (void)addShadowWithColor:(UIColor *)color
                    offset:(CGSize)offset
                   opacity:(CGFloat)opacity
                    radius:(CGFloat)radius {
    self.view.layer.masksToBounds = NO;
    self.view.layer.shadowColor = color.CGColor;
    self.view.layer.shadowOffset = offset;
    self.view.layer.shadowOpacity = opacity;
    self.view.layer.shadowRadius = radius;
    self.view.layer.shadowPath = [UIBezierPath bezierPathWithRect:self.view.frame].CGPath;
}

@end

#pragma mark - MCSideMenu View Controller
@interface MCSideMenuViewController () <UIGestureRecognizerDelegate, POPAnimationDelegate> {
    struct {
        unsigned int willShow:1;
        unsigned int didShow:1;
        unsigned int willDismiss:1;
        unsigned int didDismiss:1;
    } delegateRespondsTo;
    
    CGFloat _center;
}

@property (strong, nonatomic) UITapGestureRecognizer *tap;
@property (strong, nonatomic) UIPanGestureRecognizer *pan;
@property (strong, nonatomic) UIScreenEdgePanGestureRecognizer *leftEdgePan;
@property (strong, nonatomic) UIScreenEdgePanGestureRecognizer *rightEdgePan;
@property (strong, nonatomic) NSMutableArray *blurredImages;

@property (weak, nonatomic) ASImageNode *bkgView;
@property (weak, nonatomic) ASImageNode *bkgView2;

@property (assign, nonatomic) MCSideMenu sideMenu;

@end

static NSString *const kDismissMenuAnimation    = @"dismissMenuAnimation";
static NSString *const kShowMenuAnimation       = @"showMenuAnimation";
static NSString *const kFadeInAnimation         = @"fadeInAnimation";
static NSString *const kFadeOutAnimation        = @"fadeOutAnimation";

@implementation MCSideMenuViewController

#pragma mark - Init Methods
- (instancetype)init {
    self = [super init];
    if (self) {
        [self setupInit];
    }
    return self;
}

- (instancetype)initWithCoder:(NSCoder *)decoder {
    self = [super initWithCoder:decoder];
    if (self) {
        [self setupInit];
    }
    return self;
}

- (instancetype)initWithContentViewController:(UIViewController *)contentViewController
                       leftMenuViewController:(UIViewController *)leftMenuViewController
                      rightMenuViewController:(UIViewController *)rightMenuViewController {
    self = [self init];
    if (self) {
        _contentViewController = contentViewController;
        _leftMenuViewController = leftMenuViewController;
        _rightMenuViewController = rightMenuViewController;
        [self setupInit];
    }
    
    return self;
}

- (void)setupInit {
    _maximumBlurRadius = 15.0;
    _numberOfStages = 7.0;
    
    _animationDuration      = 0.3;
    _menuMargin             = 60.0;
    _menuGravityIntensity   = 4.0;
    _menuElasticity         = 0.3;
    _backgroundViewAlpha    = 0.4;
    
    _leftScreenEdgePanEnabled   = YES;
    _rightScreenEdgePanEnabled  = YES;
    
    _leftMenuShadowEnabled  = YES;
    _rightMenuShadowEnabled = YES;
    
    _menuShadowColor = [UIColor blackColor];
    _menuShadowOffset = CGSizeZero;
    _menuShadowOpacity = 0.8;
    _menuShadowRadius = 5.0;
}

#pragma mark - View Life Cycle
- (void)viewDidLoad {
    [super viewDidLoad];
    [self loadStoryboardControllers];
    
    self.view.autoresizingMask = UIViewAutoresizingFlexibleHeight | UIViewAutoresizingFlexibleWidth;
    self.view.backgroundColor = [UIColor blackColor];
    [self.view addSubview:self.contentViewController.view];
    [self addChildViewController:self.contentViewController];
    
    [self setupGestures];
}

#pragma mark - Delegate
@synthesize delegate;
- (void)setDelegate:(id<MCSideMenuDelegate>)aDelegate {
    if (delegate != aDelegate) {
        delegate = aDelegate;
        
        delegateRespondsTo.willShow = [delegate respondsToSelector:@selector(sideMenu:willShowMenu:fromSide:)];
        delegateRespondsTo.didShow = [delegate respondsToSelector:@selector(sideMenu:didShowMenu:fromSide:)];
        delegateRespondsTo.willDismiss = [delegate respondsToSelector:@selector(sideMenu:willDismissMenu:fromSide:)];
        delegateRespondsTo.didDismiss = [delegate respondsToSelector:@selector(sideMenu:didDismissMenu:fromSide:)];
    }
}

#pragma mark - Show/Set/Hide Public Methods
- (void)showLeftMenuViewController {
    if (!_leftMenuViewController) {
        return;
    }
    
    [self setupBurredImages];
    
    self.sideMenu = MCSideMenuLeft;
    self.leftMenuViewController.view.hidden = NO;
    self.rightMenuViewController.view.hidden = YES;
    [self.view.window endEditing:YES];
    [self setupViewForMenu:self.leftMenuViewController];
}

- (void)showRightMenuViewController {
    if (!_rightMenuViewController) {
        return;
    }
    
    [self setupBurredImages];
    
    self.sideMenu = MCSideMenuRight;
    self.leftMenuViewController.view.hidden = YES;
    self.rightMenuViewController.view.hidden = NO;
    [self.view.window endEditing:YES];
    [self setupViewForMenu:self.rightMenuViewController];
}

- (void)setContentViewController:(UIViewController *)contentViewController animated:(BOOL)animated {
    if (_contentViewController == contentViewController) {
        return;
    }
    
    if (!animated) {
        [self setContentViewController:contentViewController];
    }
    else {
        [self addChildViewController:contentViewController];
        contentViewController.view.alpha = 0.0;
        contentViewController.view.frame = self.view.bounds;
        [self.view addSubview:contentViewController.view];
        
        [UIView animateWithDuration:_animationDuration
                         animations:^{
                             contentViewController.view.alpha = 1.0;
                         }
                         completion:^(BOOL finished) {
                             [self hideViewController:self.contentViewController];
                             [contentViewController didMoveToParentViewController:self];
                             _contentViewController = contentViewController;
                             
                             if (self.leftMenuViewController && _leftScreenEdgePanEnabled) {
                                 [self.contentViewController.view addGestureRecognizer:self.leftEdgePan];
                             }
                             
                             if (self.rightMenuViewController && _rightScreenEdgePanEnabled) {
                                 [self.contentViewController.view addGestureRecognizer:self.rightEdgePan];
                             }
                         }];
    }
}

- (void)hideMenuViewController {
    UIViewController *viewController;
    CGFloat centerX;
    
    // Left Menu is showing
    if (self.sideMenu == MCSideMenuLeft) {
        viewController = self.leftMenuViewController;
        centerX = CGRectGetWidth(viewController.view.bounds) / -2;
    }
    // Right Menu is showing
    else {
        viewController = self.rightMenuViewController;
        CGFloat width = CGRectGetWidth(MCKeyWindow.bounds);
        CGFloat viewWidth = CGRectGetWidth(viewController.view.bounds);
        centerX = width + viewWidth / 2;
    }
    
    // Send message to delegate
    SIDEMENU_WILL_DISMISS(viewController);
    
    POPSpringAnimation *dismissMenuAnimation = [POPSpringAnimation animation];
    dismissMenuAnimation.property = [POPAnimatableProperty propertyWithName: kPOPLayerPositionX];
    dismissMenuAnimation.toValue = @(centerX);
    dismissMenuAnimation.name = kDismissMenuAnimation;
    dismissMenuAnimation.delegate = self;
    [viewController.view.layer pop_addAnimation:dismissMenuAnimation forKey:kDismissMenuAnimation];
    
    POPBasicAnimation *fadeOutAnimation = [POPBasicAnimation animationWithPropertyNamed:kPOPViewAlpha];
    fadeOutAnimation.timingFunction = [CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseIn];
    fadeOutAnimation.fromValue = @(1.0);
    fadeOutAnimation.toValue = @(0.0);
    fadeOutAnimation.completionBlock = ^(POPAnimation *anim, BOOL finished) {
        if (finished) {
            MCKeyWindow.tintAdjustmentMode = UIViewTintAdjustmentModeAutomatic;
            [MCKeyWindow tintColorDidChange];
            
            [self.bkgView.view removeFromSuperview];
            [self.bkgView2.view removeFromSuperview];
            self.bkgView = nil;
            self.bkgView2 = nil;
        }
    };
    
    [self.bkgView pop_addAnimation:fadeOutAnimation forKey:kFadeOutAnimation];
    [self.bkgView2 pop_addAnimation:fadeOutAnimation forKey:kFadeOutAnimation];
}

#pragma mark - Private Methods
- (BOOL)isViewBlur:(UIViewController *)viewController {
    for (UIView *view in viewController.view.subviews) {
        if ([view isKindOfClass:[UIVisualEffectView class]]) {
            return YES;
        }
    }
    
    return NO;
}

- (void)hideViewController:(UIViewController *)viewController {
    [viewController willMoveToParentViewController:nil];
    [viewController.view removeFromSuperview];
    [viewController removeFromParentViewController];
}

- (void)setScrollEnabled:(BOOL)enabled fromMenuView:(UIView *)view {
    if ([view isKindOfClass:[UIScrollView class]]) {
        UIScrollView *scroll = (UIScrollView *) view;
        scroll.scrollEnabled = enabled;
    }
}

- (void)setupViewForMenu:(UIViewController *)viewController {
    /*
    UIView *bkg = [[UIView alloc] initWithFrame:MCKeyWindow.bounds];
    bkg.backgroundColor = [UIColor colorWithWhite:0.0 alpha:_backgroundViewAlpha];
    self.bkgView = bkg;
    [MCKeyWindow addSubview:bkg];
    
    POPBasicAnimation *fadeInAnimation = [POPBasicAnimation animationWithPropertyNamed:kPOPViewAlpha];
    fadeInAnimation.timingFunction = [CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseOut];
    fadeInAnimation.fromValue = @(0.0);
    fadeInAnimation.toValue = @(1.0);
    fadeInAnimation.completionBlock = ^(POPAnimation *anim, BOOL finished) {
        MCKeyWindow.tintAdjustmentMode = UIViewTintAdjustmentModeDimmed;
        [MCKeyWindow tintColorDidChange];
    };
    [bkg pop_addAnimation:fadeInAnimation forKey:kFadeInAnimation];
    
    [bkg addGestureRecognizer:self.tap];
    */
    [viewController.view addGestureRecognizer:self.pan];
    
    [MCKeyWindow addSubview:viewController.view];
    [self addChildViewController:viewController];
    
    CGFloat height = CGRectGetHeight(MCKeyWindow.bounds);
    CGFloat width = CGRectGetWidth(MCKeyWindow.bounds);
    CGSize menuSize = CGSizeMake(width - _menuMargin, height);
    CGPoint menuPosition;
    CGFloat offset;
    
    // Is left menu
    if (viewController == self.leftMenuViewController) {
        self.sideMenu = MCSideMenuLeft;
        menuPosition = CGPointMake(-menuSize.width, 0.0);
        offset = _menuMargin / 2;
    }
    // Is right menu
    else {
        self.sideMenu = MCSideMenuRight;
        menuPosition = CGPointMake(width, 0.0);
        offset = -_menuMargin / 2;
    }
    
    viewController.view.frame = (CGRect) {menuPosition, menuSize};
    [self setupShadows];
    
    POPBasicAnimation *showMenuAnimation = [POPBasicAnimation animation];
    showMenuAnimation.property = [POPAnimatableProperty propertyWithName:kPOPLayerPositionX];
    showMenuAnimation.toValue = @((width / 2) - offset);
    showMenuAnimation.name = kShowMenuAnimation;
    showMenuAnimation.delegate = self;
    [viewController.view.layer pop_addAnimation:showMenuAnimation forKey:kShowMenuAnimation];
    
    // Send message to delegate
    SIDEMENU_WILL_SHOW(viewController);
}

- (void)loadStoryboardControllers {
    if (self.storyboard) {
        @try {
            [self performSegueWithIdentifier:MCSegueContentIdentifier sender:nil];
        }
        @catch(NSException *exception) {}
        
        @try {
            [self performSegueWithIdentifier:MCSegueLeftMenuIdentifier sender:nil];
        }
        @catch(NSException *exception) {}
        
        @try {
            [self performSegueWithIdentifier:MCSegueRightMenuIdentifier sender:nil];
        }
        @catch(NSException *exception) {}
    }
}

#pragma mark - Setup Methods
- (void)setupBurredImages {
    NSDate *stamp = [NSDate date];
    UIImage *snapshot = [UIImage takeSnapshot];
    
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
//        self.blurredImages = [NSMutableArray arrayWithCapacity:_numberOfStages];
        self.blurredImages = [[NSMutableArray alloc] initWithCapacity:_numberOfStages];
        
        @autoreleasepool {
            for (NSInteger i = 0; i <= _numberOfStages; i++) {
                CGFloat radius = i * _maximumBlurRadius / _numberOfStages;
                UIImage *blurredImage = [snapshot applyBlurWithRadius:radius];
                [self.blurredImages addObject:blurredImage];
            }
        }
        
        ASImageNode *bkg = [[ASImageNode alloc] init];
        bkg.backgroundColor = [UIColor clearColor];
        bkg.image = [self.blurredImages lastObject];
        bkg.frame = MCKeyWindow.bounds;
        bkg.userInteractionEnabled = YES;
        self.bkgView = bkg;
        
        ASImageNode *bkg2 = [[ASImageNode alloc] init];
        bkg2.backgroundColor = [UIColor clearColor];
        bkg2.frame = MCKeyWindow.bounds;
        
//        ASDisplayNode *yellowNode = [[ASDisplayNode alloc] init];
//        yellowNode.frame = MCKeyWindow.bounds;
//        yellowNode.backgroundColor = [UIColor yellowColor];
//        [bkg2 addSubnode:yellowNode];
        
        self.bkgView2 = bkg2;
        
        DDLogVerbose(@"generation of blurred images took %f seconds", -stamp.timeIntervalSinceNow);
        
        dispatch_async(dispatch_get_main_queue(), ^{
            if (self.sideMenu == MCSideMenuLeft) {
                [MCKeyWindow insertSubview:bkg.view belowSubview:self.leftMenuViewController.view];
            }
            else {
                [MCKeyWindow insertSubview:bkg.view belowSubview:self.rightMenuViewController.view];
            }
            
            POPBasicAnimation *fadeInAnimation = [POPBasicAnimation animationWithPropertyNamed:kPOPViewAlpha];
//            fadeInAnimation.timingFunction = [CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseOut];
            fadeInAnimation.fromValue = @(0.0);
            fadeInAnimation.toValue = @(1.0);
            fadeInAnimation.completionBlock = ^(POPAnimation *anim, BOOL finished) {
                [MCKeyWindow insertSubview:bkg2.view belowSubview:bkg.view];
            };
            [bkg.view pop_addAnimation:fadeInAnimation forKey:kFadeInAnimation];
            
            [bkg.view addGestureRecognizer:self.tap];
        });
    });
    
    
}

- (void)setupShadows {
    if (self.leftMenuViewController && _leftMenuShadowEnabled) {
        self.leftMenuViewController.view.layer.masksToBounds = NO;
        self.leftMenuViewController.view.layer.shadowColor = self.menuShadowColor.CGColor;
        self.leftMenuViewController.view.layer.shadowOffset = _menuShadowOffset;
        self.leftMenuViewController.view.layer.shadowOpacity = _menuShadowOpacity;
        
        if (![self isViewBlur:self.leftMenuViewController]) {
            self.leftMenuViewController.view.layer.shadowPath = [UIBezierPath bezierPathWithRect:self.leftMenuViewController.view.bounds].CGPath;
        }
        
        self.leftMenuViewController.view.layer.shadowRadius = _menuShadowRadius;
    }
    
    if (self.rightMenuViewController && _rightMenuShadowEnabled) {
        self.rightMenuViewController.view.layer.masksToBounds = NO;
        self.rightMenuViewController.view.layer.shadowColor = self.menuShadowColor.CGColor;
        self.rightMenuViewController.view.layer.shadowOffset = _menuShadowOffset;
        self.rightMenuViewController.view.layer.shadowOpacity = _menuShadowOpacity;
        
        if (![self isViewBlur:self.rightMenuViewController]) {
            self.rightMenuViewController.view.layer.shadowPath = [UIBezierPath bezierPathWithRect:self.rightMenuViewController.view.bounds].CGPath;
        }
        
        self.rightMenuViewController.view.layer.shadowRadius = _menuShadowRadius;
    }
}

- (void)setupGestures {
    if (self.leftMenuViewController && _leftScreenEdgePanEnabled) {
        self.leftEdgePan = [[UIScreenEdgePanGestureRecognizer alloc] initWithTarget:self action:@selector(handleLeftEdgePan:)];
        self.leftEdgePan.edges = UIRectEdgeLeft;
        [self.contentViewController.view addGestureRecognizer:self.leftEdgePan];
    }
    
    if (self.rightMenuViewController && _rightScreenEdgePanEnabled) {
        self.rightEdgePan = [[UIScreenEdgePanGestureRecognizer alloc] initWithTarget:self action:@selector(handleRightEdgePan:)];
        self.rightEdgePan.edges = UIRectEdgeRight;
        [self.contentViewController.view addGestureRecognizer:self.rightEdgePan];
    }
    
    self.tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(handleTap:)];
    self.pan = [[UIPanGestureRecognizer alloc] initWithTarget:self action:@selector(handlePan:)];
    self.pan.minimumNumberOfTouches = 1;
    self.pan.maximumNumberOfTouches = 1;
    self.pan.delegate = self;
}

#pragma mark - Gesture Methods
- (void)handleLeftEdgePan:(UIScreenEdgePanGestureRecognizer *)gesture {
    if (gesture.state == UIGestureRecognizerStateEnded) {
        [self showLeftMenuViewController];
    }
}

- (void)handleRightEdgePan:(UIScreenEdgePanGestureRecognizer *)gesture {
    if (gesture.state == UIGestureRecognizerStateEnded) {
        [self showRightMenuViewController];
    }
}

- (void)handleTap:(UITapGestureRecognizer *)gesture {
    [self hideMenuViewController];
}

- (void)handlePan:(UIPanGestureRecognizer *)gesture {
    if (gesture.state == UIGestureRecognizerStateBegan) {
        _center = gesture.view.center.x;
        
        [self setScrollEnabled:NO fromMenuView:gesture.view];
        [gesture.view.layer pop_removeAllAnimations];
    }
    
    if (gesture.state == UIGestureRecognizerStateChanged) {
        CGPoint translation = [gesture translationInView:self.view];
        CGFloat width = CGRectGetWidth(MCKeyWindow.bounds);
        CGFloat height = CGRectGetHeight(MCKeyWindow.bounds);
        CGFloat x = gesture.view.center.x + translation.x;
        
        // Left Menu is showing
        if (self.sideMenu == MCSideMenuLeft) {
            if (x <= (width / 2) - (_menuMargin / 2)) {
                
//                dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_HIGH, 0), ^{
                    double r = x + _center;
                    double div = (2 * _center) / _numberOfStages;
                    long blurIndex = ceil(r / div);
                    double alpha = (fmod(r, div)) / div;
                
                [UIView animateKeyframesWithDuration:0.3
                                               delay:0.0
                                             options:UIViewKeyframeAnimationOptionAllowUserInteraction
                                          animations:^{
                                              self.bkgView.image = self.blurredImages[blurIndex];
                                              self.bkgView.alpha = alpha;
                                              self.bkgView2.image = self.blurredImages[blurIndex - 1];
                                          }
                                          completion:NULL];
                    
//                    dispatch_async(dispatch_get_main_queue(), ^{
//                        self.bkgView.alpha = alpha;
//                    });
//                });
                
//                dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_HIGH, 0), ^{
//                    double r = x + center;
//                    double div = (2 * center) / 1;
//                    double alpha = r / div;
//                    
//                    NSLog(@"%f", alpha);
//                    
//                    dispatch_async(dispatch_get_main_queue(), ^{
//                        self.bkgView.alpha = alpha;
//                    });
//                });
                
                gesture.view.center = CGPointMake(x, height / 2);
                [gesture setTranslation:CGPointMake(0, 0) inView:self.view];
            }
        }
        // Right Menu is showing
        else {
            if (x >= (width / 2) + (_menuMargin / 2)) {
//                double r = x + _center;
//                double div = (2 * _center) / _numberOfStages;
//                long blurIndex = ceil(r / div);
//                double alpha = (fmod(r, div)) / div;
//                
//                self.bkgView.image = self.blurredImages[blurIndex];
//                self.bkgView.alpha = alpha;
//                self.bkgView2.image = self.blurredImages[blurIndex - 1];
                
                gesture.view.center = CGPointMake(x, height / 2);
                [gesture setTranslation:CGPointMake(0, 0) inView:self.view];
            }
        }
    }
    
    if (gesture.state == UIGestureRecognizerStateEnded) {
        [self setScrollEnabled:YES fromMenuView:gesture.view];
        
        CGPoint velocity = [gesture velocityInView:self.view];
        
        // Left Menu is showing
        if (self.sideMenu == MCSideMenuLeft) {
            // Swiping right
            if (velocity.x > 0) {
                CGFloat width = CGRectGetWidth(MCKeyWindow.bounds);
                
                POPBasicAnimation *showMenuAnimation = [POPBasicAnimation animation];
                showMenuAnimation.property = [POPAnimatableProperty propertyWithName:kPOPLayerPositionX];
                showMenuAnimation.toValue = @((width / 2) - (_menuMargin / 2));
                showMenuAnimation.name = kShowMenuAnimation;
                showMenuAnimation.delegate = self;
                [gesture.view.layer pop_addAnimation:showMenuAnimation forKey:kShowMenuAnimation];
            }
            // Swiping left (will dismiss)
            else {
                [self hideMenuViewController];
            }
        }
        // Right Menu is showing
        else {
            // Swiping right (will dismiss)
            if (velocity.x > 0) {
                [self hideMenuViewController];
            }
            // Swiping left
            else {
                CGFloat width = CGRectGetWidth(MCKeyWindow.bounds);
                
                POPBasicAnimation *showMenuAnimation = [POPBasicAnimation animation];
                showMenuAnimation.property = [POPAnimatableProperty propertyWithName:kPOPLayerPositionX];
                showMenuAnimation.toValue = @((width / 2) + (_menuMargin / 2));
                showMenuAnimation.name = kShowMenuAnimation;
                showMenuAnimation.delegate = self;
                [gesture.view.layer pop_addAnimation:showMenuAnimation forKey:kShowMenuAnimation];
            }
        }
    }
}

#pragma mark - Gesture Delegate
- (BOOL)gestureRecognizer:(UIGestureRecognizer *)gestureRecognizer shouldRecognizeSimultaneouslyWithGestureRecognizer:(UIGestureRecognizer *)otherGestureRecognizer {
    return YES;
}

- (BOOL)gestureRecognizerShouldBegin:(UIPanGestureRecognizer *)gestureRecognizer {
    CGPoint translation = [gestureRecognizer translationInView:gestureRecognizer.view];
    
    // Check for horizontal gesture
    if (fabs(translation.x) > fabs(translation.y)) {
        return YES;
    }
    
    return NO;
}

#pragma mark - Pop Delegate
- (void)pop_animationDidStop:(POPAnimation *)anim finished:(BOOL)finished {
    if (finished) {
        if ([anim.name isEqualToString:kDismissMenuAnimation]) {
            UIViewController *viewController;
            
            // Left Menu did dismiss
            if (self.sideMenu == MCSideMenuLeft) {
                viewController = self.leftMenuViewController;
                [self.leftMenuViewController.view removeFromSuperview];
                [self.leftMenuViewController removeFromParentViewController];
            }
            // Right Menu did dismiss
            else {
                viewController = self.rightMenuViewController;
                [self.rightMenuViewController.view removeFromSuperview];
                [self.rightMenuViewController removeFromParentViewController];
            }
            
            // Send message to delegate
            SIDEMENU_DID_DISMISS(viewController);
        }
        else if ([anim.name isEqualToString:kShowMenuAnimation]) {
            UIViewController *viewController;
            
            // Left Menu did dismiss
            if (self.sideMenu == MCSideMenuLeft) {
                viewController = self.leftMenuViewController;
            }
            // Right Menu did dismiss
            else {
                viewController = self.rightMenuViewController;
            }
            
            // Send message to delegate
            SIDEMENU_DID_SHOW(viewController);
        }
    }
}

@end

#pragma mark - Segue Identifiers
NSString * const MCSegueContentIdentifier   = @"mc_content";
NSString * const MCSegueLeftMenuIdentifier  = @"mc_left";
NSString * const MCSegueRightMenuIdentifier = @"mc_right";

#pragma mark - Storyboard Segue
@implementation MCSideMenuSetSegue

- (void)perform {
    MCSideMenuViewController *container = self.sourceViewController;
    
    if ([self.identifier isEqualToString:MCSegueContentIdentifier]) {
        container.contentViewController = self.destinationViewController;
    }
    else if ([self.identifier isEqualToString:MCSegueLeftMenuIdentifier]) {
        container.leftMenuViewController = self.destinationViewController;
    }
    else if ([self.identifier isEqualToString:MCSegueRightMenuIdentifier]) {
        container.rightMenuViewController = self.destinationViewController;
    }
}

@end
