//
//  MessagesDataController.h
//  Fanta
//
//  Created by Mario Concilio on 9/25/15.
//  Copyright (c) 2015 Mario Concilio. All rights reserved.
//

#import <Foundation/Foundation.h>

@class JSQMessagesAvatarImage, VOUser;
@interface MessagesDataController : NSObject

- (instancetype)initWithUsers:(NSArray<VOUser*> *)users;

@property (nonatomic, strong) NSMutableArray<VOUser*> *users;
@property (nonatomic, strong) NSMutableDictionary<NSString*, JSQMessagesAvatarImage*> *avatars;
@property (nonatomic, strong) NSMutableArray *messages;
@property (nonatomic, strong) JSQMessagesAvatarImage *meAvatar;

- (void)addUser:(VOUser *)user;
- (void)removeUser:(VOUser *)user;
- (VOUser *)userFromID:(NSUInteger)userID;

@end
