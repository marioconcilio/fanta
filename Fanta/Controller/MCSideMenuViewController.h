/*
 The MIT License (MIT)
 
 Copyright (c) 2015 Mario Concilio
 
 Permission is hereby granted, free of charge, to any person obtaining a copy
 of this software and associated documentation files (the "Software"), to deal
 in the Software without restriction, including without limitation the rights
 to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 copies of the Software, and to permit persons to whom the Software is
 furnished to do so, subject to the following conditions:
 
 The above copyright notice and this permission notice shall be included in all
 copies or substantial portions of the Software.
 
 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 SOFTWARE.
*/

#import <UIKit/UIKit.h>

#ifndef IBInspectable
#define IBInspectable
#endif

typedef NS_ENUM(NSInteger, MCSideMenu) {
    MCSideMenuLeft,
    MCSideMenuRight
};

#pragma mark - View Controller Category
@class MCSideMenuViewController;
@interface UIViewController (MCSideMenuViewController)

@property (strong, readonly, nonatomic) MCSideMenuViewController *sideMenuViewController;

- (void)addDefaultShadow;
- (void)addShadowWithColor:(UIColor *)color
                    offset:(CGSize)offset
                   opacity:(CGFloat)opacity
                    radius:(CGFloat)radius;

@end

#pragma mark - Delegate
@class MCSideMenuViewController;
@protocol MCSideMenuDelegate <NSObject>

@optional

/**
 *  @abstract   Called when a Menu will show
 *  @param  sideMenu    MCSideMenu View Controller instance
 *  @param  menu        The Menu's View Controller that will be presented
 *  @param  side        Which side will show the menu on screen
 */
- (void)sideMenu:(MCSideMenuViewController *)sideMenu willShowMenu:(UIViewController *)menu fromSide:(MCSideMenu)side;

/**
 *  @abstract   Called when a Menu is shown and all the animations are completed
 *  @param  sideMenu    MCSideMenu View Controller instance
 *  @param  menu        The Menu's View Controller presented
 *  @param  side        Which side the menu has shown
 */
- (void)sideMenu:(MCSideMenuViewController *)sideMenu didShowMenu:(UIViewController *)menu fromSide:(MCSideMenu)side;

/**
 *  @abstract   Called when a Menu will be dismissed
 *  @param  sideMenu    MCSideMenu View Controller instance
 *  @param  menu        The Menu's View Controller about to be dismissed
 *  @param  side        Which side is the menu
 */
- (void)sideMenu:(MCSideMenuViewController *)sideMenu willDismissMenu:(UIViewController *)menu fromSide:(MCSideMenu)side;

/**
 *  @abstract   Called when a Menu is successfully dismissed and all animations are complete
 *  @param  sideMenu    MCSideMenu View Controller instance
 *  @param  menu        The Menu's View Controller dismissed
 *  @param  side        Which side the menu was on screen
 */
- (void)sideMenu:(MCSideMenuViewController *)sideMenu didDismissMenu:(UIViewController *)menu fromSide:(MCSideMenu)side;

@end

#pragma mark - MCSideMenu View Controller Class
@interface MCSideMenuViewController : UIViewController

@property (weak, nonatomic) id<MCSideMenuDelegate> delegate;

@property (assign, nonatomic) CGFloat maximumBlurRadius;

@property (assign, nonatomic) CGFloat numberOfStages;

/**
 *  @abstract
 *  @discussion Default value is 0.4
 */
@property (assign, nonatomic) IBInspectable CGFloat backgroundViewAlpha;

/**
 *  @abstract
 *  @discussion Default value is 0.3
 */
@property (assign, nonatomic) IBInspectable CGFloat menuElasticity;

/**
 *  @abstract
 *  @discussion Default value is 4.0
 */
@property (assign, nonatomic) IBInspectable CGFloat menuGravityIntensity;

/**
 *  @abstract
 *  @discussion Default value is 60.0
 */
@property (assign, nonatomic) IBInspectable CGFloat menuMargin;

/**
 *  @abstract
 *  @discussion Default value is 0.3
 */
@property (assign, nonatomic) IBInspectable CGFloat animationDuration;

/**
 *  @abstract
 *  @discussion Default value is YES
 */
@property (assign, nonatomic) IBInspectable BOOL leftScreenEdgePanEnabled;

/**
 *  @abstract
 *  @discussion Default value is YES
 */
@property (assign, nonatomic) IBInspectable BOOL rightScreenEdgePanEnabled;

/**
 *  @abstract
 *  @discussion Default value is YES
 */
@property (assign, nonatomic) IBInspectable BOOL leftMenuShadowEnabled;

/**
 *  @abstract
 *  @discussion Default value is YES
 */
@property (assign, nonatomic) IBInspectable BOOL rightMenuShadowEnabled;

/// Default value is blackColor
@property (strong, nonatomic) UIColor *menuShadowColor;

/// Default value is CGSizeZero
@property (assign, nonatomic) CGSize menuShadowOffset;

/// Defautl value is 0.8
@property (assign, nonatomic) CGFloat menuShadowOpacity;

/// Default value is 5.0
@property (assign, nonatomic) CGFloat menuShadowRadius;

/// View Controller for Content View
@property (strong, nonatomic) UIViewController *contentViewController;

/// View Controller for Left Menu
@property (strong, nonatomic) UIViewController *leftMenuViewController;

/// View Controller for Right Menu
@property (strong, nonatomic) UIViewController *rightMenuViewController;

/**
 *  @abstract   Initializes a newly allocated MCSideMenu View Controller
 *  @param  contentViewController   View Controller to be used as content view
 *  @param  leftMenuViewController  View Controller to be used as left menu
 *  @param  rightMenuViewController View Controller to be used as right menu
 */
- (instancetype)initWithContentViewController:(UIViewController *)contentViewController
                       leftMenuViewController:(UIViewController *)leftMenuViewController
                      rightMenuViewController:(UIViewController *)rightMenuViewController;

/**
 *  @abstract Shows left menu
 */
- (void)showLeftMenuViewController;

/**
 *  @abstract Shows right menu
 */
- (void)showRightMenuViewController;

/**
 *  @abstract Hides opened menu
 */
- (void)hideMenuViewController;

/**
 *  @abstract Sets another View Controller as Content View Controller. Use this method to change views after menu actions
 *  @param contentViewController    New View Controller to be presented
 *  @param animated                 Still discovering
 */
#warning TODO set animated to NO
- (void)setContentViewController:(UIViewController *)contentViewController animated:(BOOL)animated;

@end

#pragma mark - StoryBoard Segue
/// @"mc_content"
extern NSString* const MCSegueContentIdentifier;

/// @"mc_left"
extern NSString* const MCSegueLeftMenuIdentifier;

/// @"mc_right"
extern NSString* const MCSegueRightMenuIdentifier;

@interface MCSideMenuSetSegue : UIStoryboardSegue

@end
