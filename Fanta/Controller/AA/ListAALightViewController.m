//
//  ListAALightViewController.m
//  Fanta
//
//  Created by Mario Concilio on 7/19/16.
//  Copyright © 2016 Mario Concilio. All rights reserved.
//

#import "ListAALightViewController.h"
#import "AAFilterCell.h"
#import "VOAthletic.h"
#import "TextStyles.h"

#import <AsyncDisplayKit.h>

@interface ListAALightViewController ()

@end

@implementation ListAALightViewController

#pragma mark - View Life Cycle
- (void)viewDidLoad {
    [super viewDidLoad];
//    self.filtering = YES;
    self.tableView.backgroundColor = [UIColor whiteColor];
    self.tableView.separatorColor = [UIColor lightGrayColor];
    
    [self.navigationController.navigationBar setBarStyle:UIBarStyleDefault];
    [self.navigationController.navigationBar setTintColor:[UIColor customPink]];
    self.searchController.searchBar.barStyle = UIBarStyleDefault;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Table View Data Source
- (ASCellNode *)tableView:(ASTableView *)tableView nodeForRowAtIndexPath:(NSIndexPath *)indexPath {
    if (indexPath.section == 0) {
        ASTextCellNode *cell = [[ASTextCellNode alloc] initWithAttributes:[TextStyles chooseAASelectAllLightStyle]
                                                                   insets:UIEdgeInsetsMake(10.0, 10.0, 10.0, 10.0)];
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        
        return cell;
    }
    
    VOAthletic *aa = [self athleticAtIndexPath:indexPath];
    AAFilterCell *cell = [[AAFilterCell alloc] initLightStyleWithAthletic:aa];
    
    return cell;
}

/*
#pragma mark - Table View Delegate
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
//    [super tableView:tableView didSelectRowAtIndexPath:indexPath];
//    [self dismiss];
}
 */

@end
