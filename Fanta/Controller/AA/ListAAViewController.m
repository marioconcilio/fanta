//
//  ListAAViewController.m
//  Fanta
//
//  Created by Mario Concilio on 7/18/16.
//  Copyright © 2016 Mario Concilio. All rights reserved.
//

#import "ListAAViewController.h"
#import "AAFilterCell.h"
#import "VOAthletic.h"
#import "VOUser.h"
#import "AthleticService.h"
#import "Helper.h"
#import "TextStyles.h"
#import "BLMultiColorLoader+DefaultLoader.h"

static NSString *const kSelectAll    = @"Selecionar Todas";
static NSString *const kDeselectAll  = @"Limpar Seleção";

@interface ListAAViewController ()

@property (nonatomic, strong) ASTableView *tableView;
@property (nonatomic, strong) UISearchController *searchController;
@property (nonatomic, strong) NSArray *searchResults;
@property (nonatomic, strong) NSArray<VOAthletic *> *dataSource;
@property (nonatomic, strong) NSHashTable<VOAthletic *> *athleticsHashTable;
//@property (nonatomic, strong) BLMultiColorLoader *loader;

@end

@implementation ListAAViewController

+ (UINavigationController *)viewInNavigation {
    ListAAViewController *controller = [[self alloc] init];
    __autoreleasing UINavigationController *nav = [[UINavigationController alloc] initWithRootViewController:controller];
    
    return nav;
}

#pragma mark - Init
- (instancetype)init {
    self = [super init];
    if (self) {
        [self commonInit];
    }
    
    return self;
}

- (instancetype)initWithCoder:(NSCoder *)aDecoder {
    self = [super initWithCoder:aDecoder];
    if (self) {
        [self commonInit];
    }
    
    return self;
}

- (void)commonInit {
    _tableView = [[ASTableView alloc] initWithFrame:CGRectZero style:UITableViewStylePlain asyncDataFetching:YES];
    _filtering = YES;
}

#pragma mark - View Life Cycle
- (void)viewDidLoad {
    [super viewDidLoad];
    [self.view addSubview:_tableView];
    
    BLMultiColorLoader *loader = [BLMultiColorLoader defaultLoader];
    loader.center = CGPointMake(UIKeyWindow().center.x,
                                CGRectGetMaxY(self.navigationController.navigationBar.frame) + CGRectGetHeight(loader.frame));
    [self.view addSubview:loader];
    [loader startAnimation];
    
//    self.dataSource = [Helper loadAAList];
//    self.dataSource = [[NSArray alloc] init];
    AthleticService *service = [[AthleticService alloc] init];
    [service listAllAthleticsWithCompletion:^(NSArray<VOAthletic *> *list, NSError *error) {
        [loader stopAnimation];
        [loader removeFromSuperview];
        
        if (error) {
            DDLogVerbose(@"list athletics HAS NOT BEEN populated");
            //TODO: show no internet connection
        }
        else {
            DDLogVerbose(@"list athletics HAS BEEN populated");
            self.dataSource = list;
            [_tableView reloadData];
            
            /*
             * hash table
             */
            self.athleticsHashTable = [NSHashTable weakObjectsHashTable];
            for (VOAthletic *aa in self.dataSource) {
                [self.athleticsHashTable addObject:aa];
            }
            
            DDLogVerbose(@"hashtable filled w/ %ld AAs", self.athleticsHashTable.allObjects.count);
            
            // if is not filtering and there is no selected row
            // select automatically the first one (the user AA)
            if (![self isFiltering] && [[_tableView indexPathsForSelectedRows] count] == 0) {
                NSIndexPath *indexPath = [NSIndexPath indexPathForRow:0 inSection:1];
//            [_tableView selectRowAtIndexPath:indexPath animated:NO scrollPosition:UITableViewScrollPositionNone];
            }
        }
    }];
    
    _tableView.allowsMultipleSelection = _filtering;
    _tableView.tableFooterView = [[UIView alloc] initWithFrame:CGRectZero];
    _tableView.separatorColor = [UIColor customDarkBackground];
    _tableView.backgroundColor = UIColorFromHEX(0x222222);
    
    _tableView.asyncDataSource = self;
    _tableView.asyncDelegate = self;
    
    /*
     * search bar
     */
    self.searchController = [[UISearchController alloc] initWithSearchResultsController:nil];
    self.searchController.searchResultsUpdater = self;
    self.searchController.dimsBackgroundDuringPresentation = NO;
    self.searchController.searchBar.searchBarStyle = UISearchBarStyleMinimal;
    self.searchController.searchBar.barStyle = UIBarStyleBlack;
    
    self.searchController.hidesNavigationBarDuringPresentation = NO;
    self.definesPresentationContext = YES;
    [self.searchController.searchBar sizeToFit];
    self.navigationItem.titleView = self.searchController.searchBar;
    
    /*
     *  navbar
     */
    if ([self isModalPresentation]) {
        self.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"fnt_cancel"]
                                                                                 style:UIBarButtonItemStylePlain
                                                                                target:self
                                                                                action:@selector(dismiss)];
    }
    else if ([self isPushPresentation]) {
        self.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"fnt_navbar_back"]
                                                                                 style:UIBarButtonItemStylePlain
                                                                                target:self
                                                                                action:@selector(pop)];
    }
}

- (void)viewWillLayoutSubviews {
    [super viewWillLayoutSubviews];
    _tableView.frame = self.view.bounds;
}

- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
    
    if ([self isFiltering]) {
        [self.delegate listAA:self didSelectListAA:[self.athleticsHashTable allObjects]];
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)dealloc {
    [self.searchController loadViewIfNeeded];
}

#pragma mark - Dismiss Methods
- (void)dismiss {
    [self dismissViewControllerAnimated:YES completion:NULL];
}

- (void)pop {
    [self.navigationController popViewControllerAnimated:YES];
}

#pragma mark - Helper Methods
- (VOAthletic *)athleticAtIndexPath:(NSIndexPath *)indexPath {
    VOAthletic *aa;
    if ([self.searchController isActive] && self.searchResults.count > 0) {
        aa = self.searchResults[indexPath.row];
    }
    else {
        aa = self.dataSource[indexPath.row];
    }
    
    return aa;
}

- (void)setFiltering:(BOOL)filtering {
    _filtering = filtering;
    _tableView.allowsMultipleSelection = filtering;
    [_tableView reloadData];
}

- (void)updateSelectAllCell {
    NSIndexPath *indexPath = [NSIndexPath indexPathForRow:0 inSection:0];
    ASTextCellNode *cell = (ASTextCellNode *) [_tableView nodeForRowAtIndexPath:indexPath];
    
    if (self.athleticsHashTable.allObjects.count == self.dataSource.count) {
        [_tableView selectRowAtIndexPath:indexPath animated:NO scrollPosition:UITableViewScrollPositionNone];
        cell.text = kDeselectAll;
    }
    else {
        [_tableView deselectRowAtIndexPath:indexPath animated:NO];
        cell.text = kSelectAll;
    }
}

#pragma mark - Table View Data Source
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 2;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    // section selecionar todas
    if (section == 0)
        return [self isFiltering];
    
    // section AAs
    if ([self.searchController isActive]) {
        return self.searchResults.count;
    }

    return self.dataSource.count;
}

- (ASCellNode *)tableView:(ASTableView *)tableView nodeForRowAtIndexPath:(NSIndexPath *)indexPath {
    // select all cell
    if (indexPath.section == 0) {
        ASTextCellNode *cell = [[ASTextCellNode alloc] initWithAttributes:[TextStyles chooseAASelectAllDarkStyle]
                                                                   insets:UIEdgeInsetsMake(10.0, 10.0, 10.0, 10.0)];
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        
        return cell;
    }
    
    // VOAthletic filter cell
    VOAthletic *aa = [self athleticAtIndexPath:indexPath];
    AAFilterCell *cell = [[AAFilterCell alloc] initDarkStyleWithAthletic:aa];
    
    return cell;
}

#pragma mark - Table View Delegate
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    // selecting all cells
    if (indexPath.section == 0) {
        GCDOnBackground(^{
            for (VOAthletic *aa in self.dataSource) {
                if (![self.athleticsHashTable containsObject:aa]) {
                    [self.athleticsHashTable addObject:aa];
                }
            }
            
            GCDOnMain(^{
                ASTextCellNode *cell = (ASTextCellNode *) [_tableView nodeForRowAtIndexPath:indexPath];
                cell.text = kDeselectAll;
                
                NSArray *indexPaths = [tableView indexPathsForVisibleRows];
                for (NSIndexPath *path in indexPaths) {
                    ASCellNode *node = [_tableView nodeForRowAtIndexPath:path];
                    node.selected = YES;
                    [tableView selectRowAtIndexPath:path animated:NO scrollPosition:UITableViewScrollPositionNone];
                }
            });
        });
    }
    // select each row
    else {
        VOAthletic *aa = [self athleticAtIndexPath:indexPath];
        if ([self isFiltering]) {
            [self.athleticsHashTable addObject:aa];
            [self updateSelectAllCell];
        }
        else {
            [self.delegate listAA:self didSelectAA:aa];
        }
    }
}

- (NSIndexPath *)tableView:(UITableView *)tableView willDeselectRowAtIndexPath:(NSIndexPath *)indexPath {
    // prevent from deselecting last row
    // delegate cannot return 0 AA
    if (indexPath.section == 1 && self.athleticsHashTable.allObjects.count == 1)
        return nil;
    
    return indexPath;
}

- (void)tableView:(UITableView *)tableView didDeselectRowAtIndexPath:(NSIndexPath *)indexPath {
    // deselecting all cells
    if (indexPath.section == 0) {
        GCDOnBackground(^{
            [self.athleticsHashTable removeAllObjects];
            
            // add user AA again to prevent dismissal with no selection
            // user AA is aways the first one
            [self.athleticsHashTable addObject:self.dataSource[0]];
            
            GCDOnMain(^{
                ASTextCellNode *cell = (ASTextCellNode *) [_tableView nodeForRowAtIndexPath:indexPath];
                cell.text = kSelectAll;
                
                NSArray *paths = [tableView indexPathsForVisibleRows];
                
                // skip the 0 index
                // will not deselect user AA
                for (NSIndexPath *path in paths) {
                    if (path.row == 0) continue;
                    
                    ASCellNode *node = [_tableView nodeForRowAtIndexPath:path];
                    node.selected = NO;
                    [tableView deselectRowAtIndexPath:path animated:NO];
                }
            });
        });
    }
    // deselect each row
    else {
        VOAthletic *aa = [self athleticAtIndexPath:indexPath];
        if ([self isFiltering]) {
            [self.athleticsHashTable removeObject:aa];
            [self updateSelectAllCell];
        }
    }
}

- (void)tableView:(ASTableView *)tableView willDisplayNodeForRowAtIndexPath:(NSIndexPath *)indexPath {
    // if tableview does not support multiple selection
    if (![self isFiltering])
        return;
    
    if (indexPath.section == 0) {
        ASTextCellNode *node = (ASTextCellNode *) [tableView nodeForRowAtIndexPath:indexPath];
        
        if (self.athleticsHashTable.allObjects.count == self.dataSource.count) {
            [tableView selectRowAtIndexPath:indexPath animated:NO scrollPosition:UITableViewScrollPositionNone];
            node.text = kDeselectAll;
        }
        else {
            [tableView deselectRowAtIndexPath:indexPath animated:NO];
            node.text = kSelectAll;
        }
    }
    else {
        ASCellNode *node = [tableView nodeForRowAtIndexPath:indexPath];
        VOAthletic *aa = [self athleticAtIndexPath:indexPath];
        
        if ([self.athleticsHashTable containsObject:aa]) {
            node.selected = YES;
            [tableView selectRowAtIndexPath:indexPath animated:NO scrollPosition:UITableViewScrollPositionNone];
        }
        else {
            node.selected = NO;
            [tableView deselectRowAtIndexPath:indexPath animated:NO];
        }
    }
}

#pragma mark - Search Results Updating
- (void)updateSearchResultsForSearchController:(UISearchController *)searchController {
    self.searchResults = nil;
    
    NSPredicate *resultPredicate = [NSPredicate predicateWithFormat:@"(name contains[c] %@) OR (athleticName contains[c] %@)",
                                    [searchController.searchBar text],
                                    [searchController.searchBar text]];
    self.searchResults = [self.dataSource filteredArrayUsingPredicate:resultPredicate];
    
    [_tableView reloadData];
}

@end
