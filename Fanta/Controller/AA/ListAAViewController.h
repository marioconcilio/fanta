//
//  ListAAViewController.h
//  Fanta
//
//  Created by Mario Concilio on 7/18/16.
//  Copyright © 2016 Mario Concilio. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <AsyncDisplayKit.h>

@class ListAAViewController, VOAthletic;
@protocol ListAADelegate <NSObject>

- (void)listAA:(ListAAViewController *)vc didSelectAA:(VOAthletic *)athletic;
- (void)listAA:(ListAAViewController *)vc didSelectListAA:(NSArray<VOAthletic *> *)list;

@end

@interface ListAAViewController : UIViewController <ASTableDataSource, ASTableDelegate, UISearchResultsUpdating>

//@property (nonatomic, strong) NSArray<VOAthletic *> *listAA;
@property (nonatomic, strong, readonly) ASTableView *tableView;
@property (nonatomic, strong, readonly) UISearchController *searchController;
@property (nonatomic, strong, readonly) NSArray *searchResults;
@property (nonatomic, strong, readonly) NSArray<VOAthletic *> *dataSource; // list AA from db

@property (nonatomic, weak) id<ListAADelegate> delegate;
@property (nonatomic, assign, getter=isFiltering) BOOL filtering;
@property (nonatomic, assign, getter=isModalPresentation) BOOL modalPresentation;
@property (nonatomic, assign, getter=isPushPresentation) BOOL pushPresentation;

+ (UINavigationController *)viewInNavigation;
- (VOAthletic *)athleticAtIndexPath:(NSIndexPath *)indexPath;
- (void)dismiss;
- (void)pop;

@end
