//
//  AAFilterCell.h
//  Fanta
//
//  Created by Mario Concilio on 7/18/16.
//  Copyright © 2016 Mario Concilio. All rights reserved.
//

#import "AACell.h"

@interface AAFilterCell : AACell

@property (nonatomic, weak, readonly) ASImageNode *checkNode;

- (instancetype)init UNAVAILABLE_ATTRIBUTE;

@end
