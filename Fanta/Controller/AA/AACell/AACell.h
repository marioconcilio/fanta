//
//  AACell.h
//  Fanta
//
//  Created by Mario Concilio on 5/13/16.
//  Copyright © 2016 Mario Concilio. All rights reserved.
//

#import <AsyncDisplayKit/AsyncDisplayKit.h>

@class VOAthletic;
@interface AACell : ASCellNode

@property (nonatomic, weak, readonly) ASNetworkImageNode *thumbNode;
@property (nonatomic, weak, readonly) ASTextNode *nameNode;

- (instancetype)init UNAVAILABLE_ATTRIBUTE;
- (instancetype)initLightStyleWithAthletic:(VOAthletic *)athletic;
- (instancetype)initDarkStyleWithAthletic:(VOAthletic *)athletic;

@end
