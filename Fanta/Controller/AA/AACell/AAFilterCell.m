//
//  AAFilterCell.m
//  Fanta
//
//  Created by Mario Concilio on 7/18/16.
//  Copyright © 2016 Mario Concilio. All rights reserved.
//

#import "AAFilterCell.h"
#import "Constants.h"

#import <AsyncDisplayKit.h>

@interface AAFilterCell ()

@property (nonatomic, weak) ASImageNode *checkNode;

@end

@implementation AAFilterCell

#pragma mark - Init
- (instancetype)initLightStyleWithAthletic:(VOAthletic *)athletic {
    self = [super initLightStyleWithAthletic:athletic];
    if (self) {
        [self commonInit];
    }
    
    return self;
}

- (instancetype)initDarkStyleWithAthletic:(VOAthletic *)athletic {
    self = [super initDarkStyleWithAthletic:athletic];
    if (self) {
        [self commonInit];
    }
    
    return self;
}

- (void)commonInit {
    ASImageNode *check = [[ASImageNode alloc] init];
    check.image = [UIImage imageNamed:@"fnt_check"];
    check.preferredFrameSize = CGSizeMake(15.0, 15.0);
    check.layerBacked = YES;
    check.alpha = 0.0;
    _checkNode = check;
    [self addSubnode:_checkNode];
}

#pragma mark - Layout
- (ASLayoutSpec *)layoutSpecThatFits:(ASSizeRange)constrainedSize {
    // flexible spacer between name and check image
    ASLayoutSpec *spacer = [[ASLayoutSpec alloc] init];
    spacer.flexGrow = YES;
    
    // horizontal stack for thumbnail, name and check image
    ASStackLayoutSpec *stack = [ASStackLayoutSpec stackLayoutSpecWithDirection:ASStackLayoutDirectionHorizontal
                                                                       spacing:kAAPadding
                                                                justifyContent:ASStackLayoutJustifyContentStart
                                                                    alignItems:ASStackLayoutAlignItemsCenter
                                                                      children:@[self.thumbNode, self.nameNode, spacer, self.checkNode]];
    stack.alignSelf = ASStackLayoutAlignSelfStretch;
    
    return [ASInsetLayoutSpec insetLayoutSpecWithInsets:UIEdgeInsetsMake(10, 10, 10, 10) child:stack];
}

- (void)setSelected:(BOOL)selected {
    [super setSelected:selected];
    if (selected) {
        _checkNode.alpha = 1.0;
    }
    else {
        _checkNode.alpha = 0.0;
    }
}

@end
