//
//  AACell.m
//  Fanta
//
//  Created by Mario Concilio on 5/13/16.
//  Copyright © 2016 Mario Concilio. All rights reserved.
//

#import "AACell.h"
#import "VOAthletic.h"
#import "Constants.h"
#import "TextStyles.h"

#import <AsyncDisplayKit/ASDisplayNode+Subclasses.h>
#import <WebASDKImageManager.h>
#import <POP.h>

@interface AACell () <ASNetworkImageNodeDelegate>

@property (nonatomic, weak) ASNetworkImageNode *thumbNode;
@property (nonatomic, weak) ASTextNode *nameNode;

@end

@implementation AACell

#pragma mark - Init
- (instancetype)initDarkStyleWithAthletic:(VOAthletic *)athletic {
    self = [super init];
    if (self) {
        [self commonInit:athletic textAttributes:[TextStyles chooseAANameDarkStyle]];
    }
    
    return self;
}

- (instancetype)initLightStyleWithAthletic:(VOAthletic *)athletic {
    self = [super init];
    if (self) {
        [self commonInit:athletic textAttributes:[TextStyles chooseAANameLightStyle]];
    }
    
    return self;
}

- (void)commonInit:(VOAthletic *)athletic textAttributes:(NSDictionary *)att {
    /*
     *  thumbnail
     */
    ASNetworkImageNode *thumb = [[ASNetworkImageNode alloc] initWithWebImage];
    thumb.URL = [NSURL URLWithString:athletic.thumbnail];
    thumb.preferredFrameSize = kCommentCellThumbSize;
    thumb.cornerRadius = kCommentCellThumbWidth / 2;
    thumb.clipsToBounds = YES;
    thumb.borderWidth = 0.5;
    thumb.borderColor = UIColorFromRGB(151, 151, 151).CGColor;
    thumb.delegate = self;
    thumb.layerBacked = YES;
    _thumbNode = thumb;
    [self addSubnode:_thumbNode];
    
    /*
     *  name
     */
    ASTextNode *name = [[ASTextNode alloc] init];
    name.maximumNumberOfLines = 0;
    name.layerBacked = YES;
    name.attributedString = [[NSAttributedString alloc] initWithString:[athletic fullName]
                                                            attributes:att];
    _nameNode = name;
    [self addSubnode:_nameNode];
    
    self.selectionStyle = UITableViewCellSelectionStyleNone;
}

#pragma mark - Layout
- (ASLayoutSpec *)layoutSpecThatFits:(ASSizeRange)constrainedSize {
    // horizontal stack for thumbnail and name
    ASStackLayoutSpec *stack = [ASStackLayoutSpec stackLayoutSpecWithDirection:ASStackLayoutDirectionHorizontal
                                                                       spacing:kAAPadding
                                                                justifyContent:ASStackLayoutJustifyContentStart
                                                                    alignItems:ASStackLayoutAlignItemsCenter
                                                                      children:@[_thumbNode, _nameNode]];
    stack.alignSelf = ASStackLayoutAlignSelfStretch;
    
    return [ASInsetLayoutSpec insetLayoutSpecWithInsets:UIEdgeInsetsMake(10, 10, 10, 10) child:stack];
}

#pragma mark - ASNetworkImage Delegate
- (void)imageNode:(ASNetworkImageNode *)imageNode didLoadImage:(UIImage *)image {
    if (image) {
        POPBasicAnimation *anim = [POPBasicAnimation animationWithPropertyNamed:kPOPViewAlpha];
        anim.fromValue = @(0.0);
        anim.toValue = @(1.0);
        [imageNode pop_removeAllAnimations];
        [imageNode pop_addAnimation:anim forKey:@"fadeIn"];
    }
}

@end
