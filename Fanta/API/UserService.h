//
//  UserService.h
//  Fanta
//
//  Created by Mario Concilio on 8/8/16.
//  Copyright © 2016 Mario Concilio. All rights reserved.
//

#import "APIClient.h"
#import <Foundation/Foundation.h>

@class VOUser;
@interface UserService : NSObject

#pragma mark - Class Methods
/**
 *  Saves the User into NSUserDefaults under key kProfileInfo
 *
 *  @param user The user to be saved.
 */
+ (void)saveUser:(VOUser *)user;

/**
 *  Loads the User stored in NSUserDefaults under kProfileInfo
 *
 *  @return The user saved.
 */
+ (VOUser *)loadUser;

/**
 *  Saves the User token into keychain under kUserToken account.
 *
 *  @param token The token to be saved.
 */
+ (void)saveToken:(NSString *)token;

/**
 *  Loads the User token from the keychain under kUserToken account.
 *
 *  @return The token as string.
 */
+ (NSString *)loadToken;

#pragma mark - Instance Methods
/**
 *  Register new User using email and password.
 *  If the email already exists, the registration will fail.
 *  If the registration is successful, the User object and its token is saved automatically.
 *
 *  POST {host}/student
 *
 *  @param name         The user name.
 *  @param email        The user email.
 *  @param password     The user password.
 *  @param completion   A block called once the operation is completed.
 *                      If the operation succeeded, the user parameter is set with its token already saved.
 *                      In case of error, error parameter is set with the error.
 */
- (void)registerUser:(NSString *)name
               email:(NSString *)email
            password:(NSString *)password
          completion:(void (^)(VOUser *user, NSError *error))completionBlock;

/**
 *  Register User using info provided by Facebook, like FacebookID and profile image.
 *  If the email already exists, the old informations will be overwitten, no error will be thrown.
 *
 *  POST {host}/student/facebook
 *
 *  @param user         The User to be registred. At least should have name, email and facebookID.
 *  @param image        Profile image provided by Facebook (400x400).
 *  @param thumbnail    Thumbnail generated from profile image (128x128).
 *  @param completion   A block called once the operation is completed.
 *                      In case of error, error parameter is set with the error.
 */
- (void)registerUserFromFacebook:(VOUser *)user
                           image:(UIImage *)image
                       thumbnail:(UIImage *)thumbnail
                      completion:(APIClientDefaultBlock)completionBlock;

/**
 *  Login user into server, using its email and password.
 *  If the process is successful, the User object and its token is saved automatically.
 *  If it is unsuccessful, e.g. wrong user/password combination, the process fails.
 *
 *  POST {host}/login
 *
 *  @param email        The user email.
 *  @param password     The user password.
 *  @param completion   A block called once the operation is completed.
 *                      If the operation succeeded, the user info and its token is saved.
 *                      In case of error, error parameter is set with the error.
 */
- (void)login:(NSString *)email
     password:(NSString *)password
   completion:(APIClientDefaultBlock)completionBlock;

/**
 *  Logout user, cleaning the NSUserDefaults and the user token from keychain.
 */
- (void)logout;

/**
 *  Edit User info, like name, password and profile image (and its thumbnail).
 *  None of the parameters are mandatory.
 *  If editing only one info, other parameters can be nil.
 *  
 *  POST {host}/student/{user_id}
 *
 *  @param name         The new user name. Can be nil, if editing other info.
 *  @param password     The new password. Can be nil, if editing other info.
 *  @param image        The new profile image. Can be nil, if editing other info.
 *  @param thumbnail    The profile image thumbnail. Can be nil, if editing other info. Cannot be nil if image is not nil.
 *  @param completion   A block called once the operation is completed.
 *                      In case of error, error parameter is set with the error.
 */
- (void)editUserName:(NSString *)name
            password:(NSString *)password
               image:(UIImage *)image
           thumbnail:(UIImage *)thumbnail
          completion:(APIClientDefaultBlock)completionBlock;

/**
 *  Fetch the database and return the User from given user_id.
 *
 *  GET {host}/student/{user_id}
 *
 *  @param userId       The user_id to fetch the database.
 *  @param completion   A block called once the operation is completed.
 *                      If the operation succeeded, the user parameter is set with user from server.
 *                      In case of error, error parameter is set with the error.
 */
- (void)getUserFromId:(NSUInteger)userId
           completion:(void (^)(VOUser *user, NSError *error))completionBlock;

@end
