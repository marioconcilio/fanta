//
//  CommentService.h
//  Fanta
//
//  Created by Mario Concilio on 8/8/16.
//  Copyright © 2016 Mario Concilio. All rights reserved.
//

#import "APIClient.h"
#import <Foundation/Foundation.h>

@class VOPost, VOComment;
@interface CommentService : NSObject

- (void)getCommentsFromPost:(VOPost *)post
                 completion:(void (^)(NSArray<VOComment *> *comments, NSError *error))completionBlock;

- (void)createComment:(NSString *)content
             fromPost:(VOPost *)post
           completion:(void (^)(VOComment *comment, NSError *error))completionBlock;

- (void)deleteComment:(VOComment *)comment
           completion:(APIClientDefaultBlock)completionBlock;

- (void)likeComment:(VOComment *)comment
         completion:(APIClientDefaultBlock)completionBlock;

- (void)dislikeComment:(VOComment *)comment
            completion:(APIClientDefaultBlock)completionBlock;

- (void)reportComment:(VOComment *)comment
           completion:(APIClientDefaultBlock)completionBlock;

@end
