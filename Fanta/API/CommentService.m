//
//  CommentService.m
//  Fanta
//
//  Created by Mario Concilio on 8/8/16.
//  Copyright © 2016 Mario Concilio. All rights reserved.
//

#import "CommentService.h"
#import "VOPost.h"
#import "VOComment.h"

@implementation CommentService

- (void)getCommentsFromPost:(VOPost *)post completion:(void (^)(NSArray<VOComment *> *comments, NSError *error))completionBlock {
    NSString *url = [NSString stringWithFormat:@"%@/feed/topic/%lu", FNTBaseURL, [post postID]];
    [[APIClient sharedInstance] GET:url
                         parameters:nil
                               auth:YES
                         completion:^(NSDictionary *response, NSError *error) {
                             if (!error) {
                                 NSArray *array = response[@"comment_list"];
                                 //TODO: cordeiro nao ta devolvendo comment_id
                                 NSMutableArray<VOComment *> *comments = [NSMutableArray arrayWithCapacity:array.count];
                                 
                                 for (NSDictionary *dict in array) {
                                     @autoreleasepool {
                                         VOComment *comment = [[VOComment alloc] initWithDictionary:dict];
                                         [comments addObject:comment];
                                     }
                                 }
                                 
                                 completionBlock([comments copy], nil);
                             }
                             else {
                                 completionBlock(nil, error);
                             }
                         }];
}

- (void)createComment:(NSString *)content fromPost:(VOPost *)post completion:(void (^)(VOComment *comment, NSError *error))completionBlock {
    NSString *url = [NSString stringWithFormat:@"%@/feed/topic/%ld/create_comment", FNTBaseURL, post.postID];
    NSDictionary *params = @{@"contents": content};
    [[APIClient sharedInstance] POST:url
                          parameters:params
                                auth:YES
                          completion:^(NSDictionary *response, NSError *error) {
                              if (!error) {
                                  //TODO: CORDEIRO: me devolve o comment completo, pelo menos o created_at
                                  VOComment *comment = [[VOComment alloc] init];
                                  comment.commentID = [response[@"comment_id"] unsignedIntegerValue];
                                  comment.content = content;
                                  comment.likes = 0;
                                  comment.dislikes = 0;
                                  comment.createdAt = [NSDate date];
                                  
                                  completionBlock(comment, nil);
                              }
                              else {
                                  completionBlock(nil, error);
                              }
                          }];
}

- (void)deleteComment:(VOComment *)comment completion:(APIClientDefaultBlock)completionBlock {
    NSString *url = [NSString stringWithFormat:@"%@/feed/topic/comment/%lu/delete", FNTBaseURL, comment.commentID];
    [[APIClient sharedInstance] POST:url
                          parameters:nil
                                auth:YES
                          completion:^(NSDictionary *response, NSError *error) {
                              completionBlock(error);
                          }];
}

- (void)likeComment:(VOComment *)comment completion:(APIClientDefaultBlock)completionBlock {
    NSString *url = [NSString stringWithFormat:@"%@/feed/topic/comment/%lu/like", FNTBaseURL, comment.commentID];
    [[APIClient sharedInstance] POST:url
                          parameters:nil
                                auth:YES
                          completion:^(NSDictionary *response, NSError *error) {
                              completionBlock(error);
                          }];
}

- (void)dislikeComment:(VOComment *)comment completion:(APIClientDefaultBlock)completionBlock {
    NSString *url = [NSString stringWithFormat:@"%@/feed/topic/comment/%lu/dislike", FNTBaseURL, comment.commentID];
    [[APIClient sharedInstance] POST:url
                          parameters:nil
                                auth:YES
                          completion:^(NSDictionary *response, NSError *error) {
                              completionBlock(error);
                          }];
}

- (void)reportComment:(VOComment *)comment completion:(APIClientDefaultBlock)completionBlock {
    NSString *url = [NSString stringWithFormat:@"%@/feed/topic/comment/%lu/report", FNTBaseURL, comment.commentID];
    [[APIClient sharedInstance] POST:url
                          parameters:nil
                                auth:YES
                          completion:^(NSDictionary *response, NSError *error) {
                              completionBlock(error);
                          }];
}

@end
