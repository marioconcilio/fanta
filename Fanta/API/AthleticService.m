//
//  AthleticService.m
//  Fanta
//
//  Created by Mario Concilio on 8/10/16.
//  Copyright © 2016 Mario Concilio. All rights reserved.
//

#import "AthleticService.h"
#import "VOAthletic.h"
#import <OHHTTPStubs.h>
#import <OHHTTPStubsResponse+JSON.h>

@implementation AthleticService

- (instancetype)init {
    self = [super init];
    if (self) {
        [OHHTTPStubs stubRequestsPassingTest:^BOOL(NSURLRequest * _Nonnull request) {
//            return [request.URL.host isEqualToString:FNTHost] && [request.URL.path isEqualToString:@"/list_athletics"];
            return NO;
        } withStubResponse:^OHHTTPStubsResponse * _Nonnull(NSURLRequest * _Nonnull request) {
//            NSError* notConnectedError = [NSError errorWithDomain:NSURLErrorDomain code:kCFURLErrorNotConnectedToInternet userInfo:nil];
//            return [OHHTTPStubsResponse responseWithError:notConnectedError];
            

            NSDictionary *aa1 = @{@"athletic_name":     @"A.A.A. da USP",
                                  @"facebook_id":       @(1144853668872928),
                                  @"id":                @(1),
                                  @"name":              @"USP"};
            
            NSDictionary *aa2 = @{@"athletic_name":     @"A.A.A. da USJT",
                                  @"facebook_id":       @(1144853668872928),
                                  @"id":                @(2),
                                  @"name":              @"USJT"};
            
            NSDictionary *dict = @{@"done":             @(1),
                                   @"error":            @(NO),
                                   @"status_code":      @(200),
                                   @"athletic_list":    @[aa1, aa2]};
            
            return [[OHHTTPStubsResponse responseWithJSONObject:dict statusCode:200 headers:nil] requestTime:5.0 responseTime:1.0];
            
        }];
    }
    
    return self;
}

- (void)listAllAthleticsWithCompletion:(void (^)(NSArray<VOAthletic *> *, NSError *))completionBlock {
    NSString *url = [NSString stringWithFormat:@"%@/list_athletics", FNTBaseURL];
    [[APIClient sharedInstance] GET:url
                         parameters:nil
                               auth:NO
                         completion:^(NSDictionary *response, NSError *error) {
                             if (!error) {
                                 NSArray *array = response[@"athletic_list"];
                                 NSMutableArray<VOAthletic *> *athletics = [NSMutableArray array];
                                 
                                 for (NSDictionary *dict in array) {
                                     @autoreleasepool {
                                         VOAthletic *aa = [[VOAthletic alloc] initWithDictionary:dict];
                                         [athletics addObject:aa];
                                     }
                                 }
                                 
                                 completionBlock([athletics copy], nil);
                             }
                             else {
                                 completionBlock(nil, error);
                             }
                             
                             [OHHTTPStubs removeAllStubs];
                         }];
}

@end
