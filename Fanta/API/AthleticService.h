//
//  AthleticService.h
//  Fanta
//
//  Created by Mario Concilio on 8/10/16.
//  Copyright © 2016 Mario Concilio. All rights reserved.
//

#import "APIClient.h"
#import <Foundation/Foundation.h>

@class VOAthletic;
@interface AthleticService : NSObject

/**
 *  List all athletics indexed on server database
 *
 *  GET {host}/list_athletics
 *
 *  @param completion   A block called once the operation is completed.
 *                      If the operation succeeded, the list parameter is set with list of all athletics on server.
 *                      In case of error, error parameter is set with the error.
 */
- (void)listAllAthleticsWithCompletion:(void (^)(NSArray<VOAthletic *> *list, NSError *error))completionBlock;

@end
