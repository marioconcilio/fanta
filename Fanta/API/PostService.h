//
//  PostService.h
//  Fanta
//
//  Created by Mario Concilio on 8/8/16.
//  Copyright © 2016 Mario Concilio. All rights reserved.
//

#import "APIClient.h"
#import <Foundation/Foundation.h>

typedef NS_ENUM(NSInteger, APIPostType) {
    APIPostTypeAthletic = 1,
    APIPostTypeStudent,
};

@class VOPost;
@interface PostService : NSObject

/*
- (void)getPosts:(APIPostType)post
  fromUniversity:(NSUInteger)university
         success:(void (^)(NSMutableArray *array))successBlock
         failure:(void (^)(NSError *error))errorBlock;
*/
 
- (void)createPost:(NSString *)content
              type:(APIPostType)type
    fromUniversity:(NSUInteger)university
        completion:(void (^)(VOPost *post, NSError *error))completionBlock;

- (void)deletePost:(VOPost *)post
        completion:(APIClientDefaultBlock)completionBlock;

- (void)likePost:(VOPost *)post
      completion:(APIClientDefaultBlock)completionBlock;

- (void)dislikePost:(VOPost *)post
         completion:(APIClientDefaultBlock)completionBlock;

- (void)reportPost:(VOPost *)post
        completion:(APIClientDefaultBlock)completionBlock;

@end
