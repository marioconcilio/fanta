//
//  UserService.m
//  Fanta
//
//  Created by Mario Concilio on 8/8/16.
//  Copyright © 2016 Mario Concilio. All rights reserved.
//

#import "UserService.h"
#import "APIClient.h"
#import "VOUser.h"
#import "VOAthletic.h"
#import "Helper.h"
#import "Constants.h"
#import "UserService.h"

#import <OHHTTPStubs.h>
#import <OHHTTPStubsResponse+JSON.h>
#import <SAMKeychain.h>

@implementation UserService

#pragma mark - Class Methods
+ (void)saveUser:(VOUser *)user {
    [Helper saveCustomObject:user forKey:kProfileInfo];
}

+ (VOUser *)loadUser {
    NSData *myEncodedObject = [NSUserDefaults objectForKey:kProfileInfo];
    VOUser* obj = (VOUser *)[NSKeyedUnarchiver unarchiveObjectWithData:myEncodedObject];
    
    return obj;
}

+ (void)saveToken:(NSString *)token {
    [SAMKeychain setPassword:token forService:kBundleID account:kUserToken];
}

+ (NSString *)loadToken {
    return [SAMKeychain passwordForService:kBundleID account:kUserToken];
}

/*
-(void)saveUserInfo:(VOUser *)user {
    
//    [NSUserDefaults setObject:[user userHash] forKey:kUserToken];
//    [NSUserDefaults synchronize];
    
    [Helper saveToken:user];
}
 */

- (instancetype)init {
    self = [super init];
    if (self) {
        
        [OHHTTPStubs stubRequestsPassingTest:^BOOL(NSURLRequest * _Nonnull request) {
//            return [request.URL.host isEqualToString:FNTHost] && [request.URL.path isEqualToString:@"/student/facebook"];
            return NO;
        } withStubResponse:^OHHTTPStubsResponse * _Nonnull(NSURLRequest * _Nonnull request) {
            NSError* notConnectedError = [NSError errorWithDomain:NSURLErrorDomain code:kCFURLErrorNotConnectedToInternet userInfo:nil];
            return [OHHTTPStubsResponse responseWithError:notConnectedError];
            
            /*
            NSDictionary *dict = @{@"error":                @(NO),
                                   @"student_id":           @(1),
                                   @"student_hash":         @"c05e01a821775ae7e6f95321f6394d8b5dd7b22b66a9833468fa0b69",
                                   @"student_image":        @"https://scontent.xx.fbcdn.net/t31.0-1/c160.0.960.960/p960x960/170234_1584167214050_7209609_o.jpg",
                                   @"student_thumbnail":    @"https://scontent.xx.fbcdn.net/v/t1.0-1/c22.0.130.130/p130x130/180526_1584167214050_7209609_n.jpg?oh=2c88e4160fd2db2962b260af193f26de&oe=584259E2"};
            return [[OHHTTPStubsResponse responseWithJSONObject:dict statusCode:200 headers:nil] responseTime:OHHTTPStubsDownloadSpeed3G];
             */
            
        }];
    }
    
    return self;
}

#pragma mark - Instance Methods
- (void)registerUser:(NSString *)name email:(NSString *)email password:(NSString *)password completion:(void (^)(VOUser *user, NSError *error))completionBlock {
    NSString *url = [NSString stringWithFormat:@"%@/student", FNTBaseURL];
    NSDictionary *params = @{@"name":       name,
                             @"email":      email,
                             @"password":   password};
    
    [[APIClient sharedInstance] POST:url
                          parameters:params
                                auth:NO
                          completion:^(NSDictionary *response, NSError *error) {
                              if (!error) {
                                  VOUser *user = [[VOUser alloc] initWithDictionary:response];
                                  user.valid = NO;
//                               [self saveUserInfo:user];
//                               [UserService saveUser:user];
                                  [UserService saveToken:response[@"student_hash"]];
                                  completionBlock(user, nil);
                              }
                              else {
                                  completionBlock(nil, error);
                              }
                          }];
}

- (void)registerUserFromFacebook:(VOUser *)user image:(UIImage *)image thumbnail:(UIImage *)thumbnail completion:(APIClientDefaultBlock)completionBlock {
    NSString *url = [NSString stringWithFormat:@"%@/student/facebook", FNTBaseURL];
    
    NSData *imageData = UIImageJPEGRepresentation(image, 1.0);
    NSData *thumbData = UIImageJPEGRepresentation(thumbnail, 1.0);
    
    NSDictionary *parameters = @{@"name":           [user name],
                                 @"email":          [user email],
                                 @"facebook_id":    [NSString stringWithFormat:@"%ld", [user facebookID]]};
    
    [[APIClient sharedInstance] POST:url
                          parameters:parameters
                                auth:NO
                               image:imageData
                           thumbnail:thumbData
                          completion:^(NSDictionary *response, NSError *error) {
                              if (!error) {
                                  [user setUserID:[response[@"student_id"] unsignedIntegerValue]];
                                  [user setImage:response[@"image"]];
                                  [user setThumbnail:response[@"thumbnail"]];
                                  [UserService saveUser:user];
                                  [UserService saveToken:response[@"student_hash"]];
                                  completionBlock(nil);
                              }
                              else {
                                  completionBlock(error);
                              }
                              
                              [OHHTTPStubs removeAllStubs];
                          }];
}

- (void)login:(NSString *)email password:(NSString *)password completion:(APIClientDefaultBlock)completionBlock {
    NSString *url = [NSString stringWithFormat:@"%@/login", FNTBaseURL];
    NSDictionary *params = @{@"password":   password,
                             @"email":      email};
    
    [[APIClient sharedInstance] POST:url
                          parameters:params
                                auth:NO
                          completion:^(NSDictionary *response, NSError *error) {
                              if (!error) {
                                  VOUser *user = [[VOUser alloc] initWithDictionary:response];
                                  
                                  //???: set athletic from list
                                  VOAthletic *aa = [[VOAthletic alloc] init];
                                  aa.name = @"E-lefante";
                                  user.athletic = aa;
                                  
//                               [self saveUserInfo:user];
                                  [UserService saveUser:user];
                                  [UserService saveToken:response[@"student_hash"]];
                                  completionBlock(nil);
                              }
                              else {
                                  completionBlock(error);
                              }
                          }];
}

- (void)logout {
    [SAMKeychain deletePasswordForService:kBundleID account:kUserToken];
    
//    [NSUserDefaults setObject:nil forKey:kUserToken];
    [NSUserDefaults setObject:nil forKey:kProfileInfo];
    [NSUserDefaults synchronize];
}

- (void)editUserName:(NSString *)name password:(NSString *)password image:(UIImage *)image thumbnail:(UIImage *)thumbnail completion:(APIClientDefaultBlock)completionBlock {
    VOUser *user = [UserService loadUser];
    NSString *url = [NSString stringWithFormat:@"%@/student/%ld", FNTBaseURL, [user userID]];
    
    NSMutableDictionary *params = [NSMutableDictionary dictionary];
    if (name)
        [params setValue:name forKey:@"name"];
    if (password)
        [params setValue:password forKey:@"password"];
    
    NSData *imageData = nil;
    NSData *thumbData = nil;
    
    if (image) {
        imageData = UIImageJPEGRepresentation(image, 1.0);
        
        if (thumbnail) {
            thumbData = UIImageJPEGRepresentation(thumbnail, 1.0);
        }
        else {
            NSError *error = [NSError errorWithDomain:kBundleID
                                                 code:1001
                                             userInfo:@{NSLocalizedDescriptionKey: @"thumbnail cannot be nil if image is not nil"}];
            completionBlock(error);
        }
    }
    
    [[APIClient sharedInstance] POST:url
                          parameters:params
                                auth:YES
                          completion:^(NSDictionary *response, NSError *error) {
                              completionBlock(error);
                          }];
}

- (void)getUserFromId:(NSUInteger)userId completion:(void (^)(VOUser *user, NSError *error))completionBlock {
    NSString *url = [NSString stringWithFormat:@"%@/student/%lu", FNTBaseURL, userId];
    
    [[APIClient sharedInstance] GET:url
                         parameters:nil
                               auth:YES
                         completion:^(NSDictionary *response, NSError *error) {
                             if (!error) {
                                 VOUser *user = [[VOUser alloc] initWithDictionary:response];
                                 completionBlock(user, nil);
                             }
                             else {
                                 completionBlock(nil, error);
                             }
                         }];
}

@end
