//
//  PostService.m
//  Fanta
//
//  Created by Mario Concilio on 8/8/16.
//  Copyright © 2016 Mario Concilio. All rights reserved.
//

#import "PostService.h"
#import "VOPost.h"

@implementation PostService

- (void)createPost:(NSString *)content type:(APIPostType)type fromUniversity:(NSUInteger)university completion:(void (^)(VOPost *post, NSError *error))completionBlock {
    NSString *url = [NSString stringWithFormat:@"%@/feed/topic/create", FNTBaseURL];
    NSDictionary *parameters = @{@"university_id":  @(university),
                                 @"contents":       content,
                                 @"feed_id":        @(type)};
    
    [[APIClient sharedInstance] POST:url
                          parameters:parameters
                                auth:YES
                          completion:^(NSDictionary *response, NSError *error) {
                              if (!error) {
                                  VOPost *post = [[VOPost alloc] init];
                                  post.content = content;
                                  post.postID = [response[@"topic_id"] integerValue];
                                  completionBlock(post, nil);
                              }
                              else {
                                  completionBlock(nil, error);
                              }
                          }];
}

- (void)deletePost:(VOPost *)post completion:(APIClientDefaultBlock)completionBlock {
    NSString *url = [NSString stringWithFormat:@"%@/feed/topic/%lu/delete", FNTBaseURL, (unsigned long)post.postID];
    [[APIClient sharedInstance] POST:url
                          parameters:nil
                                auth:YES
                          completion:^(NSDictionary *response, NSError *error) {
                              completionBlock(error);
                          }];
}

- (void)likePost:(VOPost *)post completion:(APIClientDefaultBlock)completionBlock {
    NSString *url = [NSString stringWithFormat:@"%@/feed/topic/%lu/like", FNTBaseURL, (unsigned long)post.postID];
    [[APIClient sharedInstance] POST:url
                          parameters:nil
                                auth:YES
                          completion:^(NSDictionary *response, NSError *error) {
                              completionBlock(error);
                          }];
}

- (void)dislikePost:(VOPost *)post completion:(APIClientDefaultBlock)completionBlock {
    NSString *url = [NSString stringWithFormat:@"%@/feed/topic/%lu/dislike", FNTBaseURL, (unsigned long)post.postID];
    [[APIClient sharedInstance] POST:url
                          parameters:nil
                                auth:YES
                          completion:^(NSDictionary *response, NSError *error) {
                              completionBlock(error);
                          }];
}

- (void)reportPost:(VOPost *)post completion:(APIClientDefaultBlock)completionBlock {
    NSString *url = [NSString stringWithFormat:@"%@/feed/topic/%lu/report", FNTBaseURL, (unsigned long)post.postID];
    [[APIClient sharedInstance] POST:url
                          parameters:nil
                                auth:YES
                          completion:^(NSDictionary *response, NSError *error) {
                              completionBlock(error);
                          }];
}

@end
