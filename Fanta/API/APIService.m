//
//  APIService.m
//  Fanta
//
//  Created by Mario Concilio on 28/04/15.
//  Copyright (c) 2015 Mario Concilio. All rights reserved.
//

#import "APIService.h"
#import "Constants.h"
#import "VOUser.h"
#import "VOPost.h"
#import "VOComment.h"
#import "VOAthletic.h"
#import "Helper.h"
#import "UserService.h"

#import <AFNetworking.h>

#ifdef LOCALHOST
NSString *const kBaseURL = @"http://192.168.1.32:8000";
#else
NSString *const kBaseURL = @"http://52.67.112.198";
#endif

@implementation APIService

#pragma mark - Shared Instance
+ (instancetype)sharedInstance {
    static APIService *shared = nil;
    static dispatch_once_t onceToken;
    
    dispatch_once(&onceToken, ^{
        shared = [[self alloc] init];
    });
    
    return shared;
}

+ (void)imageFromURL:(NSURL *)url callback:(void(^)(UIImage *image, NSError *error))callback {
    NSURLRequest *request = [NSURLRequest requestWithURL:url];
    AFHTTPRequestOperation *requestOperation = [[AFHTTPRequestOperation alloc] initWithRequest:request];
    requestOperation.responseSerializer = [AFImageResponseSerializer serializer];
    [requestOperation setCompletionBlockWithSuccess:^(AFHTTPRequestOperation *operation, id responseObject) {
        callback(responseObject, nil);
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        callback(nil, error);
    }];
    
    [requestOperation start];
}

#pragma mark - Private Methods
- (AFHTTPSessionManager *)managerWithAuth:(BOOL)auth {
    AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
    manager.responseSerializer = [AFJSONResponseSerializer serializer];
//    manager.requestSerializer = [AFJSONRequestSerializer serializer];
    
    [manager.requestSerializer setValue:@"application/json" forHTTPHeaderField:@"Accept"];
    [manager.requestSerializer setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    
    if (auth) {
        [manager.requestSerializer setAuthorizationHeaderFieldWithUsername:@"x"
                                                                  password:[NSUserDefaults objectForKey:kUserToken]];
    }
    
    return manager;
}

-(void)saveUserInfo:(VOUser *)user {
    [Helper saveCustomObject:user forKey:kProfileInfo];
//    [NSUserDefaults setObject:@(user.userID) forKey:kUserToken];
//    [NSUserDefaults setObject:[user userHash] forKey:kUserToken];
    
    [NSUserDefaults synchronize];
}

- (void)defaultPOSTWithPost:(VOPost *)post type:(NSString *)type callback:(APIDefaultCallback)callback {
    NSString *url = [NSString stringWithFormat:@"%@/feed/topic/%ld/%@", kBaseURL, post.postID, type];
    AFHTTPSessionManager *manager = [self managerWithAuth:YES];
    
    [manager POST:url
       parameters:nil
          success:^(NSURLSessionDataTask *task, id responseObject) {
              NSDictionary *response = (NSDictionary *)responseObject;
              if (![[response valueForKey:@"error"] boolValue]) {
                  callback(nil);
              }
              else {
                  NSInteger statusCode = [[response valueForKey:@"status_code"] integerValue];
                  NSDictionary *info = @{NSLocalizedDescriptionKey: [response valueForKey:@"message"]};
                  callback([NSError errorWithDomain:@"Fanta" code:statusCode userInfo:info]);
              }
          }
          failure:^(NSURLSessionDataTask *task, NSError *error) {
              callback(error);
              DDLogWarn(@"%@", error.debugDescription);
          }];
}

- (void)defaultPOSTWithComment:(VOComment *)comment type:(NSString *)type callback:(APIDefaultCallback)callback {
    NSString *url = [NSString stringWithFormat:@"%@/feed/topic/%ld/%@", kBaseURL, comment.commentID, type];
    AFHTTPSessionManager *manager = [self managerWithAuth:YES];
    
    [manager POST:url
       parameters:nil
          success:^(NSURLSessionDataTask *task, id responseObject) {
              NSDictionary *response = (NSDictionary *)responseObject;
              if (![[response valueForKey:@"error"] boolValue]) {
                  callback(nil);
              }
              else {
                  NSInteger statusCode = [[response valueForKey:@"status_code"] integerValue];
                  NSDictionary *info = @{NSLocalizedDescriptionKey: [response valueForKey:@"message"]};
                  callback([NSError errorWithDomain:@"Fanta" code:statusCode userInfo:info]);
              }
          }
          failure:^(NSURLSessionDataTask *task, NSError *error) {
              callback(error);
              DDLogWarn(@"%@", error.debugDescription);
          }];
}

#pragma mark - Posts
/*
- (void)getPosts:(APIPostType)post fromUniversity:(NSUInteger)university success:(void (^)(NSMutableArray *array))successBlock failure:(void (^)(NSError *error))errorBlock {
    NSString *str = post == APIPostTypeAthletic ? @"athletic" : @"regular";
    
    NSString *url = [NSString stringWithFormat:@"%@/feed/%@/%ld", kBaseURL, str, university];
    AFHTTPSessionManager *manager = [self managerWithAuth:YES];
    
    [manager GET:url
      parameters:nil
         success:^(NSURLSessionDataTask *task, id responseObject) {
             NSDictionary *response = (NSDictionary *)responseObject;
             if (![[response valueForKey:@"error"] boolValue]) {
                 NSArray *array = responseObject[@"topic_list"];
                 NSMutableArray *posts = [NSMutableArray arrayWithCapacity:array.count];
                 
                 for (NSDictionary *dict in array) {
                     @autoreleasepool {
                         VOPost *post = [[VOPost alloc] initWithDictionary:dict];
                         [posts addObject:post];
                     }
                 }
                 
                 successBlock(posts);
             }
             else {
                 NSInteger statusCode = [[response valueForKey:@"status_code"] integerValue];
                 NSDictionary *info = @{NSLocalizedDescriptionKey: [response valueForKey:@"message"]};
                 errorBlock([NSError errorWithDomain:@"Fanta" code:statusCode userInfo:info]);
             }
         }
         failure:^(NSURLSessionDataTask *task, NSError *error) {
             errorBlock(error);
             DDLogWarn(@"%@", error.debugDescription);
         }];
}

- (void)createPost:(NSString *)content type:(APIPostType)type fromUniversity:(NSUInteger)university callback:(APIErrorCallback)callback {
    NSDictionary *parameters = @{@"university_id": @(university),
                                 @"contents": content,
                                 @"feed_id": @(type)};
    NSString *url = [NSString stringWithFormat:@"%@/feed/topic/create", kBaseURL];
    AFHTTPSessionManager *manager = [self managerWithAuth:YES];
    
    [manager POST:url
       parameters:parameters
          success:^(NSURLSessionDataTask *task, id responseObject) {
              NSDictionary *response = (NSDictionary *)responseObject;
              if (![[response valueForKey:@"error"] boolValue]) {
                  callback(nil);
              }
              else {
                  NSInteger statusCode = [[response valueForKey:@"status_code"] integerValue];
                  NSDictionary *info = @{NSLocalizedDescriptionKey: [response valueForKey:@"message"]};
                  callback([NSError errorWithDomain:@"Fanta" code:statusCode userInfo:info]);
              }
          }
          failure:^(NSURLSessionDataTask *task, NSError *error) {
              callback(error);
              DDLogWarn(@"%@", error.debugDescription);
          }];
}
 */

- (void)likePost:(VOPost *)post callback:(APIErrorCallback)callback {
    [self defaultPOSTWithPost:post type:@"like" callback:callback];
}

- (void)dislikePost:(VOPost *)post callback:(APIErrorCallback)callback {
    [self defaultPOSTWithPost:post type:@"dislike" callback:callback];
}

- (void)reportPost:(VOPost *)post callback:(APIErrorCallback)callback {
    [self defaultPOSTWithPost:post type:@"report" callback:callback];
}

#pragma mark - Comments
- (void)getCommentsFromPost:(VOPost *)post success:(void (^)(NSMutableArray *array))successBlock failure:(void (^)(NSError *error))errorBlock {
    NSString *url = [NSString stringWithFormat:@"%@/feed/topic/%lu", kBaseURL, post.postID];
    AFHTTPSessionManager *manager = [self managerWithAuth:YES];
    
    [manager GET:url
      parameters:nil
         success:^(NSURLSessionDataTask *task, id responseObject) {
             NSDictionary *response = (NSDictionary *)responseObject;
             if (![[response valueForKey:@"error"] boolValue]) {
                 NSArray *array = responseObject[@"comment_list"];
                 
#warning CORDEIRO nao ta devolvendo id dos comment
                 NSMutableArray *comments = [NSMutableArray arrayWithCapacity:array.count];
                 for (NSDictionary *dict in array) {
                     @autoreleasepool {
                         VOComment *comment = [[VOComment alloc] initWithDictionary:dict];
                         [comments addObject:comment];
                     }
                 }
                 
                 successBlock(comments);
             }
             else {
                 NSInteger statusCode = [[response valueForKey:@"status_code"] integerValue];
                 NSDictionary *info = @{NSLocalizedDescriptionKey: [response valueForKey:@"message"]};
                 errorBlock([NSError errorWithDomain:@"Fanta" code:statusCode userInfo:info]);
             }
         }
         failure:^(NSURLSessionDataTask *task, NSError *error) {
             errorBlock(error);
             DDLogWarn(@"%@", error.debugDescription);
         }];
}

- (void)createComment:(NSString *)comment fromPost:(VOPost *)post callback:(APIErrorCallback)callback {
    NSDictionary *parameters = @{@"contents": comment};
    NSString *url = [NSString stringWithFormat:@"%@/feed/topic/%ld/create_comment", kBaseURL, post.postID];
    AFHTTPSessionManager *manager = [self managerWithAuth:YES];
    
    [manager POST:url
       parameters:parameters
          success:^(NSURLSessionDataTask *task, id responseObject) {
              NSDictionary *response = (NSDictionary *)responseObject;
              if (![[response valueForKey:@"error"] boolValue]) {
                  callback(nil);
              }
              else {
                  NSInteger statusCode = [[response valueForKey:@"status_code"] integerValue];
                  NSDictionary *info = @{NSLocalizedDescriptionKey: [response valueForKey:@"message"]};
                  callback([NSError errorWithDomain:@"Fanta" code:statusCode userInfo:info]);
              }
          }
          failure:^(NSURLSessionDataTask *task, NSError *error) {
              callback(error);
              DDLogWarn(@"%@", error.debugDescription);
          }];
}

- (void)likeComment:(VOComment *)comment callback:(APIErrorCallback)callback {
    [self defaultPOSTWithComment:comment type:@"like" callback:callback];
}

- (void)dislikeComment:(VOComment *)comment callback:(APIErrorCallback)callback {
    [self defaultPOSTWithComment:comment type:@"dislike" callback:callback];
}

- (void)reportComment:(VOComment *)comment callback:(APIErrorCallback)callback {
    [self defaultPOSTWithComment:comment type:@"report" callback:callback];
}

#pragma mark - Login & Register User Flow
- (void)registerUser:(NSString *)name email:(NSString *)email password:(NSString *)password block:(void (^)(NSError *error))block {
    NSDictionary *parameters = @{@"name": name,
                                 @"email": email,
                                 @"password": password};
    
    NSString *url = [NSString stringWithFormat:@"%@/student", kBaseURL];
    AFHTTPSessionManager *manager = [self managerWithAuth:NO];
    
    [manager POST:url
       parameters:parameters
          success:^(NSURLSessionDataTask *operation, id responseObject) {
              NSDictionary *response = (NSDictionary *)responseObject;
              if (![[response valueForKey:@"error"] boolValue]) {
                  VOUser *user = [[VOUser alloc] initWithDictionary:response];
                  user.valid = NO;
                  [self saveUserInfo:user];
                  block(nil);
              }
              else {
                  NSInteger statusCode = [[response valueForKey:@"status_code"] integerValue];
                  NSDictionary *info = @{NSLocalizedDescriptionKey: [response valueForKey:@"message"]};
                  block([NSError errorWithDomain:@"Fanta" code:statusCode userInfo:info]);
              }
          }
          failure:^(NSURLSessionDataTask *operation, NSError *error) {
              block(error);
              DDLogWarn(@"%@", error.debugDescription);
          }];
}

- (void)registerUserFromFacebook:(NSString *)name email:(NSString *)email image:(UIImage *)image thumbnail:(UIImage *)thumbnail facebookID:(NSUInteger)facebookID block:(void (^)(NSError *))block {
    NSData *imageData = UIImageJPEGRepresentation(image, 1.0);
    NSData *thumbData = UIImageJPEGRepresentation(thumbnail, 1.0);
    
    NSDictionary *parameters = @{@"name": name,
                                 @"email": email,
                                 @"facebook_id": [NSString stringWithFormat:@"%ld", facebookID]};
    
    NSString *url = [NSString stringWithFormat:@"%@/student/facebook", kBaseURL];
    AFHTTPSessionManager *manager = [self managerWithAuth:NO];
    
    [manager POST:url
       parameters:parameters
constructingBodyWithBlock:^(id<AFMultipartFormData> formData) {
    [formData appendPartWithFileData:imageData
                                name:@"image"
                            fileName:@"profile.jpg"
                            mimeType:@"image/jpg"];
    
    [formData appendPartWithFileData:thumbData
                                name:@"thumbnail"
                            fileName:@"thumb.jpg"
                            mimeType:@"image/jpg"];
}
          success:^(NSURLSessionDataTask *task, id responseObject) {
              NSDictionary *response = (NSDictionary *)responseObject;
              if (![[response valueForKey:@"error"] boolValue]) {
//                  VOUser *user = [[VOUser alloc] initWithDictionary:response];
//                  user.valid = NO;
//                  [self saveUserInfo:user];
                  block(nil);
              }
              else {
                  NSInteger statusCode = [[response valueForKey:@"status_code"] integerValue];
                  NSDictionary *info = @{NSLocalizedDescriptionKey: [response valueForKey:@"message"]};
                  block([NSError errorWithDomain:@"Fanta" code:statusCode userInfo:info]);
              }
          }
          failure:^(NSURLSessionDataTask *task, NSError *error) {
              block(error);
              DDLogWarn(@"%@", error.debugDescription);
          }];

}

- (void)login:(NSString *)email password:(NSString *)password block:(void (^)(NSError *error))block {
    NSDictionary *parameters = @{@"password": password,
                                 @"email": email};
    
    NSString *url = [NSString stringWithFormat:@"%@/login", kBaseURL];
    AFHTTPSessionManager *manager = [self managerWithAuth:NO];
    
    [manager POST: url
       parameters:parameters
          success:^(NSURLSessionDataTask *operation, id responseObject) {
              NSDictionary *response = (NSDictionary *)responseObject;
              if (![[response valueForKey: @"error"] boolValue]) {
                  VOUser *user = [[VOUser alloc] initWithDictionary:response];
                  [self saveUserInfo:user];
//                  [self saveUserInfo:response];
                  block(nil);
              }
              else {
                  NSInteger statusCode = [[response valueForKey:@"status_code"] integerValue];
                  NSDictionary *info = @{NSLocalizedDescriptionKey: [response valueForKey:@"message"]};
                  block([NSError errorWithDomain:@"Fanta" code:statusCode userInfo:info]);
              }
          }
          failure:^(NSURLSessionDataTask *operation, NSError *error) {
              block(error);
              DDLogWarn(@"%@", error.debugDescription);
          }];
}

- (void)logout {
    [NSUserDefaults setObject:nil forKey:kUserToken];
    [NSUserDefaults setObject:nil forKey:kProfileInfo];
    [NSUserDefaults synchronize];
}

- (void)listAllAthletics:(void (^)(NSArray<VOAthletic *> *))successBlock failure:(void (^)(NSError *))errorBlock {
    NSString *url = [NSString stringWithFormat:@"%@/list_athletics", kBaseURL];
    AFHTTPSessionManager *manager = [self managerWithAuth:NO];
    
    [manager GET:url
       parameters:nil
          success:^(NSURLSessionDataTask *operation, id responseObject) {
              NSDictionary *response = (NSDictionary *)responseObject;
              if (![[response valueForKey: @"error"] boolValue]) {
                  //TODO: popular com VOAthletic
              }
              else {
                  NSInteger statusCode = [[response valueForKey:@"status_code"] integerValue];
                  NSDictionary *info = @{NSLocalizedDescriptionKey: [response valueForKey:@"message"]};
                  errorBlock([NSError errorWithDomain:@"Fanta" code:statusCode userInfo:info]);
              }
          }
          failure:^(NSURLSessionDataTask *operation, NSError *error) {
              errorBlock(error);
              DDLogWarn(@"%@", error.debugDescription);
          }];
}

/*
- (void)saveFacebookUserInfo:(NSDictionary *)dict {
#warning enviar tokenAccess pro cordeiro e resgatar o hash
    NSString *hash = dict[@"id"];
    
    NSString *name = [NSString stringWithFormat:@"%@ %@", dict[@"first_name"], dict[@"last_name"]];
    NSString *email = dict[@"email"];
    NSDictionary *userDict;
    
    // if facebook profile image is silhouette
    // then do not save thumbnail, the app will generate its own placeholder
    if ([dict[@"picture"][@"data"][@"is_silhouette"] boolValue]) {
        userDict = @{@"student_name": name,
                     @"student_email": email,
                     @"student_hash": hash};
    }
    else {
        NSString *thumb = dict[@"picture"][@"data"][@"url"];
        userDict = @{@"student_name": name,
                     @"student_email": email,
                     @"student_thumbnail": thumb,
                     @"student_hash": hash};
    }
    
    [self saveUserInfo:userDict];
}
 */

#pragma mark - Edit User Info
- (void)editUserName:(NSString *)name password:(NSString *)password image:(UIImage *)image thumbnail:(UIImage *)thumbnail block:(void (^)(NSError *))block {
    NSUInteger userID = [[UserService loadUser] userID];
    
    NSData *imageData = UIImageJPEGRepresentation(image, 1.0);
    NSData *thumbData = UIImageJPEGRepresentation(thumbnail, 1.0);
    
    NSMutableDictionary *params = [NSMutableDictionary dictionary];
    if (name) [params setValue:name forKey:@"name"];
    if (password) [params setValue:password forKey:@"password"];
    
    // athletic_list: athleticID
    
    NSString *url = [NSString stringWithFormat:@"%@/student/%ld", kBaseURL, userID];
    AFHTTPSessionManager *manager = [self managerWithAuth:YES];
    [manager POST:url
       parameters:params
constructingBodyWithBlock:^(id<AFMultipartFormData> formData) {
    if (image && thumbnail) {
        [formData appendPartWithFileData:imageData
                                    name:@"image"
                                fileName:@"profile.jpg"
                                mimeType:@"image/jpg"];
        
        [formData appendPartWithFileData:thumbData
                                    name:@"thumbnail"
                                fileName:@"thumb.jpg"
                                mimeType:@"image/jpg"];
    }
}
          success:^(NSURLSessionDataTask *task, id responseObject) {
              block(nil);
          }
          failure:^(NSURLSessionDataTask *task, NSError *error) {
              block(error);
              DDLogWarn(@"%@", error.debugDescription);
          }];
}

#pragma mark - Friendship
- (void)getFriendsOfUser:(VOUser *)user success:(void (^)(NSArray *array))successBlock failure:(void (^)(NSError *error))errorBlock {
    //!!! CORDEIRO criar opcao get friendship qqr user
    NSString *url = [NSString stringWithFormat:@"%@/student/friendship/get", kBaseURL];
    AFHTTPSessionManager *manager = [self managerWithAuth:YES];
    
    [manager GET:url
      parameters:nil
         success:^(NSURLSessionDataTask *task, id responseObject) {
             NSDictionary *response = (NSDictionary *)responseObject;
             if (![[response valueForKey:@"error"] boolValue]) {
                 NSArray *array = responseObject[@"friend_list"];
                 successBlock(array);
             }
             else {
                 NSInteger statusCode = [[response valueForKey:@"status_code"] integerValue];
                 NSDictionary *info = @{NSLocalizedDescriptionKey: [response valueForKey:@"message"]};
                 errorBlock([NSError errorWithDomain:@"Fanta" code:statusCode userInfo:info]);
             }
         }
         failure:^(NSURLSessionDataTask *task, NSError *error) {
             errorBlock(error);
             DDLogWarn(@"%@", error.debugDescription);
         }];
}

- (void)createFriendshipWithUser:(VOUser *)user callback:(APIErrorCallback)callback {
    NSDictionary *params = @{@"student_id": @(user.userID)};
    NSString *url = [NSString stringWithFormat:@"%@/student/friendship/create", kBaseURL];
    AFHTTPSessionManager *manager = [self managerWithAuth:YES];
    
    [manager POST:url
       parameters:params
          success:^(NSURLSessionDataTask * _Nonnull task, id  _Nonnull responseObject) {
              NSDictionary *response = (NSDictionary *)responseObject;
              if (![[response valueForKey:@"error"] boolValue]) {
                  callback(nil);
              }
              else {
                  NSInteger statusCode = [[response valueForKey:@"status_code"] integerValue];
                  NSDictionary *info = @{NSLocalizedDescriptionKey: [response valueForKey:@"message"]};
                  callback([NSError errorWithDomain:@"Fanta" code:statusCode userInfo:info]);
              }
          }
          failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
              callback(error);
              DDLogWarn(@"%@", error.debugDescription);
          }];
}

- (void)deleteFriendshipWithUser:(VOUser *)user callback:(APIErrorCallback)callback {
    NSDictionary *params = @{@"student_id": @(user.userID)};
    NSString *url = [NSString stringWithFormat:@"%@/student/friendship/delete", kBaseURL];
    AFHTTPSessionManager *manager = [self managerWithAuth:YES];
    
    [manager POST:url
       parameters:params
          success:^(NSURLSessionDataTask * _Nonnull task, id  _Nonnull responseObject) {
              NSDictionary *response = (NSDictionary *)responseObject;
              if (![[response valueForKey:@"error"] boolValue]) {
                  callback(nil);
              }
              else {
                  NSInteger statusCode = [[response valueForKey:@"status_code"] integerValue];
                  NSDictionary *info = @{NSLocalizedDescriptionKey: [response valueForKey:@"message"]};
                  callback([NSError errorWithDomain:@"Fanta" code:statusCode userInfo:info]);
              }
          }
          failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
              callback(error);
              DDLogWarn(@"%@", error.debugDescription);
          }];
}

#pragma mark - Winks
- (void)sendWinkForUser:(VOUser *)user callback:(APIErrorCallback)callback {
    NSDictionary *params = @{@"student_id": @(user.userID)};
    NSString *url = [NSString stringWithFormat:@"%@/student/wink/create", kBaseURL];
    AFHTTPSessionManager *manager = [self managerWithAuth:YES];
    
    [manager POST:url
       parameters:params
          success:^(NSURLSessionDataTask * _Nonnull task, id  _Nonnull responseObject) {
              NSDictionary *response = (NSDictionary *)responseObject;
              if (![[response valueForKey:@"error"] boolValue]) {
                  callback(nil);
              }
              else {
                  NSInteger statusCode = [[response valueForKey:@"status_code"] integerValue];
                  NSDictionary *info = @{NSLocalizedDescriptionKey: [response valueForKey:@"message"]};
                  callback([NSError errorWithDomain:@"Fanta" code:statusCode userInfo:info]);
              }
          }
          failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
              callback(error);
              DDLogWarn(@"%@", error.debugDescription);
          }];
}

- (void)getWinksWithSuccess:(void (^)(NSArray *))successBlock failure:(void (^)(NSError *))errorBlock {
    NSString *url = [NSString stringWithFormat:@"%@/student/wink/get", kBaseURL];
    AFHTTPSessionManager *manager = [self managerWithAuth:YES];
    
    [manager GET:url
      parameters:nil
         success:^(NSURLSessionDataTask * _Nonnull task, id  _Nonnull responseObject) {
             NSDictionary *response = (NSDictionary *)responseObject;
             if (![[response valueForKey:@"error"] boolValue]) {
                 NSArray *array = response[@"wink_list"];
                 successBlock(array);
             }
             else {
                 NSInteger statusCode = [[response valueForKey:@"status_code"] integerValue];
                 NSDictionary *info = @{NSLocalizedDescriptionKey: [response valueForKey:@"message"]};
                 errorBlock([NSError errorWithDomain:@"Fanta" code:statusCode userInfo:info]);
             }
         }
         failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
             errorBlock(error);
             DDLogWarn(@"%@", error.debugDescription);
         }];
}

#pragma mark - Messages
- (void)sendMessageToUser:(VOUser *)user content:(NSString *)content callback:(APIErrorCallback)callback {
    NSDictionary *params = @{@"student_id": @(user.userID),
                             @"contents": content};
    NSString *url = [NSString stringWithFormat:@"%@/student/message/create", kBaseURL];
    AFHTTPSessionManager *manager = [self managerWithAuth:YES];
    
    [manager POST:url
       parameters:params
          success:^(NSURLSessionDataTask * _Nonnull task, id  _Nonnull responseObject) {
              NSDictionary *response = (NSDictionary *)responseObject;
              if (![[response valueForKey:@"error"] boolValue]) {
                  callback(nil);
              }
              else {
                  NSInteger statusCode = [[response valueForKey:@"status_code"] integerValue];
                  NSDictionary *info = @{NSLocalizedDescriptionKey: [response valueForKey:@"message"]};
                  callback([NSError errorWithDomain:@"Fanta" code:statusCode userInfo:info]);
              }
          }
          failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
              callback(error);
              DDLogWarn(@"%@", error.debugDescription);
          }];
}

- (void)getMessagesFromUser:(VOUser *)user success:(void (^)(NSArray *))successBlock failure:(void (^)(NSError *))errorBlock {
    NSString *url = [NSString stringWithFormat:@"%@/student/message/get/%lu", kBaseURL, user.userID];
    AFHTTPSessionManager *manager = [self managerWithAuth:YES];
    
    [manager GET:url
      parameters:nil
         success:^(NSURLSessionDataTask * _Nonnull task, id  _Nonnull responseObject) {
             NSDictionary *response = (NSDictionary *)responseObject;
             if (![[response valueForKey:@"error"] boolValue]) {
                 NSArray *array = response[@"message_list"];
                 successBlock(array);
             }
             else {
                 NSInteger statusCode = [[response valueForKey:@"status_code"] integerValue];
                 NSDictionary *info = @{NSLocalizedDescriptionKey: [response valueForKey:@"message"]};
                 errorBlock([NSError errorWithDomain:@"Fanta" code:statusCode userInfo:info]);
             }
         }
         failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
             errorBlock(error);
             DDLogWarn(@"%@", error.debugDescription);
         }];
}

- (void)listMessagesWithSuccess:(void (^)(NSArray *))successBlock failure:(void (^)(NSError *))errorBlock {
    NSString *url = [NSString stringWithFormat:@"%@/student/list_conversations", kBaseURL];
    AFHTTPSessionManager *manager = [self managerWithAuth:YES];
    
    [manager GET:url
      parameters:nil
         success:^(NSURLSessionDataTask * _Nonnull task, id  _Nonnull responseObject) {
             NSDictionary *response = (NSDictionary *)responseObject;
             if (![[response valueForKey:@"error"] boolValue]) {
                 NSArray *array = response[@"conversation_list"];
                 successBlock(array);
             }
             else {
                 NSInteger statusCode = [[response valueForKey:@"status_code"] integerValue];
                 NSDictionary *info = @{NSLocalizedDescriptionKey: [response valueForKey:@"message"]};
                 errorBlock([NSError errorWithDomain:@"Fanta" code:statusCode userInfo:info]);
             }
         }
         failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
             errorBlock(error);
             DDLogWarn(@"%@", error.debugDescription);
         }];
}

- (void)createGroupWithUsers:(NSArray<VOUser *> *)users name:(NSString *)name success:(void (^)(NSArray *))successBlock failure:(void (^)(NSError *))errorBlock {
    NSMutableString *list = [NSMutableString string];
    for (VOUser *user in users) {
        [list appendFormat:@"%ld,", user.userID];
    }
    
    NSDictionary *params = @{@"name": name,
                             @"student_list": list};
    
    NSString *url = [NSString stringWithFormat:@"%@/student/group/create", kBaseURL];
    AFHTTPSessionManager *manager = [self managerWithAuth:YES];
    
    [manager POST:url
       parameters:params
          success:^(NSURLSessionDataTask *task, id responseObject) {
              NSDictionary *response = (NSDictionary *)responseObject;
              if (![[response valueForKey:@"error"] boolValue]) {
                  
                  errorBlock(nil);
              }
              else {
                  NSInteger statusCode = [[response valueForKey:@"status_code"] integerValue];
                  NSDictionary *info = @{NSLocalizedDescriptionKey: [response valueForKey:@"message"]};
                  errorBlock([NSError errorWithDomain:@"Fanta" code:statusCode userInfo:info]);
              }
          }
          failure:^(NSURLSessionDataTask *task, NSError *error) {
              errorBlock(error);
              DDLogWarn(@"%@", error.debugDescription);
          }];
}

- (void)sendMessageToGroup:(NSUInteger)groupID content:(NSString *)content callback:(APIErrorCallback)callback {
    NSDictionary *params = @{@"contents": content};
    
    NSString *url = [NSString stringWithFormat:@"%@/student/group/%ld/send_message", kBaseURL, groupID];
    AFHTTPSessionManager *manager = [self managerWithAuth:YES];
    
    [manager POST:url
       parameters:params
          success:^(NSURLSessionDataTask *task, id responseObject) {
              NSDictionary *response = (NSDictionary *)responseObject;
              if (![[response valueForKey:@"error"] boolValue]) {
                  callback(nil);
              }
              else {
                  NSInteger statusCode = [[response valueForKey:@"status_code"] integerValue];
                  NSDictionary *info = @{NSLocalizedDescriptionKey: [response valueForKey:@"message"]};
                  callback([NSError errorWithDomain:@"Fanta" code:statusCode userInfo:info]);
              }
          }
          failure:^(NSURLSessionDataTask *task, NSError *error) {
              callback(error);
              DDLogWarn(@"%@", error.debugDescription);
          }];
}

- (void)listMessagesFromGroup:(NSUInteger)groupID withSuccess:(void (^)(NSArray *))successBlock failure:(void (^)(NSError *))errorBlock {
    NSString *url = [NSString stringWithFormat:@"%@/student/group/%ld/get_message", kBaseURL, groupID];
    AFHTTPSessionManager *manager = [self managerWithAuth:YES];
    
    [manager GET:url
      parameters:nil
         success:^(NSURLSessionDataTask *task, id responseObject) {
             NSDictionary *response = (NSDictionary *)responseObject;
             if (![[response valueForKey:@"error"] boolValue]) {
                 successBlock(@[]);
                 errorBlock(nil);
             }
             else {
                 NSInteger statusCode = [[response valueForKey:@"status_code"] integerValue];
                 NSDictionary *info = @{NSLocalizedDescriptionKey: [response valueForKey:@"message"]};
                 errorBlock([NSError errorWithDomain:@"Fanta" code:statusCode userInfo:info]);
             }
         }
         failure:^(NSURLSessionDataTask *task, NSError *error) {
             errorBlock(error);
             DDLogWarn(@"%@", error.debugDescription);
         }];
}

#pragma mark - User
- (void)getUserFromId:(NSUInteger)userId success:(void (^)(VOUser *))successBlock failure:(void (^)(NSError *))errorBlock {
    //!!! CORDEIRO devolver student_thumbnail tambem
    NSString *url = [NSString stringWithFormat:@"%@/student/%lu", kBaseURL, userId];
    AFHTTPSessionManager *manager = [self managerWithAuth:YES];
    
    [manager GET:url
      parameters:nil
         success:^(NSURLSessionDataTask * _Nonnull task, id  _Nonnull responseObject) {
             NSDictionary *response = (NSDictionary *)responseObject;
             if (![[response valueForKey:@"error"] boolValue]) {
                 VOUser *user = [[VOUser alloc] initWithDictionary:response];
                 successBlock(user);
             }
             else {
                 NSInteger statusCode = [[response valueForKey:@"status_code"] integerValue];
                 NSDictionary *info = @{NSLocalizedDescriptionKey: [response valueForKey:@"message"]};
                 errorBlock([NSError errorWithDomain:@"Fanta" code:statusCode userInfo:info]);
             }
         }
         failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
             errorBlock(error);
             DDLogWarn(@"%@", error.debugDescription);
         }];
}

@end
