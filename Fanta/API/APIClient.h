//
//  APIClient.h
//  Fanta
//
//  Created by Mario Concilio on 8/8/16.
//  Copyright © 2016 Mario Concilio. All rights reserved.
//

#import <AFNetworking/AFNetworking.h>

typedef void(^APISuccessCallback)(NSDictionary *response);
typedef void(^APIProgressBlock)(float progress);
typedef void(^APIDefaultCallback)();

typedef void(^APIClientCompletionBlock)(NSDictionary *response, NSError *error);
typedef void(^APIClientDefaultBlock)(NSError *error);

extern NSString *const FNTBaseURL;
extern NSString *const FNTHost;

@interface APIClient : NSObject

+ (instancetype)sharedInstance;

/**
 *  Default POST request.
 *
 *  @param path         The URL as string.
 *  @param params       A dictionary containing all parameters that will be embedded in url. Can be nil.
 *  @param auth         Does the request needs the authorization token?
 *  @param completion   A block called once the operation is completed.
 *                      If the operation succeeded, the dictionary parameter is set with the server response.
 *                      In case of error, error parameter is set with the error.
 */
- (void)POST:(NSString *)path
  parameters:(NSDictionary *)params
        auth:(BOOL)auth
  completion:(APIClientCompletionBlock)completionBlock;

/**
 *  Default POST request with images.
 *
 *  @param path         The URL as string.
 *  @param params       A dictionary containing all parameters that will be embedded in url. Can be nil.
 *  @param auth         Does the request needs the authorization token?
 *  @param imageData    The image as NSData to be uploaded.
 *  @param thumbData    The thumbnail as NSData to be uploaded.
 *  @param completion   A block called once the operation is completed.
 *                      If the operation succeeded, the dictionary parameter is set with the server response.
 *                      In case of error, error parameter is set with the error.
 */
- (void)POST:(NSString *)path
  parameters:(NSDictionary *)params
        auth:(BOOL)auth
       image:(NSData *)imageData
   thumbnail:(NSData *)thumbData
  completion:(APIClientCompletionBlock)completionBlock;

/**
 *  Default GET request.
 *
 *  @param path         The URL as string.
 *  @param params       A dictionary containing all parameters that will be embedded in url. Can be nil.
 *  @param auth         Does the request needs the authorization token?
 *  @param completion   A block called once the operation is completed.
 *                      If the operation succeeded, the dictionary parameter is set with the server response.
 *                      In case of error, error parameter is set with the error.
 */
- (void)GET:(NSString *)path
 parameters:(NSDictionary *)params
       auth:(BOOL)auth
 completion:(APIClientCompletionBlock)completionBlock;

/**
 *  Downloads a image with given URL.
 *
 *  @param url          The image URL
 *  @param completion   A block called once the operation is completed.
 *                      If the operation succeeded, the dictionary parameter is set with the server response.
 *                      In case of error, error parameter is set with the error.
 *  @param success If the request is successful, this block is executed with the downloaded image as parameter.
 *  @param failure If the request is unsuccessful, this block is executed with the NSError as parameter.
 */
- (void)imageFromURL:(NSURL *)url
          completion:(void(^)(UIImage *image, NSError *error))completionBlock;

@end
