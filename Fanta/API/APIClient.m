//
//  APIClient.m
//  Fanta
//
//  Created by Mario Concilio on 8/8/16.
//  Copyright © 2016 Mario Concilio. All rights reserved.
//

#import "APIClient.h"
#import "Constants.h"
#import "UserService.h"

#import <SDWebImageDownloader.h>
#import <OHHTTPStubs.h>

#if LOCALHOST
NSString *const FNTBaseURL  = @"http://192.168.1.32:8000";
NSString *const FNTHost     = @"192.168.1.32";
#else
NSString *const FNTBaseURL  = @"http://52.67.163.194";
NSString *const FNTHost     = @"52.67.163.194";
#endif

@implementation APIClient

+ (instancetype)sharedInstance {
    static APIClient *shared = nil;
    static dispatch_once_t onceToken;
    
    dispatch_once(&onceToken, ^{
        shared = [[self alloc] init];
    });
    
    return shared;
}

- (AFHTTPSessionManager *)managerWithAuth:(BOOL)auth {
    AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
    manager.responseSerializer = [AFJSONResponseSerializer serializer];
    
//    manager.requestSerializer = [AFJSONRequestSerializer serializer];
    
    [manager.requestSerializer setValue:@"application/json" forHTTPHeaderField:@"Accept"];
    [manager.requestSerializer setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    
    if (auth) {
        [manager.requestSerializer setAuthorizationHeaderFieldWithUsername:@"x"
                                                                  password:[UserService loadToken]];
        
//        [manager.requestSerializer setAuthorizationHeaderFieldWithUsername:@"x"
//                                                                  password:[NSUserDefaults objectForKey:kUserToken]];
    }
    
    return manager;
}

- (void)POST:(NSString *)path parameters:(NSDictionary *)params auth:(BOOL)auth completion:(APIClientCompletionBlock)completionBlock {
    AFHTTPSessionManager *manager = [self managerWithAuth:auth];
    [manager POST:path
       parameters:params
          success:^(NSURLSessionDataTask * _Nonnull task, id  _Nonnull responseObject) {
              NSDictionary *response = responseObject;
              if (![[response valueForKey:@"error"] boolValue]) {
                  completionBlock(response, nil);
              }
              else {
                  NSInteger statusCode = [[response valueForKey:@"status_code"] integerValue];
                  NSDictionary *info = @{NSLocalizedDescriptionKey: [response valueForKey:@"message"]};
                  NSError *error = [NSError errorWithDomain:kBundleID code:statusCode userInfo:info];
                  completionBlock(nil, error);
                  DDLogWarn(@"%@", error.debugDescription);
              }
          }
          failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
              completionBlock(nil, error);
              DDLogWarn(@"%@", error.debugDescription);
          }];
}

- (void)POST:(NSString *)path parameters:(NSDictionary *)params auth:(BOOL)auth image:(NSData *)imageData thumbnail:(NSData *)thumbData completion:(APIClientCompletionBlock)completionBlock {
    AFHTTPSessionManager *manager = [self managerWithAuth:auth];
    [manager POST:path parameters:params constructingBodyWithBlock:^(id<AFMultipartFormData>  _Nonnull formData) {
        if(imageData) {
            [formData appendPartWithFileData:imageData
                                        name:@"image"
                                    fileName:@"profile.jpg"
                                    mimeType:@"image/jpeg"];
        }
        
        if (thumbData) {
            [formData appendPartWithFileData:thumbData
                                        name:@"thumbnail"
                                    fileName:@"thumb.jpg"
                                    mimeType:@"image/jpeg"];
        }
    }
          success:^(NSURLSessionDataTask * _Nonnull task, id  _Nonnull responseObject) {
              NSDictionary *response = responseObject;
              if (![[response valueForKey:@"error"] boolValue]) {
                  completionBlock(response, nil);
              }
              else {
                  NSInteger statusCode = [[response valueForKey:@"status_code"] integerValue];
                  NSDictionary *info = @{NSLocalizedDescriptionKey: [response valueForKey:@"message"]};
                  NSError *error = [NSError errorWithDomain:kBundleID code:statusCode userInfo:info];
                  completionBlock(nil, error);
                  DDLogWarn(@"%@", error.debugDescription);
              }
          }
          failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
              completionBlock(nil, error);
              DDLogWarn(@"%@", error.debugDescription);
          }];
}

- (void)GET:(NSString *)path parameters:(NSDictionary *)params auth:(BOOL)auth completion:(APIClientCompletionBlock)completionBlock {
    AFHTTPSessionManager *manager = [self managerWithAuth:auth];
    [manager GET:path
      parameters:params
         success:^(NSURLSessionDataTask * _Nonnull task, id  _Nonnull responseObject) {
             NSDictionary *response = responseObject;
             if (![[response valueForKey:@"error"] boolValue]) {
                 completionBlock(response, nil);
             }
             else {
                 NSInteger statusCode = [[response valueForKey:@"status_code"] integerValue];
                 NSDictionary *info = @{NSLocalizedDescriptionKey: [response valueForKey:@"message"]};
                 NSError *error = [NSError errorWithDomain:kBundleID code:statusCode userInfo:info];
                 completionBlock(nil, error);
                 DDLogWarn(@"%@", error.debugDescription);
             }
         }
         failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
             completionBlock(nil, error);
             DDLogWarn(@"%@", error.debugDescription);
         }];
}

- (void)imageFromURL:(NSURL *)url completion:(void(^)(UIImage *image, NSError *error))completionBlock {
    [OHHTTPStubs stubRequestsPassingTest:^BOOL(NSURLRequest * _Nonnull request) {
//        return [[request.URL pathExtension] isEqualToString:@"jpg"];
        return NO;
    } withStubResponse:^OHHTTPStubsResponse * _Nonnull(NSURLRequest * _Nonnull request) {
        NSError* notConnectedError = [NSError errorWithDomain:NSURLErrorDomain code:kCFURLErrorNotConnectedToInternet userInfo:nil];
        return [OHHTTPStubsResponse responseWithError:notConnectedError];
    }];
    
    SDWebImageDownloader *downloader = [SDWebImageDownloader sharedDownloader];
    [downloader downloadImageWithURL:url
                             options:0
                            progress:NULL
                           completed:^(UIImage *image, NSData *data, NSError *error, BOOL finished) {
                               [OHHTTPStubs removeAllStubs];
                               if (!error) {
                                   completionBlock(image, nil);
                               }
                               else {
                                   completionBlock(nil, error);
                               }
                           }];
}

@end
