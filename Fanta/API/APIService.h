//
//  APIService.h
//  Fanta
//
//  Created by Mario Concilio on 28/04/15.
//  Copyright (c) 2015 Mario Concilio. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <Foundation/Foundation.h>

/*
typedef NS_ENUM(NSInteger, APIPostType) {
    APIPostTypeAthletic = 1,
    APIPostTypeRegular,
};
 */

typedef void (^APIErrorCallback)(NSError *error);

@class VOPost, VOComment, VOUser, VOAthletic;
@interface APIService : NSObject

+ (instancetype)sharedInstance;

+ (void)imageFromURL:(NSURL *)url callback:(void(^)(UIImage *image, NSError *error))callback;

-(void)saveUserInfo:(VOUser *)user;

#pragma mark - Login & Signup Flow
- (void)registerUser:(NSString *)name
               email:(NSString *)email
            password:(NSString *)password
               block:(void (^)(NSError *error))block;

- (void)registerUserFromFacebook:(NSString *)name
                           email:(NSString *)email
                           image:(UIImage *)image
                       thumbnail:(UIImage *)thumbnail
                      facebookID:(NSUInteger)facebookID
                           block:(void (^)(NSError *error))block;

- (void)login:(NSString *)email
     password:(NSString *)password
        block:(void (^)(NSError *error))block;

- (void)logout;

- (void)listAllAthletics:(void (^)(NSArray<VOAthletic *> *list))successBlock
                 failure:(void (^)(NSError *error))errorBlock;

#pragma mark - User Edit
- (void)editUserName:(NSString *)name
            password:(NSString *)password
               image:(UIImage *)image
           thumbnail:(UIImage *)thumbnail
               block:(void (^)(NSError *error))block;

//- (void)saveFacebookUserInfo:(NSDictionary *)dict;

#pragma mark - Posts
/*
- (void)getPosts:(APIPostType)post
  fromUniversity:(NSUInteger)university
         success:(void (^)(NSMutableArray *array))successBlock
         failure:(void (^)(NSError *error))errorBlock;

- (void)createPost:(NSString *)content
              type:(APIPostType)type
    fromUniversity:(NSUInteger)university
          callback:(APIErrorCallback)callback;
 */

- (void)likePost:(VOPost *)post
        callback:(APIErrorCallback)callback;

- (void)dislikePost:(VOPost *)post
           callback:(APIErrorCallback)callback;

- (void)reportPost:(VOPost *)post
          callback:(APIErrorCallback)callback;

#pragma mark - Comments
- (void)getCommentsFromPost:(VOPost *)post
                    success:(void (^)(NSMutableArray *array))successBlock
                    failure:(void (^)(NSError *error))errorBlock;

- (void)createComment:(NSString *)comment
             fromPost:(VOPost *)post
             callback:(APIErrorCallback)callback;

- (void)likeComment:(VOComment *)comment
           callback:(APIErrorCallback)callback;

- (void)dislikeComment:(VOComment *)comment
              callback:(APIErrorCallback)callback;

- (void)reportComment:(VOComment *)comment
             callback:(APIErrorCallback)callback;

#pragma mark - Friendship
- (void)getFriendsOfUser:(VOUser *)user
                 success:(void (^)(NSArray *array))successBlock
                 failure:(void (^)(NSError *error))errorBlock;

- (void)createFriendshipWithUser:(VOUser *)user
                        callback:(APIErrorCallback)callback;

- (void)deleteFriendshipWithUser:(VOUser *)user
                        callback:(APIErrorCallback)callback;

#pragma mark - Winks
- (void)sendWinkForUser:(VOUser *)user
               callback:(APIErrorCallback)callback;

- (void)getWinksWithSuccess:(void (^)(NSArray *array))successBlock
                    failure:(void (^)(NSError *error))errorBlock;

#pragma mark - Messages User
- (void)sendMessageToUser:(VOUser *)user
                  content:(NSString *)content
                 callback:(APIErrorCallback)callback;

- (void)getMessagesFromUser:(VOUser *)user
                    success:(void (^)(NSArray *array))successBlock
                    failure:(void (^)(NSError *error))errorBlock;

- (void)listMessagesWithSuccess:(void (^)(NSArray *array))successBlock
                        failure:(void (^)(NSError *error))errorBlock;

#pragma mark - Messages Group
- (void)createGroupWithUsers:(NSArray<VOUser *> *)users
                        name:(NSString *)name
                     success:(void (^)(NSArray *array))successBlock
                     failure:(void (^)(NSError *error))errorBlock;

- (void)sendMessageToGroup:(NSUInteger)groupID
                   content:(NSString *)content
                  callback:(APIErrorCallback)callback;

- (void)listMessagesFromGroup:(NSUInteger)groupID
                  withSuccess:(void (^)(NSArray *array))successBlock
                      failure:(void (^)(NSError *error))errorBlock;

#pragma mark - User
- (void)getUserFromId:(NSUInteger)userId
              success:(void (^)(VOUser *user))successBlock
              failure:(void (^)(NSError *error))errorBlock;


@end
