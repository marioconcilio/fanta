//
//  AppDelegate.h
//  Fanta
//
//  Created by Mario Concilio on 19/01/15.
//  Copyright (c) 2015 Mario Concilio. All rights reserved.
//

#import <UIKit/UIKit.h>

@class VOCart;
@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;
@property (strong, nonatomic) VOCart *myCart;

- (void)defineRootViewControllerAnimated:(BOOL)animated;
- (void)setNetworkActivityIndicatorVisible:(BOOL)visible;
- (void)setDarkNavigationBar;
- (void)setLightNavigationBar;

@end

