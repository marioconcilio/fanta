//
//  AppDelegate.m
//  Fantaa
//
//  Created by Mario Concilio on 19/01/15.
//  Copyright (c) 2015 Mario Concilio. All rights reserved.
//

#import "AppDelegate.h"
#import "DrawerViewController.h"
#import "UIStoryboard+Storyboards.h"
#import "FeedViewController.h"
#import "UIViewController+BaseViewController.h"
#import "Constants.h"
#import "Macros.h"
#import "WelcomeViewController.h"
#import "Helper.h"
#import "UserService.h"
#import "VOCart.h"
#import "VOUser.h"
#import "ConnectionAlertManager.h"
#import "LogFormatter.h"

#import <DDASLLogger.h>
#import <DDTTYLogger.h>
#import <TSMessages/TSMessageView.h>
#import <AFNetworkActivityIndicatorManager.h>
#import <AFNetworkReachabilityManager.h>
#import <FBSDKCoreKit/FBSDKCoreKit.h>
#import <SWRevealViewController.h>
//#import <TestFairy.h>
#import <Fabric/Fabric.h>
#import <Crashlytics/Crashlytics.h>
#import <SIAlertView.h>
#import <SDImageCache.h>
#import <KVNProgress.h>

@interface AppDelegate ()

@end

@implementation AppDelegate

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {
//    [TestFairy begin:@"939e8063f824164d535ee589a9000b7765bd5ed2"];
    
    SDImageCache *imageCache = [SDImageCache sharedImageCache];
    [imageCache clearDisk];
    [imageCache clearMemory];
    
    /*
     * Loading My Cart
     */
    self.myCart = [Helper loadMyCart];
    
    /*
     * logger
     */
    [DDLog addLogger:[DDASLLogger sharedInstance]];
    [DDLog addLogger:[DDTTYLogger sharedInstance]];
    [[DDTTYLogger sharedInstance] setColorsEnabled:YES];
    [[DDTTYLogger sharedInstance] setLogFormatter:[[LogFormatter alloc] init]];
    [[DDTTYLogger sharedInstance] setForegroundColor:UIColorFromRGB(0, 160, 190)
                                     backgroundColor:nil
                                             forFlag:DDLogFlagInfo];
    [[DDTTYLogger sharedInstance] setForegroundColor:UIColorFromHEX(0x55747C)
                                     backgroundColor:nil
                                             forFlag:DDLogFlagVerbose];
    NSSetUncaughtExceptionHandler(&uncaughtExceptionHandler);
    
    [[UIBarButtonItem appearance] setBackButtonBackgroundImage:[UIImage imageNamed:@"navbar_back"]
                                                      forState:UIControlStateNormal barMetrics:UIBarMetricsDefault];
//    [application setStatusBarStyle:UIStatusBarStyleLightContent animated:YES];
    [self setDarkNavigationBar];
    [self defineRootViewControllerAnimated:NO];
    
    /*
     *  AFNetworking
     */
    [[AFNetworkActivityIndicatorManager sharedManager] setEnabled:YES];
    [[AFNetworkReachabilityManager sharedManager] startMonitoring];
    
    /*
     *  SIAlertView
     */
    [[SIAlertView appearance] setTitleFont:[UIFont semiboldFontWithSize:20.0]];
    [[SIAlertView appearance] setMessageFont:[UIFont lightFontWithSize:16.0]];
    [[SIAlertView appearance] setButtonFont:[UIFont lightFontWithSize:[UIFont buttonFontSize]]];
    
    /*
     *  TSMessageView
     */
    [[TSMessageView appearance] setTitleFont:[UIFont boldFontWithSize:12.0]];
    [[TSMessageView appearance] setContentFont:[UIFont lightFontWithSize:14.0]];
    
    /*
     *  Connection Label
     */
    [[ConnectionAlertManager sharedManager] setBackgroundColor:[UIColor customPink]];
    [[ConnectionAlertManager sharedManager] setTextFont:[UIFont lightFontWithSize:14.0]];
    [[ConnectionAlertManager sharedManager] setMessage:@"Ops! Sem conexão com a Internet!"];
    
    /*
     *  KVNProgress
     */
    KVNProgressConfiguration *configuration = [[KVNProgressConfiguration alloc] init];
    configuration.statusFont = [UIFont lightFontWithSize:15.0];
    configuration.circleSize = 60.0;
    configuration.lineWidth = 1.0;
    configuration.fullScreen = NO;
    configuration.minimumDisplayTime = configuration.minimumErrorDisplayTime = configuration.minimumSuccessDisplayTime = 0.3;
    
    [KVNProgress setConfiguration:configuration];
    
    /*
     *  Crashlytics
     */
    [Fabric with:@[[Crashlytics class]]];
    
    return [[FBSDKApplicationDelegate sharedInstance] application:application
                                    didFinishLaunchingWithOptions:launchOptions];
}

- (void)applicationWillResignActive:(UIApplication *)application {
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
}

- (void)applicationDidEnterBackground:(UIApplication *)application {
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
    
    /*
     * Saving My Cart
     */
    [Helper saveMyCart:self.myCart];
}

- (void)applicationWillEnterForeground:(UIApplication *)application {
    // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
}

- (void)applicationDidBecomeActive:(UIApplication *)application {
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
    [FBSDKAppEvents activateApp];
}

- (void)applicationWillTerminate:(UIApplication *)application {
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
}

- (BOOL)application:(UIApplication *)application
            openURL:(NSURL *)url
  sourceApplication:(NSString *)sourceApplication
         annotation:(id)annotation {
    return [[FBSDKApplicationDelegate sharedInstance] application:application
                                                          openURL:url
                                                sourceApplication:sourceApplication
                                                       annotation:annotation];
}

#pragma mark - Public Methods
- (void)defineRootViewControllerAnimated:(BOOL)animated {
    UIViewController *vc = nil;
    VOUser *user = [UserService loadUser];
    
    if (user && [user isValid]) {
        UIViewController *feed = [[UIStoryboard feedStoryboard] instantiateInitialViewController];
        SWRevealViewController *reveal = (SWRevealViewController *) [[UIStoryboard mainStoryboard] instantiateInitialViewController];
        reveal.frontViewController = feed;
        reveal.toggleAnimationType = SWRevealToggleAnimationTypeEaseOut;
        reveal.rightViewRevealOverdraw = 0.0;
        reveal.rearViewRevealOverdraw = 0.0;
        reveal.rearViewRevealWidth = -55.0;
        reveal.rightViewRevealWidth = -55.0;
        
        vc = reveal;
//        [[UIApplication sharedApplication] setStatusBarHidden:NO withAnimation:UIStatusBarAnimationSlide];
    }
    else {
        vc = [[UIStoryboard welcomeStoryboard] instantiateInitialViewController];
//        [[UIApplication sharedApplication] setStatusBarHidden:YES withAnimation:UIStatusBarAnimationSlide];
    }
    
    if (animated) {
        [UIView transitionWithView:self.window
                          duration:0.3
                           options:UIViewAnimationOptionTransitionCrossDissolve
                        animations:^{
                            self.window.rootViewController = vc;
                        }
                        completion:NULL];
    }
    else {
        self.window.rootViewController = vc;
    }
}

- (void)setNetworkActivityIndicatorVisible:(BOOL)visible {
    static NSInteger NumberOfCallsToSetVisible = 0;
    if (visible)
        NumberOfCallsToSetVisible++;
    else
        NumberOfCallsToSetVisible--;
    
    // The assertion helps to find programmer errors in activity indicator management.
    // Since a negative NumberOfCallsToSetVisible is not a fatal error,
    // it should probably be removed from production code.
//    NSAssert(NumberOfCallsToSetVisible >= 0, @"Network Activity Indicator was asked to hide more often than shown");
    
    // Display the indicator as long as our static counter is > 0.
//    MCLogArgs(@"%d", NumberOfCallsToSetVisible);
    [[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:(NumberOfCallsToSetVisible > 0)];
}

- (void)setDarkNavigationBar {
    [[UINavigationBar appearance] setBarStyle:UIBarStyleBlack];
    [[UINavigationBar appearance] setTintColor:[UIColor whiteColor]];
    [[UINavigationBar appearance] setTitleTextAttributes:@{NSFontAttributeName:[UIFont titleFontWithSize:16.f],
                                                           NSForegroundColorAttributeName:[UIColor whiteColor]}];
}

- (void)setLightNavigationBar {
    [[UINavigationBar appearance] setBarStyle:UIBarStyleDefault];
    [[UINavigationBar appearance] setTintColor:UIColorFromRGB(64, 64, 64)];
    [[UINavigationBar appearance] setTitleTextAttributes:@{NSFontAttributeName:[UIFont lightFontWithSize:16.f],
                                                           NSForegroundColorAttributeName:UIColorFromRGB(64, 64, 64)}];
}

void uncaughtExceptionHandler(NSException *exception) {
    DDLogError(@"%@", exception);
    DDLogError(@"Stack Trace: %@", exception.callStackSymbols);
}

@end
