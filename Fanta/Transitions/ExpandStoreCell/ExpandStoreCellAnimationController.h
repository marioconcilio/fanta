//
//  ExpandStoreCellAnimationController.h
//  Fanta
//
//  Created by Mario Concilio on 3/8/16.
//  Copyright © 2016 Mario Concilio. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface ExpandStoreCellAnimationController : NSObject <UIViewControllerAnimatedTransitioning>

@property (nonatomic, assign) NSTimeInterval duration;
@property (nonatomic, assign) CGFloat dampingRatio;
@property (nonatomic, assign) CGFloat springVelocity;

@end
