//
//  ExpandStoreCellAnimationController.m
//  Fanta
//
//  Created by Mario Concilio on 3/8/16.
//  Copyright © 2016 Mario Concilio. All rights reserved.
//

#import "ExpandStoreCellAnimationController.h"
#import "StoreViewController.h"
#import "ItemViewController.h"
#import "Constants.h"
#import "UIViewController+ExpandCellTransition.h"
#import "StoreCell.h"

#import <AsyncDisplayKit.h>
#import <SWRevealViewController.h>
#import <POP.h>

@implementation ExpandStoreCellAnimationController {
    CGPoint _imageCenter;
    CGPoint _contentCenter;
}

- (instancetype)init {
    self = [super init];
    if (self) {
        _duration = 0.6;
        _dampingRatio = 0.8;
        _springVelocity = 0.1;
    }
    
    return self;
}

#pragma mark - View Controller Animated Transitioning
- (NSTimeInterval)transitionDuration:(id<UIViewControllerContextTransitioning>)transitionContext {
    return self.duration;
}

- (void)animateTransition:(id<UIViewControllerContextTransitioning>)transitionContext {
    UIViewController *toController = [transitionContext viewControllerForKey:UITransitionContextToViewControllerKey];
    
    if ([toController isBeingPresented]) {
        [self animatePresentation:transitionContext];
    }
    else {
        [self animateDismissal:transitionContext];
    }
}

#pragma mark - Transitioning
- (void)animatePresentation:(id<UIViewControllerContextTransitioning>)transitionContext {
    SWRevealViewController *reveal = (SWRevealViewController *) [transitionContext viewControllerForKey:UITransitionContextFromViewControllerKey];
    UINavigationController *nav = (UINavigationController *) reveal.frontViewController;
    StoreViewController *storeVC = nav.childViewControllers[0];
    ItemViewController *itemVC = (ItemViewController *) [transitionContext viewControllerForKey:UITransitionContextToViewControllerKey];
    UIView *itemView = [transitionContext viewForKey:UITransitionContextToViewKey];
    UIView *containerView = [transitionContext containerView];
    
    itemView.frame = itemVC.expand_startFrame;
    itemVC.page.view.hidden = YES;
//    itemVC.page.view.alpha = 0.0;
//    itemVC.pageControl.hidden = YES;
//    itemvc.pageControl.alpha = 0.0;
    
    [containerView addSubview:itemView];
    
    [nav setNavigationBarHidden:YES animated:YES];
    [[UIApplication sharedApplication] setStatusBarHidden:YES withAnimation:UIStatusBarAnimationSlide];
    
    [UIView animateWithDuration:self.duration
                          delay:0.0
         usingSpringWithDamping:self.dampingRatio
          initialSpringVelocity:self.springVelocity
                        options:UIViewAnimationOptionAllowUserInteraction
                     animations:^{
                         itemView.frame = storeVC.view.frame;
//                         itemView.alpha = 1.0;
                         
//                         itemView.backgroundColor = [UIColor whiteColor];
                         itemVC.imageNode.layer.frame = (CGRect) {
                             .size.width = CGRectGetWidth(itemView.frame),
                             .size.height = CGRectGetWidth(itemView.frame) * kPostImageNodeHeightScale,
                         };
//                         itemVC.imageNode.view.center = imageNodeCenter;
//                         itemVC.contentNode.view.center = contentNodeCenter;
                     }
                     completion:^(BOOL finished) {
                         storeVC.cell.hidden = YES;
                         itemVC.page.view.hidden = NO;
//                         itemVC.pageControl.hidden = NO;
                         itemVC.imageNode.hidden = YES;
                         [transitionContext completeTransition:finished];
                     }];
    
}

- (void)animateDismissal:(id<UIViewControllerContextTransitioning>)transitionContext {
    SWRevealViewController *reveal = (SWRevealViewController *) [transitionContext viewControllerForKey:UITransitionContextToViewControllerKey];
    UINavigationController *nav = (UINavigationController *) reveal.frontViewController;
    StoreViewController *storeVC = nav.childViewControllers[0];
    ItemViewController *itemVC = (ItemViewController *) [transitionContext viewControllerForKey:UITransitionContextFromViewControllerKey];
    UIView *itemView = [transitionContext viewForKey:UITransitionContextFromViewKey];
    
//    itemVC.imageNode.image = itemVC.currentImage;
    itemVC.imageNode.hidden = NO;
//    itemVC.page.view.alpha = 0.0;
    itemVC.page.view.hidden = YES;
    
    [nav setNavigationBarHidden:NO animated:YES];
    [[UIApplication sharedApplication] setStatusBarHidden:NO withAnimation:UIStatusBarAnimationSlide];
    
    [storeVC beginAppearanceTransition:YES animated:YES];
    
    [UIView animateWithDuration:self.duration
                          delay:0.0
         usingSpringWithDamping:self.dampingRatio
          initialSpringVelocity:self.springVelocity
                        options:UIViewAnimationOptionAllowUserInteraction
                     animations:^{
                         itemView.frame = itemVC.expand_startFrame;
                         itemVC.blurNavbar.hidden = YES;
                         itemVC.buyButton.hidden = YES;
//                         postVC.blurToolbar.hidden = YES;
//                         postVC.scrollView.contentOffset = CGPointZero;
                         
                         itemVC.imageNode.layer.frame = (CGRect) {
                             .origin.x = kStoreCellInsets,
                             .origin.y = kStoreCellInsets,
                             .size.width = CGRectGetWidth(itemVC.expand_startFrame) - 2*kStoreCellInsets,
                             .size.height = CGRectGetWidth(itemVC.expand_startFrame) - 2*kStoreCellInsets,
                         };
                         
//                         postVC.imageNode.view.transform = CGAffineTransformMakeScale(1.0, 1.0);
//                         postVC.imageNode.view.center = _imageCenter;
//                         postVC.contentNode.view.center = _contentCenter;
                     }
                     completion:^(BOOL finished) {
                         if (finished) {
                             [itemView removeFromSuperview];
                             [itemVC removeFromParentViewController];
                             [storeVC endAppearanceTransition];
                         }
                         
                         storeVC.cell.hidden = NO;
                         [transitionContext completeTransition:finished];
                     }];

}

@end
