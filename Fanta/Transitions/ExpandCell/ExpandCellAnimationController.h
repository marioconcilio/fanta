//
//  ExpandCellAnimationController.h
//  Fanta
//
//  Created by Mario Concilio on 9/9/15.
//  Copyright (c) 2015 Mario Concilio. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface ExpandCellAnimationController : NSObject <UIViewControllerAnimatedTransitioning>

@property (nonatomic, assign) NSTimeInterval duration;
@property (nonatomic, assign) CGFloat dampingRatio;
@property (nonatomic, assign) CGFloat springVelocity;

@end
