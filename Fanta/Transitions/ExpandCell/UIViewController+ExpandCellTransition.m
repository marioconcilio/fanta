//
//  UIViewController+ExpandCellTransition.m
//  Fanta
//
//  Created by Mario Concilio on 9/9/15.
//  Copyright (c) 2015 Mario Concilio. All rights reserved.
//

#import "UIViewController+ExpandCellTransition.h"
#import <objc/runtime.h>

@implementation UIViewController (ExpandCellTransition)

static const void *StartFrameKey = &StartFrameKey;

#pragma mark - Getters & Setters
#pragma mark expand_startFrame
- (void)setExpand_startFrame:(CGRect)expand_startFrame {
    objc_setAssociatedObject(self, StartFrameKey, [NSValue valueWithCGRect:expand_startFrame], OBJC_ASSOCIATION_RETAIN_NONATOMIC);
}

- (CGRect)expand_startFrame {
    NSValue *value = objc_getAssociatedObject(self, StartFrameKey);
    if (!value) {
        return CGRectZero;
    }
    
    return [value CGRectValue];
}

@end
