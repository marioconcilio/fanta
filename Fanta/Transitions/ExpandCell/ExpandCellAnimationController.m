//
//  ExpandCellAnimationController.m
//  Fanta
//
//  Created by Mario Concilio on 9/9/15.
//  Copyright (c) 2015 Mario Concilio. All rights reserved.
//

#import "ExpandCellAnimationController.h"
#import "FeedViewController.h"
#import "PostViewController.h"
#import "UIViewController+ExpandCellTransition.h"
#import "PostNode.h"
#import "FeedCell.h"
#import "VOPost.h"
#import "Constants.h"

#import <AsyncDisplayKit.h>
#import <SWRevealViewController.h>
#import <RKTabView.h>

@implementation ExpandCellAnimationController {
//    CGPoint _imageCenter;
//    CGPoint _contentCenter;
}

- (instancetype)init {
    self = [super init];
    if (self) {
        _duration = 0.6;
        _dampingRatio = 0.8;
        _springVelocity = 0.1;
    }
    
    return self;
}

#pragma mark - View Controller Animated Transitioning
- (NSTimeInterval)transitionDuration:(id<UIViewControllerContextTransitioning>)transitionContext {
    return self.duration;
}

- (void)animateTransition:(id<UIViewControllerContextTransitioning>)transitionContext {
    UIViewController *toController = [transitionContext viewControllerForKey:UITransitionContextToViewControllerKey];
    
    if ([toController isBeingPresented]) {
        [self animatePresentation:transitionContext];
    }
    else {
        [self animateDismissal:transitionContext];
    }
}

#pragma mark - Transitioning
- (void)animatePresentation:(id<UIViewControllerContextTransitioning>)transitionContext {
    SWRevealViewController *reveal = (SWRevealViewController *) [transitionContext viewControllerForKey:UITransitionContextFromViewControllerKey];
    UINavigationController *nav = (UINavigationController *) reveal.frontViewController;
    FeedViewController *feedVC = nav.childViewControllers[0];
    PostViewController *postVC = (PostViewController *) [transitionContext viewControllerForKey:UITransitionContextToViewControllerKey];
    UIView *postView = [transitionContext viewForKey:UITransitionContextToViewKey];
    UIView *containerView = [transitionContext containerView];
    
    postView.frame = postVC.expand_startFrame;
    postVC.contentNode.alpha = 0.0;
    
    BOOL hasImage = (postVC.post.postImage != nil);
    
    if (hasImage) {
        postView.backgroundColor = [UIColor clearColor];
    }
    else {
        postView.backgroundColor = [UIColor whiteColor];
    }
    
    [nav setNavigationBarHidden:YES animated:YES];
//    [[UIApplication sharedApplication] setStatusBarHidden:YES withAnimation:UIStatusBarAnimationSlide];
    
    [containerView addSubview:postView];
    
//    _imageCenter = postVC.imageNode.view.center;
//    _contentCenter = postVC.contentNode.view.center;
    
    CGRect viewFrame = feedVC.view.bounds;
    
    if (hasImage) {
        [UIView animateWithDuration:self.duration
                              delay:0.0
             usingSpringWithDamping:self.dampingRatio
              initialSpringVelocity:self.springVelocity
                            options:UIViewAnimationOptionAllowUserInteraction
                         animations:^{
                             /*
                              *  slide out tabview
                              */
                             CGRect oldFrame = feedVC.tabView.frame;
                             feedVC.tabView.frame = (CGRect) {
                                 .origin.y = CGRectGetMaxY(feedVC.view.frame),
                                 .size = oldFrame.size,
                             };
                             
                             postView.frame = viewFrame;
                             postVC.contentNode.alpha = 1.0;
                             
                             postView.backgroundColor = [UIColor whiteColor];

                             postVC.imageNode.layer.frame = (CGRect) {
                                 .size.width = CGRectGetWidth(feedVC.view.frame),
                                 .size.height = CGRectGetWidth(feedVC.view.frame) * kPostImageNodeHeightScale,
                             };

                             postVC.contentNode.layer.frame = (CGRect) {
                                 .origin.x = kPostImageNodePadding,
                                 .origin.y = CGRectGetMaxY(postVC.imageNode.frame) + kPostImageNodePadding,
                                 .size = postVC.contentNode.calculatedSize,
                             };
                         }
                         completion:^(BOOL finished) {
                             feedVC.cell.frameNode.alpha = 0.0;
//                             [feedVC animateButtonsToDefault];
                             [transitionContext completeTransition:finished];
                         }];
        
    }
    else {
        [UIView animateWithDuration:self.duration
                              delay:0.0
             usingSpringWithDamping:self.dampingRatio
              initialSpringVelocity:self.springVelocity
                            options:UIViewAnimationOptionAllowUserInteraction
                         animations:^{
                             /*
                              *  slide out tabview
                              */
                             CGRect oldFrame = feedVC.tabView.frame;
                             feedVC.tabView.frame = (CGRect) {
                                 .origin.y = CGRectGetMaxY(feedVC.view.frame),
                                 .size = oldFrame.size,
                             };
                             
                             postView.frame = viewFrame;
                             postVC.contentNode.alpha = 1.0;
                         }
                         completion:^(BOOL finished) {
                             feedVC.cell.frameNode.alpha = 0.0;
//                             [feedVC animateButtonsToDefault];
                             [transitionContext completeTransition:finished];
                         }];
    }
    
}

- (void)animateDismissal:(id<UIViewControllerContextTransitioning>)transitionContext {
    SWRevealViewController *reveal = (SWRevealViewController *) [transitionContext viewControllerForKey:UITransitionContextToViewControllerKey];
    UINavigationController *nav = (UINavigationController *) reveal.frontViewController;
    FeedViewController *feedVC = nav.childViewControllers[0];
    PostViewController *postVC = (PostViewController *) [transitionContext viewControllerForKey:UITransitionContextFromViewControllerKey];
    UIView *postView = [transitionContext viewForKey:UITransitionContextFromViewKey];
    
    [feedVC.cell reloadThumbs];
//    [feedVC animateButtons];
    [nav setNavigationBarHidden:NO animated:YES];
//    [[UIApplication sharedApplication] setStatusBarHidden:NO withAnimation:UIStatusBarAnimationSlide];
    
    [feedVC beginAppearanceTransition:YES animated:YES];
    
    BOOL hasImage = (postVC.post.postImage != nil);
    if (hasImage) {
        [UIView animateWithDuration:self.duration
                              delay:0.0
             usingSpringWithDamping:self.dampingRatio
              initialSpringVelocity:self.springVelocity
                            options:UIViewAnimationOptionAllowUserInteraction
                         animations:^{
                             /*
                              *  slide in tabView
                              */
                             CGRect oldFrame = feedVC.tabView.frame;
                             feedVC.tabView.frame = (CGRect) {
                                 .origin.y = CGRectGetMaxY(feedVC.view.frame) - CGRectGetHeight(oldFrame),
                                 .size = oldFrame.size,
                             };
                             
                             postView.frame = postVC.expand_startFrame;
                             postVC.blurNavbar.hidden = YES;
                             postVC.blurToolbar.hidden = YES;
                             postVC.scrollView.contentOffset = CGPointZero;
                             
                             postVC.imageNode.layer.frame = (CGRect) {
                                 .origin.x = 0.5 * kPostImageNodePadding,
                                 .origin.y = 0.5 * kPostImageNodePadding,
                                 .size.width = CGRectGetWidth(postVC.expand_startFrame) - kPostImageNodePadding,
                                 .size.height = CGRectGetWidth(postVC.expand_startFrame) * kPostImageNodeHeightScale - kPostImageNodePadding,
                             };
                             
                             postVC.contentNode.layer.frame = (CGRect) {
                                 .origin.x = kPostImageNodePadding,
                                 .origin.y = CGRectGetMaxY(postVC.imageNode.frame) + kPostImageNodePadding,
                                 .size = postVC.contentNode.calculatedSize,
                             };
                         }
                         completion:^(BOOL finished) {
                             if (finished) {
                                 [postView removeFromSuperview];
                                 [postVC removeFromParentViewController];
                                 [feedVC endAppearanceTransition];
                             }
                             
                             feedVC.cell.frameNode.alpha = 1.0;
                             [transitionContext completeTransition:finished];
                         }];
    }
    else {
        [UIView animateWithDuration:self.duration
                              delay:0.0
             usingSpringWithDamping:self.dampingRatio
              initialSpringVelocity:self.springVelocity
                            options:UIViewAnimationOptionAllowUserInteraction
                         animations:^{
                             /*
                              *  slide in tabView
                              */
                             CGRect oldFrame = feedVC.tabView.frame;
                             feedVC.tabView.frame = (CGRect) {
                                 .origin.y = CGRectGetMaxY(feedVC.view.frame) - CGRectGetHeight(oldFrame),
                                 .size = oldFrame.size,
                             };
                             
                             postView.frame = postVC.expand_startFrame;
                             postVC.blurNavbar.hidden = YES;
                             postVC.blurToolbar.hidden = YES;
                             postVC.scrollView.contentOffset = CGPointZero;
                         }
                         completion:^(BOOL finished) {
                             feedVC.cell.frameNode.alpha = 1.0;
                             [transitionContext completeTransition:finished];
                         }];
    }
}

@end
