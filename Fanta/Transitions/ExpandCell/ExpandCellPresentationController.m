//
//  ExpandCellPresentationController.m
//  Fanta
//
//  Created by Mario Concilio on 9/9/15.
//  Copyright (c) 2015 Mario Concilio. All rights reserved.
//

#import "ExpandCellPresentationController.h"
#import "UIViewController+ExpandCellTransition.h"

@interface ExpandCellPresentationController () <UIGestureRecognizerDelegate>

@property (nonatomic, strong) UIView *dimmedView;

@end

@implementation ExpandCellPresentationController {
//    CGFloat _dragStartLocation;
//    CGPoint _startPoint;
//    CGPoint _endPoint;
}

#pragma mark - Init
- (instancetype)init {
    self = [super init];
    if (self) {
        UIPanGestureRecognizer *pan = [[UIPanGestureRecognizer alloc] initWithTarget:self action:@selector(handlePan:)];
        pan.delegate = self;
        [self.presentedView addGestureRecognizer:pan];
    }
    
    return self;
}

- (instancetype)initWithPresentedViewController:(UIViewController *)presentedViewController
                       presentingViewController:(UIViewController *)presentingViewController {
    self = [super initWithPresentedViewController:presentedViewController presentingViewController:presentingViewController];
    if (self) {
        UIPanGestureRecognizer *pan = [[UIPanGestureRecognizer alloc] initWithTarget:self action:@selector(handlePan:)];
        pan.delegate = self;
        [self.presentedView addGestureRecognizer:pan];
        
//        _startPoint = (CGPoint) {
//            .x = CGRectGetMidX(self.presentedViewController.expand_startFrame),
//            .y = CGRectGetMaxY(self.presentedViewController.expand_startFrame),
//        };
//        
//        _endPoint = (CGPoint) {
//            .x = CGRectGetMidX(self.presentedViewController.expand_endFrame),
//            .y = CGRectGetMidY(self.presentedViewController.expand_endFrame),
//        };
    }
    
    return self;
}

#pragma mark - Getters
- (UIView *)dimmedView {
    if (!_dimmedView) {
        _dimmedView = [[UIView alloc] init];
        _dimmedView.backgroundColor = [UIColor colorWithWhite:0.0 alpha:0.6];
        
        UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(handleTap:)];
        [_dimmedView addGestureRecognizer:tap];
    }
    
    return _dimmedView;
}

#pragma mark - Presentation Life Cycle
- (void)presentationTransitionWillBegin {
    self.dimmedView.frame = self.containerView.bounds;
    self.dimmedView.alpha = 0.0;
    
    [self.containerView addSubview:self.dimmedView];
    [self.containerView addSubview:[self presentedView]];
    
    [self.presentingViewController.transitionCoordinator animateAlongsideTransition:^(id<UIViewControllerTransitionCoordinatorContext> context) {
        self.dimmedView.alpha = 1.0;
    } completion:NULL];
}

- (void)presentationTransitionDidEnd:(BOOL)completed {
    if (!completed) {
        [self.dimmedView removeFromSuperview];
    }
}

- (void)dismissalTransitionWillBegin {
    [self.presentingViewController.transitionCoordinator animateAlongsideTransition:^(id<UIViewControllerTransitionCoordinatorContext> context) {
        self.dimmedView.alpha = 0.0;
    } completion:NULL];
}

- (void)dismissalTransitionDidEnd:(BOOL)completed {
    if (completed) {
        [self.dimmedView removeFromSuperview];
    }
}


#pragma mark - Content Container
- (void)viewWillTransitionToSize:(CGSize)size withTransitionCoordinator:(id<UIViewControllerTransitionCoordinator>)coordinator {
    [super viewWillTransitionToSize:size withTransitionCoordinator:coordinator];
    
    [coordinator animateAlongsideTransition:^(id<UIViewControllerTransitionCoordinatorContext> context) {
        self.dimmedView.frame = self.containerView.bounds;
    } completion:NULL];
}

#pragma mark - Helper Methods
- (UIScrollView *)scrollViewInView {
    if ([self.presentedView isKindOfClass:[UIScrollView class]]) {
        return (UIScrollView *) self.presentedView;
    }
    
    for (UIView *view in self.presentedView.subviews) {
        if ([view isKindOfClass:[UIScrollView class]]) {
            return (UIScrollView *)view;
        }
    }
    
    return nil;
}

#pragma mark - Gesture
- (void)handleTap:(UITapGestureRecognizer *)tap {
    [self.presentedViewController dismissViewControllerAnimated:YES completion:NULL];
}

- (void)handlePan:(UIPanGestureRecognizer *)pan {
    if (pan.state == UIGestureRecognizerStateBegan) {
        UIScrollView *scrollView = [self scrollViewInView];
        if (scrollView) {
            scrollView.scrollEnabled = NO;
        }
        
//        [self.view endEditing:YES];
    }
    else if (pan.state == UIGestureRecognizerStateChanged) {
        CGPoint translation = [pan translationInView:UIKeyWindow()];
        
        if (translation.y < 0.0) {
            return;
        }
        
        CGPoint newCenter = (CGPoint) {
            .x = UIKeyWindow().center.x,
            .y = UIKeyWindow().center.y + translation.y,
        };
        
//        CATransform3D transform = CATransform3DMakeRotation(translation.y/1000, 0.0, 1.0, 0.0);
        
        [UIView animateWithDuration:0.3
                              delay:0.0
             usingSpringWithDamping:0.8
              initialSpringVelocity:0.8
                            options:UIViewAnimationOptionCurveEaseInOut
                         animations:^{
                             self.presentedView.center = newCenter;
                             self.dimmedView.alpha = 1.0 - (translation.y / 800.0);
//                             self.presentedView.layer.transform = transform;
                         }
                         completion:NULL];
        
    }
    else if (pan.state == UIGestureRecognizerStateEnded) {
        UIScrollView *scrollView = [self scrollViewInView];
        if (scrollView) {
            scrollView.scrollEnabled = YES;
        }
        
        if ([pan velocityInView:self.presentedView].y > 0.0) {
            [self.presentedViewController dismissViewControllerAnimated:YES completion:NULL];
        }
        else {
            [UIView animateWithDuration:0.3
                                  delay:0.0
                 usingSpringWithDamping:0.8
                  initialSpringVelocity:0.8
                                options:UIViewAnimationOptionCurveEaseInOut
                             animations:^{
                                 self.presentedView.center = UIKeyWindow().center;
                             }
                             completion:NULL];
        }
    }
}

#pragma mark - Gesture Delegate
- (BOOL)gestureRecognizer:(UIGestureRecognizer *)gestureRecognizer shouldRecognizeSimultaneouslyWithGestureRecognizer:(UIGestureRecognizer *)otherGestureRecognizer {
    CGFloat offset = 0.0;
    UIScrollView *scrollView = [self scrollViewInView];
    if (scrollView) {
        UIPanGestureRecognizer *pan = (UIPanGestureRecognizer *)gestureRecognizer;
        CGFloat velocityY = [pan velocityInView:self.presentedView].y;
        CGFloat offsetY = scrollView.contentOffset.y;
        
        BOOL vel, off;
        vel = velocityY > 0.0;
        off = offsetY <= offset;
        
        return off && vel;
    }
    
    return YES;
}

@end
