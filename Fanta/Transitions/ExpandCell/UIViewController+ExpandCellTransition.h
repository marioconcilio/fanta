//
//  UIViewController+ExpandCellTransition.h
//  Fanta
//
//  Created by Mario Concilio on 9/9/15.
//  Copyright (c) 2015 Mario Concilio. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIViewController (ExpandCellTransition)

/// default value is CGRectZero
@property (nonatomic, assign) CGRect expand_startFrame;

@end
