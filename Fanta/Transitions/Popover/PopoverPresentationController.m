//
//  PopoverPresentationController.m
//  Fanta
//
//  Created by Mario Concilio on 9/3/15.
//  Copyright (c) 2015 Mario Concilio. All rights reserved.
//

#import "PopoverPresentationController.h"
#import "UIViewController+PopoverTransition.h"

#define PopoverKeyWindow     [[UIApplication sharedApplication] keyWindow]

@interface PopoverPresentationController () <UIGestureRecognizerDelegate>

@property (nonatomic, strong) UIView *dimmedView;
@property (nonatomic, assign) PopoverDirection direction;

@end

@implementation PopoverPresentationController {
    CGFloat _dragStartLocation;
    CGPoint _startPoint;
    CGPoint _endPoint;
}

#pragma mark - Init
- (instancetype)init {
    self = [super init];
    if (self) {
        UIPanGestureRecognizer *pan = [[UIPanGestureRecognizer alloc] initWithTarget:self action:@selector(handlePan:)];
        pan.delegate = self;
        [self.presentedView addGestureRecognizer:pan];
    }
    
    return self;
}

- (instancetype)initWithPresentedViewController:(UIViewController *)presentedViewController
                       presentingViewController:(UIViewController *)presentingViewController {
    self = [super initWithPresentedViewController:presentedViewController presentingViewController:presentingViewController];
    if (self) {
        UIPanGestureRecognizer *pan = [[UIPanGestureRecognizer alloc] initWithTarget:self action:@selector(handlePan:)];
        pan.delegate = self;
        [self.presentedView addGestureRecognizer:pan];
        
        _startPoint = (CGPoint) {
            .x = CGRectGetMidX(self.presentedViewController.popover_startFrame),
            .y = CGRectGetMaxY(self.presentedViewController.popover_startFrame),
//            .y = CGRectGetMaxY(self.presentedViewController.popover_startFrame) + CGRectGetHeight(self.presentedView.frame),
//            .y = CGRectGetMidY(self.presentedViewController.popover_startFrame),
        };
        
        _endPoint = (CGPoint) {
            .x = CGRectGetMidX(self.presentedViewController.popover_endFrame),
            .y = CGRectGetMidY(self.presentedViewController.popover_endFrame),
        };
        
        // if startFrame is above endFrame, direction is down
        if (CGRectGetMidY(self.presentedViewController.popover_startFrame) < CGRectGetMidY(self.presentedViewController.popover_endFrame)) {
            self.direction = PopoverDirectionDown;
        }
        else {
            self.direction = PopoverDirectionUp;
        }
    }
    
    return self;
}

#pragma mark - Getters
- (UIView *)dimmedView {
    if (!_dimmedView) {
        _dimmedView = [[UIView alloc] init];
        _dimmedView.backgroundColor = [UIColor colorWithWhite:0.0 alpha:0.6];
        
        UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(handleTap:)];
        [_dimmedView addGestureRecognizer:tap];
    }
    
    return _dimmedView;
}

#pragma mark - Helper Methods
- (UIScrollView *)scrollViewInView {
    if ([self.presentedView isKindOfClass:[UIScrollView class]]) {
        return (UIScrollView *)self.presentedView;
    }
    
    for (UIView *view in self.presentedView.subviews) {
        if ([view isKindOfClass:[UIScrollView class]]) {
            return (UIScrollView *)view;
        }
    }
    
    return nil;
}

- (void)animateToStart {
    CGPoint endPoint = (CGPoint) {
        .x = CGRectGetMidX(self.presentedViewController.popover_endFrame),
        .y = CGRectGetMidY(self.presentedViewController.popover_endFrame),
    };
    
    [UIView animateWithDuration:0.6
                          delay:0.0
         usingSpringWithDamping:0.8
          initialSpringVelocity:0.8
                        options:UIViewAnimationOptionAllowUserInteraction
                     animations:^{
                         self.presentedView.transform = CGAffineTransformMakeScale(1.0, 1.0);
                         self.presentedView.center = endPoint;
                         self.presentedView.alpha = 1.0;
                     }
                     completion:NULL];
}

double GetXFromStraightLine(CGPoint start, CGPoint end, double y) {
    double gradient = (end.y - start.y) / (end.x - start.x);
    double x = ((y - start.y) / gradient) + start.x;
    return x;
}

#pragma mark - Presentation Life Cycle
- (void)presentationTransitionWillBegin {
    self.dimmedView.frame = self.containerView.bounds;
    self.dimmedView.alpha = 0.0;
    
    [self.containerView addSubview:self.dimmedView];
    [self.containerView addSubview:[self presentedView]];
    
    [self.presentingViewController.transitionCoordinator animateAlongsideTransition:^(id<UIViewControllerTransitionCoordinatorContext> context) {
        self.dimmedView.alpha = 1.0;
    } completion:NULL];
}

- (void)presentationTransitionDidEnd:(BOOL)completed {
    if (!completed) {
        [self.dimmedView removeFromSuperview];
    }
}

- (void)dismissalTransitionWillBegin {
    [self.presentingViewController.transitionCoordinator animateAlongsideTransition:^(id<UIViewControllerTransitionCoordinatorContext> context) {
        self.dimmedView.alpha = 0.0;
    } completion:NULL];
}

- (void)dismissalTransitionDidEnd:(BOOL)completed {
    if (completed) {
        [self.dimmedView removeFromSuperview];
    }
}

#pragma mark - Content Container
- (void)viewWillTransitionToSize:(CGSize)size withTransitionCoordinator:(id<UIViewControllerTransitionCoordinator>)coordinator {
    [super viewWillTransitionToSize:size withTransitionCoordinator:coordinator];
    
    [coordinator animateAlongsideTransition:^(id<UIViewControllerTransitionCoordinatorContext> context) {
        self.dimmedView.frame = self.containerView.bounds;
    } completion:NULL];
}

#pragma mark - Gesture
- (void)handleTap:(UITapGestureRecognizer *)tap {
    [self.presentedViewController dismissViewControllerAnimated:YES completion:NULL];
}

- (void)handlePan:(UIPanGestureRecognizer *)pan {
    CGPoint startLocation = [pan locationInView:PopoverKeyWindow];
    
    if (pan.state == UIGestureRecognizerStateBegan) {
        UIScrollView *scrollView = [self scrollViewInView];
        if (scrollView) {
            scrollView.scrollEnabled = NO;
        }
        
        [self.presentedView endEditing:YES];
        _dragStartLocation = startLocation.y;
    }
    else if (pan.state == UIGestureRecognizerStateChanged) {
        CGFloat scale;
        if (self.direction == PopoverDirectionUp) {
            scale = _endPoint.y / self.presentedView.center.y;
        }
        else {
            scale = self.presentedView.center.y / _endPoint.y;
        }
        
        if (scale < 0.1 || scale > 1.2) {
            return;
        }
        
        CGPoint translation = [pan translationInView:PopoverKeyWindow];
        CGPoint location = [pan locationInView:PopoverKeyWindow];
        
        // 80% of speed
        translation.y *= 0.8;
        
        CGPoint newCenter = (CGPoint) {
            .x = GetXFromStraightLine(_startPoint, _endPoint, self.presentedView.center.y),
            .y = _endPoint.y + (location.y - _dragStartLocation),
        };
        
        [UIView animateWithDuration:0.3
                              delay:0.0
             usingSpringWithDamping:0.8
              initialSpringVelocity:0.8
                            options:UIViewAnimationOptionCurveEaseIn
                         animations:^{
                             self.presentedView.transform = CGAffineTransformMakeScale(scale, scale);
                             self.presentedView.layer.position = newCenter;
                             self.presentedView.alpha = scale;
                             self.dimmedView.alpha = scale;
                         }
                         completion:NULL];
    }
    else if (pan.state == UIGestureRecognizerStateEnded) {
        UIScrollView *scrollView = [self scrollViewInView];
        if (scrollView) {
            scrollView.scrollEnabled = YES;
        }
        
        if ([pan velocityInView:self.presentedView].y > 0.0) {
            // velocity down
            if (self.direction == PopoverDirectionDown) {
                [self animateToStart];
            }
            else {
                [self.presentedViewController dismissViewControllerAnimated:YES completion:NULL];
            }
        }
        else {
            // velocity up
            if (self.direction == PopoverDirectionDown) {
                [self.presentedViewController dismissViewControllerAnimated:YES completion:NULL];
            }
            else {
                [self animateToStart];
            }
        }
    }
    
}

#pragma mark - Gesture Recognizer Delegate
- (BOOL)gestureRecognizer:(UIGestureRecognizer *)gestureRecognizer shouldRecognizeSimultaneouslyWithGestureRecognizer:(UIGestureRecognizer *)otherGestureRecognizer {
    
    CGFloat _offset = 0.0;
    
    UIScrollView *scrollView = [self scrollViewInView];
    if (scrollView) {
        if (CGSizeEqualToSize(scrollView.contentSize, self.presentedView.frame.size)) {
            return YES;
        }
        
        UIPanGestureRecognizer *pan = (UIPanGestureRecognizer *)gestureRecognizer;
        CGFloat velocityY = [pan velocityInView:self.presentedView].y;
        CGFloat offsetY = scrollView.contentOffset.y;
        
        BOOL vel, off;
//        if (_direction == PanAnimationDirectionDown) {
            vel = velocityY > 0.0;
            off = offsetY <= _offset;
//        }
//        else {
//            vel = velocityY < 0.0;
//            off = offsetY >= _offset;
//        }
        
        return off && vel;
    }
    
    return YES;
}

@end
