//
//  PopoverPresentationController.h
//  Fanta
//
//  Created by Mario Concilio on 9/3/15.
//  Copyright (c) 2015 Mario Concilio. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PopoverPresentationController : UIPresentationController

@end
