//
//  UIViewController+PopoverTransition.m
//  Fanta
//
//  Created by Mario Concilio on 9/3/15.
//  Copyright (c) 2015 Mario Concilio. All rights reserved.
//

#import "UIViewController+PopoverTransition.h"
#import <objc/runtime.h>

#define PopoverKeyWindow     [[UIApplication sharedApplication] keyWindow]

@implementation UIViewController (PopoverTransition)

static const void *StartFrameKey = &StartFrameKey;
static const void *EndFrameKey = &EndFrameKey;
static const void *DirectionKey = &DirectionKey;
static const void *ScalableKey = &ScalableKey;

#pragma mark - Getters & Setters
#pragma mark popover_startFrame
- (void)setPopover_startFrame:(CGRect)popover_startFrame {
    objc_setAssociatedObject(self, StartFrameKey, [NSValue valueWithCGRect:popover_startFrame], OBJC_ASSOCIATION_RETAIN_NONATOMIC);
}

- (CGRect)popover_startFrame {
    NSValue *value = objc_getAssociatedObject(self, StartFrameKey);
    if (!value) {
        return CGRectZero;
    }
    
    return [value CGRectValue];
}

#pragma mark popover_endFrame
- (void)setPopover_endFrame:(CGRect)popover_endFrame {
    objc_setAssociatedObject(self, EndFrameKey, [NSValue valueWithCGRect:popover_endFrame], OBJC_ASSOCIATION_RETAIN_NONATOMIC);
}

- (CGRect)popover_endFrame {
    NSValue *value = objc_getAssociatedObject(self, EndFrameKey);
    if (!value) {
        return PopoverKeyWindow.frame;
    }
    
    return [value CGRectValue];
}

/*
#pragma mark popover_direction
- (void)setPopover_direction:(PopoverDirection)popover_direction {
    objc_setAssociatedObject(self, DirectionKey, @(popover_direction), OBJC_ASSOCIATION_ASSIGN);
}

- (PopoverDirection)popover_direction {
    NSValue *value = objc_getAssociatedObject(self, DirectionKey);
    if (!value) {
        return PopoverDirectionDown;
    }
    
    return (PopoverDirection) value;
}
 */

#pragma mark popover_scalable
- (void)setPopover_scalable:(BOOL)popover_scalable {
    objc_setAssociatedObject(self, ScalableKey, @(popover_scalable), OBJC_ASSOCIATION_RETAIN_NONATOMIC);
}

- (BOOL)popover_isScalable {
    NSNumber *value = objc_getAssociatedObject(self, ScalableKey);
    if (value == nil) {
        return YES;
    }
    
    return [value boolValue];
}

@end
