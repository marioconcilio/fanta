//
//  PopoverProtocol.h
//  Fanta
//
//  Created by Mario Concilio on 9/4/15.
//  Copyright (c) 2015 Mario Concilio. All rights reserved.
//

#import <Foundation/Foundation.h>

@protocol PopoverProtocol <NSObject, UIViewControllerTransitioningDelegate>

@optional
- (CGRect)popoverTransitionStartFrame;
- (CGRect)popoverTransitionEndFrame;
- (BOOL)popoverTransitionShouldScale;

@required
- (UIPresentationController *)presentationControllerForPresentedViewController:(UIViewController *)presented
                                                      presentingViewController:(UIViewController *)presenting
                                                          sourceViewController:(UIViewController *)source;

- (id <UIViewControllerAnimatedTransitioning>)animationControllerForPresentedController:(UIViewController *)presented
                                                                   presentingController:(UIViewController *)presenting
                                                                       sourceController:(UIViewController *)source;

- (id <UIViewControllerAnimatedTransitioning>)animationControllerForDismissedController:(UIViewController *)dismissed;

@end
