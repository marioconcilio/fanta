//
//  PopoverNavigationController.h
//  Fanta
//
//  Created by Mario Concilio on 9/8/15.
//  Copyright (c) 2015 Mario Concilio. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PopoverNavigationController : UINavigationController

@end
