//
//  PopoverNavigationController.m
//  Fanta
//
//  Created by Mario Concilio on 9/8/15.
//  Copyright (c) 2015 Mario Concilio. All rights reserved.
//

#import "PopoverNavigationController.h"
#import "PopoverTransition.h"

@interface PopoverNavigationController () <PopoverProtocol>

@end

@implementation PopoverNavigationController

#pragma mark - Init
- (instancetype)init {
    self = [super init];
    if (self) {
        [self commonInit];
    }
    
    return self;
}

- (instancetype)initWithCoder:(NSCoder *)aDecoder {
    self = [super initWithCoder:aDecoder];
    if (self) {
        [self commonInit];
    }
    
    return self;
}

- (instancetype)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil {
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        [self commonInit];
    }
    
    return self;
}

- (instancetype)initWithRootViewController:(UIViewController *)rootViewController {
    self = [super initWithRootViewController:rootViewController];
    if (self) {
        [self commonInit];
    }
    
    return self;
}

- (instancetype)initWithNavigationBarClass:(Class)navigationBarClass toolbarClass:(Class)toolbarClass {
    self = [super initWithNavigationBarClass:navigationBarClass toolbarClass:toolbarClass];
    if (self) {
        [self commonInit];
    }
    
    return self;
}

- (void)commonInit {
    self.modalPresentationStyle = UIModalPresentationCustom;
    self.transitioningDelegate = self;
}

#pragma mark - View Life Cycle
- (void)viewDidLoad {
    [super viewDidLoad];
    
    CGFloat s = CGRectGetHeight([[UIApplication sharedApplication] statusBarFrame]);
    CGFloat h = CGRectGetHeight(self.navigationBar.frame);
    self.popover_startFrame = CGRectMake(0,
                                         UIKeyWindowHeight(),
                                         UIKeyWindowWidth(),
                                         UIKeyWindowHeight() - s - h);
    
    self.popover_endFrame = CGRectMake(0,
                                       s + h + 20.0,
                                       UIKeyWindowWidth(),
                                       UIKeyWindowHeight() - s - h);
    
    self.popover_scalable = NO;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Popover Protocol
- (UIPresentationController *)presentationControllerForPresentedViewController:(UIViewController *)presented
                                                      presentingViewController:(UIViewController *)presenting
                                                          sourceViewController:(UIViewController *)source {
    return [[PopoverPresentationController alloc] initWithPresentedViewController:presented
                                                         presentingViewController:presenting];
}

- (id <UIViewControllerAnimatedTransitioning>)animationControllerForPresentedController:(UIViewController *)presented
                                                                   presentingController:(UIViewController *)presenting
                                                                       sourceController:(UIViewController *)source {
    return [[PopoverAnimationController alloc] init];
}

- (id <UIViewControllerAnimatedTransitioning>)animationControllerForDismissedController:(UIViewController *)dismissed {
    return [[PopoverAnimationController alloc] init];
}

- (BOOL)popoverTransitionShouldScale {
    return NO;
}

@end
