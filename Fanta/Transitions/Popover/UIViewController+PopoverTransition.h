//
//  UIViewController+PopoverTransition.h
//  Fanta
//
//  Created by Mario Concilio on 9/3/15.
//  Copyright (c) 2015 Mario Concilio. All rights reserved.
//

#import <UIKit/UIKit.h>

typedef NS_ENUM(NSInteger, PopoverDirection) {
    PopoverDirectionDown,
    PopoverDirectionUp,
};

@interface UIViewController (PopoverTransition)

/// default value is CGRectZero
@property (nonatomic, assign) CGRect popover_startFrame;

/// default value is keyWindow.frame
@property (nonatomic, assign) CGRect popover_endFrame;

/// default value is PopoverDirectionDown
//@property (nonatomic, assign) PopoverDirection popover_direction;

/// default value is YES
@property (nonatomic, assign, getter=popover_isScalable) BOOL popover_scalable;

@end
