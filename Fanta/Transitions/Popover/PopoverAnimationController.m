//
//  PopoverAnimationController.m
//  Fanta
//
//  Created by Mario Concilio on 9/3/15.
//  Copyright (c) 2015 Mario Concilio. All rights reserved.
//

#import "PopoverAnimationController.h"
#import "UIViewController+PopoverTransition.h"
#import "PopoverProtocol.h"

#import <SWRevealViewController.h>

#define PopoverKeyWindow        [[UIApplication sharedApplication] keyWindow]

@interface PopoverAnimationController ()

@property (nonatomic, assign) PopoverDirection direction;

@end

@implementation PopoverAnimationController

- (instancetype)init {
    self = [super init];
    if (self) {
        _duration = 0.6;
        _dampingRatio = 0.8;
        _springVelocity = 0.1;
    }
    
    return self;
}

#pragma mark - Helper
- (PopoverDirection)getDirectionStartFrame:(CGRect)startFrame endFrame:(CGRect)endFrame {
    // if startFrame is above endFrame, direction is down
    if (CGRectGetMidY(startFrame) < CGRectGetMidY(endFrame)) {
        return PopoverDirectionDown;
    }
    
    return PopoverDirectionUp;
}

#pragma mark - View Controller Animated Transitioning
- (NSTimeInterval)transitionDuration:(id<UIViewControllerContextTransitioning>)transitionContext {
    return self.duration;
}

- (void)animateTransition:(id<UIViewControllerContextTransitioning>)transitionContext {
    UIViewController *toController = [transitionContext viewControllerForKey:UITransitionContextToViewControllerKey];
    
    if ([toController isBeingPresented]) {
        if ([toController popover_isScalable]) {
            [self animateScalablePresentation:transitionContext];
        }
        else {
            [self animateNonScalablePresentation:transitionContext];
        }
    }
    else {
        UIViewController *fromController = [transitionContext viewControllerForKey:UITransitionContextFromViewControllerKey];
        
        if ([fromController popover_isScalable]) {
            [self animateScalableDismissal:transitionContext];
        }
        else {
            [self animateNonScalableDismissal:transitionContext];
        }
    }
}

#pragma mark - Scalable Transitioning
- (void)animateScalablePresentation:(id<UIViewControllerContextTransitioning>)transitionContext {
//    SWRevealViewController *vc = (SWRevealViewController *) [transitionContext viewControllerForKey:UITransitionContextFromViewControllerKey];
//    UINavigationController *nav = (UINavigationController *) vc.frontViewController;
//    UIViewController *sourceViewController = nav.topViewController;
    UIViewController<PopoverProtocol> *presentedController = (UIViewController<PopoverProtocol> *) [transitionContext viewControllerForKey:UITransitionContextToViewControllerKey];
    UIView *presentedView = [transitionContext viewForKey:UITransitionContextToViewKey];
    UIView *containerView = [transitionContext containerView];
    
    self.direction = [self getDirectionStartFrame:presentedController.popover_startFrame endFrame:presentedController.popover_endFrame];
    
//    presentedView.frame = [transitionContext finalFrameForViewController:presentedController];
    
    // resize view to final frame
    presentedView.frame = presentedController.popover_endFrame;
    
//    presentedView.frame = [presentedController popoverTransitionEndFrame];
    
    // scale to 10%
    presentedView.transform = CGAffineTransformMakeScale(0.1, 0.1);
    
    // position according to direction
//    CGFloat y;
//    if (self.direction == PopoverDirectionDown) {
//        y = CGRectGetMaxY(presentedController.popover_startFrame) + CGRectGetHeight(presentedView.frame);
//    }
//    else {
//        y = CGRectGetMinY(presentedController.popover_startFrame) - CGRectGetHeight(presentedView.frame);
//    }
    
    presentedView.center = (CGPoint) {
        .x = CGRectGetMidX(presentedController.popover_startFrame),
        .y = CGRectGetMidY(presentedController.popover_startFrame),
    };
    
    [containerView addSubview:presentedView];
    
    CGPoint endPoint = (CGPoint) {
        .x = CGRectGetMidX(presentedController.popover_endFrame),
        .y = CGRectGetMidY(presentedController.popover_endFrame),
    };
    
    [UIView animateWithDuration:self.duration
                          delay:0.0
         usingSpringWithDamping:self.dampingRatio
          initialSpringVelocity:self.springVelocity
                        options:UIViewAnimationOptionAllowUserInteraction
                     animations:^{
                         presentedView.transform = CGAffineTransformMakeScale(1.0, 1.0);
                         presentedView.center = endPoint;
                     }
                     completion:^(BOOL finished) {
                         [transitionContext completeTransition:finished];
                     }];
}

- (void)animateScalableDismissal:(id<UIViewControllerContextTransitioning>)transitionContext {
    id vc = [transitionContext viewControllerForKey:UITransitionContextToViewControllerKey];
    
    UIViewController *sourceViewController;
    if ([vc isKindOfClass:[SWRevealViewController class]]) {
        SWRevealViewController *reveal = vc;
        UINavigationController *nav = (UINavigationController *) reveal.frontViewController;
        sourceViewController = nav.topViewController;
    }
    else if ([vc isKindOfClass:[UIViewController class]]) {
        sourceViewController = vc;
    }
    // is UINavigationViewController
    else {
        UINavigationController *nav = vc;
        sourceViewController = nav.topViewController;
    }

    UIViewController *presentedController = [transitionContext viewControllerForKey:UITransitionContextFromViewControllerKey];
    UIView *presentedView = [transitionContext viewForKey:UITransitionContextFromViewKey];
    
    CGPoint endPoint = (CGPoint) {
        .x = CGRectGetMidX(presentedController.popover_startFrame),
        .y = CGRectGetMidY(presentedController.popover_startFrame),
    };
    
    [sourceViewController beginAppearanceTransition:YES animated:YES];
    
    [UIView animateWithDuration:self.duration
                          delay:0.0
         usingSpringWithDamping:self.dampingRatio
          initialSpringVelocity:self.springVelocity
                        options:UIViewAnimationOptionAllowUserInteraction
                     animations:^{
                         presentedView.transform = CGAffineTransformMakeScale(0.1, 0.1);
                         presentedView.center = endPoint;
                         presentedView.alpha = 0.0;
                     }
                     completion:^(BOOL finished) {
                         if (finished) {
                             [presentedView removeFromSuperview];
                             [presentedController removeFromParentViewController];
                             [sourceViewController endAppearanceTransition];
                         }
                         
                         [transitionContext completeTransition:finished];
                     }];
}

#pragma mark - Non-Scalable Transitioning
- (void)animateNonScalablePresentation:(id<UIViewControllerContextTransitioning>)transitionContext {
    UIViewController *presentedController = [transitionContext viewControllerForKey:UITransitionContextToViewControllerKey];
    UIView *presentedView = [transitionContext viewForKey:UITransitionContextToViewKey];
    UIView *containerView = [transitionContext containerView];
    
    self.direction = [self getDirectionStartFrame:presentedController.popover_startFrame endFrame:presentedController.popover_endFrame];
    
//    presentedView.frame = [transitionContext finalFrameForViewController:presentedController];
    
    // resize view to final frame
//    presentedView.frame = presentedController.popover_endFrame;
    presentedView.frame = presentedController.popover_startFrame;
    
    // scale to 10%
//    presentedView.transform = CGAffineTransformMakeScale(0.1, 0.1);
    
    // position according to direction
//    CGFloat y;
    //    if (_direction == PopoverDirectionDown) {
    if (self.direction == PopoverDirectionDown) {
//        y = CGRectGetMaxY(presentedController.popover_startFrame) + CGRectGetHeight(presentedView.frame);
    }
    else {
//        y = CGRectGetMinY(presentedController.popover_startFrame) - CGRectGetHeight(presentedView.frame);
    }
    
//    presentedView.center = (CGPoint) {
//        .x = CGRectGetMidX(presentedController.popover_startFrame),
//        .y = y,
//    };;
    
    [containerView addSubview:presentedView];
    
    CGPoint endPoint = (CGPoint) {
        .x = CGRectGetMidX(presentedController.popover_endFrame),
        .y = CGRectGetMidY(presentedController.popover_endFrame),
    };
    
    [UIView animateWithDuration:self.duration
                          delay:0.0
         usingSpringWithDamping:self.dampingRatio
          initialSpringVelocity:self.springVelocity
                        options:UIViewAnimationOptionAllowUserInteraction
                     animations:^{
                         presentedView.center = endPoint;
                     }
                     completion:^(BOOL finished) {
                         [transitionContext completeTransition:finished];
                     }];
}

- (void)animateNonScalableDismissal:(id<UIViewControllerContextTransitioning>)transitionContext {
    SWRevealViewController *vc = (SWRevealViewController *) [transitionContext viewControllerForKey:UITransitionContextToViewControllerKey];
    UINavigationController *nav = (UINavigationController *) vc.frontViewController;
    UIViewController *sourceViewController = nav.topViewController;
    UIViewController *presentedController = [transitionContext viewControllerForKey:UITransitionContextFromViewControllerKey];
    UIView *presentedView = [transitionContext viewForKey:UITransitionContextFromViewKey];
//    UIView *containerView = [transitionContext containerView];
    
    self.direction = [self getDirectionStartFrame:presentedController.popover_startFrame endFrame:presentedController.popover_endFrame];
    
    CGPoint endPoint = (CGPoint) {
        .x = CGRectGetMidX(presentedController.popover_startFrame),
        .y = CGRectGetMaxY(presentedController.popover_startFrame),
    };
    
    [sourceViewController beginAppearanceTransition:YES animated:YES];
    
    [UIView animateWithDuration:self.duration
                          delay:0.0
         usingSpringWithDamping:self.dampingRatio
          initialSpringVelocity:self.springVelocity
                        options:UIViewAnimationOptionAllowUserInteraction
                     animations:^{
                         presentedView.center = endPoint;
                     }
                     completion:^(BOOL finished) {
                         if (finished) {
                             [presentedView removeFromSuperview];
                             [presentedController removeFromParentViewController];
                             [sourceViewController endAppearanceTransition];
                         }
                         
                         [transitionContext completeTransition:finished];
                     }];
}

@end
