//
//  ExpandCellTransition.h
//  Fanta
//
//  Created by Mario Concilio on 9/9/15.
//  Copyright (c) 2015 Mario Concilio. All rights reserved.
//

#ifndef Fanta_ExpandCellTransition_h
#define Fanta_ExpandCellTransition_h

#import "ExpandCellPresentationController.h"
#import "ExpandCellAnimationController.h"
#import "UIViewController+ExpandCellTransition.h"

#endif
