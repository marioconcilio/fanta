//
//  PostNode.m
//  Fanta
//
//  Created by Mario Concilio on 03/04/15.
//  Copyright (c) 2015 Mario Concilio. All rights reserved.
//

#import "PostNode.h"
#import "Constants.h"
#import "GradientNode.h"
#import "VOPost.h"
#import <WebASDKImageManager.h>
#import <AsyncDisplayKit.h>
#import <ASDisplayNode+Subclasses.h>
#import <POP.h>

@interface PostNode() {
//    GradientNode *_gradientNode;
//    ASNetworkImageNode *_imageNode;
    ASImageNode *_imageNode;
//    ASTextNode *_titleNode;
//    ASTextNode *_contentNode;
//    CGFloat titlePositionX;
//    CGFloat titlePositionY;
    CGFloat contentPositionX;
    CGFloat contentPositionY;
}

@end

@implementation PostNode

- (instancetype)initWithImage:(UIImage *)image {
    self = [super init];
    if (self) {
//        _imageNode = [[ASNetworkImageNode alloc] initWithWebImage];
//        _imageNode.URL = [NSURL URLWithString:post.postImage];
        _imageNode = [[ASImageNode alloc] init];
        _imageNode.image = image;
        _imageNode.contentMode = UIViewContentModeScaleAspectFill;
//        _imageNode.delegate = self;
        _imageNode.layerBacked = YES;
        
//        _titleNode = [[ASTextNode alloc] init];
//        _titleNode.attributedString = [[NSAttributedString alloc] initWithString:@"Título do Post"
//                                                                      attributes:[self titleStyle]];
//        _titleNode.layerBacked = YES;
        
//        _contentNode = [[ASTextNode alloc] init];
//        _contentNode.attributedString = [[NSAttributedString alloc] initWithString:post.content
//                                                                        attributes:[self contentStyle]];
//        _contentNode.layerBacked = YES;
//        
//        _gradientNode = [[GradientNode alloc] init];
//        _gradientNode.layerBacked = YES;
//        _gradientNode.opaque = NO;
        
        [self addSubnode:_imageNode];
//        [self addSubnode:_gradientNode];
//        [self addSubnode:_titleNode];
//        [self addSubnode:_contentNode];
    }
    
    return self;
}

- (CGSize)calculateSizeThatFits:(CGSize)constrainedSize {
//    CGSize titleSize = [_titleNode measure:CGSizeMake(constrainedSize.width - (2 * kPostImageNodePadding),
//                                                      constrainedSize.height)];
//    CGSize contentSize = [_contentNode measure:CGSizeMake(constrainedSize.width - (2 * kPostImageNodePadding),
//                                                          constrainedSize.height)];
    
    CGFloat imageWidth = constrainedSize.width;
    CGFloat imageHeight = imageWidth * kPostImageNodeHeightScale;
    //    CGFloat height = MAX(titleSize.height + contentSize.height, imageHeight);
    
    //    CGSize profileNameSize = [_profileNameNode measure:CGSizeMake(constrainedSize.width, constrainedSize.height)];
    //    CGSize profileImageSize = CGSizeMake(kProfileThumbSize, kProfileThumbSize);
    //    CGSize footerSize = CGSizeMake(constrainedSize.width - (2 * kPadding),
    //                                   MAX(profileNameSize.height, profileImageSize.height) + (3 * kPadding));
    
    return CGSizeMake(constrainedSize.width,
                      imageHeight);
}

- (void)layout {
    [super layout];
    
    CGFloat imageWidth = self.calculatedSize.width;
    CGFloat imageHeight = imageWidth * kPostImageNodeHeightScale;
//    CGSize titleSize = _titleNode.calculatedSize;
//    CGSize contentSize = _contentNode.calculatedSize;
//    _contentNode.frame = CGRectMake(kPostImageNodePadding,
//                                    imageHeight - kPostImageNodePadding - contentSize.height,
//                                    contentSize.width,
//                                    contentSize.height);
    
//    _titleNode.frame = CGRectMake(kPostImageNodePadding,
//                                  imageHeight - kPostImageNodePadding - contentSize.height - titleSize.height,
//                                  titleSize.width,
//                                  titleSize.height);
    
    _imageNode.frame = CGRectMake(0.0,
                                  0.0,
                                  imageWidth,
                                  imageHeight);
    
//    _gradientNode.frame = CGRectMake(0.0,
//                                     CGRectGetMinY(_contentNode.frame),
//                                     imageWidth,
//                                     CGRectGetMaxY(_imageNode.frame) - CGRectGetMinY(_contentNode.frame));
    
//    self.frame = CGRectMake(kPostImageNodePadding,
//                            kPostImageNodePadding,
//                            imageWidth,
//                            imageHeight);
    
//    titlePositionX = _titleNode.position.x;
//    titlePositionY = _titleNode.position.y;
//    contentPositionX = _contentNode.position.x;
//    contentPositionY = _contentNode.position.y;
}

#pragma mark - Getters
- (UIImage *)image {
    return _imageNode.image;
}

#pragma mark - Helper Methods
- (NSDictionary *)titleStyle {
    return @{NSFontAttributeName: [UIFont boldFontWithSize:20.0],
             NSForegroundColorAttributeName: [UIColor whiteColor]};
}

- (NSDictionary *)contentStyle {
    return @{NSFontAttributeName: [UIFont lightFontWithSize:15.0],
             NSForegroundColorAttributeName: [UIColor whiteColor]};
}

- (void)animateOnScrollContentOffset:(CGPoint)contentOffset {
//    CGFloat titleX = _titleNode.layer.position.x;
//    CGFloat contentX = _contentNode.layer.position.x;
    
//    _titleNode.layer.position = (CGPoint) {
//        .x = titlePositionX,
//        .y = titlePositionY - (contentOffset.y / 10),
//    };
    
//    _contentNode.layer.position = (CGPoint) {
//        .x = contentPositionX,
//        .y = contentPositionY - (contentOffset.y / 10),
//    };
}

#pragma mark - Network ImageNode Delegate
- (void)imageNode:(ASNetworkImageNode *)imageNode didLoadImage:(UIImage *)image {
//    POPBasicAnimation *fadeInAnimation = [POPBasicAnimation animationWithPropertyNamed:kPOPViewAlpha];
//    fadeInAnimation.timingFunction = [CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseOut];
//    fadeInAnimation.duration = 0.2;
//    fadeInAnimation.fromValue = @(0.0);
//    fadeInAnimation.toValue = @(1.0);
//    
//    dispatch_async(dispatch_get_main_queue(), ^{
//        [self pop_addAnimation:fadeInAnimation forKey:@"fadeIn"];
//    });
}

@end
