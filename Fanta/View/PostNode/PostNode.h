//
//  PostNode.h
//  Fanta
//
//  Created by Mario Concilio on 03/04/15.
//  Copyright (c) 2015 Mario Concilio. All rights reserved.
//

#import "ASControlNode.h"

@class VOPost;
@interface PostNode : ASControlNode

//- (instancetype)initWithPost:(VOPost *)post;
- (instancetype)initWithImage:(UIImage *)image;
- (void)animateOnScrollContentOffset:(CGPoint)contentOffset;
- (UIImage *)image;

@end
