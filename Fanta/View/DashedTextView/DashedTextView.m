//
//  DashedTextView.m
//  Fanta
//
//  Created by Mario Concilio on 10/13/15.
//  Copyright © 2015 Mario Concilio. All rights reserved.
//

#import "DashedTextView.h"

@implementation DashedTextView {
    CAShapeLayer *_shapeLayer;
}

- (void)setFrame:(CGRect)frame {
    [super setFrame:frame];
    [self drawRect:frame];
}

- (void)drawRect:(CGRect)rect {
    //border definitions
    CGFloat cornerRadius = 5.0;
    CGFloat borderWidth = 1.0;
    
//    CGFloat cornerRadius = self.layer.cornerRadius;
//    CGFloat borderWidth = self.layer.borderWidth;
    NSInteger dashPattern1 = 4.0;
    NSInteger dashPattern2 = 4.0;
    UIColor *lineColor = [UIColor customLightBackground];
    
    //drawing
    CGRect frame = self.bounds;
    
    if (_shapeLayer) {
        [_shapeLayer removeFromSuperlayer];
        _shapeLayer = nil;
    }
    
    _shapeLayer = [CAShapeLayer layer];
    
    //creating a path
    CGMutablePathRef path = CGPathCreateMutable();
    
    //drawing a border around a view
    CGPathMoveToPoint(path, NULL, 0, frame.size.height - cornerRadius);
    CGPathAddLineToPoint(path, NULL, 0, cornerRadius);
    CGPathAddArc(path, NULL, cornerRadius, cornerRadius, cornerRadius, M_PI, -M_PI_2, NO);
    CGPathAddLineToPoint(path, NULL, frame.size.width - cornerRadius, 0);
    CGPathAddArc(path, NULL, frame.size.width - cornerRadius, cornerRadius, cornerRadius, -M_PI_2, 0, NO);
    CGPathAddLineToPoint(path, NULL, frame.size.width, frame.size.height - cornerRadius);
    CGPathAddArc(path, NULL, frame.size.width - cornerRadius, frame.size.height - cornerRadius, cornerRadius, 0, M_PI_2, NO);
    CGPathAddLineToPoint(path, NULL, cornerRadius, frame.size.height);
    CGPathAddArc(path, NULL, cornerRadius, frame.size.height - cornerRadius, cornerRadius, M_PI_2, M_PI, NO);
    
    //path is set as the _shapeLayer object's path
    _shapeLayer.path = path;
    CGPathRelease(path);
    
    _shapeLayer.backgroundColor = [[UIColor clearColor] CGColor];
    _shapeLayer.frame = frame;
    _shapeLayer.masksToBounds = NO;
    [_shapeLayer setValue:[NSNumber numberWithBool:NO] forKey:@"isCircle"];
    _shapeLayer.fillColor = [[UIColor clearColor] CGColor];
    _shapeLayer.strokeColor = [lineColor CGColor];
    _shapeLayer.lineWidth = borderWidth;
    _shapeLayer.lineDashPattern = @[@(dashPattern1), @(dashPattern2)];
    _shapeLayer.lineCap = kCALineCapRound;
    
    //_shapeLayer is added as a sublayer of the view, the border is visible
    [self.layer addSublayer:_shapeLayer];
    self.layer.cornerRadius = cornerRadius;
}

@end
