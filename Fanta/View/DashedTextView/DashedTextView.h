//
//  DashedTextView.h
//  Fanta
//
//  Created by Mario Concilio on 10/13/15.
//  Copyright © 2015 Mario Concilio. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface DashedTextView : UITextView

@property (nonatomic, assign) CGFloat dashedCornerRadius;
@property (nonatomic, assign) CGFloat borderWidth;
@property (nonatomic, strong) NSArray *dashPattern;
@property (nonatomic, strong) UIColor *lineColor;

@end
