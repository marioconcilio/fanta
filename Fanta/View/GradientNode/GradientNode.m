//
//  GradientNode.m
//  Fanta
//
//  Created by Mario Concilio on 23/03/15.
//  Copyright (c) 2015 Mario Concilio. All rights reserved.
//

#import "GradientNode.h"
#import <ASDisplayNode+Subclasses.h>

@implementation GradientNode

+ (void)drawRect:(CGRect)bounds
  withParameters:(id<NSObject>)parameters
     isCancelled:(asdisplaynode_iscancelled_block_t)isCancelledBlock
   isRasterizing:(BOOL)isRasterizing {
    CGContextRef context = UIGraphicsGetCurrentContext();
    
    CGColorSpaceRef colorSpace = CGColorSpaceCreateDeviceRGB();
    CGFloat locations[] = {0.0, 1.0};
    
    CGColorRef startColor = [UIColor blackColor].CGColor;
    CGColorRef endColor = [UIColor clearColor].CGColor;
    
    NSArray *colors = @[(__bridge id) startColor, (__bridge id) endColor];
    CGGradientRef gradient = CGGradientCreateWithColors(colorSpace, (__bridge CFArrayRef) colors, locations);
    
    CGPoint startPoint = CGPointMake(CGRectGetMidX(bounds), CGRectGetMaxY(bounds));
    CGPoint endPoint = CGPointMake(CGRectGetMidX(bounds), CGRectGetMinY(bounds));
    
    CGContextSaveGState(context);
    CGContextAddRect(context, bounds);
    CGContextClip(context);
    CGContextDrawLinearGradient(context, gradient, startPoint, endPoint, 0);
    CGContextRestoreGState(context);
    
    CGGradientRelease(gradient);
    CGColorSpaceRelease(colorSpace);
}

@end
