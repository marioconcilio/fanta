//
//  GradientNode.h
//  Fanta
//
//  Created by Mario Concilio on 23/03/15.
//  Copyright (c) 2015 Mario Concilio. All rights reserved.
//

#import "ASDisplayNode.h"

@interface GradientNode : ASDisplayNode

@end
