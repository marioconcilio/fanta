//
//  FloatLabeledTextField.m
//  Fanta
//
//  Created by Mario Concilio on 8/21/15.
//  Copyright (c) 2015 Mario Concilio. All rights reserved.
//

#import "FloatLabeledTextField.h"

@implementation FloatLabeledTextField

- (instancetype)initWithCoder:(NSCoder *)aDecoder {
    self = [super initWithCoder:aDecoder];
    if (self) {
        self.floatingLabelFont = [UIFont semiboldFontWithSize:10.0];
        self.floatingLabelTextColor = [UIColor lightGrayColor];
        self.clearButtonMode = UITextFieldViewModeWhileEditing;
    }
    
    return self;
}

- (instancetype)initWithPlaceholderText:(NSString *)text {
    self = [super init];
    if (self) {
        [self setupWithPlaceholderText:text];
    }
    
    return self;
}

- (instancetype)initWithFrame:(CGRect)frame placeholderText:(NSString *)text {
    self = [super initWithFrame:frame];
    if (self) {
        [self setupWithPlaceholderText:text];
    }
    
    return self;
}

- (void)setupWithPlaceholderText:(NSString *)placeholderText {
    self.font = [UIFont lightFontWithSize:16.0];
    self.attributedPlaceholder = [[NSAttributedString alloc] initWithString:placeholderText
                                                                 attributes:@{NSForegroundColorAttributeName: [UIColor lightGrayColor]}];
    self.floatingLabelFont = [UIFont semiboldFontWithSize:10.0];
    self.floatingLabelTextColor = [UIColor lightGrayColor];
    self.clearButtonMode = UITextFieldViewModeWhileEditing;
//    pass.borderStyle = UITextBorderStyleRoundedRect;
    self.backgroundColor = [UIColor clearColor];
//    pass.placeholder = @"Senha";
}

// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    [super drawRect:rect];
    
    CGFloat borderWidth = 0.5;
    CALayer *border = [CALayer layer];
    border.borderColor = [UIColor lightGrayColor].CGColor;
    border.frame = CGRectMake(0, self.frame.size.height - borderWidth, self.frame.size.width, self.frame.size.height);
    border.borderWidth = borderWidth;
    [self.layer addSublayer:border];
    self.layer.masksToBounds = YES;
}


@end
