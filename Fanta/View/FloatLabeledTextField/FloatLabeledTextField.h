//
//  FloatLabeledTextField.h
//  Fanta
//
//  Created by Mario Concilio on 8/21/15.
//  Copyright (c) 2015 Mario Concilio. All rights reserved.
//

#import "JVFloatLabeledTextField.h"

@interface FloatLabeledTextField : JVFloatLabeledTextField

- (instancetype)initWithPlaceholderText:(NSString *)text;
- (instancetype)initWithFrame:(CGRect)frame placeholderText:(NSString *)text;

@end
