//
//  BkgView.h
//  Fanta
//
//  Created by Mario Concilio on 23/04/15.
//  Copyright (c) 2015 Mario Concilio. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface BkgView : UIView

@property (nonatomic, strong) UITapGestureRecognizer *tap;

//+ (void)presentBelowView:(UIView *)view target:(id)target action:(SEL)action;
- (instancetype)initWithTarget:(id)target action:(SEL)action;
- (void)addTarget:(id)target action:(SEL)action;

@end
