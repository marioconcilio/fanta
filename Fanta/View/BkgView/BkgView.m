//
//  BkgView.m
//  Fanta
//
//  Created by Mario Concilio on 23/04/15.
//  Copyright (c) 2015 Mario Concilio. All rights reserved.
//

#import "BkgView.h"
#import <POP.h>

//#define kViewTag    89

@interface BkgView () <UIGestureRecognizerDelegate>

@end

@implementation BkgView

- (instancetype)init {
    self = [super initWithFrame:UIKeyWindow().bounds];
    if (self) {
        self.backgroundColor = [UIColor colorWithWhite:0.0 alpha:0.6];
        self.alpha = 0.0;
    }
    
    return self;
}

- (instancetype)initWithTarget:(id)target action:(SEL)action {
    self = [self init];
    if (self) {
        _tap = [[UITapGestureRecognizer alloc] init];
        [_tap addTarget:target action:action];
        _tap.delegate = self;
    }
    
    return self;
}

- (void)layoutSubviews {
    POPBasicAnimation *fadeInAnimation = [POPBasicAnimation animationWithPropertyNamed:kPOPViewAlpha];
    fadeInAnimation.fromValue = @(0.0);
    fadeInAnimation.toValue = @(1.0);
    fadeInAnimation.completionBlock = ^(POPAnimation *anim, BOOL finished) {
        if (finished && _tap) {
            [self addGestureRecognizer:_tap];
        }
    };
    
    [self pop_addAnimation:fadeInAnimation forKey:@"fadeInAnimation"];
}

- (void)removeFromSuperview {
    POPBasicAnimation *fadeInAnimation = [POPBasicAnimation animationWithPropertyNamed:kPOPViewAlpha];
//    fadeInAnimation.fromValue = @(1.0);
    fadeInAnimation.toValue = @(0.0);
    fadeInAnimation.completionBlock = ^(POPAnimation *anim, BOOL finished) {
        if (finished) {
            [super removeFromSuperview];
        }
    };
    
    [self pop_addAnimation:fadeInAnimation forKey:@"fadeInAnimation"];
}

- (void)addTarget:(id)target action:(SEL)action {
    [_tap addTarget:target action:action];
}

#pragma mark - Gesture Delegate
- (BOOL)gestureRecognizer:(UIGestureRecognizer *)gestureRecognizer shouldRecognizeSimultaneouslyWithGestureRecognizer:(UIGestureRecognizer *)otherGestureRecognizer {
    return YES;
}

@end
