//
//  PostImageNode.m
//  Fanta
//
//  Created by Mario Concilio on 01/04/15.
//  Copyright (c) 2015 Mario Concilio. All rights reserved.
//

#import "PostImageNode.h"
#import "VOPost.h"
#import "GradientNode.h"
#import "Constants.h"
#import <WebASDKImageManager.h>
#import <AsyncDisplayKit/ASDisplayNode+Subclasses.h>
#import <POP.h>

@interface PostImageNode() <ASNetworkImageNodeDelegate> {
    GradientNode *_gradientNode;
    ASNetworkImageNode *_imageNode;
//    ASTextNode *_titleNode;
    ASTextNode *_contentNode;
}

@end

@implementation PostImageNode

- (instancetype)initWithPost:(VOPost *)post {
    self = [super init];
    if (self) {
        _imageNode = [[ASNetworkImageNode alloc] initWithWebImage];
        _imageNode.URL = [NSURL URLWithString:post.postImage];
        _imageNode.contentMode = UIViewContentModeScaleAspectFill;
        _imageNode.delegate = self;
        _imageNode.alpha = 0.0;
        _imageNode.layerBacked = YES;
//        self.shouldRasterizeDescendants = YES;

//        _titleNode = [[ASTextNode alloc] init];
//        _titleNode.attributedString = [[NSAttributedString alloc] initWithString:@"Título do Post"
//                                                                      attributes:[self titleStyle]];
//        _titleNode.layerBacked = YES;
        
        _contentNode = [[ASTextNode alloc] init];
        _contentNode.attributedString = [[NSAttributedString alloc] initWithString:post.content
                                                                        attributes:[self contentStyle]];
        _contentNode.layerBacked = YES;
        
        _gradientNode = [[GradientNode alloc] init];
        _gradientNode.layerBacked = YES;
        _gradientNode.opaque = NO;
        
        [self addSubnode:_imageNode];
        [self addSubnode:_gradientNode];
//        [self addSubnode:_titleNode];
        [self addSubnode:_contentNode];

        self.backgroundColor = [UIColor clearColor];
    }
    
    return self;
}

- (CGSize)calculateSizeThatFits:(CGSize)constrainedSize {
#warning Truncar texto title e content
//    CGSize titleSize = [_titleNode measure:CGSizeMake(constrainedSize.width - (4 * kPostImageNodePadding),
//                                                      constrainedSize.height)];
    CGSize contentSize = [_contentNode measure:CGSizeMake(constrainedSize.width - (4 * kPostImageNodePadding),
                                                          constrainedSize.height)];
    CGFloat imageWidth = constrainedSize.width;
    CGFloat imageHeight = imageWidth * kPostImageNodeHeightScale;
//    CGFloat height = MAX(titleSize.height + contentSize.height, imageHeight);
    
//    CGSize profileNameSize = [_profileNameNode measure:CGSizeMake(constrainedSize.width, constrainedSize.height)];
//    CGSize profileImageSize = CGSizeMake(kProfileThumbSize, kProfileThumbSize);
//    CGSize footerSize = CGSizeMake(constrainedSize.width - (2 * kPadding),
//                                   MAX(profileNameSize.height, profileImageSize.height) + (3 * kPadding));
    
    return CGSizeMake(constrainedSize.width,
                      imageHeight);
}

- (void)layout {
    CGFloat imageWidth = self.calculatedSize.width - (2 * kPostImageNodePadding);
    CGFloat imageHeight = imageWidth * kPostImageNodeHeightScale;
//    CGSize titleSize = _titleNode.calculatedSize;
    CGSize contentSize = _contentNode.calculatedSize;
    _contentNode.frame = CGRectMake(kPostImageNodePadding,
                                    imageHeight - kPostImageNodePadding - contentSize.height,
                                    contentSize.width,
                                    contentSize.height);
    
//    _titleNode.frame = CGRectMake(kPostImageNodePadding,
//                                  imageHeight - kPostImageNodePadding - contentSize.height - titleSize.height,
//                                  titleSize.width,
//                                  titleSize.height);
    
    _imageNode.frame = CGRectMake(0.0,
                                  0.0,
                                  imageWidth,
                                  imageHeight);
    
    _gradientNode.frame = CGRectMake(0.0,
                                     CGRectGetMinY(_contentNode.frame),
                                     imageWidth,
                                     CGRectGetMaxY(_imageNode.frame) - CGRectGetMinY(_contentNode.frame));
    
//    self.frame = CGRectMake(kPostImageNodePadding,
//                            kPostImageNodePadding,
//                            imageWidth,
//                            imageHeight);
}

#pragma mark - Helper Methods
- (NSDictionary *)titleStyle {
    return @{NSFontAttributeName: [UIFont boldFontWithSize:20.0],
             NSForegroundColorAttributeName: [UIColor whiteColor]};
}

- (NSDictionary *)contentStyle {
    return @{NSFontAttributeName: [UIFont lightFontWithSize:15.0],
             NSForegroundColorAttributeName: [UIColor whiteColor]};
}

#pragma mark - Network ImageNode Delegate
- (void)imageNode:(ASNetworkImageNode *)imageNode didLoadImage:(UIImage *)image {
    POPBasicAnimation *fadeInAnimation = [POPBasicAnimation animationWithPropertyNamed:kPOPViewAlpha];
    fadeInAnimation.timingFunction = [CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseOut];
    fadeInAnimation.duration = 0.6;
    fadeInAnimation.fromValue = @(0.0);
    fadeInAnimation.toValue = @(1.0);
    
    dispatch_async(dispatch_get_main_queue(), ^{
        [_imageNode pop_addAnimation:fadeInAnimation forKey:@"fadeIn"];
    });
}

@end
