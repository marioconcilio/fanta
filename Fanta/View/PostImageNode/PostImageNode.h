//
//  PostImageNode.h
//  Fanta
//
//  Created by Mario Concilio on 01/04/15.
//  Copyright (c) 2015 Mario Concilio. All rights reserved.
//

#import "ASNetworkImageNode.h"

@class VOPost;
@interface PostImageNode : ASDisplayNode

- (instancetype)initWithPost:(VOPost *)post;

@end
