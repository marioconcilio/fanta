//
//  LoadingView.h
//  Fanta
//
//  Created by Mario Concilio on 5/18/16.
//  Copyright © 2016 Mario Concilio. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface LoadingView : NSObject

+ (void)show;
+ (void)hide;

@end
