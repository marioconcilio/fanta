//
//  LoadingView.m
//  Fanta
//
//  Created by Mario Concilio on 5/18/16.
//  Copyright © 2016 Mario Concilio. All rights reserved.
//

#import "LoadingView.h"
#import "BLMultiColorLoader+DefaultLoader.h"

#pragma mark - Background Window
@interface LoadingBackgroundWindow : UIWindow
@end

@implementation LoadingBackgroundWindow

- (instancetype)initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    if (self) {
        self.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight;
        self.opaque = NO;
        self.windowLevel = UIWindowLevelAlert;
    }
    
    return self;
}

- (void)drawRect:(CGRect)rect {
    CGContextRef context = UIGraphicsGetCurrentContext();
    size_t locationsCount = 2;
    CGFloat locations[2] = {0.0f, 1.0f};
    CGFloat colors[8] = {0.0f, 0.0f, 0.0f, 0.0f, 0.0f, 0.0f, 0.0f, 0.75f};
    CGColorSpaceRef colorSpace = CGColorSpaceCreateDeviceRGB();
    CGGradientRef gradient = CGGradientCreateWithColorComponents(colorSpace, colors, locations, locationsCount);
    CGColorSpaceRelease(colorSpace);
    
    CGPoint center = CGPointMake(self.bounds.size.width / 2, self.bounds.size.height / 2);
    CGFloat radius = MIN(self.bounds.size.width, self.bounds.size.height) ;
    CGContextDrawRadialGradient (context, gradient, center, 0, center, radius, kCGGradientDrawsAfterEndLocation);
    CGGradientRelease(gradient);
}

@end

#pragma mark - Loading View
@interface LoadingView ()

@property (nonatomic, strong) LoadingBackgroundWindow *backgroundWindow;
@property (nonatomic, assign, getter=isVisible) BOOL visible;

@end

@implementation LoadingView

+ (instancetype)sharedView {
    static LoadingView *shared = nil;
    static dispatch_once_t onceToken;
    
    dispatch_once(&onceToken, ^{
        shared = [[self alloc] init];
    });
    
    return shared;
}

+ (void)show {
    if ([[self sharedView] isVisible]) {
        return;
    }
    
    LoadingBackgroundWindow *backgroundWindow = [[LoadingBackgroundWindow alloc] initWithFrame:[UIScreen mainScreen].bounds];
    
    UIView *container = [[UIView alloc] initWithFrame:CGRectMake(0.0, 0.0, 120.0, 120.0)];
    container.layer.cornerRadius = 5.0;
    container.clipsToBounds = YES;
    container.center = backgroundWindow.center;
    
    UIBlurEffect *blurEffect = [UIBlurEffect effectWithStyle:UIBlurEffectStyleLight];
    UIVisualEffectView *blurView = [[UIVisualEffectView alloc] initWithEffect:blurEffect];
    blurView.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight;
    blurView.frame = CGRectMake(0.0, 0.0, 120.0, 120.0);
    
    [container addSubview:blurView];
    
    BLMultiColorLoader *loader = [BLMultiColorLoader defaultLoaderWithSize:CGSizeMake(60.0, 60.0)];
    loader.center = [loader convertPoint:container.center toView:container];
    [container addSubview:loader];
    
    [loader startAnimation];
    
    [backgroundWindow addSubview:container];
    
    [[self sharedView] setBackgroundWindow:backgroundWindow];
    [[[self sharedView] backgroundWindow] makeKeyAndVisible];
    [[self sharedView] setVisible:YES];
}

+ (void)hide {
    if (![[self sharedView] isVisible]) {
        return;
    }
    
    [[[self sharedView] backgroundWindow] setHidden:YES];
    [[self sharedView] setBackgroundWindow:nil];
    [[self sharedView] setVisible:NO];
}

@end
