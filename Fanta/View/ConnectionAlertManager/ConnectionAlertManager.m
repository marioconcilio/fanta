//
//  ConnectionAlertManager.m
//  Fanta
//
//  Created by Mario Concilio on 4/15/16.
//  Copyright © 2016 Mario Concilio. All rights reserved.
//

#import "ConnectionAlertManager.h"
#import <POP.h>

@interface ConnectionAlertManager ()

@property (nonatomic, strong) UILabel *label;

@end

@implementation ConnectionAlertManager

- (instancetype)init {
    self = [super init];
    if (self) {
        _message = @"No internet connection";
        _backgroundColor = [UIColor blackColor];
        _textColor = [UIColor whiteColor];
        _textFont = [UIFont systemFontOfSize:14.0];
        _height = 30.0;
    }
    
    return self;
}

#pragma mark - Shared Instance
+ (instancetype)sharedManager {
    static ConnectionAlertManager *shared = nil;
    static dispatch_once_t onceToken;
    
    dispatch_once(&onceToken, ^{
        shared = [[self alloc] init];
    });
    
    return shared;
}

+ (void)showInNavigationBar:(UINavigationBar *)navbar {
    if (![[self sharedManager] isShowing]) {
        CGFloat navbarHeight = CGRectGetHeight(navbar.frame);
        CGFloat height = [[self sharedManager] height];
        
        UILabel *label = [[UILabel alloc] initWithFrame:CGRectMake(0, navbarHeight-height, UIKeyWindowWidth(), height)];
        label.backgroundColor = [[self sharedManager] backgroundColor];
        label.text = [[self sharedManager] message];
        label.textColor = [[self sharedManager] textColor];
        label.textAlignment = NSTextAlignmentCenter;
        label.font = [[self sharedManager] textFont];
        
        [navbar addSubview:label];
        
        POPBasicAnimation *anim = [POPBasicAnimation animationWithPropertyNamed:kPOPLayerPositionY];
        anim.toValue = @(navbarHeight + height/2);
        [label pop_addAnimation:anim forKey:@"slide_in"];
        
        [[self sharedManager] setLabel:label];
        [[self sharedManager] setShowing:YES];
    }
}

+ (void)hide {
    if ([[self sharedManager] isShowing]) {
        POPBasicAnimation *anim = [POPBasicAnimation animationWithPropertyNamed:kPOPViewAlpha];
        anim.toValue = @(0.0);
        [anim setCompletionBlock:^(POPAnimation *anim, BOOL finished) {
            if (finished) {
                [[[self sharedManager] label] removeFromSuperview];
                [[self sharedManager] setLabel:nil];
                [[self sharedManager] setShowing:NO];
            }
        }];
        
        [[[self sharedManager] label] pop_addAnimation:anim forKey:@"fade_out"];
    }
}

@end
