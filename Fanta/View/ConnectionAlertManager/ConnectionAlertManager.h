//
//  ConnectionAlertManager.h
//  Fanta
//
//  Created by Mario Concilio on 4/15/16.
//  Copyright © 2016 Mario Concilio. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

@interface ConnectionAlertManager : NSObject

@property (nonatomic, strong) NSString *message;
@property (nonatomic, strong) UIColor *backgroundColor;
@property (nonatomic, strong) UIColor *textColor;
@property (nonatomic, strong) UIFont *textFont;
@property (nonatomic, assign) CGFloat height;
@property (nonatomic, assign, getter=isShowing) BOOL showing;

+ (instancetype)sharedManager;
+ (void)showInNavigationBar:(UINavigationBar *)navbar;
+ (void)hide;

@end
