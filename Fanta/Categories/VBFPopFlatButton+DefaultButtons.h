//
//  VBFPopFlatButton+DefaultButtons.h
//  Fanta
//
//  Created by Mario Concilio on 4/7/16.
//  Copyright © 2016 Mario Concilio. All rights reserved.
//

#import <VBFPopFlatButton/VBFPopFlatButton.h>

typedef NS_ENUM(NSInteger, VBFButtonStyle) {
    VBFButtonStyleLight,
    VBFButtonStyleDark
};

@interface VBFPopFlatButton (DefaultButtons)

+ (instancetype)drawerButtonWithFrame:(CGRect)frame style:(VBFButtonStyle)style;
+ (instancetype)drawerButtonLight;
+ (instancetype)drawerButtonDark;

+ (instancetype)closeButtonLight;
+ (instancetype)closeButtonDark;

+ (instancetype)actionButtonLight;
+ (instancetype)actionButtonDark;

@end
