//
//  UIFont+DefaultFont.m
//  Fanta
//
//  Created by Mario Concilio on 22/01/15.
//  Copyright (c) 2015 Mario Concilio. All rights reserved.
//

#import "UIFont+DefaultFont.h"
#import <objc/runtime.h>

@implementation UIFont (DefaultFont)

#pragma mark - Private Methods <Swizzling>
+ (void)load {
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        Class clazz = object_getClass((id) self);
        
        // regular font
        SEL originalSelector = @selector(systemFontOfSize:);
        SEL swizzledSelector = @selector(_regularFontWithSize:);
        
        Method originalMethod = class_getClassMethod(clazz, originalSelector);
        Method swizzledMethod = class_getClassMethod(clazz, swizzledSelector);
        
        BOOL didAddMethod = class_addMethod(clazz,
                                            originalSelector,
                                            method_getImplementation(swizzledMethod),
                                            method_getTypeEncoding(swizzledMethod));
        
        if (didAddMethod) {
            class_replaceMethod(clazz,
                                swizzledSelector,
                                method_getImplementation(originalMethod),
                                method_getTypeEncoding(originalMethod));
        }
        else {
            method_exchangeImplementations(originalMethod, swizzledMethod);
        }
        
        // bold font
        SEL bold_originalSelector = @selector(boldSystemFontOfSize:);
        SEL bold_swizzledSelector = @selector(_boldFontWithSize:);
        
        Method bold_originalMethod = class_getClassMethod(clazz, bold_originalSelector);
        Method bold_swizzledMethod = class_getClassMethod(clazz, bold_swizzledSelector);
        
        BOOL bold_didAddMethod = class_addMethod(clazz,
                                                 bold_originalSelector,
                                                 method_getImplementation(bold_swizzledMethod),
                                                 method_getTypeEncoding(bold_swizzledMethod));
        
        if (bold_didAddMethod) {
            class_replaceMethod(clazz,
                                bold_swizzledSelector,
                                method_getImplementation(bold_originalMethod),
                                method_getTypeEncoding(bold_originalMethod));
        }
        else {
            method_exchangeImplementations(bold_originalMethod, bold_swizzledMethod);
        }
    });
}

+ (UIFont *)_boldFontWithSize:(CGFloat)size {
    return [UIFont fontWithName:@"SourceSansPro-Bold" size:size];
}

+ (UIFont *)_regularFontWithSize:(CGFloat)size {
    return [UIFont fontWithName:@"SourceSansPro-Regular" size:size];
}

#pragma mark - Public Methods
+ (UIFont *)boldFontWithSize:(CGFloat)size {
    return [UIFont fontWithName:@"SourceSansPro-Bold" size:size];
}

+ (UIFont *)extraBoldFontWithSize:(CGFloat)size {
    return [UIFont fontWithName:@"SourceSansPro-Black" size:size];
}

+ (UIFont *)lightFontWithSize:(CGFloat)size {
    return [UIFont fontWithName:@"SourceSansPro-Light" size:size];
}

+ (UIFont *)extraLightFontWithSize:(CGFloat)size {
    return [UIFont fontWithName:@"SourceSansPro-ExtraLight" size:size];
}

+ (UIFont *)regularFontWithSize:(CGFloat)size {
    return [UIFont fontWithName:@"SourceSansPro-Regular" size:size];
}

+ (UIFont *)semiboldFontWithSize:(CGFloat)size {
    return [UIFont fontWithName:@"SourceSansPro-Semibold" size:size];
}

+ (UIFont *)titleFontWithSize:(CGFloat)size {
    return [UIFont fontWithName:@"aspace" size:size];
}

+ (UIFont *)titleLightFontWithSize:(CGFloat)size {
    return [UIFont fontWithName:@"aspaceLight" size:size];
}

@end
