//
//  UIImage+GPUImage.h
//  Fanta
//
//  Created by Mario Concilio on 17/03/15.
//  Copyright (c) 2015 Mario Concilio. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIImage (GPUImage)

+ (UIImage *)takeSnapshot;
- (UIImage *)applyiOSBlurWithRadius:(CGFloat)radius;
- (UIImage *)applyBlurWithRadius:(CGFloat)radius;

@end
