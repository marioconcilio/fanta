//
//  UIImage+GPUImage.m
//  Fanta
//
//  Created by Mario Concilio on 17/03/15.
//  Copyright (c) 2015 Mario Concilio. All rights reserved.
//

#import "UIImage+GPUImage.h"
#import <GPUImage.h>

@implementation UIImage (GPUImage)

+ (UIImage *)takeSnapshot {
    UIGraphicsBeginImageContextWithOptions(UIKeyWindow().bounds.size, NO, 0);
    [UIKeyWindow() drawViewHierarchyInRect:UIKeyWindow().bounds afterScreenUpdates:YES];
    UIImage *snapshot = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    
    return snapshot;
}

- (UIImage *)applyiOSBlurWithRadius:(CGFloat)radius {
    GPUImageiOSBlurFilter *blurFilter = [[GPUImageiOSBlurFilter alloc] init];
    blurFilter.blurRadiusInPixels = radius;
    
    return [blurFilter imageByFilteringImage:self];
}

- (UIImage *)applyBlurWithRadius:(CGFloat)radius {
    GPUImageGaussianBlurFilter *blurFilter = [[GPUImageGaussianBlurFilter alloc] init];
    blurFilter.blurRadiusInPixels = radius;
    
    return [blurFilter imageByFilteringImage:self];
}

@end
