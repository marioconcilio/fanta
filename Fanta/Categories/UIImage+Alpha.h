// UIImage+Alpha.h
// Created by Trevor Harmon on 9/20/09.
// Free for personal or commercial use, with or without modification.
// No warranty is expressed or implied.
// http://vocaro.com/trevor/blog/2009/10/12/resize-a-uiimage-the-right-way/

// Helper methods for adding an alpha layer to an image
@interface UIImage (Alpha)

/**
 *  Tells whether the image has an alpha layer.
 */
- (BOOL)hasAlpha;

/**
 *  Returns a copy of the image, adding an alpha channel if it doesn’t already have one. 
 *  An alpha is required when adding transparent regions (e.g., rounded corners) to an image. 
 *  It may also be necessary when loading certain kinds of image files that are not directly supported by Quartz 2D.
 */
- (UIImage *)imageWithAlpha;

/**
 *  Returns a copy of the image with a transparent border of the given size added around its edges. 
 *  This solves a special problem that occurs when rotating a UIImageView using Core Animation: 
 *  Its borders look incredibly ugly. There’s simply no antialiasing around the view’s edges. 
 *  Luckily, adding a one-pixel transparent border around the image fixes the problem.
 */
- (UIImage *)transparentBorderImage:(NSUInteger)borderSize;

@end
