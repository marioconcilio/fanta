//
//  UIStoryboard+Storyboards.m
//  Fanta
//
//  Created by Mario Concilio on 21/01/15.
//  Copyright (c) 2015 Mario Concilio. All rights reserved.
//

#import "UIStoryboard+Storyboards.h"

@implementation WelcomeStoryboard

- (WelcomeViewController *)welcomeViewController {
    return (WelcomeViewController *)[self instantiateViewControllerWithIdentifier:@"welcome"];
}

- (SignupViewController *)signupViewController {
    return (SignupViewController *)[self instantiateViewControllerWithIdentifier:@"signup"];
}

- (LoginViewController *)loginViewController {
    return (LoginViewController *)[self instantiateViewControllerWithIdentifier:@"login"];
}

@end

@implementation UIStoryboard (Storyboards)

+ (UIStoryboard *)mainStoryboard {
    return [UIStoryboard storyboardWithName:@"Main" bundle:nil];
}

+ (UIStoryboard *)pubStoryboard {
    return [UIStoryboard storyboardWithName:@"Pub" bundle:nil];
}

+ (WelcomeStoryboard *)welcomeStoryboard {
    return (WelcomeStoryboard *)[UIStoryboard storyboardWithName:@"Welcome" bundle:nil];
}

+ (UIStoryboard *)storeStoryboard {
    return [UIStoryboard storyboardWithName:@"Store" bundle:nil];
}

+ (UIStoryboard *)profileStoryboard {
    return [UIStoryboard storyboardWithName:@"Profile" bundle:nil];
}

+ (UIStoryboard *)feedStoryboard {
    return [UIStoryboard storyboardWithName:@"Feed" bundle:nil];
}

+ (UIStoryboard *)messagesStoryboard {
    return [UIStoryboard storyboardWithName:@"Messages" bundle:nil];
}

@end
