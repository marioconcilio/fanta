//
//  UIView+Shadow.h
//  Fanta
//
//  Created by Mario Concilio on 6/16/15.
//  Copyright (c) 2015 Mario Concilio. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIView (Shadow)

- (void)addDefaultShadow;
- (void)addShadowWithColor:(UIColor *)color
                    offset:(CGSize)offset
                   opacity:(CGFloat)opacity
                    radius:(CGFloat)radius;

- (void)drawDashedBorderAroundView;

@end
