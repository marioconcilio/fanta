//
//  UIStoryboard+Storyboards.h
//  Fanta
//
//  Created by Mario Concilio on 21/01/15.
//  Copyright (c) 2015 Mario Concilio. All rights reserved.
//

#import <UIKit/UIKit.h>

@class WelcomeViewController, SignupViewController, LoginViewController;
@interface WelcomeStoryboard : UIStoryboard

- (WelcomeViewController *)welcomeViewController;
- (SignupViewController *)signupViewController;
- (LoginViewController *)loginViewController;

@end

@interface UIStoryboard (Storyboards)

+ (UIStoryboard *)mainStoryboard;
+ (UIStoryboard *)pubStoryboard;
+ (WelcomeStoryboard *)welcomeStoryboard;
+ (UIStoryboard *)storeStoryboard;
+ (UIStoryboard *)profileStoryboard;
+ (UIStoryboard *)feedStoryboard;
+ (UIStoryboard *)messagesStoryboard;

@end
