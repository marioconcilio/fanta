//
//  UIColor+CustomColor.m
//  Fanta
//
//  Created by Mario Concilio on 26/01/15.
//  Copyright (c) 2015 Mario Concilio. All rights reserved.
//

#import "UIColor+CustomColor.h"

@implementation UIColor (CustomColor)

+ (UIColor *)customDarkBackground {
    return UIColorFromHEX(0x333333);
}

+ (UIColor *)customLightBackground {
    return UIColorFromHEX(0xcbcbcb);
}

+ (UIColor *)customOrange {
    return UIColorFromHEX(0xff6000);
}

+ (UIColor *)customPink {
    return UIColorFromHEX(0xff009c);
}

+ (UIColor *)customYellow {
    return UIColorFromHEX(0xffd800);
}

+ (UIColor *)customLightOrange {
    return UIColorFromRGB(238, 210, 194);
}

+ (UIColor *)customLightGray {
    return UIColorFromRGB(124, 133, 146);
}

+ (UIColor *)customDarkGray {
    return UIColorFromHEX(0x404040);
}

+ (UIColor *)customLightBorder {
    return UIColorFromRGB(227, 223, 224);
}

+ (UIColor *)randomColor {
    return [UIColor colorWithHue:arc4random() % 256 / 256.0 saturation:0.7 brightness:0.8 alpha:1.0];
}

@end
