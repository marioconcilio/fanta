// UIImage+RoundedCorner.h
// Created by Trevor Harmon on 9/20/09.
// Free for personal or commercial use, with or without modification.
// No warranty is expressed or implied.
// http://vocaro.com/trevor/blog/2009/10/12/resize-a-uiimage-the-right-way/

// Extends the UIImage class to support making rounded corners
@interface UIImage (RoundedCorner)

/**
 *  Creates a copy of the image, adding rounded corners of the specified radius. 
 *  If borderSize is non-zero, a transparent border of the given size will also be added.
 */
- (UIImage *)roundedCornerImage:(NSInteger)cornerSize borderSize:(NSInteger)borderSize;

@end
