//
//  UIImageView+Cache.m
//  Fanta
//
//  Created by Mario Concilio on 8/29/16.
//  Copyright © 2016 Mario Concilio. All rights reserved.
//

#import "UIImageView+Cache.h"
#import <UIImageView+AFNetworking.h>

@implementation UIImageView (Cache)

- (void)clearImageCacheForURL:(NSURL *)url {
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url];
    [request addValue:@"image/*" forHTTPHeaderField:@"Accept"];
    
    UIImage *cachedImage = [[[self class] sharedImageCache] cachedImageForRequest:request];
    if (cachedImage) {
//        [[[self class] sharedImageCache] clearCachedRequest:request];
        id<AFImageCache> cache = [[self class] sharedImageCache];
        [self clearCachedRequest:request inCache:cache];
    }
}

- (void)clearCachedRequest:(NSURLRequest *)request inCache:(NSCache *)cache {
    if (request) {
        [cache removeObjectForKey:[[request URL] absoluteString]];
    }
}

@end
