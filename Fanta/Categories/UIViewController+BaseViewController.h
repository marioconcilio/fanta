//
//  UIViewController+BaseViewController.h
//  Fanta
//
//  Created by Mario Concilio on 21/01/15.
//  Copyright (c) 2015 Mario Concilio. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <SWRevealViewController.h>
#import "VBFPopFlatButton+DefaultButtons.h"

@interface UIViewController (BaseViewController) <SWRevealViewControllerDelegate>

//@property (nonatomic, weak) VBFPopFlatButton *drawerButton;

+ (UINavigationController *)viewControllerWithNavigation;

- (void)addDrawerButtonDark;
- (void)addDrawerButtonLight;
- (void)toggleDrawer;
- (void)setupReachability;

@end
