//
//  BLMultiColorLoader+DefaultLoader.h
//  Fanta
//
//  Created by Mario Concilio on 4/7/16.
//  Copyright © 2016 Mario Concilio. All rights reserved.
//

#import <BLMultiColorLoader/BLMultiColorLoader.h>

@interface BLMultiColorLoader (DefaultLoader)

+ (instancetype)defaultLoaderWithSize:(CGSize)size;
+ (instancetype)defaultLoader;

@end
