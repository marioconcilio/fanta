//
//  BLMultiColorLoader+DefaultLoader.m
//  Fanta
//
//  Created by Mario Concilio on 4/7/16.
//  Copyright © 2016 Mario Concilio. All rights reserved.
//

#import "BLMultiColorLoader+DefaultLoader.h"

@implementation BLMultiColorLoader (DefaultLoader)

+ (instancetype)defaultLoaderWithSize:(CGSize)size {
    __autoreleasing BLMultiColorLoader *indicator = [[BLMultiColorLoader alloc] init];
    indicator.frame = (CGRect) {.size = size};
    indicator.lineWidth = 1.0;
    indicator.colorArray = @[[UIColor customPink],
                             [UIColor customYellow],
                             [UIColor customOrange]];
    
    return indicator;
}

+ (instancetype)defaultLoader {
    return [self defaultLoaderWithSize:CGSizeMake(20.0, 20.0)];
}

@end
