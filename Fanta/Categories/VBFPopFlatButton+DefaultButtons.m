//
//  VBFPopFlatButton+DefaultButtons.m
//  Fanta
//
//  Created by Mario Concilio on 4/7/16.
//  Copyright © 2016 Mario Concilio. All rights reserved.
//

#import "VBFPopFlatButton+DefaultButtons.h"
#import "Constants.h"

@implementation VBFPopFlatButton (DefaultButtons)

#pragma mark - Drawer
+ (instancetype)drawerButtonWithFrame:(CGRect)frame style:(VBFButtonStyle)style {
    __autoreleasing VBFPopFlatButton *button = [[VBFPopFlatButton alloc] initWithFrame:frame
                                                                            buttonType:buttonMenuType
                                                                           buttonStyle:buttonPlainStyle
                                                                 animateToInitialState:YES];
    button.lineThickness = 1.0;
    
    if (style == VBFButtonStyleDark) {
        button.tintColor = [UIColor whiteColor];
    }
    else {
        button.tintColor = [UIColor customPink];
    }
//    style == VBFButtonStyleDark ? button.tintColor = [UIColor whiteColor] : [UIColor customPink];
    
    return button;
}

+ (instancetype)drawerButtonLight {
    CGRect frame = (CGRect) {
        .size = kVBFButtonSize,
    };
    
    return [self drawerButtonWithFrame:frame style:VBFButtonStyleLight];
}

+ (instancetype)drawerButtonDark {
    CGRect frame = (CGRect) {
        .size = kVBFButtonSize,
    };
    
    return [self drawerButtonWithFrame:frame style:VBFButtonStyleDark];
}

#pragma mark - Close
+ (instancetype)closeButtonLight {
    VBFPopFlatButton *closeButton = [[VBFPopFlatButton alloc] initWithFrame:(CGRect) {
        .origin.x = 10,
        .origin.y = (kBlurNavbarHeight - kVBFButtonSize.height) / 2,
        .size = kVBFButtonSize,
    }
                                                                 buttonType:buttonCloseType
                                                                buttonStyle:buttonPlainStyle
                                                      animateToInitialState:YES];
    closeButton.lineThickness = 1.0;
    closeButton.tintColor = [UIColor customPink];
    
    return closeButton;
}

+ (instancetype)closeButtonDark {
    VBFPopFlatButton *closeButton = [[VBFPopFlatButton alloc] initWithFrame:(CGRect) {
        .origin.x = 10,
        .origin.y = (kBlurNavbarHeight - kVBFButtonSize.height) / 2,
        .size = kVBFButtonSize,
    }
                                                                 buttonType:buttonCloseType
                                                                buttonStyle:buttonPlainStyle
                                                      animateToInitialState:YES];
    closeButton.lineThickness = 1.0;
    closeButton.tintColor = [UIColor whiteColor];
    
    return closeButton;
}

#pragma mark - Action
+ (instancetype)actionButtonLight {
    __autoreleasing VBFPopFlatButton *actionButton = [[VBFPopFlatButton alloc] initWithFrame:(CGRect) {
        .origin.x = 10,
        .origin.y = (kBlurNavbarHeight - kVBFButtonSize.height) / 2,
        .size = kVBFButtonSize,
    }
                                                                  buttonType:buttonDownBasicType
                                                                 buttonStyle:buttonPlainStyle
                                                       animateToInitialState:YES];
    actionButton.lineThickness = 1.0;
    actionButton.tintColor = [UIColor customPink];
    
    return actionButton;
}

+ (instancetype)actionButtonDark {
    __autoreleasing VBFPopFlatButton *actionButton = [[VBFPopFlatButton alloc] initWithFrame:(CGRect) {
        .origin.x = 10,
        .origin.y = (kBlurNavbarHeight - kVBFButtonSize.height) / 2,
        .size = kVBFButtonSize,
    }
                                                                  buttonType:buttonDownBasicType
                                                                 buttonStyle:buttonPlainStyle
                                                       animateToInitialState:YES];
    actionButton.lineThickness = 1.0;
    actionButton.tintColor = [UIColor whiteColor];
    
    return actionButton;
}

@end
