//
//  UIColor+CustomColor.h
//  Fanta
//
//  Created by Mario Concilio on 26/01/15.
//  Copyright (c) 2015 Mario Concilio. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIColor (CustomColor)

/// UIColor with RGB (51, 51, 51)
+ (UIColor *)customDarkBackground;

/// UIColor with RGB (240, 240, 240)
+ (UIColor *)customLightBackground;

/// UIColor with RGB (211, 106, 41)
+ (UIColor *)customOrange;

/// UIColor with RBG (204, 76, 150)
+ (UIColor *)customPink;

/// UIColor with RGB (242, 216, 68)
+ (UIColor *)customYellow;

// UIColor with RGB (238, 210, 194)
+ (UIColor *)customLightOrange;

/// UIColor with RGB (124, 133, 146)
+ (UIColor *)customLightGray;

+ (UIColor *)customDarkGray;

/// UIColor with RGB (227, 223, 224)
+ (UIColor *)customLightBorder;

+ (UIColor *)randomColor;

@end
