//
//  UIFont+DefaultFont.h
//  Fanta
//
//  Created by Mario Concilio on 22/01/15.
//  Copyright (c) 2015 Mario Concilio. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIFont (DefaultFont)

/// Source Sans Pro Bold
+ (UIFont *)boldFontWithSize:(CGFloat)size;

/// Source Sans Black
+ (UIFont *)extraBoldFontWithSize:(CGFloat)size;

/// Source Sans Light
+ (UIFont *)lightFontWithSize:(CGFloat)size;

/// Source Sans Extra Light
+ (UIFont *)extraLightFontWithSize:(CGFloat)size;

/// Source Sans Regular
+ (UIFont *)regularFontWithSize:(CGFloat)size;

/// Source Sans Semibold
+ (UIFont *)semiboldFontWithSize:(CGFloat)size;

/// Source Sans Semibold Italic
+ (UIFont *)semiboldItalicFontWithSize:(CGFloat)size;

/// a-space Regular
+ (UIFont *)titleFontWithSize:(CGFloat)size;

/// a-space Light
+ (UIFont *)titleLightFontWithSize:(CGFloat)size;

@end
