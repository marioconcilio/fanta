//
//  UIViewController+BaseViewController.m
//  Fanta
//
//  Created by Mario Concilio on 21/01/15.
//  Copyright (c) 2015 Mario Concilio. All rights reserved.
//

#import "UIViewController+BaseViewController.h"
#import "Constants.h"
#import "ConnectionAlertManager.h"

#import <SWRevealViewController.h>
#import <objc/runtime.h>
#import <AFNetworkReachabilityManager.h>
#import <POP.h>

static const void *DrawerButtonKey = &DrawerButtonKey;

@implementation UIViewController (BaseViewController)

+ (UINavigationController *)viewControllerWithNavigation {
//    __autoreleasing ProfileViewController *vc = [[ProfileViewController alloc] initWithUserMe];
    return [[UINavigationController alloc] initWithRootViewController:[[self alloc] init]];
}

- (void)addDrawerButtonDark {
    self.revealViewController.delegate = self;

//    VBFPopFlatButton *button = [VBFPopFlatButton drawerButtonDark];
//    self.drawerButton = button;
//    [button addTarget:self action:@selector(toggleDrawer) forControlEvents:UIControlEventTouchUpInside];
//    
//    self.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc] initWithCustomView:button];
    self.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"fnt_navbar_menu"]
                                                                             style:UIBarButtonItemStylePlain
                                                                            target:self
                                                                            action:@selector(toggleDrawer)];
}

- (void)addDrawerButtonLight {
    self.revealViewController.delegate = self;

//    VBFPopFlatButton *button = [VBFPopFlatButton drawerButtonLight];
//    self.drawerButton = button;
//    [button addTarget:self action:@selector(toggleDrawer) forControlEvents:UIControlEventTouchUpInside];
    
//    self.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc] initWithCustomView:button];
    self.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"fnt_navbar_menu"]
                                                                             style:UIBarButtonItemStylePlain
                                                                            target:self
                                                                            action:@selector(toggleDrawer)];
}

- (void)toggleDrawer {
    [self.view endEditing:YES];
//    [self.sideMenuViewController presentLeftMenuViewController];
//    [DrawerViewController presentInViewController:self];
//    [self.sideMenuViewController showLeftMenuViewController];
    [self.revealViewController revealToggleAnimated:YES];
}

/*
#pragma mark - Getter/Setter for drawerButton
- (void)setDrawerButton:(VBFPopFlatButton *)drawerButton {
    objc_setAssociatedObject(self, DrawerButtonKey, drawerButton, OBJC_ASSOCIATION_ASSIGN);
}

- (VBFPopFlatButton *)drawerButton {
    return objc_getAssociatedObject(self, DrawerButtonKey);
}
 */

#pragma mark - SWRevealViewController Delegate
- (void)revealController:(SWRevealViewController *)revealController willMoveToPosition:(FrontViewPosition)position {
    if ([self.view viewWithTag:1989]) {
        [[self.view viewWithTag:1989] removeFromSuperview];
    }
}

- (void)revealController:(SWRevealViewController *)revealController didMoveToPosition:(FrontViewPosition)position {
    // opened menu
    if (position == FrontViewPositionRight || position == FrontViewPositionLeftSide) {
        UIView *view = [[UIView alloc] initWithFrame:self.view.frame];
        view.backgroundColor = [UIColor clearColor];
        [view addGestureRecognizer:self.revealViewController.panGestureRecognizer];
        view.tag = 1989;
        [self.view addSubview:view];
    }
}

#pragma mark - Reachability
- (void)setupReachability {
    [[AFNetworkReachabilityManager sharedManager] setReachabilityStatusChangeBlock:^(AFNetworkReachabilityStatus status) {
        if ([[AFNetworkReachabilityManager sharedManager] isReachable]) {
            [ConnectionAlertManager hide];
            DDLogInfo(@"internet connection ok");
        }
        else {
            [ConnectionAlertManager showInNavigationBar:self.navigationController.navigationBar];
            DDLogInfo(@"no internet connection");
        }
        
    }];
}

@end
