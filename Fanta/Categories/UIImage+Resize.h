// UIImage+Resize.h
// Created by Trevor Harmon on 8/5/09.
// Free for personal or commercial use, with or without modification.
// No warranty is expressed or implied.
// http://vocaro.com/trevor/blog/2009/10/12/resize-a-uiimage-the-right-way/

// Extends the UIImage class to support resizing/cropping
@interface UIImage (Resize)

/**
 *  Returns a copy of the image that is cropped to the given bounds. 
 *  The bounds will be adjusted using CGRectIntegral, 
 *  meaning that any fractional values will be converted to integers.
 */
- (UIImage *)croppedImage:(CGRect)bounds;

/**
 *  Returns a copy of the image reduced to the given thumbnail dimensions. 
 *  If the image has a non-square aspect ratio, the longer portion will be cropped. 
 *  If borderSize is non-zero, a transparent border of the given size will also be added. 
 *  Finally, the quality parameter determines the amount of antialiasing to perform when scaling the image.
 */
- (UIImage *)thumbnailImage:(NSInteger)thumbnailSize
          transparentBorder:(NSUInteger)borderSize
               cornerRadius:(NSUInteger)cornerRadius
       interpolationQuality:(CGInterpolationQuality)quality;

/**
 *  Returns a resized copy of the image. 
 *  The quality parameter determines the amount of antialiasing to perform when scaling the image.
 *  Note that the image will be scaled disproportionately if necessary to fit the specified bounds. 
 *  In other words, the aspect ratio is not preserved.
 */
- (UIImage *)resizedImage:(CGSize)newSize
     interpolationQuality:(CGInterpolationQuality)quality;

/**
 *  Scale To Fill is the default behavior of resizedImage:interpolationQuality:, 
 *  while resizedImageWithContentMode: supports both Aspect Fit and Aspect Fill.
 */
- (UIImage *)resizedImageWithContentMode:(UIViewContentMode)contentMode
                                  bounds:(CGSize)bounds
                    interpolationQuality:(CGInterpolationQuality)quality;
@end
