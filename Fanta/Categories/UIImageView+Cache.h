//
//  UIImageView+Cache.h
//  Fanta
//
//  Created by Mario Concilio on 8/29/16.
//  Copyright © 2016 Mario Concilio. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIImageView (Cache)

- (void)clearImageCacheForURL:(NSURL *)url;

@end
