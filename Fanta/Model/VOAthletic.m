//
//  VOAthletic.m
//  Fanta
//
//  Created by Mario Concilio on 5/10/16.
//  Copyright © 2016 Mario Concilio. All rights reserved.
//

#import "VOAthletic.h"

NSString *const kNameKey        = @"name";
NSString *const kAthleticKey    = @"athletic_name";
NSString *const kIDKey          = @"athletic_id";
NSString *const kFacebookKey    = @"facebook_id";
NSString *const kThumbnailKey   = @"thumbnail";

@implementation VOAthletic

- (instancetype)initWithDictionary:(NSDictionary *)dict {
    self = [super init];
    if (self) {
        _athleticName = dict[@"athletic_name"];
        _name = dict[@"name"];
        _athleticID = [dict[@"id"] integerValue];
        _facebookID = [dict[kFacebookKey] integerValue];
        //TODO: thumbnail from dict
    }
    
    return self;
}

- (instancetype)initWithFacebookDictionary:(NSDictionary *)dict {
    self = [super init];
    if (self) {
        _name = dict[@"name"];
        _facebookID = [dict[@"id"] integerValue];
//        _thumbnail = dict[@"picture"][@"data"][@"url"];
    }
    
    return self;
}

- (NSString *)fullName {
    return [NSString stringWithFormat:@"%@ - %@", _name, _athleticName];
}

#pragma mark - NSCoding
- (instancetype)initWithCoder:(NSCoder *)decoder {
    self = [super init];
    if (self) {
        _name = [decoder decodeObjectForKey:kNameKey];
        _athleticName = [decoder decodeObjectForKey:kAthleticKey];
        _thumbnail = [decoder decodeObjectForKey:kThumbnailKey];
        _facebookID = [[decoder decodeObjectForKey:kFacebookKey] unsignedIntegerValue];
        _athleticID = [[decoder decodeObjectForKey:kIDKey] unsignedIntegerValue];
    }
    
    return self;
}

- (void)encodeWithCoder:(NSCoder *)encoder {
    [encoder encodeObject:_name forKey:kNameKey];
    [encoder encodeObject:_athleticName forKey:kAthleticKey];
    [encoder encodeObject:_thumbnail forKey:kThumbnailKey];
    [encoder encodeObject:@(_facebookID) forKey:kFacebookKey];
    [encoder encodeObject:@(_athleticID) forKey:kIDKey];
}

#pragma mark - NSCopying
- (id)copyWithZone:(NSZone *)zone {
    VOAthletic *copy = [[VOAthletic allocWithZone:zone] init];
    copy->_name = [_name copyWithZone:zone];
    copy->_athleticName = [_athleticName copyWithZone:zone];
    copy->_thumbnail = [_thumbnail copyWithZone:zone];
    copy->_facebookID = [[@(_facebookID) copyWithZone:zone] unsignedIntegerValue];
    copy->_athleticID = [[@(_athleticID) copyWithZone:zone] unsignedIntegerValue];
    
    return copy;
}

#pragma mark - NSObject
- (BOOL)isEqualToAthletic:(VOAthletic *)aa {
    if (!aa)
        return NO;
    
    return _athleticID == aa.athleticID;
}

- (BOOL)isEqual:(id)object {
    if (self == object)
        return YES;
    
    if (![object isKindOfClass:[VOAthletic class]])
        return NO;
    
    return [self isEqualToAthletic:object];
}

- (NSUInteger)hash {
    return [@(_facebookID) hash];
}

- (NSString *)description {
    return [NSString stringWithFormat:@"<%@> {\n\
        athleticID = %ld, \n\
        facebookID = %ld, \n\
        name = %@, \n\
        athletic_name = %@, \n\
    }",
            [self class], self.athleticID, self.facebookID, self.name, self.athleticName];
}

- (id)debugQuickLookObject {
    return [NSURL URLWithString:self.thumbnail];
}

@end
