//
//  VOComment.m
//  Fanta
//
//  Created by Mario Concilio on 07/04/15.
//  Copyright (c) 2015 Mario Concilio. All rights reserved.
//

#import "VOComment.h"
#import "Formatter.h"

@implementation VOComment

- (instancetype)initWithDictionary:(NSDictionary *)dict {
    self = [super init];
    if (self) {
        _content = dict[@"contents"];
        _userName = dict[@"student_name"];
        _userID = [dict[@"student_id"] unsignedIntegerValue];
        _likes = [dict[@"likes"] unsignedIntegerValue];
        _dislikes = [dict[@"dislikes"] unsignedIntegerValue];
        _createdAt = [Formatter dateFromString:dict[@"created_at"]];
        _liked = [dict[@"i_like"] boolValue];
        _disliked = [dict[@"i_dislike"] boolValue];
    }
    
    return self;
}

#pragma mark - NSCoder
- (instancetype)initWithCoder:(NSCoder *)decoder {
    self = [super init];
    if (self) {
        _content = [decoder decodeObjectForKey:@"contents"];
        _likes = [[decoder decodeObjectForKey:@"likes"] unsignedIntegerValue];
        _dislikes = [[decoder decodeObjectForKey:@"dislikes"] unsignedIntegerValue];
        _userName = [decoder decodeObjectForKey:@"student_name"];
        _userID = [[decoder decodeObjectForKey:@"student_id"] unsignedIntegerValue];
        _createdAt = [decoder decodeObjectForKey:@"created_at"];
        _liked = [[decoder decodeObjectForKey:@"i_like"] boolValue];
        _disliked = [[decoder decodeObjectForKey:@"i_dislike"] boolValue];
    }
    
    return self;
}

- (void)encodeWithCoder:(NSCoder *)encoder {
    [encoder encodeObject:_content forKey:@"contents"];
    [encoder encodeObject:@(_likes) forKey:@"likes"];
    [encoder encodeObject:@(_dislikes) forKey:@"dislikes"];
    [encoder encodeObject:_userName forKey:@"student_name"];
    [encoder encodeObject:@(_userID) forKey:@"student_id"];
    [encoder encodeObject:_createdAt forKey:@"created_at"];
    [encoder encodeObject:@(_liked) forKey:@"i_like"];
    [encoder encodeObject:@(_disliked) forKey:@"i_dislike"];
}

#pragma mark - NSObject
- (NSString *)description {
    NSString *shortContent = self.content.length > 100 ? [[self.content substringToIndex:100] stringByAppendingString:@"..."] : self.content;
    
    return [NSString stringWithFormat:@"<%@> {\n\tuserId = %lu, \n\tuserName = %@, \n\tcreatedAt = %@, \n\tcontent = %@, \n\tlikes = %lu, \n\tdislikes = %lu, \n\tliked = %@, \n\tdisliked = %@ \n}",
            [self class], self.userID, self.userName, self.createdAt, shortContent, self.likes, self.dislikes,
            self.liked ? @"YES" : @"NO",
            self.disliked ? @"YES" : @"NO"];
}

@end
