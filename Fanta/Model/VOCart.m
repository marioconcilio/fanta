//
//  VOCart.m
//  Fanta
//
//  Created by Mario Concilio on 3/16/16.
//  Copyright © 2016 Mario Concilio. All rights reserved.
//

#import "VOCart.h"
#import "VOStoreItem.h"
#import "Constants.h"

static NSString *const kItemsKey = @"items";
static NSString *const kTotalKey = @"total";

@implementation VOCart

#pragma mark - NSCoding
- (instancetype)initWithCoder:(NSCoder *)coder {
    self = [super init];
    if (self) {
        _items = [coder decodeObjectForKey:kItemsKey];
        _total = [coder decodeObjectForKey:kTotalKey];
    }
    
    return self;
}

- (void)encodeWithCoder:(NSCoder *)encoder {
    [encoder encodeObject:_items forKey:kItemsKey];
    [encoder encodeObject:_total forKey:kTotalKey];
}

#pragma mark - NSObject
- (NSString *)description {
    return [NSString stringWithFormat:@"<%@> {\n\titems = %@, \n\ttotal = %@ \n}",
            [self class], self.items, self.total];
}

#pragma mark - Public Methods
- (void)addStoreItem:(VOStoreItem *)item quantity:(NSUInteger)qty {
    NSNumber *oldQty = _items[item];
    
    // if Item already exists on MyCart
    // increment 1 qty
    if (oldQty) {
        NSUInteger q = [oldQty unsignedIntegerValue];
        [_items setObject:@(qty + q) forKey:item];
    }
    // add Item to MyCart with 1 qty
    else {
        [_items setObject:@(qty) forKey:item];
    }
    
    [self updateTotal];
}

- (void)removeStoreItem:(VOStoreItem *)item {
    [_items removeObjectForKey:item];
    [self updateTotal];
}

- (void)updateStoreItem:(VOStoreItem *)item plusQuantity:(NSUInteger)qty {
    NSNumber *oldQty = _items[item];
    if (!oldQty)
        return;
    
    NSUInteger q = [oldQty unsignedIntegerValue];
    if (q == kCartMaxItems)
        return;
    
    
    [_items setObject:@(q + qty) forKey:item];
    [self updateTotal];
}

- (void)updateStoreItem:(VOStoreItem *)item minusQuantity:(NSUInteger)qty {
    NSNumber *oldQty = _items[item];
    if (!oldQty)
        return;
    
    NSUInteger q = [oldQty unsignedIntegerValue];
    if (q == kCartMinItems)
        return;
    
    [_items setObject:@(q - qty) forKey:item];
    [self updateTotal];
}


- (void)updateTotal {
    NSDecimalNumber *total = [NSDecimalNumber decimalNumberWithString:@"0"];
    NSDecimalNumber *subtotal;
    
    for (VOStoreItem *item in _items.allKeys) {
        @autoreleasepool {
            NSDecimal qty = [_items[item] decimalValue];
            subtotal = [item.price decimalNumberByMultiplyingBy:[NSDecimalNumber decimalNumberWithDecimal:qty]];
            total = [total decimalNumberByAdding:subtotal];
        }
    }
    
    [self setValue:total forKey:@"total"];
}

@end
