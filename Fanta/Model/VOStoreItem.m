//
//  VOStoreItem.m
//  Fanta
//
//  Created by Mario Concilio on 3/7/16.
//  Copyright © 2016 Mario Concilio. All rights reserved.
//

#import "VOStoreItem.h"

static NSString *const kImagesKey  = @"images";
static NSString *const kTitleKey   = @"title";
static NSString *const kContentKey = @"content";
static NSString *const kPriceKey   = @"price";

@implementation VOStoreItem

#pragma mark - NSCoding
- (instancetype)initWithCoder:(NSCoder *)coder {
    self = [super init];
    if (self) {
        _images= [coder decodeObjectForKey:kImagesKey];
        _title = [coder decodeObjectForKey:kTitleKey];
        _content = [coder decodeObjectForKey:kContentKey];
        _price = [coder decodeObjectForKey:kPriceKey];
    }
    
    return self;
}

- (void)encodeWithCoder:(NSCoder *)encoder {
    [encoder encodeObject:_images forKey:kImagesKey];
    [encoder encodeObject:_title forKey:kTitleKey];
    [encoder encodeObject:_content forKey:kContentKey];
    [encoder encodeObject:_price forKey:kPriceKey];
}

#pragma mark - NSCopying
- (id)copyWithZone:(NSZone *)zone {
    VOStoreItem *copy = [[VOStoreItem allocWithZone:zone] init];
    copy->_images = [_images copyWithZone:zone];
    copy->_title = [_title copyWithZone:zone];
    copy->_content = [_content copyWithZone:zone];
    copy->_price = [_price copyWithZone:zone];
    
    return copy;
}

#pragma mark - NSObject
- (BOOL)isEqualToStoreItem:(VOStoreItem *)item {
    if (!item)
        return NO;
    
    return _title == item.title && _content == item.content;
}

- (BOOL)isEqual:(id)object {
    if (self == object)
        return YES;
    
    if (![object isKindOfClass:[VOStoreItem class]])
        return NO;
    
    return [self isEqualToStoreItem:object];
}

- (NSUInteger)hash {
    return [_title hash] ^ [_content hash];
}

- (NSString *)description {
    NSString *shortContent = self.content.length > 100 ? [[self.content substringToIndex:100] stringByAppendingString:@"..."] : self.content;
    
    return [NSString stringWithFormat:@"<%@> {\n\ttitle = %@, \n\tcontent = %@, \n\tprice = %@, \n\timages = %@ \n}",
            [self class], self.title, shortContent, self.price, self.images];
}

- (id)debugQuickLookObject {
    return [NSURL URLWithString:self.images[0]];
}

@end
