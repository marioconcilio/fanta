//
//  VOMessage.h
//  Fanta
//
//  Created by Mario Concilio on 3/21/16.
//  Copyright © 2016 Mario Concilio. All rights reserved.
//

#import <Foundation/Foundation.h>

@class VOUser;
@interface VOMessage : NSObject

@property (nonatomic, strong) VOUser *sender;
@property (nonatomic, copy) NSString *content;

@property (nonatomic, copy) NSString *title;
@property (nonatomic, copy) NSString *thumbnail;
@property (nonatomic, copy) NSString *lastMessage;
@property (nonatomic, copy) NSString *lastUser;
@property (nonatomic, copy) NSDate *lastMessageDate;
@property (nonatomic, assign) NSUInteger userID;
@property (nonatomic, assign) NSUInteger groupID;

@end
