//
//  VOComment.h
//  Fanta
//
//  Created by Mario Concilio on 07/04/15.
//  Copyright (c) 2015 Mario Concilio. All rights reserved.
//

#import <Foundation/Foundation.h>

@class VOUser;
@interface VOComment : NSObject

//TODO: remover property user, quando precisar realizar novo fetch
@property (nonatomic, strong) VOUser *user;
@property (nonatomic, copy) NSString *comment;

@property (nonatomic, copy) NSString *content;
@property (nonatomic, copy) NSString *userName;
@property (nonatomic, assign) NSUInteger commentID;
@property (nonatomic, assign) NSUInteger userID;
@property (nonatomic, assign) NSUInteger likes;
@property (nonatomic, assign) NSUInteger dislikes;
@property (nonatomic, strong) NSDate *createdAt;
@property (nonatomic, assign, getter=isLiked) BOOL liked;
@property (nonatomic, assign, getter=isDisliked) BOOL disliked;

- (instancetype)initWithDictionary:(NSDictionary *)dict;

@end
