//
//  VOAthletic.h
//  Fanta
//
//  Created by Mario Concilio on 5/10/16.
//  Copyright © 2016 Mario Concilio. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface VOAthletic : NSObject <NSCopying, NSCoding>

@property (nonatomic, assign) NSUInteger facebookID;
@property (nonatomic, assign) NSUInteger athleticID;
@property (nonatomic, copy) NSString *thumbnail;
@property (nonatomic, copy) NSString *name;
@property (nonatomic, copy) NSString *athleticName;

- (instancetype)initWithDictionary:(NSDictionary *)dict;
- (instancetype)initWithFacebookDictionary:(NSDictionary *)dict;
- (BOOL)isEqualToAthletic:(VOAthletic *)aa;
- (NSString *)fullName;

@end
