//
//  VOPost.h
//  Fanta
//
//  Created by Mario Concilio on 19/03/15.
//  Copyright (c) 2015 Mario Concilio. All rights reserved.
//

#import <Foundation/Foundation.h>

@class VOUser;
@interface VOPost : NSObject

#warning UNUSABLE PROPERTIES
@property (nonatomic, strong) VOUser *user;
@property (nonatomic, copy) NSString *postImage;
@property (nonatomic, copy) NSString *postTitle;

@property (nonatomic, copy) NSString *content;
@property (nonatomic, copy) NSString *userName;
@property (nonatomic, copy) NSDate *createdAt;
@property (nonatomic, assign) NSUInteger userID;
@property (nonatomic, assign) NSUInteger postID;
@property (nonatomic, assign) NSUInteger comments;
@property (nonatomic, assign) NSUInteger likes;
@property (nonatomic, assign) NSUInteger dislikes;
@property (nonatomic, assign, getter=isLiked) BOOL liked;
@property (nonatomic, assign, getter=isDisliked) BOOL disliked;

- (instancetype)initWithDictionary:(NSDictionary *)dict;

@end
