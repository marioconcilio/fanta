//
//  VOMessage.m
//  Fanta
//
//  Created by Mario Concilio on 3/21/16.
//  Copyright © 2016 Mario Concilio. All rights reserved.
//

#import "VOMessage.h"

@implementation VOMessage

#pragma mark - NSObject
- (NSString *)description {
    NSString *shortMessage = self.lastMessage.length > 100 ? [[self.lastMessage substringToIndex:100] stringByAppendingString:@"..."] : self.lastMessage;
    
    return [NSString stringWithFormat:@"<%@> {\n\tuserId = %lu, \n\tgroupId = %lu, \n\ttitle = %@, \n\tlastUser = %@, \n\tlastMessage = %@, \n\tlastMessageDate = %@, \n\tthumbnail = %@ \n}",
            [self class], self.userID, self.groupID, self.title, self.lastUser, shortMessage, self.lastMessageDate, self.thumbnail];
}

- (id)debugQuickLookObject {
    return [NSURL URLWithString:self.thumbnail];
}

@end
