//
//  VOUser.m
//  Fanta
//
//  Created by Mario Concilio on 19/03/15.
//  Copyright (c) 2015 Mario Concilio. All rights reserved.
//

#import "VOUser.h"

static NSString *const kNameKey     = @"student_name";
static NSString *const kEmailKey    = @"student_email";
static NSString *const kImageKey    = @"image";
static NSString *const kThumbKey    = @"thumbnail";
static NSString *const kIDKey       = @"student_id";
//static NSString *const kHashKey     = @"student_hash";

@implementation VOUser

- (instancetype)initWithDictionary:(NSDictionary *)dict {
    self = [super init];
    if (self) {
        _name = dict[kNameKey];
        _email = dict[kEmailKey];
        _image = dict[kImageKey];
        _thumbnail = dict[kThumbKey];
        _userID = [dict[kIDKey] unsignedIntegerValue];
//        _userHash = dict[kHashKey];
        _valid = YES;
    }
    
    return self;
}

- (instancetype)initWithFacebookDictionary:(NSDictionary *)dict {
    self = [super init];
    if (self) {
        _userID = 0; //id provisorio, ate receber um do banco
        _facebookID = [dict[@"id"] integerValue];
        _name = [NSString stringWithFormat:@"%@ %@", dict[@"first_name"], dict[@"last_name"]];
        _email = dict[@"email"];
        _valid = YES;
        
        // if facebook profile image is not silhouette
        if (![dict[@"picture"][@"data"][@"is_silhouette"] boolValue]) {
            _image = dict[@"picture"][@"data"][@"url"];
        }
    }
    
    return self;
}

#pragma mark - NSCoding
- (instancetype)initWithCoder:(NSCoder *)decoder {
    self = [super init];
    if (self) {
        _name = [decoder decodeObjectForKey: kNameKey];
        _email = [decoder decodeObjectForKey: kEmailKey];
        _image = [decoder decodeObjectForKey:kImageKey];
        _thumbnail = [decoder decodeObjectForKey: kThumbKey];
        _userID = [[decoder decodeObjectForKey: kIDKey] unsignedIntegerValue];
//        _userHash = [decoder decodeObjectForKey:kHashKey];
        _athletic = [decoder decodeObjectForKey:@"athletic"];
        _valid = [[decoder decodeObjectForKey:@"is_valid"] boolValue];
    }
    
    return self;
}

- (void)encodeWithCoder:(NSCoder *)encoder {
    [encoder encodeObject:_name forKey: kNameKey];
    [encoder encodeObject:_email forKey: kEmailKey];
    [encoder encodeObject:_image forKey:kImageKey];
    [encoder encodeObject:_thumbnail forKey: kThumbKey];
    [encoder encodeObject:@(_userID) forKey: kIDKey];
//    [encoder encodeObject:_userHash forKey:kHashKey];
    [encoder encodeObject:_athletic forKey:@"athletic"];
    [encoder encodeObject:@(_valid) forKey:@"is_valid"];
}

#pragma mark - NSCopying
- (id)copyWithZone:(NSZone *)zone {
    VOUser *copy = [[VOUser allocWithZone:zone] init];
    copy->_name = [_name copyWithZone:zone];
    copy->_email = [_email copyWithZone:zone];
    copy->_image = [_image copyWithZone:zone];
    copy->_thumbnail = [_thumbnail copyWithZone:zone];
    copy->_userID = [[@(_userID) copyWithZone:zone] unsignedIntegerValue];
    
    return copy;
}

#pragma mark - NSObject
- (BOOL)isEqualToUser:(VOUser *)user {
    if (!user)
        return NO;
    
    return _userID == user.userID;
}

- (BOOL)isEqual:(id)object {
    if (self == object)
        return YES;
    
    if (![object isKindOfClass:[VOUser class]])
        return NO;
    
    return [self isEqualToUser:object];
}

- (NSUInteger)hash {
    return [_name hash] ^ [_email hash];
}

- (NSString *)description {
    return [NSString stringWithFormat:@"<%@> {\n\
        userID = %lu, \n\
        facebookID = %lu, \n\
        name = %@, \n\
        email = %@, \n\
        image = %@, \n\
        is_valid = %@, \n\
    }",
            [self class],
            (unsigned long) self.userID,
            (unsigned long) self.facebookID,
            self.name,
            self.email,
            self.image,
            StringFromBOOL([self isValid])];
}

- (id)debugQuickLookObject {
    return [NSURL URLWithString:self.thumbnail];
}

@end
