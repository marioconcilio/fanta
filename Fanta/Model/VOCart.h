//
//  VOCart.h
//  Fanta
//
//  Created by Mario Concilio on 3/16/16.
//  Copyright © 2016 Mario Concilio. All rights reserved.
//

#import <Foundation/Foundation.h>

@class VOStoreItem;
@interface VOCart : NSObject <NSCoding>

@property (nonatomic, strong) NSMutableDictionary<VOStoreItem *, NSNumber *> *items;
@property (nonatomic, strong) NSDecimalNumber *total;

- (void)addStoreItem:(VOStoreItem *)item quantity:(NSUInteger)qty;
- (void)removeStoreItem:(VOStoreItem *)item;
- (void)updateStoreItem:(VOStoreItem *)item plusQuantity:(NSUInteger)qty;
- (void)updateStoreItem:(VOStoreItem *)item minusQuantity:(NSUInteger)qty;

- (void)updateTotal;

@end
