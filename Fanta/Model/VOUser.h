//
//  VOUser.h
//  Fanta
//
//  Created by Mario Concilio on 19/03/15.
//  Copyright (c) 2015 Mario Concilio. All rights reserved.
//

#import <Foundation/Foundation.h>

@class VOAthletic;
@interface VOUser : NSObject <NSCoding, NSCopying>

@property (nonatomic, copy) VOAthletic *athletic;

@property (nonatomic, copy) NSString *name;
@property (nonatomic, copy) NSString *email;
@property (nonatomic, copy) NSString *image;
@property (nonatomic, copy) NSString *thumbnail;
//@property (nonatomic, copy) NSString *userHash;

@property (nonatomic, assign) NSUInteger userID;
@property (nonatomic, assign) NSUInteger facebookID;
@property (nonatomic, assign, getter=isValid) BOOL valid;
@property (nonatomic, assign, getter=isAthleticMember) BOOL *athleticMember;

@property (nonatomic, copy) NSString *facebook;
@property (nonatomic, copy) NSString *instagram;
@property (nonatomic, copy) NSString *twitter;

- (instancetype)initWithDictionary:(NSDictionary *)dict;
- (instancetype)initWithFacebookDictionary:(NSDictionary *)dict;
- (BOOL)isEqualToUser:(VOUser *)user;

@end
