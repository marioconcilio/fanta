//
//  VOStoreItem.h
//  Fanta
//
//  Created by Mario Concilio on 3/7/16.
//  Copyright © 2016 Mario Concilio. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface VOStoreItem : NSObject <NSCopying, NSCoding>

@property (nonatomic, copy) NSArray<NSString *> *images;
@property (nonatomic, copy) NSString *title;
@property (nonatomic, copy) NSString *content;
@property (nonatomic, copy) NSDecimalNumber *price;

@end
