//
//  VOPost.m
//  Fanta
//
//  Created by Mario Concilio on 19/03/15.
//  Copyright (c) 2015 Mario Concilio. All rights reserved.
//

#import "VOPost.h"
#import "Formatter.h"

@implementation VOPost

- (instancetype)initWithDictionary:(NSDictionary *)dict {
    self = [super init];
    if (self) {
        _content = dict[@"contents"];
        _postID = [dict[@"topic_id"] unsignedIntegerValue];
        _comments = [dict[@"comments"] unsignedIntegerValue];
        _likes = [dict[@"likes"] unsignedIntegerValue];
        _dislikes = [dict[@"dislikes"] unsignedIntegerValue];
        _userName = dict[@"student"][@"name"];
        _userID = [dict[@"student"][@"id"] unsignedIntegerValue];
        _createdAt = [Formatter dateFromString:dict[@"created_at"]];
        _liked = [dict[@"i_like"] boolValue];
        _disliked = [dict[@"i_dislike"] boolValue];
    }
    
    return self;
}

#pragma mark - NSCoder
- (instancetype)initWithCoder:(NSCoder *)decoder {
    self = [super init];
    if (self) {
        _content = [decoder decodeObjectForKey:@"contents"];
        _postID = [[decoder decodeObjectForKey:@"topic_id"] unsignedIntegerValue];
        _comments = [[decoder decodeObjectForKey:@"comments"] unsignedIntegerValue];
        _likes = [[decoder decodeObjectForKey:@"likes"] unsignedIntegerValue];
        _dislikes = [[decoder decodeObjectForKey:@"dislikes"] unsignedIntegerValue];
        _userName = [decoder decodeObjectForKey:@"student.name"];
        _userID = [[decoder decodeObjectForKey:@"student.id"] unsignedIntegerValue];
        _createdAt = [decoder decodeObjectForKey:@"created_at"];
        _liked = [[decoder decodeObjectForKey:@"i_like"] boolValue];
        _disliked = [[decoder decodeObjectForKey:@"i_dislike"] boolValue];
    }
    
    return self;
}

- (void)encodeWithCoder:(NSCoder *)encoder {
    [encoder encodeObject:_content forKey:@"contents"];
    [encoder encodeObject:@(_postID) forKey:@"topic_id"];
    [encoder encodeObject:@(_comments) forKey:@"comments"];
    [encoder encodeObject:@(_likes) forKey:@"likes"];
    [encoder encodeObject:@(_dislikes) forKey:@"dislikes"];
    [encoder encodeObject:_userName forKey:@"student.name"];
    [encoder encodeObject:@(_userID) forKey:@"student.id"];
    [encoder encodeObject:_createdAt forKey:@"created_at"];
    [encoder encodeObject:@(_liked) forKey:@"i_like"];
    [encoder encodeObject:@(_disliked) forKey:@"i_dislike"];
}

#pragma mark - NSObject
- (NSString *)description {
    NSString *shortContent = self.content.length > 100 ? [[self.content substringToIndex:100] stringByAppendingString:@"..."] : self.content;

    return [NSString stringWithFormat:@"<%@> {\n\tpostId = %lu, \n\tuserId = %lu, \n\tuserName = %@, \n\tcreatedAt = %@, \n\tcontent = %@, \n\tpostImage = %@, \n\tlikes = %lu, \n\tdislikes = %lu, \n\tcomments = %lu, \n\tliked = %@, \n\tdisliked = %@ \n}",
            [self class], self.postID, self.userID, self.userName, self.createdAt, shortContent, self.postImage, self.likes, self.dislikes, self.comments,
            self.liked ? @"YES" : @"NO",
            self.disliked ? @"YES" : @"NO"];
}

- (id)debugQuickLookObject {
    if (self.postImage)
        return [NSURL URLWithString:self.postImage];
    
    return self.content;
}

@end
