//
//  main.m
//  Fanta
//
//  Created by Mario Concilio on 19/01/15.
//  Copyright (c) 2015 Mario Concilio. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        /*
        NSArray *fontFamilyNames = [UIFont familyNames];
        
        for (NSString *familyName in fontFamilyNames) {
            NSLog(@"Font Family Name = %@", familyName);
            NSArray *names = [UIFont fontNamesForFamilyName:familyName];
            
            for (NSString *name in names) {
                NSLog(@"\tFont Name = %@", name);
            }
        }
         */
        
        Class appDelegateClass = NSClassFromString(@"TestingAppDelegate");
        if (!appDelegateClass)
            appDelegateClass = [AppDelegate class];
        
        return UIApplicationMain(argc, argv, nil, NSStringFromClass(appDelegateClass));
    }
}
