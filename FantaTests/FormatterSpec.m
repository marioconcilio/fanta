//
//  FormatterSpec.m
//  Fanta
//
//  Created by Mario Concilio on 8/15/16.
//  Copyright © 2016 Mario Concilio. All rights reserved.
//

#import "Formatter.h"
#import <NSDate+DateTools.h>
#import <Kiwi.h>
#import <OCHamcrest.h>

SPEC_BEGIN(FormatterSpec)

describe(@"Formatter", ^{
    
    context(@"format likes and dislikes number", ^{
        it(@"when less than 1k", ^{
            assertThat([Formatter formatNumber:89], is(@"89"));
            assertThat([Formatter formatNumber:999], is(@"999"));
        });
        
        it(@"when equal 1k", ^{
            assertThat([Formatter formatNumber:1000], is(@"1.0k"));
        });
        
        it(@"when greater than 1k", ^{
            assertThat([Formatter formatNumber:1989], is(@"1.9k"));
            assertThat([Formatter formatNumber:3500], is(@"3.5k"));
            assertThat([Formatter formatNumber:20234], is(@"20k"));
        });
    });

    it(@"format currency", ^{
        NSDecimalNumber *n1 = [NSDecimalNumber decimalNumberWithString:@"1234.56" locale:[NSLocale localeWithLocaleIdentifier:@"en_US"]];
        NSString *c1 = [Formatter formatCurrency:n1];
        assertThat(c1, is(@"R$1.234,56"));
        
        NSDecimalNumber *n2 = [NSDecimalNumber decimalNumberWithString:@"1234,56" locale:[NSLocale localeWithLocaleIdentifier:@"pt_BR"]];
        NSString *c2 = [Formatter formatCurrency:n2];
        assertThat(c2, is(@"R$1.234,56"));
    });
    
    it(@"create date from string", ^{
        NSDate *date1 = [Formatter dateFromString:@"2016-08-15 21:59:14"];
        NSDate *date2 = [NSDate dateWithYear:2016 month:8 day:15 hour:21 minute:59 second:14];
        assertThatBool([date1 isEqualToDate:date2], isTrue());
    });
    
    it(@"parse date", ^{
        NSDate *date = [NSDate dateWithYear:2016 month:8 day:15 hour:21 minute:59 second:0];
        NSString *str = [Formatter parseDate:date];
        assertThat(str, is(@"Em 15 de agosto de 2016 às 21:59"));
    });

});

SPEC_END
