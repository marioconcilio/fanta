//
//  TestingAppDelegate.h
//  Fanta
//
//  Created by Mario Concilio on 8/4/16.
//  Copyright © 2016 Mario Concilio. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <Foundation/Foundation.h>

@interface TestingAppDelegate : NSObject

@property (nonatomic, strong) UIWindow *window;

@end
