//
//  VOAthleticSpec.m
//  Fanta
//
//  Created by Mario Concilio on 8/18/16.
//  Copyright © 2016 Mario Concilio. All rights reserved.
//

#import "VOAthletic.h"
#import <Kiwi.h>
#import <OCHamcrest.h>

SPEC_BEGIN(VOAthleticSpec)

describe(@"VOAthletic", ^{
    
    context(@"initializing", ^{
        it(@"with dictionary", ^{
            NSDictionary *dict = @{@"athletic_school":  @"School Test",
                                   @"athletic_name":    @"A.A.A. Test",
                                   @"athletic_id":      @(999),
                                   @"facebook_id":      @(888)};
            
            VOAthletic *aa = [[VOAthletic alloc] initWithDictionary:dict];
            assertThat([aa name], is(@"School Test"));
            assertThat([aa athleticName], is(@"A.A.A. Test"));
            assertThatInt([aa athleticID], is(@(999)));
            assertThatInt([aa facebookID], is(@(888)));
        });
        
        it(@"with facebook dictionary", ^{
            // http://graph.facebook.com/%@/picture?type=large, facebookID 10202669285123247
            fail(@"thumbnail do facebook");
        });
    });
    
    context(@"comparing", ^{
        __block VOAthletic *aa1, *aa2, *aa3, *aa4;
        
        beforeAll(^{
            aa1 = [[VOAthletic alloc] init];
            aa2 = [[VOAthletic alloc] init];
            aa3 = [[VOAthletic alloc] init];
            
            aa1.facebookID = 1;
            aa2.facebookID = 2;
            aa3.facebookID = 1;
            aa4 = [aa2 copy];
        });
        
        it(@"itself using facebook_id", ^{
            assertThat(aa1, isNot(aa2));
            assertThat(aa1, is(aa3));
            assertThat(aa2, is(aa4));
        });
        
        it(@"itself using isEqualToAthletic", ^{
            assertThatBool([aa1 isEqualToAthletic:aa2], is(@(NO)));
            assertThatBool([aa1 isEqualToAthletic:aa3], is(@(YES)));
            assertThatBool([aa2 isEqualToAthletic:aa4], is(@(YES)));
        });
    });
    
    it(@"should conform to NSCoding and NSCopying", ^{
        VOAthletic *aa = [[VOAthletic alloc] init];
        assertThat(aa, conformsTo(@protocol(NSCoding)));
        assertThat(aa, conformsTo(@protocol(NSCopying)));
    });

});

SPEC_END
