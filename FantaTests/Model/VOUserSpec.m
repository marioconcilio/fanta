//
//  VOUserSpec.m
//  Fanta
//
//  Created by Mario Concilio on 8/18/16.
//  Copyright © 2016 Mario Concilio. All rights reserved.
//

#import "VOUser.h"
#import <Kiwi.h>
#import <OCHamcrest.h>

SPEC_BEGIN(VOUserSpec)

describe(@"VOUser", ^{
    
    context(@"initializing", ^{
        it(@"with dictionary", ^{
            NSDictionary *dict = @{@"student_name":     @"User Test",
                                   @"student_email":    @"email@test.com",
                                   @"student_image":    @"image.jpg",
                                   @"student_thumbnail":@"thumb.jpg",
                                   @"student_id":       @(999)};
            
            VOUser *user = [[VOUser alloc] initWithDictionary:dict];
            assertThat([user name], is(@"User Test"));
            assertThat([user email], is(@"email@test.com"));
            assertThat([user image], is(@"image.jpg"));
            assertThat([user thumbnail], is(@"thumb.jpg"));
            assertThatInt([user userID], is(@(999)));
        });
        
        it(@"with facebook dictionary", ^{
            NSDictionary *dict = @{@"id":           @(999),
                                   @"first_name":   @"User",
                                   @"last_name":    @"Test",
                                   @"email":        @"email@test.com",
                                   @"picture":      @{@"data": @{@"is_silhouette":  @(NO),
                                                                 @"url":            @"img.jpg"}}};
            
            VOUser *user = [[VOUser alloc] initWithFacebookDictionary:dict];
            assertThatInt([user facebookID], is(@(999)));
            assertThatInt([user userID], is(@(0)));
            assertThat([user name], is(@"User Test"));
            assertThat([user email], is(@"email@test.com"));
            assertThat([user thumbnail], is(@"img.jpg"));
            assertThat(@([user isValid]), isTrue());
        });
    });
    
    context(@"comparing", ^{
        __block VOUser *user1, *user2, *user3, *user4;
        
        beforeAll(^{
            user1 = [[VOUser alloc] init];
            user2 = [[VOUser alloc] init];
            user3 = [[VOUser alloc] init];
            
            user1.userID = 1;
            user2.userID = 2;
            user3.userID = 1;
            user4 = [user2 copy];
        });
        
        it(@"itself using id", ^{
            assertThat(user1, isNot(user2));
            assertThat(user1, is(user3));
            assertThat(user2, is(user4));
        });
        
        it(@"itself using isEqualToUser", ^{
            assertThatBool([user1 isEqualToUser:user2], is(@(NO)));
            assertThatBool([user1 isEqualToUser:user3], is(@(YES)));
            assertThatBool([user2 isEqualToUser:user4], is(@(YES)));
        });
    });
    
    it(@"should conform to NSCoding and NSCopying", ^{
        VOUser *user = [[VOUser alloc] init];
        assertThat(user, conformsTo(@protocol(NSCoding)));
        assertThat(user, conformsTo(@protocol(NSCopying)));
    });

});

SPEC_END
