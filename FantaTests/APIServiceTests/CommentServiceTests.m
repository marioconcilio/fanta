//
//  CommentServiceTests.m
//  Fanta
//
//  Created by Mario Concilio on 8/19/16.
//  Copyright © 2016 Mario Concilio. All rights reserved.
//

#import "UserService.h"
#import "CommentService.h"
#import "PostService.h"
#import "VOComment.h"
#import "VOPost.h"
#import "MCAsyncing.h"
#import "XCTestCase+Auth.h"

#import <XCTest/XCTest.h>
#import <OCHamcrest.h>

@interface CommentServiceTests : XCTestCase

@end

@implementation CommentServiceTests {
    CommentService *_service;
    VOComment *_comment;
    PostService *_postService;
    VOPost *_post;
}

#pragma mark - Class Methods
+ (void)setUp {
    [super setUp];
    [self mockAuthorization];
}

+ (void)tearDown {
    [self stopMocking];
    [super tearDown];
}

#pragma mark - Instance Methods
- (void)setUp {
    [super setUp];
    _service = [[CommentService alloc] init];
    _postService = [[PostService alloc] init];
    
    dispatch_semaphore_t sem = dispatch_semaphore_create(0);
    
    // creating post
    asyncBlock(self, @"creating post", ^(Exp *exp) {
        [_postService createPost:@"Testing Post"
                            type:APIPostTypeStudent
                  fromUniversity:1
                      completion:^(VOPost *post, NSError *error) {
                          dispatch_semaphore_signal(sem);
                          assertThat(post, isNot(nilValue()));
                          assertThat(error, is(nilValue()));
                          _post = post;
                          fulfill(exp);
                      }];
    });

    // creating comment
    dispatch_semaphore_wait(sem, DISPATCH_TIME_FOREVER);
    asyncBlock(self, @"creating comment", ^(Exp *exp) {
        [_service createComment:@"Testing Comment"
                       fromPost:_post
                     completion:^(VOComment *comment, NSError *error) {
                         assertThat(comment, isNot(nilValue()));
                         assertThat(error, is(nilValue()));
                         _comment = comment;
                         fulfill(exp);
                     }];
    });
}

- (void)tearDown {
    dispatch_semaphore_t sem = dispatch_semaphore_create(0);
    
    // delete comment
    asyncBlock(self, @"deleting comment", ^(Exp *exp) {
        [_service deleteComment:_comment completion:^(NSError *error) {
            dispatch_semaphore_signal(sem);
            assertThat(error, is(nilValue()));
            fulfill(exp);
        }];
    });
    
    // delete post
    dispatch_semaphore_wait(sem, DISPATCH_TIME_FOREVER);
    asyncBlock(self, @"deleting post", ^(Exp *exp) {
        [_postService deletePost:_post completion:^(NSError *error) {
            assertThat(error, is(nilValue()));
            fulfill(exp);
        }];
    });
    
    [super tearDown];
}

- (void)testGetCommentsFromPost {
    asyncBlock(self, @"get comments from post", ^(Exp *exp) {
        [_service getCommentsFromPost:_post completion:^(NSArray<VOComment *> *comments, NSError *error) {
            assertThat(comments, isNot(nilValue()));
            assertThat(error, is(nilValue()));
            fulfill(exp);
        }];
    });
}

- (void)testLikeComment {
    asyncBlock(self, @"like comment", ^(Exp *exp) {
        [_service likeComment:_comment completion:^(NSError *error) {
            assertThat(error, is(nilValue()));
            fulfill(exp);
        }];
    });
}

- (void)testDislikeComment {
    asyncBlock(self, @"dislike comment", ^(Exp *exp) {
        [_service dislikeComment:_comment completion:^(NSError *error) {
            assertThat(error, is(nilValue()));
            fulfill(exp);
        }];
    });
}

- (void)testReportComment {
    asyncBlock(self, @"report comment", ^(Exp *exp) {
        [_service reportComment:_comment completion:^(NSError *error) {
            assertThat(error, is(nilValue()));
            fulfill(exp);
        }];
    });
}

@end
