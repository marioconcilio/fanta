//
//  APIClientTests.m
//  Fanta
//
//  Created by Mario Concilio on 9/1/16.
//  Copyright © 2016 Mario Concilio. All rights reserved.
//

#import "APIClient.h"
#import "MCAsyncing.h"
#import <XCTest/XCTest.h>
#import <OCHamcrest.h>

@interface APIClientTests : XCTestCase

@end

@implementation APIClientTests

- (void)setUp {
    [super setUp];
    // Put setup code here. This method is called before the invocation of each test method in the class.
}

- (void)tearDown {
    // Put teardown code here. This method is called after the invocation of each test method in the class.
    [super tearDown];
}

- (void)testImageFromURL {
    asyncBlock(self, @"image from url", ^(Exp *exp) {
        NSURL *url = [NSURL URLWithString:@"http://lorempixel.com/800/800/"];
        [[APIClient sharedInstance] imageFromURL:url
                                      completion:^(UIImage *image, NSError *error) {
                                          assertThat(image, isNot(nilValue()));
                                          assertThat(error, is(nilValue()));
                                          fulfill(exp);
                                      }];
    });
}

@end
