//
//  PostServiceTests.m
//  Fanta
//
//  Created by Mario Concilio on 8/22/16.
//  Copyright © 2016 Mario Concilio. All rights reserved.
//

#import "PostService.h"
#import "MCAsyncing.h"
#import "VOPost.h"
#import "XCTestCase+Auth.h"

#import <XCTest/XCTest.h>
#import <OCHamcrest.h>

@interface PostServiceTests : XCTestCase

@end

@implementation PostServiceTests {
    PostService *_service;
    VOPost *_post;
}

#pragma mark - Class Methods
+ (void)setUp {
    [super setUp];
    [self mockAuthorization];
}

+ (void)tearDown {
    [self stopMocking];
    [super tearDown];
}

#pragma mark - Instance Methods
- (void)setUp {
    [super setUp];
    _service = [[PostService alloc] init];
    
    dispatch_semaphore_t sem = dispatch_semaphore_create(0);
    
    // creating post
    asyncBlock(self, @"creating post", ^(Exp *exp) {
        [_service createPost:@"Testing Post"
                        type:APIPostTypeStudent
              fromUniversity:1
                  completion:^(VOPost *post, NSError *error) {
                      _post = post;
                      assertThat(post, isNot(nilValue()));
                      assertThat(error, is(nilValue()));
                      fulfill(exp);
                      dispatch_semaphore_signal(sem);
                  }];
    });
    
    dispatch_semaphore_wait(sem, DISPATCH_TIME_FOREVER);
}

- (void)tearDown {
    // deleting post
    asyncBlock(self, @"deleting post", ^(Exp *exp) {
        [_service deletePost:_post
                  completion:^(NSError *error) {
                      assertThat(error, is(nilValue()));
                      fulfill(exp);
                  }];
    });
    
    [super tearDown];
}

- (void)testLikePost {
    asyncBlock(self, @"like post", ^(Exp *exp) {
        [_service likePost:_post completion:^(NSError *error) {
            assertThat(error, is(nilValue()));
            fulfill(exp);
        }];
    });
}

- (void)testDislikePost {
    asyncBlock(self, @"dislike post", ^(Exp *exp) {
        [_service dislikePost:_post completion:^(NSError *error) {
            assertThat(error, is(nilValue()));
            fulfill(exp);
        }];
    });
}

- (void)testReportPost {
    asyncBlock(self, @"report post", ^(Exp *exp) {
        [_service reportPost:_post completion:^(NSError *error) {
            assertThat(error, is(nilValue()));
            fulfill(exp);
        }];
    });
}

@end
