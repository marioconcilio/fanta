//
//  AthleticServiceTests.m
//  Fanta
//
//  Created by Mario Concilio on 8/16/16.
//  Copyright © 2016 Mario Concilio. All rights reserved.
//

#import "AthleticService.h"
#import "MCAsyncing.h"
#import <OCHamcrest.h>
#import <XCTest/XCTest.h>

@interface AthleticServiceTests : XCTestCase

@end

@implementation AthleticServiceTests {
    AthleticService *_service;
}

- (void)setUp {
    [super setUp];
    _service = [[AthleticService alloc] init];
}

- (void)tearDown {
    // Put teardown code here. This method is called after the invocation of each test method in the class.
    [super tearDown];
}

- (void)testListAllAthletics {
    asyncBlock(self, @"list all", ^(Exp *exp) {
        [_service listAllAthleticsWithCompletion:^(NSArray<VOAthletic *> *list, NSError *error) {
            assertThat(list, is(notNilValue()));
            assertThat(error, is(nilValue()));
            fulfill(exp);
        }];
    });
}

@end
