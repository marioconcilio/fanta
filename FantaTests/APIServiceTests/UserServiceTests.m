//
//  UserServiceTests.m
//  Fanta
//
//  Created by Mario Concilio on 8/16/16.
//  Copyright © 2016 Mario Concilio. All rights reserved.
//

#import "UserService.h"
#import "MCAsyncing.h"
#import "Constants.h"
#import "VOUser.h"
#import "XCTestCase+Auth.h"

#import <OCMock.h>
#import <OCHamcrest.h>
#import <XCTest/XCTest.h>

@interface UserServiceTests : XCTestCase

@end

@implementation UserServiceTests {
    UserService *_service;
}

#pragma mark - Class Methods
+ (void)setUp {
    [super setUp];
    [self mockAuthorization];
}

+ (void)tearDown {
    [self stopMocking];
    [super tearDown];
}

#pragma mark - Instance Methods
- (void)setUp {
    [super setUp];
    _service = [[UserService alloc] init];
}

- (void)tearDown {
    [super tearDown];
}

/*
- (void)testRegisterUser {
    asyncBlock(self, @"register user", ^(Exp *exp) {
        [_service registerUser:@"Test User"
                         email:@"test_user1@test.com"
                      password:@"123456"
                     onSuccess:^(VOUser *user) {
                         assertThat(user, isNot(nilValue()));
                         fulfill(exp);
                     }
                     onFailure:^(NSError *error) {
                         assertThat(error, is(nilValue()));
                         fulfill(exp);
                     }];
    });
}
 */

- (void)testLogin {
    asyncBlock(self, @"login", ^(Exp *exp) {
        [_service login:@"test_user1@test.com"
               password:@"123456"
             completion:^(NSError *error) {
                 assertThat(error, is(nilValue()));
                 fulfill(exp);
             }];
    });
}

- (void)testRegisterFacebookUser {
    UIImage *image = [UIImage imageNamed:@"profile_image.jpg"];
    UIImage *thumb = [UIImage imageNamed:@"profile_image.jpg"];
    
    VOUser *user = [[VOUser alloc] init];
    [user setName:@"Mario Concilio"];
    [user setEmail:@"marioconcilio@gmail.com"];
    [user setFacebookID:10202669285123247];
    [user setValid:YES];
    
    asyncBlock(self, @"facebook user", ^(Exp *exp) {
        [_service registerUserFromFacebook:user
                                     image:image
                                 thumbnail:thumb
                                completion:^(NSError *error) {
                                    assertThat(error, is(nilValue()));
                                    fulfill(exp);
                                }];
    });
}

- (void)testRegisterFacebookUserWithoutImage {
    VOUser *user = [[VOUser alloc] init];
    [user setName:@"Mario Concilio"];
    [user setEmail:@"marioconcilio@gmail.com"];
    [user setFacebookID:10202669285123247];
    [user setValid:YES];
    
    asyncBlock(self, @"facebook user", ^(Exp *exp) {
        [_service registerUserFromFacebook:user
                                     image:nil
                                 thumbnail:nil
                                completion:^(NSError *error) {
                                    assertThat(error, is(nilValue()));
                                    fulfill(exp);
                                }];
    });
}

- (void)testEditUserImage {
    UIImage *image = [UIImage imageNamed:@"profile_image.jpg"];
    UIImage *thumb = [UIImage imageNamed:@"profile_image.jpg"];
    
    asyncBlock(self, @"edit image", ^(Exp *exp) {
        [_service editUserName:nil
                      password:nil
                         image:image
                     thumbnail:thumb
                    completion:^(NSError *error) {
                        assertThat(error, is(nilValue()));
                        fulfill(exp);
                    }];
    });
}

- (void)testEditUserName {
    asyncBlock(self, @"edit name", ^(Exp *exp) {
        [_service editUserName:@"Test User"
                      password:nil
                         image:nil
                     thumbnail:nil
                    completion:^(NSError *error) {
                        assertThat(error, is(nilValue()));
                        fulfill(exp);
                    }];
    });
}

- (void)testEditUserPassword {
    asyncBlock(self, @"edit password", ^(Exp *exp) {
        [_service editUserName:nil
                      password:@"123456"
                         image:nil
                     thumbnail:nil
                    completion:^(NSError *error) {
                        assertThat(error, is(nilValue()));
                        fulfill(exp);
                    }];
    });
}

- (void)testGetUserFromID {
    asyncBlock(self, @"user from id", ^(Exp *exp) {
        [_service getUserFromId:1
                     completion:^(VOUser *user, NSError *error) {
                         assertThat(user, is(notNilValue()));
                         assertThat(error, is(nilValue()));
                         fulfill(exp);
                     }];
    });
}

@end
