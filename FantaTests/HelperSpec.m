//
//  HelperSpec.m
//  Fanta
//
//  Created by Mario Concilio on 8/9/16.
//  Copyright © 2016 Mario Concilio. All rights reserved.
//

#import "Helper.h"
#import <Kiwi.h>
#import <OCHamcrest.h>

SPEC_BEGIN(HelperSpec)

describe(@"Helper", ^{

	it(@"generate initials from name", ^{
        NSString *initials = [Helper initialsFromName:@"Mario Concilio"];
        assertThat(initials, is(@"MC"));
        
        initials = [Helper initialsFromName:@"User Test Edit"];
        assertThat(initials, is(@"UTE"));
        
        initials = [Helper initialsFromName:@"User Test 2"];
        assertThat(initials, is(@"UT2"));
    });
    
    it(@"create avatar from name", ^{
        __block UIImage *img = nil;
        
        [Helper avatarFromName:@"Mario Concilio"
                          font:[UIFont systemFontOfSize:14.0]
                      diameter:20.0
                      callback:^(UIImage *image) {
                          img = image;
                      }];
        
        [[expectFutureValue(img) shouldEventually] beNonNil];
        [[expectFutureValue(@(img.size.width)) shouldEventually] equal:@(20.0)];
    });

});

SPEC_END
