//
//  XCTestCase+Auth.h
//  Fanta
//
//  Created by Mario Concilio on 8/19/16.
//  Copyright © 2016 Mario Concilio. All rights reserved.
//

#import <XCTest/XCTest.h>

@interface XCTestCase (Auth)

+ (void)mockAuthorization;
+ (void)stopMocking;

@end
