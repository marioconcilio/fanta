//
//  MCAsyncing.h
//  Fanta
//
//  Created by Mario Concilio on 8/16/16.
//  Copyright © 2016 Mario Concilio. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <XCTest/XCTest.h>

typedef XCTestExpectation Exp;
typedef void(^ExpBlock)(Exp *exp);

@interface MCAsyncing : NSObject

void asyncBlock(id sender, NSString *description, ExpBlock block);
void fulfill(Exp *exp);

@end
