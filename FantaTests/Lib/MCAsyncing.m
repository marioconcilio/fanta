//
//  MCAsyncing.m
//  Fanta
//
//  Created by Mario Concilio on 8/16/16.
//  Copyright © 2016 Mario Concilio. All rights reserved.
//

#import "MCAsyncing.h"

@implementation MCAsyncing

void asyncBlock(id sender, NSString *description, ExpBlock block) {
    __weak XCTestExpectation *exp = [sender expectationWithDescription:description];
    block(exp);
    [sender waitForExpectationsWithTimeout:10.0 handler:NULL];
}

void fulfill(Exp *exp) {
    [exp fulfill];
    exp = nil;
}

@end
