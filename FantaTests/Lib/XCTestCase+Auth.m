//
//  XCTestCase+Auth.m
//  Fanta
//
//  Created by Mario Concilio on 8/19/16.
//  Copyright © 2016 Mario Concilio. All rights reserved.
//

#import "XCTestCase+Auth.h"
#import "VOUser.h"
#import "UserService.h"
#import <OCMock.h>
#import <objc/runtime.h>

static const void *MockingObject = &MockingObject;

@implementation XCTestCase (Auth)

+ (void)mockAuthorization {
    VOUser *user = [[VOUser alloc] init];
    user.userID = 2;
    
    id serviceMock = OCMClassMock([UserService class]);
    OCMStub([serviceMock loadToken]).andReturn(@"c05e01a821775ae7e6f95321f6394d8b5dd7b22b66a9833468fa0b69");
    OCMStub([serviceMock loadUser]).andReturn(user);
    
    objc_setAssociatedObject(self, MockingObject, serviceMock, OBJC_ASSOCIATION_RETAIN_NONATOMIC);
}

+ (void)stopMocking {
    id serviceMock = objc_getAssociatedObject(self, MockingObject);
    [serviceMock stopMocking];
}

@end
