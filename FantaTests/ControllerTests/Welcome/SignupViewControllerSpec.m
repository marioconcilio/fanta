//
//  SignupViewControllerSpec.m
//  Fanta
//
//  Created by Mario Concilio on 8/8/16.
//  Copyright © 2016 Mario Concilio. All rights reserved.
//

#import "SignupViewController.h"
#import "UIStoryboard+Storyboards.h"
#import "FloatLabeledTextField.h"
#import <Kiwi.h>

SPEC_BEGIN(SignupViewControllerSpec)

describe(@"SignupViewController", ^{
    __block SignupViewController *sut;

	beforeAll(^{
    	sut = [[UIStoryboard welcomeStoryboard] instantiateViewControllerWithIdentifier:@"signup"];
        [sut view];
  	});
    
    context(@"testing outlets", ^{
        it(@"name textfield outlet", ^{
            [[sut.nameTextField shouldNot] beNil];
        });
        
        it(@"surname textfield outlet", ^{
            [[sut.surnameTextField shouldNot] beNil];
        });
        
        it(@"email textfield outlet", ^{
            [[sut.emailTextField shouldNot] beNil];
        });
        
        it(@"password textfield outlet", ^{
            [[sut.passwordTextField shouldNot] beNil];
        });
        
        it(@"signup button outlet", ^{
            [[sut.signupButton shouldNot] beNil];
        });
        
        it(@"container view outlet", ^{
            [[sut.containerView shouldNot] beNil];
        });
        
        it(@"error label outlet", ^{
            [[sut.errorLabel shouldNot] beNil];
        });
        
        it(@"signup button action", ^{
            NSArray *actions = [sut.signupButton actionsForTarget:sut
                                                  forControlEvent:UIControlEventTouchUpInside];
            [[actions should] contain:@"doSignup:"];
        });
    });
    
    it(@"show error message", ^{
        NSString *errorMsg = @"Error Message";
        [sut showError:errorMsg];
        
        [[[sut.errorLabel text] should] equal:errorMsg];
        [[theValue([sut.errorLabel alpha]) should] equal:@(1.0)];
    });

	afterAll(^{
        sut = nil;
	});

});

SPEC_END
