//
//  LoginViewControllerSpec.m
//  Fanta
//
//  Created by Mario Concilio on 8/8/16.
//  Copyright © 2016 Mario Concilio. All rights reserved.
//

#import "LoginViewController.h"
#import "UIStoryboard+Storyboards.h"
#import "FloatLabeledTextField.h"
#import <Kiwi.h>

SPEC_BEGIN(LoginViewControllerSpec)

describe(@"LoginViewController", ^{
    __block LoginViewController *sut;

	beforeAll(^{
        sut = [[UIStoryboard welcomeStoryboard] instantiateViewControllerWithIdentifier:@"login"];
        [sut view];
  	});
    
    context(@"testing outlets", ^{
        it(@"email textfield outlet", ^{
            [[sut.emailTextField shouldNot] beNil];
        });
        
        it(@"password textfield outlet", ^{
            [[sut.passwordTextField shouldNot] beNil];
        });
        
        it(@"login button outlet", ^{
            [[sut.loginButton shouldNot] beNil];
        });
        
        it(@"container view outlet", ^{
            [[sut.containerView shouldNot] beNil];
        });
        
        it(@"error label outlet", ^{
            [[sut.errorLabel shouldNot] beNil];
        });
        
        it(@"login button action", ^{
            NSArray *actions = [sut.loginButton actionsForTarget:sut
                                                 forControlEvent:UIControlEventTouchUpInside];
            [[actions should] contain:@"doLogin:"];
        });
    });
    
    it(@"show error message", ^{
        NSString *errorMsg = @"Error Message";
        [sut showError:errorMsg];
        
        [[[sut.errorLabel text] should] equal:errorMsg];
        [[theValue([sut.errorLabel alpha]) should] equal:@(1.0)];
    });

	afterAll(^{
        sut = nil;
	});

});

SPEC_END
