//
//  WelcomeViewControllerSpec.m
//  Fanta
//
//  Created by Mario Concilio on 8/8/16.
//  Copyright © 2016 Mario Concilio. All rights reserved.
//

#import "WelcomeViewController.h"
#import "UIStoryboard+Storyboards.h"
#import <Kiwi.h>

SPEC_BEGIN(WelcomeViewControllerSpec)

describe(@"WelcomeViewController", ^{
    __block WelcomeViewController *sut;

	beforeAll(^{
    	sut = [[UIStoryboard welcomeStoryboard] instantiateViewControllerWithIdentifier:@"welcome"];
        [sut view];
  	});
    
    context(@"testing outlets", ^{
        it(@"signup button outlet", ^{
            [[sut.signupButton shouldNot] beNil];
        });
        
        it(@"facebook button outlet", ^{
            [[sut.facebookButton shouldNot] beNil];
        });
        
        it(@"login button outlet", ^{
            [[sut.loginButton shouldNot] beNil];
        });
        
        it(@"container view outlet", ^{
            [[sut.containerView shouldNot] beNil];
        });
    });

	afterAll(^{
        sut = nil;
	});

});

SPEC_END
