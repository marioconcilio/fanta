//
//  PostViewControllerSpec.m
//  Fanta
//
//  Created by Mario Concilio on 8/8/16.
//  Copyright © 2016 Mario Concilio. All rights reserved.
//

#import "PostViewController.h"
#import "VOPost.h"
#import "UIStoryboard+Storyboards.h"
#import "VBFPopFlatButton+DefaultButtons.h"
#import <Kiwi.h>
#import <OCHamcrest.h>
#import <AsyncDisplayKit/AsyncDisplayKit.h>

SPEC_BEGIN(PostViewControllerSpec)

describe(@"PostViewController", ^{
    __block PostViewController *sut;

	context(@"testing outlets", ^{
        beforeAll(^{
            sut = [[UIStoryboard feedStoryboard] instantiateViewControllerWithIdentifier:@"post"];
            [sut view];
        });
        
        it(@"blur navbar outlet", ^{
            [[sut.blurNavbar shouldNot] beNil];
        });
        
        it(@"blur toolbar outlet", ^{
            [[sut.blurToolbar shouldNot] beNil];
        });
        
        it(@"scrollview outlet", ^{
            [[sut.scrollView shouldNot] beNil];
        });
        
        it(@"comment button outlet", ^{
            [[sut.commentButton shouldNot] beNil];
        });
        
        it(@"dislike button outlet", ^{
            [[sut.dislikeButton shouldNot] beNil];
        });
        
        it(@"like button outlet", ^{
            [[sut.likeButton shouldNot] beNil];
        });
        
        it(@"comment label outlet", ^{
            [[sut.commentLabel shouldNot] beNil];
        });
        
        it(@"dislike label outlet", ^{
            [[sut.dislikeLabel shouldNot] beNil];
        });
        
        it(@"like label outlet", ^{
            [[sut.likeLabel shouldNot] beNil];
        });
        
        it(@"title label outlet", ^{
            [[sut.titleLabel shouldNot] beNil];
        });
        
        it(@"close button outlet", ^{
            [[sut.closeButton shouldNot] beNil];
        });
        
        it(@"comment button action", ^{
            NSArray *actions = [sut.commentButton actionsForTarget:sut
                                                  forControlEvent:UIControlEventTouchUpInside];
            [[actions should] contain:@"doOpenComments:"];
        });
        
        it(@"dislike button action", ^{
            NSArray *actions = [sut.dislikeButton actionsForTarget:sut
                                                  forControlEvent:UIControlEventTouchUpInside];
            [[actions should] contain:@"doDislike:"];
        });
        
        it(@"like button action", ^{
            NSArray *actions = [sut.likeButton actionsForTarget:sut
                                                  forControlEvent:UIControlEventTouchUpInside];
            [[actions should] contain:@"doLike:"];
        });
        
        it(@"close button action", ^{
            NSArray *actions = [sut.closeButton actionsForTarget:sut
                                                  forControlEvent:UIControlEventTouchUpInside];
            [[actions should] contain:@"dismiss"];
        });
        
    });
    
    context(@"formatting labels", ^{
        __block id postMock = nil;
        
        beforeEach(^{
            sut = [[UIStoryboard feedStoryboard] instantiateViewControllerWithIdentifier:@"post"];
            postMock = [VOPost nullMock];
        });
        
        it(@"comment label", ^{
            [postMock stub:@selector(comments) andReturn:theValue(89)];
            [sut setPost:postMock];
            [sut view];
            
            [[[sut.commentLabel text] should] equal:@"89"];
        });
        
        it(@"like label", ^{
            [postMock stub:@selector(likes) andReturn:theValue(10)];
            [sut setPost:postMock];
            [sut view];
            
            [[[sut.likeLabel text] should] equal:@"10"];
        });
        
        it(@"dislike label", ^{
            [postMock stub:@selector(dislikes) andReturn:theValue(67)];
            [sut setPost:postMock];
            [sut view];
            
            [[[sut.dislikeLabel text] should] equal:@"67"];
        });
        
        it(@"title label", ^{
            [postMock stub:@selector(userName) andReturn:@"Username"];
            [sut setPost:postMock];
            [sut view];
            
            [[[sut.titleLabel text] should] equal:@"username"];
        });
        
        it(@"content node", ^{
            [postMock stub:@selector(content) andReturn:@"Loren ipsum"];
            [sut setupPost:postMock cachedImage:nil frame:CGRectZero];
            [sut view];
            
            [[[[sut.contentNode attributedString] string] should] equal:@"Loren ipsum"];
        });
        
    });
    
    context(@"formatting labels when above 1k", ^{
        __block id postMock = nil;
        
        beforeEach(^{
            sut = [[UIStoryboard feedStoryboard] instantiateViewControllerWithIdentifier:@"post"];
            postMock = [VOPost nullMock];
        });
        
        it(@"comment label", ^{
            [postMock stub:@selector(comments) andReturn:theValue(1989)];
            [sut setPost:postMock];
            [sut view];
            
            [[[sut.commentLabel text] should] equal:@"1.9k"];
        });
        
        it(@"like label", ^{
            [postMock stub:@selector(likes) andReturn:theValue(2989)];
            [sut setPost:postMock];
            [sut view];
            
            [[[sut.likeLabel text] should] equal:@"2.9k"];
        });
        
        it(@"dislike label", ^{
            [postMock stub:@selector(dislikes) andReturn:theValue(1089)];
            [sut setPost:postMock];
            [sut view];
            
            [[[sut.dislikeLabel text] should] equal:@"1.0k"];
        });
    });
    
    context(@"", ^{
        __block id postMock = nil;
        
        beforeEach(^{
            sut = [[UIStoryboard feedStoryboard] instantiateViewControllerWithIdentifier:@"post"];
            postMock = [VOPost nullMock];
        });
        
        it(@"do like", ^{
            //given
            [postMock stub:@selector(likes) andReturn:theValue(89)];
            
            //when
            [sut setPost:postMock];
            [sut view];
            [sut doLike:nil];
            
            //then
            assertThat([sut.likeLabel text], is(@"90"));
        });
        
        it(@"do dislike", ^{
            //given
            [postMock stub:@selector(dislikes) andReturn:theValue(89)];
            
            //when
            [sut setPost:postMock];
            [sut view];
            [sut doDislike:nil];
            
            //then
            assertThat([sut.dislikeLabel text], is(@"90"));
        });
        
        it(@"do like when post is disliked", ^{
            //given
            [postMock stub:@selector(likes) andReturn:theValue(89)];
            [postMock stub:@selector(dislikes) andReturn:theValue(10)];
            [postMock stub:@selector(isDisliked) andReturn:theValue(YES)];
            
            //when
            [sut setPost:postMock];
            [sut view];
            [sut doLike:nil];
            
            //then
            assertThat([sut.dislikeLabel text], is(@"9")); // dislike--
            assertThat([sut.likeLabel text], is(@"90")); // like++
        });
        
        it(@"do dislike when post is liked", ^{
            //given
            [postMock stub:@selector(likes) andReturn:theValue(89)];
            [postMock stub:@selector(dislikes) andReturn:theValue(10)];
            [postMock stub:@selector(isLiked) andReturn:theValue(YES)];
            
            //when
            [sut setPost:postMock];
            [sut view];
            [sut doDislike:nil];
            
            //then
            assertThat([sut.likeLabel text], is(@"88")); // like--
            assertThat([sut.dislikeLabel text], is(@"11")); // dislike++
        });
    });

});

SPEC_END
