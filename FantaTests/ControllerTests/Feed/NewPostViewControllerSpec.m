//
//  NewPostViewControllerSpec.m
//  Fanta
//
//  Created by Mario Concilio on 8/9/16.
//  Copyright © 2016 Mario Concilio. All rights reserved.
//

#import "NewPostViewController.h"
#import "UIStoryboard+Storyboards.h"
#import "DashedTextView.h"
#import "UserService.h"
#import "VOAthletic.h"
#import "VOUser.h"

#import <Kiwi.h>
#import <OCHamcrest.h>
#import <OCMock.h>

SPEC_BEGIN(NewPostViewControllerSpec)

describe(@"NewPostViewController", ^{
    __block NewPostViewController *sut;
    __block id _serviceMock;
    
    beforeAll(^{
        VOAthletic *aa = [[VOAthletic alloc] init];
        aa.name = @"Athletic Test";
        
        VOUser *user = [[VOUser alloc] init];
        user.athletic = aa;
        
        _serviceMock = OCMClassMock([UserService class]);
        OCMStub([_serviceMock loadUser]).andReturn(user);
    });
    
    afterAll(^{
        [_serviceMock stopMocking];
    });

	context(@"testing outlets", ^{
        beforeAll(^{
            sut = [[UIStoryboard feedStoryboard] instantiateViewControllerWithIdentifier:@"new_post"];
            [sut view];
        });
        
        pending_(@"imageview outlet when adding image", ^{
            [[sut.postImageView shouldNot] beNil];
        });
        
        pending_(@"placeholder label outlet", ^{
            [[sut.placeholderLabel shouldNot] beNil];
        });
        
        pending_(@"textview outlet", ^{
            [[sut.textView shouldNot] beNil];
        });
        
        it(@"scrollview outlet", ^{
            [[sut.scrollView shouldNot] beNil];
        });
        
        it(@"list aa button outlet", ^{
            [[sut.listAAButton shouldNot] beNil];
        });
        
        it(@"store barbutton outlet", ^{
            [[sut.storeBarButton shouldNot] beNil];
        });
        
        it(@"gallery barbutton outlet", ^{
            [[sut.galleryBarButton shouldNot] beNil];
        });
        
        it(@"camera barbutton outlet", ^{
            [[sut.cameraBarButton shouldNot] beNil];
        });
        
        it(@"toolbar outlet", ^{
            [[sut.toolbar shouldNot] beNil];
        });
        
        it(@"list aa button action", ^{
            SEL action = [sut.listAAButton action];
            id target = [sut.listAAButton target];
            
            assertThat(NSStringFromSelector(action), is(@"doOpenListAA:"));
            assertThat(NSStringFromClass([target class]), is(@"NewPostViewController"));
        });
        
        it(@"gallery barbutton action", ^{
            SEL action = [sut.galleryBarButton action];
            id target = [sut.galleryBarButton target];
            
            assertThat(NSStringFromSelector(action), is(equalTo(@"doOpenGallery:")));
            assertThat(NSStringFromClass([target class]), is(@"NewPostViewController"));
        });
        
        it(@"camera barbutton action", ^{
            SEL action = [sut.cameraBarButton action];
            id target = [sut.cameraBarButton target];
            
            assertThat(NSStringFromSelector(action), is(equalTo(@"doOpenCamera:")));
            assertThat(NSStringFromClass([target class]), is(@"NewPostViewController"));
        });
        
        it(@"store barbutton action", ^{
            SEL action = [sut.storeBarButton action];
            id target = [sut.storeBarButton target];
            
            assertThat(NSStringFromSelector(action), is(equalTo(@"doOpenStore:")));
            assertThat(NSStringFromClass([target class]), is(@"NewPostViewController"));
        });

    });
    
    context(@"testing ", ^{
        beforeEach(^{
            sut = [[UIStoryboard feedStoryboard] instantiateViewControllerWithIdentifier:@"new_post"];
        });
        
        it(@"can choose athletic", ^{
            [sut setChooseAA:YES];
            [sut view];
            NSArray<UIBarButtonItem *> *buttons = [sut.toolbar items];
            
            assertThat(buttons, hasItem(sut.listAAButton));
        });
        
        it(@"cannot choose athletic", ^{
            [sut setChooseAA:NO];
            [sut view];
            NSArray<UIBarButtonItem *> *buttons = [sut.toolbar items];
            
            assertThat(buttons, isNot(hasItem(sut.listAAButton)));
        });
        
        it(@"can open store", ^{
            [sut setOpenStore:YES];
            [sut view];
            NSArray<UIBarButtonItem *> *buttons = [sut.toolbar items];
            
            assertThat(buttons, hasItem(sut.storeBarButton));
        });
        
        it(@"cannot open store", ^{
            [sut setOpenStore:NO];
            [sut view];
            NSArray<UIBarButtonItem *> *buttons = [sut.toolbar items];
            
            assertThat(buttons, isNot(hasItem(sut.storeBarButton)));
        });
        
    });

});

SPEC_END
