//
//  MyProfilePopoverViewControllerSpec.m
//  Fanta
//
//  Created by Mario Concilio on 8/8/16.
//  Copyright © 2016 Mario Concilio. All rights reserved.
//

#import "MyProfilePopoverViewController.h"
#import "VOUser.h"
#import "VOAthletic.h"
#import "UIStoryboard+Storyboards.h"
#import <Kiwi.h>
#import <OCHamcrest.h>

SPEC_BEGIN(MyProfilePopoverViewControllerSpec)

describe(@"MyProfilePopoverViewController", ^{
    __block MyProfilePopoverViewController *sut;
    
    context(@"testing outlets", ^{
        beforeAll(^{
            sut = [[UIStoryboard profileStoryboard] instantiateViewControllerWithIdentifier:@"my_profile_popover"];
            [sut view];
        });
        
        it(@"name label outlet", ^{
            [[sut.nameLabel shouldNot] beNil];
        });
        
        it(@"athletic label outlet", ^{
            [[sut.athleticLabel shouldNot] beNil];
        });
    });
    
    context(@"filling info", ^{
        beforeAll(^{
            sut = [[UIStoryboard profileStoryboard] instantiateViewControllerWithIdentifier:@"my_profile_popover"];
        });
        
        it(@"uesr info", ^{
            //given
            NSString *userName = @"Mario Concilio";
            NSString *aaName = @"EACH - DASI";
            
            id aa = [VOAthletic nullMock];
            [aa stub:@selector(fullName) andReturn:aaName];
            
            id user = [VOUser nullMock];
            [user stub:@selector(name) andReturn:userName];
            [user stub:@selector(athletic) andReturn:aa];
            
            //when
            [sut setUser:user];
            [sut view];
            
            //then
            assertThat([sut.nameLabel text], is(userName));
            assertThat([sut.athleticLabel text], is(aaName));
        });
    });

	afterAll(^{
        sut = nil;
	});

});

SPEC_END
