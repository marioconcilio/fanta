//
//  ProfileViewControllerSpec.m
//  Fanta
//
//  Created by Mario Concilio on 8/8/16.
//  Copyright © 2016 Mario Concilio. All rights reserved.
//

#import "ProfileViewController.h"
#import "UIStoryboard+Storyboards.h"
#import "VOUser.h"
#import "VOAthletic.h"
#import "Helper.h"
#import "Constants.h"
#import "UserService.h"
#import "VBFPopFlatButton+DefaultButtons.h"
#import "BLMultiColorLoader+DefaultLoader.h"

#import <Kiwi.h>
#import <OCMock.h>

SPEC_BEGIN(ProfileViewControllerSpec)

describe(@"ProfileViewController", ^{
    __block ProfileViewController *sut;

    context(@"all user", ^{
        beforeAll(^{
            sut = [[UIStoryboard profileStoryboard] instantiateViewControllerWithIdentifier:@"profile"];
            [sut view];
        });
        
        it(@"scroll view outlet", ^{
            [[sut.scrollView shouldNot] beNil];
        });
        
        it(@"profile imageview outlet", ^{
            [[sut.profileImageView shouldNot] beNil];
        });
        
        it(@"name label outlet", ^{
            [[sut.nameLabel shouldNot] beNil];
        });
        
        it(@"athletic label outlet", ^{
            [[sut.athleticLabel shouldNot] beNil];
        });
        
        it(@"friends label outlet", ^{
            [[sut.friendsLabel shouldNot] beNil];
        });
        
        it(@"posts label outlet", ^{
            [[sut.postsLabel shouldNot] beNil];
        });
        
        it(@"circle view outlet", ^{
            [[sut.circleView shouldNot] beNil];
        });
        
        it(@"background imageview outlet", ^{
            [[sut.backgroundImageView shouldNot] beNil];
        });
        
        it(@"action button outlet", ^{
            [[sut.actionButton shouldNot] beNil];
        });
        
        it(@"friends indicator outlet", ^{
            [[sut.friendsIndicator shouldNot] beNil];
        });
        
        pending_(@"profile imageview action", ^{
            
        });
        
        it(@"action button action", ^{
            NSArray *actions = [sut.actionButton actionsForTarget:sut
                                                  forControlEvent:UIControlEventTouchUpInside];
            [[actions should] contain:@"doOpenPopup:"];
        });
        
        afterAll(^{
            sut = nil;
        });
    });
    
    context(@"is me", ^{
        __block id _serviceMock;
        
        beforeAll(^{
            VOUser *user = [[VOUser alloc] init];
            user.name = @"User Test";
            
            _serviceMock = OCMClassMock([UserService class]);
            OCMStub([_serviceMock loadUser]).andReturn(user);
            
            sut = [[UIStoryboard profileStoryboard] instantiateViewControllerWithIdentifier:@"profile"];
            [sut view];
        });
        
        it(@"change picture button outlet", ^{
            [[sut.changePictureButton shouldNot] beNil];
        });
        
        it(@"logout button outlet", ^{
            [[sut.logoutButton shouldNot] beNil];
        });
        
        it(@"logout button action", ^{
            NSArray *actions = [sut.logoutButton actionsForTarget:sut
                                                  forControlEvent:UIControlEventTouchUpInside];
            [[actions should] contain:@"doLogout:"];
        });
        
        it(@"change picture button action", ^{
            NSArray *actions = [sut.changePictureButton actionsForTarget:sut
                                                         forControlEvent:UIControlEventTouchUpInside];
            [[actions should] contain:@"doChangePicture:"];
        });
        
        it(@"fill user info", ^{
            [[[sut.nameLabel text] should] equal:@"User Test"];
//            [[[sut.universityLabel text] should] equal:_aaName];
        });
        
        afterAll(^{
            sut = nil;
            [_serviceMock stopMocking];
        });
        
    });
    
    context(@"is not me", ^{
        __block NSString *_userName;
        __block NSString *_aaName;
        
        beforeAll(^{
            sut = [[UIStoryboard profileStoryboard] instantiateViewControllerWithIdentifier:@"profile"];
            
            _userName = @"Mario Concilio";
            _aaName = @"EACH - DASI";
            
            //given
            id aa = [VOAthletic nullMock];
            [aa stub:@selector(fullName) andReturn:_aaName];
            
            id user = [VOUser nullMock];
            [user stub:@selector(name) andReturn:_userName];
            [user stub:@selector(athletic) andReturn:aa];
            
            [sut setUser:user];
            [sut view];
        });
        
        it(@"add friend button outlet", ^{
            [[sut.addFriendButton shouldNot] beNil];
        });
        
        it(@"add friend button action", ^{
            NSArray *actions = [sut.addFriendButton actionsForTarget:sut
                                                     forControlEvent:UIControlEventTouchUpInside];
            
            [[actions should] contain:@"doAddFriend:"];
        });
        
        it(@"fill user info", ^{
            [[[sut.nameLabel text] should] equal:_userName];
            [[[sut.athleticLabel text] should] equal:_aaName];
        });
        
        afterAll(^{
            sut = nil;
        });
        
    });

});

SPEC_END
