//
//  GeneratorSpec.m
//  Fanta
//
//  Created by Mario Concilio on 8/18/16.
//  Copyright © 2016 Mario Concilio. All rights reserved.
//

#import "Generator.h"
#import <Kiwi.h>
#import <OCHamcrest.h>

SPEC_BEGIN(GeneratorSpec)

describe(@"Generator", ^{

	it(@"random text", ^{
        NSString *str = [Generator mussumIpsum];
        assertThat(str, is(notNilValue()));
    });
    
    it(@"random name and surname", ^{
        NSString *str = [Generator randomName];
        assertThat(str, is(notNilValue()));
        
        NSArray *arr = [str componentsSeparatedByCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
        assertThat(arr, hasCountOf(2));
        assertThat(arr[0], is(notNilValue()));
        assertThat(arr[1], is(notNilValue()));
    });
    
    it(@"random users", ^{
        NSArray *users = [Generator randomUsers:50];
        assertThat(users, hasCountOf(50));
    });
    
    context(@"random images", ^{
        it(@"random image", ^{
            NSString *str = [Generator randomImage];
            assertThat(str, is(notNilValue()));
        });
        
        it(@"random sport image", ^{
            NSString *str = [Generator randomSport];
            assertThat(str, is(notNilValue()));
        });
        
        it(@"random person image", ^{
            NSString *str = [Generator randomPerson];
            assertThat(str, is(notNilValue()));
        });
    });

});

SPEC_END
